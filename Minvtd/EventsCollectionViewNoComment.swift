//
//  EventsCollectionViewNoComment.swift
//  Minvtd
//
//  Created by Tushar Aggarwal on 25/07/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit

class EventsCollectionViewNoComment: UICollectionViewCell {
    
    @IBOutlet weak var userImage: UIImageView!
    
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var date: UILabel!
    
    @IBOutlet weak var moreButton: UIButton!
    
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var image: UIImageView!
    
    @IBOutlet weak var playIcon: UIButton!
    
    @IBOutlet weak var heartBtn: UIButton!
    
    @IBOutlet weak var heartLabel: UILabel!
    
    @IBOutlet weak var commentBtn: UIButton!
    
    @IBOutlet weak var commentLabel: UILabel!
    
    @IBOutlet weak var postSubCommentTF: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        postSubCommentTF.returnKeyType = UIReturnKeyType.send
        
    }

    
}
