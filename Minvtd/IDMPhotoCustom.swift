//
//  IDMPhotoCustom.swift
//  Minvtd
//
//  Created by Vineet Singh on 06/06/18.
//  Copyright © 2018 Euro Infotek Pvt Ltd. All rights reserved.
//

import Foundation
import IDMPhotoBrowser


class IDMPhotoCustom:IDMPhoto {
    
    var counter:String?
    var title:String?
    
    init(counter : String? ,imageUrl:URL?) {
        
        self.counter = counter
        super.init(url: imageUrl)
        self.photoURL = imageUrl
    }
}
