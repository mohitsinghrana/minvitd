//
//  VenueSectionHeader.swift
//  Minvtd
//
//  Created by admin on 13/11/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit

protocol VenueSectionHeaderDelegate {
    func toggleSection(_ header: VenueSectionHeader, section: Int)
}

class VenueSectionHeader :UITableViewHeaderFooterView{
    
    var delegate: VenueSectionHeaderDelegate?
    var vc:VenueVC?
    @objc var section: Int = 0
    private var isSetup = true
    @objc var bottomHeight = 0
    @objc var ticketBottom : NSLayoutConstraint?
    

    let imageView = { () -> UIImageView in
        let im = UIImageView()
        im.tintColor = UIColor.white
        im.image = UIImage(named: "locationWhite")
        im.translatesAutoresizingMaskIntoConstraints = false

        return im
        
    }()
    
    let textView = { () -> UILabel in
        let ti = UILabel()
        ti.text = "stsujykjhk"
        ti.textColor = UIColor.white
        ti.translatesAutoresizingMaskIntoConstraints = false

        return ti
    }()
    
    let vi = { () -> UIView in
        let vie = UIView()
        let colour1 = UserDefaults.standard.string(forKey: "colour1") ?? "#E12459"
        let colour2 = UserDefaults.standard.string(forKey: "colour2") ?? "#E12459"
    
        let layer = CAGradientLayer()
        layer.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 90)
        layer.colors = [UIColor.init(hexString:colour1)?.cgColor,
                        UIColor.init(hexString:colour2)?.cgColor]
        layer.startPoint = CGPoint(x: 0.0, y: 0.5)
        layer.endPoint = CGPoint(x: 1.0, y: 0.5)
        vie.layer.addSublayer(layer)
//        let colour1 = UserDefaults.standard.string(forKey: "colour2")
//        vie.backgroundColor = UIColor.init(hexString: colour1!)
        vie.translatesAutoresizingMaskIntoConstraints = false

        return vie
    }()

    let vi2 = { () -> UIView in
        let vie = UIView()
        let colour1 = UserDefaults.standard.string(forKey: "colour1") ?? "#E12459"
        let colour2 = UserDefaults.standard.string(forKey: "colour2") ?? "#E12459"
        
        let layer = CAGradientLayer()
        layer.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 90)
        layer.colors = [UIColor.init(hexString:colour1)?.cgColor,
                        UIColor.init(hexString:colour2)?.cgColor]
        layer.startPoint = CGPoint(x: 0.0, y: 0.5)
        layer.endPoint = CGPoint(x: 1.0, y: 0.5)
        vie.layer.addSublayer(layer)
//         let colour1 = UserDefaults.standard.string(forKey: "colour1")
//        vie.backgroundColor = UIColor.init(hexString: colour1!)
        vie.translatesAutoresizingMaskIntoConstraints = false
        
        return vie
    }()

    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
            SetupUI()
            addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(VenueSectionHeader.tapHeader(_:))))
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func tapHeader(_ gestureRecognizer: UITapGestureRecognizer) {
        guard let cell = gestureRecognizer.view as? VenueSectionHeader else {
            return
        }
        
        delegate?.toggleSection(self, section: cell.section)
    }
    
    @objc func setCollapsed(_ collapsed: Bool) {   
    }
    
    @objc func SetupUI(){
        
        isSetup = false
        contentView.addSubview(vi)
        contentView.addSubview(vi2)
        contentView.addSubview(imageView)
        contentView.addSubview(textView)
        
        translatesAutoresizingMaskIntoConstraints = false
        
        vi.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0).isActive = true
        vi.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0).isActive = true
        vi.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 0).isActive = true
        vi.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: 0).isActive = true
        
        vi2.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0).isActive = true
        vi2.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0).isActive = true
        vi2.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 0).isActive = true
        vi2.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: 0).isActive = true

        textView.leftAnchor.constraint(equalTo: imageView.rightAnchor, constant: 20).isActive = true
        textView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: 8).isActive = true
        textView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 35).isActive = true
        textView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -35).isActive = true

        // imageView
        imageView.centerYAnchor.constraint(equalTo: textView.centerYAnchor, constant: 0).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        imageView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 20).isActive = true
    }
}
