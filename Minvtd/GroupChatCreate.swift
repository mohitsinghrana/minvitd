
//  GroupChatCreate.swift
//  Minvtd
//  Created by admin on 27/07/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.
//  Copyright © 2017 Euroinfotek. All rights reserved.


import UIKit
import Alamofire
import Firebase

protocol AddedMemberDelegate {
	func userAddedMembers()
}

class GroupChatCreate : UIViewController {
    
    @IBOutlet weak var navigationBar: UINavigationBar!
	
	var guests: [MInvitdGuest] = [MInvitdGuest]()
	var filterData = [MInvitdGuest]()
	var tempSelectedGuests = [MInvitdGuest]()
	var removeGuest : MInvitdGuest?
	
	var searchBar:UISearchBar = {
		let srch = UISearchBar()
		srch.placeholder = "Search"
		
		return srch
	}()
	
	var isSearching:Bool = false
	
	var chatID : String?
	var chatName : String?
	var fileID : String?
	var fileUrl : String?
	
	var delegate : AddedMemberDelegate?
    
    
    @IBOutlet weak var guestsCollectionView: UITableView!
    
	func setupNavBarContent(){
		
		let button1 = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(cancelButton(_:)))
		self.navigationItem.leftBarButtonItem = button1
		if !((chatID?.isEmpty)!){
			self.title = chatName
		}
		else{
			self.title = "New Group"
		}
		if !((chatID?.isEmpty)!){
			let button2 = UIBarButtonItem(title:"Add", style: .plain, target: self, action: #selector(addButton))
			self.navigationItem.rightBarButtonItem = button2
		}
		else{
			let button2 = UIBarButtonItem(title:"Create", style: .plain, target: self, action: #selector(groupButton))
			self.navigationItem.rightBarButtonItem = button2
		}
		
	}
	

    override func viewDidLoad() {
        
        super.viewDidLoad()
        guestsCollectionView.allowsMultipleSelection = true
        guestsCollectionView.delegate = self
        guestsCollectionView.dataSource = self
        
        navigationBarSetup(nil)
		setupNavBarContent()
		
    }
	
	@IBAction func addButton(_ sender: UIBarButtonItem) {
		
		let selectedContacts = tempSelectedGuests
		
			// do stuff here
			let alertVc = UIAlertController(title: "Add Members", message: "Do you want to add selected members?", preferredStyle: .alert)
			
			alertVc.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
			
			alertVc.addAction(UIAlertAction(title: "Add", style: .default, handler: { (action:UIAlertAction) in
				
				//parameters -
				
				let groupName =  self.chatName
				guard let eventID = UserDefaults.standard.string(forKey: "invitationID") else {return}
				let roomID = self.chatID!
		
				let urlStr = MInvitdClient.Constants.BaseUrl + "chat/members"
			
				guard let myUserID = UserDefaults.standard.string(forKey: "userID") else {return}
				guard let iName = UserDefaults.standard.string(forKey: "invitationTitle") else {return}
			
				let mem1 = "{\"userID\":\"\(myUserID)\",\"name\":\"\(groupName!)\",\"fileID\":\"\(self.fileID!)\",\"fileUrl\":\"\(self.fileUrl!)\"}"

				var usersArray: [String] = [mem1]
				
				if groupName != "" {
					
					for each in selectedContacts {
						
						//new
						if let userID = each.userID{
							usersArray.append("{\"userID\":\"\(userID)\",\"name\":\"\(groupName!)\",\"fileID\":\"\(self.fileID!)\",\"fileUrl\":\"\(self.fileUrl!)\"}")
						}
					}
					
					let parameters:Parameters = ["invitationID":eventID,
												 "chatID":roomID,
												 "guestID":myUserID,
												 "users": usersArray,
												 "iName": iName,
												 "name":groupName!,
												 "group":1
					                              ]
					print("\(parameters)")
					
					let authHeader = ["Authorization" : UserDefaults.standard.string(forKey: "userToken")!, "Content-Type" : "application/json" ]
					
					Alamofire.request(urlStr, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: authHeader).responseJSON { (response) in
					
						if let dataZ = response.data {
							
							print("\(response)")
							print("\(dataZ)")
						}
					}
				}
				// dissmiss to home
				self.delegate?.userAddedMembers()
				self.dismiss(animated: true, completion: nil)
			}))
			self.present(alertVc, animated: true, completion: nil)
	}

    
    @IBAction func groupButton(_ sender: UIBarButtonItem) {
		
		let selectedContacts = tempSelectedGuests
		
		if selectedContacts.count < 2 || selectedContacts.count > 50 {
			self.CreateAlertPopUp("Oops", "Number of members should be greater than 2 and less than 50", OnTap: nil)
			print(selectedContacts.count)
		}
		else{
			print(selectedContacts.count)
			// do stuff here
			let alertVc = UIAlertController(title: "Group Name", message: "", preferredStyle: .alert)
			alertVc.addTextField { (fild1) in
				fild1.placeholder = "Enter name of the group"
			}

			//alertVc.addTextField(configurationHandler: nil)
			alertVc.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

			alertVc.addAction(UIAlertAction(title: "Create", style: .default, handler: { (action:UIAlertAction) in

				let groupName =  alertVc.textFields?[0].text
				let myfirebaseID = Auth.auth().currentUser?.uid
				guard let eventID = UserDefaults.standard.string(forKey: "invitationID") else {return}

				let ref  = Database.database().reference().child("chatrooms").child(eventID)
				let messageRef = ref.childByAutoId()
				let roomID = messageRef.key

				//new
				let urlStr = MInvitdClient.Constants.BaseUrl + "chat/members"

				let myUserID = UserDefaults.standard.string(forKey: "userID")
				let iName = UserDefaults.standard.string(forKey: "invitationTitle")

				let mem1 = "{\"userID\":\"\(myUserID!)\",\"name\":\"\(groupName!)\",\"fileID\":\"0\",\"fileUrl\":\"\"}"

				//new end

				var usersArray: [String] = [mem1]

				if groupName != "" {

					for each in selectedContacts {

						//new
						if let userID = each.userID{
							usersArray.append("{\"userID\":\"\(userID)\",\"name\":\"\(groupName!)\",\"fileID\":\"0\",\"fileUrl\":\"\"}")
						}
					}

					let parameters:Parameters = ["invitationID":eventID,
												 "chatID":roomID,
												 "guestID":myUserID!,
												 "users": usersArray,
												 "iName": iName!,
												 "name":groupName!,
												 "group":1
					]

					print("these are the params \(parameters)")

					let vc = self.storyboard?.instantiateViewController(withIdentifier: "Chat") as! UINavigationController

					Alamofire.request(urlStr, method: .post, parameters: parameters).responseJSON(completionHandler: { (response:DataResponse<Any>) in

						print("\(response)")

						if let dataZ = response.data {

							print("tis is response\(response)")
							print("this is dataz\(dataZ)")

						}
					})
				}

				// dissmiss to home

				let presentingViewController = self.presentingViewController
				self.dismiss(animated: false, completion: {
					presentingViewController?.dismiss(animated: true, completion: nil)
				})
			}))
			self.present(alertVc, animated: true, completion: nil)
		}
	}
	
	
	@IBAction func cancelButton(_ sender: UIBarButtonItem) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
		searchBar.frame =  CGRect(x: 0, y: 0, width: view.bounds.width - 44.0, height: 44.0)
		searchBar.delegate = self
		guestsCollectionView.tableHeaderView = searchBar
		self.guestsCollectionView.setContentOffset(CGPoint(x: 0, y: 44), animated: true)
		
        
    }
    
    @objc func imageLayerForGradientBackgroundSmall() -> UIImage {
        
		let updatedFrame = CGRect(x: 0, y: 0, width: 40, height: 40)
        // take into account the status bar
		let layer = CAGradientLayer.gradientLayerForBounds(bounds: updatedFrame)
        UIGraphicsBeginImageContext(layer.bounds.size)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}

// MARK: - GuestsViewController: UICollectionViewDelegate, UICollectionViewDataSource

extension GroupChatCreate : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return CGFloat(60)
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let guest = isSearching ? filterData[indexPath.row]:  guests[indexPath.row]
		
        let cell = self.guestsCollectionView.dequeueReusableCell(withIdentifier: "guestListViewCell", for: indexPath) as! ChatGuestsCollectionViewCell
        
		cell.nameLabel.text = guest.name
		var firstLetter : String?
		if let firstChar = guest.name?.characters.first {
			let firstString = String(describing: firstChar)
			firstLetter = firstString
		}else {
			firstLetter = "?"
		}
		
		cell.imageLabel.text = firstLetter
        cell.imageLabel.layer.cornerRadius  = cell.imageLabel.frame.size.width/2
        cell.imageLabel.layer.masksToBounds = true
        cell.imageLabel.clipsToBounds = true

		
		if guest.userID == 0{
            //Fade UI
            cell.imageLabel.alpha = 0.5
            cell.nameLabel.alpha = 0.5
 
        }else{
            cell.imageLabel.alpha = 1
            cell.nameLabel.alpha = 1

        }
        
        let bgImage : UIImage = self.imageLayerForGradientBackgroundSmall()
        cell.imageLabel.backgroundColor = UIColor(patternImage: bgImage)
        
		if guest.isSelected {
			cell.accessoryType = .checkmark
			
		}else{
			cell.accessoryType = .none
		}
		
		for gg in tempSelectedGuests{
			if gg.phone == guest.phone{
				cell.accessoryType = .checkmark
			}
		}
		
        return cell
		
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return isSearching ? filterData.count : guests.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		let guest = isSearching ? filterData[indexPath.row] : guests[indexPath.row]
        
        let cell = tableView.cellForRow(at: indexPath) as? ChatGuestsCollectionViewCell
		
		if isSearching {
			
			if cell?.accessoryType == .checkmark {
				self.filterData[indexPath.row].isSelected = false
				removeGuest = guest
				print(" THIS IS DE-Select tempSelectedGuests : \n \(removeGuest)")
				
				var filter = [MInvitdGuest]()
				for guest in tempSelectedGuests {
					if guest.phone != removeGuest?.phone{
						filter.append(guest)
					}
				}
				tempSelectedGuests = filter
				cell?.accessoryType = .none
				searchBar.text = ""
				searchBar.resignFirstResponder()
				searchBar.setShowsCancelButton(false, animated: true)
				isSearching = false
				tableView.reloadData()
				
			}
			else{
				
				self.filterData[indexPath.row].isSelected = true
				self.tempSelectedGuests.append(guest)
				
				searchBar.text = ""
				searchBar.resignFirstResponder()
				searchBar.setShowsCancelButton(false, animated: true)
				isSearching = false
				tableView.reloadData()
			}
			
		}
		else{
			if cell?.accessoryType == .checkmark {
				self.guests[indexPath.row].isSelected = false
				removeGuest = guest
				print(" THIS IS DE-Select tempSelectedGuests : \n \(removeGuest)")
				
				var filter = [MInvitdGuest]()
				for guest in tempSelectedGuests {
					if guest.phone != removeGuest?.phone{
						filter.append(guest)
					}
				}
				tempSelectedGuests = filter
				cell?.accessoryType = .none
				tableView.reloadData()
			}
			else{
				
				
				self.guests[indexPath.row].isSelected = true
				self.tempSelectedGuests.append(guest)
				cell?.accessoryType = .checkmark
				tableView.reloadData()
			}
			
		}
	}
    
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
		
		let guest = isSearching ? filterData[indexPath.row] : guests[indexPath.row]

        let cell = tableView.cellForRow(at: indexPath) as? ChatGuestsCollectionViewCell
		
		if isSearching {
			if cell?.accessoryType == .none {
				self.filterData[indexPath.row].isSelected = true
				self.tempSelectedGuests.append(guest)
				
				searchBar.text = ""
				searchBar.resignFirstResponder()
				searchBar.setShowsCancelButton(false, animated: true)
				isSearching = false
				tableView.reloadData()
			}
			else{
				self.filterData[indexPath.row].isSelected = false
				removeGuest = guest
				print(" THIS IS DE-Select tempSelectedGuests : \n \(removeGuest)")
				
				var filter = [MInvitdGuest]()
				for guest in tempSelectedGuests {
					if guest.phone != removeGuest?.phone{
						filter.append(guest)
					}
				}
				tempSelectedGuests = filter
				cell?.accessoryType = .none
				searchBar.text = ""
				searchBar.resignFirstResponder()
				searchBar.setShowsCancelButton(false, animated: true)
				isSearching = false
				tableView.reloadData()
			}
		}
		else{
			if cell?.accessoryType == .none {
				
				self.guests[indexPath.row].isSelected = true
				self.tempSelectedGuests.append(guest)
				cell?.accessoryType = .checkmark
				tableView.reloadData()
				
			}
			else{
				self.guests[indexPath.row].isSelected = false
				removeGuest = guest
				print(" THIS IS DE-Select tempSelectedGuests : \n \(removeGuest)")
				
				var filter = [MInvitdGuest]()
				for guest in tempSelectedGuests {
					if guest.phone != removeGuest?.phone{
						filter.append(guest)
					}
				}
				tempSelectedGuests = filter
				cell?.accessoryType = .none
				tableView.reloadData()
			}
		}
		print(" THIS IS filter Select tempSelectedGuests : \n \(self.tempSelectedGuests)")
	}
}

extension GroupChatCreate:UISearchBarDelegate {
	
	func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
		searchBar.setShowsCancelButton(true, animated: true)
	}
	func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
		searchBar.resignFirstResponder()
		searchBar.setShowsCancelButton(false, animated: true)
		// tableView.reloadData()
		self.guestsCollectionView.setContentOffset(CGPoint(x: 0, y: 44), animated: true)
		
	}
	
	func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
		searchBar.text = ""
		searchBar.resignFirstResponder()
		searchBar.setShowsCancelButton(false, animated: true)
		isSearching = false
		guestsCollectionView.reloadData()
		self.guestsCollectionView.setContentOffset(CGPoint(x: 0, y: 44), animated: true)
		
	}
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		
		//reloading here
		if searchBar.text == nil || searchBar.text == ""{
			isSearching = false
			searchBar.setShowsCancelButton(false, animated: true)
			view.endEditing(true)
			guestsCollectionView.reloadData()
			self.guestsCollectionView.setContentOffset(CGPoint(x: 0, y: 44), animated: true)
			return
		}
		else {
			isSearching = true
			filterData = guests.filter({
				return ($0.name?.contains(searchText))!})
			guestsCollectionView.reloadData()
			
		}
		
	}
}
