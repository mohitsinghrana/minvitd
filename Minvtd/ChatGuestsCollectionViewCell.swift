
//  ChatGuestsCollectionViewCell.swift
//  Minvtd
//
//  Created by Tushar Aggarwal on 16/06/17.
//  Copyright © 2017 Euroinfotek. All rights reserved.


import UIKit

class ChatGuestsCollectionViewCell : UITableViewCell {
    
   
    @IBOutlet weak var imageLabel: UILabel!
  
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var guestImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       selectionStyle = .none
    }
    
}
