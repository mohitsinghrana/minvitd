//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//


import UIKit
import Alamofire
import AlamofireImage
import UITextView_Placeholder
import SlideMenuControllerSwift
import IDMPhotoBrowser


class AllFeedViewController: UITableViewController , IDMPhotoBrowserDelegate{
    
    lazy var refresher:UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .gray
        refreshControl.addTarget(self, action: #selector(requestData), for: .valueChanged )
        
        return refreshControl
    }()
    
    private let cellId = "cellId"
    private let first = "firstCell"
    private let header = "header"
    private let ImageCellt = "ImageCell"
    private let VideoCellt = "VideoCellt"

    var allFeeds: [MInvtdAllFeed] = []
    var nonfilteredArray: [MInvtdAllFeed] = []
    var filteredArray: [MInvtdAllFeed] = []
    var adminFeedArray: [MInvtdAllFeed] = []
    var selectedSegmentIndex1 = 0

    var photos : [IDMPhoto] = [IDMPhoto]()
    var browser:IDMPhotoBrowser? = nil
    var tagesVC:GuestListProfileVC?
    var previous:IndexPath?
   
    var paramaters : MInvtdAllFeed?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBarSetup(nil)
        
        //refresh setup
        tableView.refreshControl = refresher

        tableView.register(TextONLY.self, forCellReuseIdentifier: cellId)
        tableView.register(firstCell.self, forCellReuseIdentifier: first)
        tableView.register(ImageCell.self, forCellReuseIdentifier: ImageCellt)
        tableView.register(VideoCell.self, forCellReuseIdentifier: VideoCellt)
        tableView.register(FeedSectionHeader.self, forHeaderFooterViewReuseIdentifier: header)

        self.navigationController?.navigationBar.isTranslucent = false
        let invitationName = UserDefaults.standard.string(forKey: "invitationTitle")
        self.navigationItem.title = invitationName
        // main header vc
        
        //changing 82 to 500
        let vi = FeedHeaderView(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height:  55 + heightImg))
        vi.backgroundColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1.0)
        vi.VC = self
        
        vi.coverImg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(imageTapMain)))
        vi.profileImg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(imageTapSmall)))

        tableView.tableHeaderView = vi
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1.0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getFeedsRequest(indexpath: previous)
    }
    
    @IBAction func menuOpen(_ sender: UIBarButtonItem) {
        slideMenuController()?.openLeft()
    }
    @objc func requestData() {
        //print("request refresh")
        
        getFeedsRequest(indexpath: previous)
        self.tableView.reloadData()
        
        let deadline = DispatchTime.now() + .milliseconds(500)
        DispatchQueue.main.asyncAfter(deadline: deadline) {
            self.refresher.endRefreshing()
        }
        // Code to refresh table view
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let vi = tableView.dequeueReusableHeaderFooterView(withIdentifier: header) as? FeedSectionHeader
        vi?.VC = self
        
        let allFullString = NSMutableAttributedString(string: "ALL ", attributes:  [NSAttributedStringKey.foregroundColor : UIColor.black])
        let countAllString = NSMutableAttributedString(string: "("+self.nonfilteredArray.count.description + ")", attributes: [NSAttributedStringKey.foregroundColor : UIColor.gray])
        allFullString.append(countAllString)
        
        vi?.textLabel1.setAttributedTitle(allFullString, for: .normal)
        
        let upFullString = NSMutableAttributedString(string: "UPDATES ", attributes:  [NSAttributedStringKey.foregroundColor : UIColor.black])
        let countupString = NSMutableAttributedString(string: "("+self.adminFeedArray.count.description + ")", attributes: [NSAttributedStringKey.foregroundColor : UIColor.gray])
        upFullString.append(countupString)
        
        vi?.textLabel2.setAttributedTitle(upFullString, for: .normal)
        vi?.textLabel1.addTarget(self, action: #selector(segmentFilter(_:)), for: .touchUpInside)
        vi?.textLabel2.addTarget(self, action: #selector(segmentFilter(_:)), for: .touchUpInside)
        vi?.textLabel2.tag = 1
        vi?.configure(num: selectedSegmentIndex1)
        
        return vi
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allFeeds.count + 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: first, for: indexPath) as? firstCell
            cell?.backgroundColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1.0)
            cell?.VC = self
            cell?.selectionStyle = .none
            return cell!
        }
        
        let item = allFeeds[indexPath.row - 1]
        
        if item.fileType! == "gallery"{

            let cell = tableView.dequeueReusableCell(withIdentifier: ImageCellt, for: indexPath) as? ImageCell
            cell?.backgroundColor = .white
            cell?.selectionStyle = .none

            cell?.feed = item
            cell?.VC = self

            cell?.contentView.layer.cornerRadius = 4.0
            cell?.contentView.layer.borderWidth = 1.0
            cell?.contentView.layer.borderColor = UIColor.clear.cgColor
            cell?.contentView.layer.masksToBounds = false
            cell?.layer.shadowColor = UIColor(red: 211/255, green: 211/255, blue: 211/255, alpha: 1.0).cgColor
            cell?.layer.shadowOffset = CGSize(width: 4, height: 8)
            cell?.layer.shadowRadius = 4.0
            cell?.layer.shadowOpacity = 1.0
            cell?.layer.masksToBounds = false
            cell?.layer.shadowPath = UIBezierPath(roundedRect: (cell?.bounds)!, cornerRadius: (cell?.contentView.layer.cornerRadius)!).cgPath
            
            return cell!
        }
        
        else if item.fileType! == "text"{
                
            //print("This is item in feedCommentVC in text :\n\(item)\n AND for index\n \(indexPath.row)")
            
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? TextONLY
            cell?.backgroundColor = .white
            cell?.selectionStyle = .none
            cell?.feed = item
            cell?.VC = self
            
            cell?.contentView.layer.cornerRadius = 4.0
            cell?.contentView.layer.borderWidth = 1.0
            cell?.contentView.layer.borderColor = UIColor.clear.cgColor
            cell?.contentView.layer.masksToBounds = false
            cell?.layer.shadowColor = UIColor(red: 211/255, green: 211/255, blue: 211/255, alpha: 1.0).cgColor
            cell?.layer.shadowOffset = CGSize(width: 4, height: 8)
            cell?.layer.shadowRadius = 4.0
            cell?.layer.shadowOpacity = 1.0
            cell?.layer.masksToBounds = false
            cell?.layer.shadowPath = UIBezierPath(roundedRect: (cell?.bounds)!, cornerRadius: (cell?.contentView.layer.cornerRadius)!).cgPath
           
            return cell!
            
        }else if item.fileType! == "image"{
            
            //print("This is item in feedCommentVC in image :\n\(item)\n AND for index\n \(indexPath.row)")
            
            let cell = tableView.dequeueReusableCell(withIdentifier: ImageCellt, for: indexPath) as? ImageCell
            cell?.backgroundColor = .white
            cell?.selectionStyle = .none
            
            cell?.feed = item
            cell?.VC = self
            
            if let galleryIcon = cell?.contentImg.viewWithTag(99) {
                galleryIcon.removeFromSuperview()
            }
            cell?.contentView.layer.cornerRadius = 4.0
            cell?.contentView.layer.borderWidth = 1.0
            cell?.contentView.layer.borderColor = UIColor.clear.cgColor
            cell?.contentView.layer.masksToBounds = false
            cell?.layer.shadowColor = UIColor(red: 211/255, green: 211/255, blue: 211/255, alpha: 1.0).cgColor
            cell?.layer.shadowOffset = CGSize(width: 4, height: 8)
            cell?.layer.shadowRadius = 4.0
            cell?.layer.shadowOpacity = 1.0
            cell?.layer.masksToBounds = false
            cell?.layer.shadowPath = UIBezierPath(roundedRect: (cell?.bounds)!, cornerRadius: (cell?.contentView.layer.cornerRadius)!).cgPath
            
            return cell!
        }
            else if item.fileType! == "video"{
            
            //print("This is item in feedCommentVC in video :\n\(item)\n AND for index\n \(indexPath.row)")
            
            let cell = tableView.dequeueReusableCell(withIdentifier: VideoCellt, for: indexPath) as? VideoCell
            cell?.backgroundColor = .white
            cell?.selectionStyle = .none
            cell?.feed = item
            cell?.VC = self
            
            let videoIcon: UIImageView = {

                let imgView = UIImageView()
                imgView.clipsToBounds = true
                imgView.translatesAutoresizingMaskIntoConstraints = false
                imgView.backgroundColor = UIColor.black
                imgView.isUserInteractionEnabled = true
                imgView.contentMode = .scaleToFill
                imgView.image = UIImage(named: "videoPlayIcon")
                imgView.backgroundColor = UIColor.clear
                imgView.frame = CGRect(x: 0, y: 0, width: 18, height: 18)
                return imgView
            }()
            
            cell?.contentImg.addSubview(videoIcon)
            videoIcon.anchorCenterSuperview()
            
            cell?.contentView.layer.cornerRadius = 4.0
            cell?.contentView.layer.borderWidth = 1.0
            cell?.contentView.layer.borderColor = UIColor.clear.cgColor
            cell?.contentView.layer.masksToBounds = false
            cell?.layer.shadowColor = UIColor(red: 211/255, green: 211/255, blue: 211/255, alpha: 1.0).cgColor
            cell?.layer.shadowOffset = CGSize(width: 4, height: 8)
            cell?.layer.shadowRadius = 4.0
            cell?.layer.shadowOpacity = 1.0
            cell?.layer.masksToBounds = false
            cell?.layer.shadowPath = UIBezierPath(roundedRect: (cell?.bounds)!, cornerRadius: (cell?.contentView.layer.cornerRadius)!).cgPath
            return cell!
            
        }else {
            return UITableViewCell()
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var cellheight:CGFloat = 0
        
        if indexPath.row == 0{
            return 80
        }
        else{
            
            let feed = allFeeds[indexPath.row - 1]
            let options:NSStringDrawingOptions =  NSStringDrawingOptions.usesFontLeading.union(NSStringDrawingOptions.usesLineFragmentOrigin)
            
            if let comment = feed.comment{
                
                let size = CGSize(width: UIScreen.main.bounds.width - 40, height: 1000)
                let estimateFrame = NSString(string: comment).boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font:UIFont(name: "GothamRounded-Book", size: 16)!], context: nil)
                cellheight = 94 + estimateFrame.height + 8
            }
            else{
                cellheight = 94 + 8
            }
            
            //footer height
            let footerheight:CGFloat = 35 + 16 + 45 + 12   // no comment
            
            if feed.subCommentsCount! == 0{
                cellheight += footerheight
            }else{
                //comment height
                if let comment = feed.subCommentArray?.first {
                    let size = CGSize(width: UIScreen.main.bounds.width - 78, height: 1000)
                    
                    if let mycomment = comment.comment {
                        if let myuserName = comment.userName{
                            let estimatedFrame = NSString(string: mycomment + "  " + myuserName ).boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font:UIFont(name: "GothamRounded-Book", size: 15)!], context: nil)
                            
                            cellheight += footerheight + 16 + 16 + 6 + estimatedFrame.height + 50
                            // comment then comment height
                        }
                    }
                }
            }
            
            if feed.fileType! != "text"{
                cellheight += heightImg
            }
            
            return cellheight
        }
    }
}

