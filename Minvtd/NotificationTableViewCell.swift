//
//  NotificationTableViewCell.swift
//  Minvtd
//
//  Created by Tushar Aggarwal on 11/06/17.
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

   
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var typeImage: UIImageView!
}
