//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

class PopUpImageOnlyCellVC :  UIViewController {
    
   
    @IBOutlet weak var eventTitle: UILabel!
    @IBOutlet weak var descript: UITextView!
    @IBOutlet weak var area: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var time: UILabel!
    var event : MInvitdEvent?
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var okayBtn: UIButton!
    @IBOutlet weak var changeStatusBtn: UIButton!
    @IBAction func dismissVC(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var locationImage: UIImageView!
    @IBOutlet weak var calenderImage: UIImageView!
    @IBOutlet weak var clockImage: UIImageView!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.okayBtn.layer.cornerRadius = 10
        self.okayBtn.clipsToBounds = true
        self.changeStatusBtn.layer.cornerRadius = 10
        self.changeStatusBtn.clipsToBounds = true
        self.mainView.layer.cornerRadius = 10
        self.mainView.clipsToBounds = true
        
        self.date.text = event?.date
        self.time.text = event?.time
        let stringEventDescription = (event?.description)?.replacingOccurrences(of: "amp;", with: "")
        self.descript.text = stringEventDescription
        let stringEventName = (event?.name)?.replacingOccurrences(of: "amp;", with: "")
        self.eventTitle.text = stringEventName
        
        self.changeStatusBtn.addTarget(self, action: #selector(self.statusChange(sender:)), for: UIControlEvents.touchUpInside)
        
        self.locationImage.image = self.locationImage.image?.withRenderingMode(.alwaysTemplate)
        self.calenderImage.image = self.calenderImage.image?.withRenderingMode(.alwaysTemplate)
        self.clockImage.image = self.clockImage.image?.withRenderingMode(.alwaysTemplate)
        
        
    
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        super.viewDidDisappear(animated)
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @objc func statusChange(sender : UIButton){
        
        let alertController = UIAlertController(title: "Minvitd", message: "Change your response", preferredStyle: .alert)
        
        if self.event?.guestStatus == 1 {
            
            // Create the actions
            let accept  = UIAlertAction(title: "Accept", style: UIAlertActionStyle.destructive) {
                UIAlertAction in
                self.changeStatus(status: "1")
                
            }
            let reject = UIAlertAction(title: "Reject", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.changeStatus(status: "2")
            }
            
            let maybe = UIAlertAction(title: "Maybe", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.changeStatus(status: "3")
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                // when cancel is tapped
            }
            
            // Add the actions
            alertController.addAction(accept)
            alertController.addAction(reject)
            alertController.addAction(maybe)
            alertController.addAction(cancelAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
            
            
            
        }else if self.event?.guestStatus == 2 {
            // Create the actions
            let accept  = UIAlertAction(title: "Accept", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.changeStatus(status: "1")
                
            }
            let reject = UIAlertAction(title: "Reject", style: UIAlertActionStyle.destructive) {
                UIAlertAction in
                self.changeStatus(status: "2")
                
            }
            let maybe = UIAlertAction(title: "Maybe", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.changeStatus(status: "3")
                
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                // when cancel is tapped
            }
            
            // Add the actions
            alertController.addAction(accept)
            alertController.addAction(reject)
            alertController.addAction(maybe)
            alertController.addAction(cancelAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
            
            
            
        }else if self.event?.guestStatus == 3 {
            
            // Create the actions
            let accept  = UIAlertAction(title: "Accept", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.changeStatus(status: "1")
                
            }
            let reject = UIAlertAction(title: "Reject", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.changeStatus(status: "2")
                
            }
            let maybe = UIAlertAction(title: "Maybe", style: UIAlertActionStyle.destructive) {
                UIAlertAction in
                self.changeStatus(status: "3")
                
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                // when cancel is tapped
            }
            
            // Add the actions
            alertController.addAction(accept)
            alertController.addAction(reject)
            alertController.addAction(maybe)
            alertController.addAction(cancelAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
            
            
            
        }else{
            
            // Create the actions
            let accept  = UIAlertAction(title: "Accept", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.changeStatus(status: "1")
                
            }
            let reject = UIAlertAction(title: "Reject", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.changeStatus(status: "2")
                
            }
            let maybe = UIAlertAction(title: "Maybe", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.changeStatus(status: "3")
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                // when cancel is tapped
            }
            
            // Add the actions
            alertController.addAction(accept)
            alertController.addAction(reject)
            alertController.addAction(maybe)
            alertController.addAction(cancelAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
            
        }
        
    }
    
    @objc func changeStatus(status : String){
        
        let guestID = self.event?.guestID
        
        let parameters: Parameters = ["guestID" : guestID as AnyObject, "status" : status as AnyObject]
        
        let url = MInvitdClient.Constants.BaseUrl + "guest/status"
        
        let authHeader = ["Authorization" : UserDefaults.standard.string(forKey: "userToken")!, "Content-Type" : "application/json" ]
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: authHeader).responseJSON { (response) in
            
            if response.data != nil{
            }else{
                
                let alert = UIAlertController(title: "Couldn't connect to server", message: "Check Internet", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: { (action : UIAlertAction) in
                    alert.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    
    
}
