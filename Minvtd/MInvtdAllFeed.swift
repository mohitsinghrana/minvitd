//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//


import UIKit


struct MInvtdAllFeed{
    
    
    // MARK: Properties
    
    var postID : Int?  //feed objectID
    var userID : Int? // post owner
    var invitationID : Int? // invitation
    var eventID : Int? // event ID
    var commentID : Int? // commentID
    var comment : String? // top textview
    var created : Double? // date formating
    var userName : String? // profile name
    var userImageUrl : String? // profileirl
    var likes : Int?
    var liked : Bool?
    var subCommentsCount : Int?
    var galleryCount : Int?
    var fileID : Int? // image ID
    var fileUrl : String? // image
    var fileThumbnail : String?
    var fileType : String?
    var typeName : String? // text
    var subCommentArray : [MInvtdAllFeedSubComment]?
    var imageUrl : String? //extra
    var title : String? //extra
    var description : String? //extra
    var date : String? //
    var gallery: [MInvitdAllFeedTwo]?
    var users:[MInvitdGuest] = []
    var parentTypeName: String?
    var type: Int?
    var likeID: Int?
    
    
    // MARK: Initializers
    init(){
        postID = nil
        userID  = nil
        invitationID  = nil
        eventID  = nil
        commentID  = nil
        fileID = nil
        comment  = nil
        created  = nil
        userName  = nil
        userImageUrl  = nil
        likes  = nil
        liked  = nil
        subCommentsCount = nil
        fileUrl  = nil
        fileThumbnail = nil
        fileType  = nil
        typeName  = nil
        title  = nil
        description  = nil
        date  = nil
        subCommentArray = nil
        gallery = nil
        parentTypeName = nil
        type = nil
        likeID = nil
    }
    
    init(dictionary: [String:AnyObject]) {
        
        postID = dictionary[MInvitdClient.JSONResponseKeys.postID] as? Int
        userID = dictionary[MInvitdClient.JSONResponseKeys.userID] as? Int
        invitationID = dictionary[MInvitdClient.JSONResponseKeys.invitationID] as? Int
        eventID = dictionary[MInvitdClient.JSONResponseKeys.eventID] as? Int
        commentID = dictionary[MInvitdClient.JSONResponseKeys.commentID] as? Int
        fileID = dictionary[MInvitdClient.JSONResponseKeys.fileID] as? Int
        comment = dictionary[MInvitdClient.JSONResponseKeys.comment] as? String
        created = dictionary[MInvitdClient.JSONResponseKeys.created] as? Double
        userImageUrl = dictionary[MInvitdClient.JSONResponseKeys.userImageUrl] as? String
        userName = dictionary[MInvitdClient.JSONResponseKeys.userName] as? String
        likes = dictionary[MInvitdClient.JSONResponseKeys.likes] as? Int
        galleryCount = dictionary["galleryCount"] as? Int
        liked = dictionary[MInvitdClient.JSONResponseKeys.liked] as? Bool
        subCommentsCount = dictionary[MInvitdClient.JSONResponseKeys.subCommentsCount] as? Int
        fileUrl = dictionary[MInvitdClient.JSONResponseKeys.fileUrl] as? String
        fileThumbnail = dictionary[MInvitdClient.JSONResponseKeys.fileThumbnail] as? String
        fileType = dictionary[MInvitdClient.JSONResponseKeys.fileType] as? String
        typeName = dictionary["typeName"] as? String
        description = dictionary[MInvitdClient.JSONResponseKeys.description] as? String
        title = dictionary[MInvitdClient.JSONResponseKeys.title] as? String
        date = dictionary[MInvitdClient.JSONResponseKeys.date] as? String
        imageUrl = dictionary[MInvitdClient.JSONResponseKeys.imageUrl] as? String
        parentTypeName = dictionary[MInvitdClient.JSONResponseKeys.parentTypeName] as? String
        type = dictionary[MInvitdClient.JSONResponseKeys.type] as? Int
        likeID  = dictionary[MInvitdClient.JSONResponseKeys.likeID] as? Int
        
        if let subCommentArray = dictionary["subComments"] as? [AnyObject], subCommentArray.isEmpty == false {
            var temp1 = [MInvtdAllFeedSubComment]()
            for i in 0...subCommentArray.count-1 {
                let temp = MInvtdAllFeedSubComment(dictionary: subCommentArray[i] as! [String : AnyObject])
                temp1.append(temp)
            }
            self.subCommentArray = temp1
        } else {
            self.subCommentArray = nil
        }
        
        if let gallery = dictionary["gallery"] as? [AnyObject], gallery.isEmpty == false {
            var temp1 = [MInvitdAllFeedTwo]()
            for i in 0...gallery.count-1 {
                let temp = MInvitdAllFeedTwo(dictionary: gallery[i] as! [String : AnyObject])
                temp1.append(temp)
            }
            self.gallery = temp1
        } else {
            self.gallery = nil
        }

        if let users = dictionary["users"] as? [[String:AnyObject]] {
          self.users =   MInvitdGuest.guestsFromResults(users)
        }
    }
    
    static func allFeedsFromResults(_ results: [[String:AnyObject]]) -> [MInvtdAllFeed] {
        var feeds = [MInvtdAllFeed]()
        
        // iterate through array of dictionaries, each Article is a dictionary
        for feed in results {
            feeds.append(MInvtdAllFeed(dictionary: feed))
        }
        
        return feeds
    }
}
