//
//  ListingCollectionReusableViewForRecommendation.swift
//  Minvtd
//
//  Created by Vineet Singh on 18/07/18.
//  Copyright © 2018 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit

class ListingCollectionReusableViewForRecommendation: UICollectionReusableView{
    
    @IBOutlet weak var vni: UIImageView!
    
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var vai: UIImageView!
    
    @IBOutlet weak var address: UILabel!
    
    @IBOutlet weak var vci: UIImageView!

    @IBOutlet weak var contact: UILabel!
    
    @IBOutlet weak var view1: UIView!
    
    @IBOutlet weak var view2: UIView!
    
    @IBOutlet weak var vdi: UIImageView!
    
    @IBOutlet weak var descLabel: UILabel!
    
    @IBOutlet weak var desc: UILabel!
    
    @IBOutlet weak var descriptionViewExpandBtn: UIButton!
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var callButton: UIButton!
}
