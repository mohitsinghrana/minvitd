
//  Copyright © 2017 Euroinfotek. All rights reserved.



import UIKit
import SlideMenuControllerSwift
import Alamofire
import Firebase
import FBSDKLoginKit


class SlideMenuViewController : UIViewController  {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var coverImage: UIImageView!

    @IBOutlet weak var logoutImage: UIImageView!
    @IBOutlet weak var settingsImage: UIImageView!
    @IBOutlet weak var invitationImage: UIImageView!
    @IBOutlet weak var homeImage: UIImageView!
    
    @IBOutlet weak var homeView: UIView!
    @IBOutlet weak var invitationView: UIView!
    
    @IBOutlet weak var settingsView: UIView!
    
    @IBOutlet weak var logoutView: UIView!
    @objc var menuTag : Int = 1
    @IBOutlet weak var versionNumLabel:UILabel!
    @IBOutlet weak var buildNumLabel:UILabel!

    @IBOutlet weak var termsView: UIView!
    @IBOutlet weak var aboutView: UIView!
    @IBOutlet weak var termsImage: UIImageView!
    @IBOutlet weak var aboutImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        homeImage.image  = homeImage.image?.withRenderingMode(.alwaysTemplate)
        homeImage.tintColor = UIColor.darkGray
        
        invitationImage.image  = invitationImage.image?.withRenderingMode(.alwaysTemplate)
        invitationImage.tintColor = UIColor.darkGray

        settingsImage.image  = settingsImage.image?.withRenderingMode(.alwaysTemplate)
        settingsImage.tintColor = UIColor.darkGray

        logoutImage.image  = logoutImage.image?.withRenderingMode(.alwaysTemplate)
        logoutImage.tintColor = UIColor.darkGray

        DispatchQueue.main.async {
            
            if let imageUrl = UserDefaults.standard.string(forKey: "userAvatar"){
                self.profileImage.af_setImage(withURL: URL(string: imageUrl)!)
                self.profileImage.contentMode = .scaleAspectFill
                self.profileImage.layer.cornerRadius  = self.profileImage.frame.size.width/2
                self.profileImage.layer.masksToBounds = true
                self.profileImage.clipsToBounds = true
            }
            
            
            if let name = UserDefaults.standard.string(forKey: "name") {
                self.nameLabel.text = name
            }
        }
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.locations = [0.0,0.35,0.65,1.0]
        let myColor1 = UIColor(red: 77/255, green: 182/255, blue: 172/255, alpha: 1.0).cgColor
        let myColor2 = UIColor(red: 0, green: 150/255, blue: 136/255, alpha: 1.0).cgColor
        let myColor3 = UIColor(red: 0, green: 105/255, blue: 92/255, alpha: 1.0).cgColor
        gradientLayer.colors = [myColor1,myColor2,myColor3]
        gradientLayer.frame = CGRect(x: 0, y: 0, width: self.coverImage.frame.width, height: self.coverImage.frame.height)
        self.coverImage.backgroundColor = UIColor.white
        self.coverImage.image = nil
        self.coverImage.layer.masksToBounds = true
        self.coverImage.layer.sublayers = nil
        self.coverImage.layer.addSublayer(gradientLayer)
        
        if menuTag == 1
        {
            let homeTap = UITapGestureRecognizer(target: self, action: #selector(self.homeTapDetected(_:)))
            homeTap.numberOfTapsRequired = 1
            homeView.isUserInteractionEnabled = true
            homeView.addGestureRecognizer(homeTap)
            
            let invitationTap = UITapGestureRecognizer(target: self, action: #selector(self.invitationTapDetected(_:)))
            invitationTap.numberOfTapsRequired = 1
            invitationView.isUserInteractionEnabled = true
            invitationView.addGestureRecognizer(invitationTap)
        }
        else{
            
            let homeTap2 = UITapGestureRecognizer(target: self, action: #selector(self.homeTapDetected2(_:)))
            homeTap2.numberOfTapsRequired = 1
            homeView.isUserInteractionEnabled = true
            homeView.addGestureRecognizer(homeTap2)
            
            let invitationTap2 = UITapGestureRecognizer(target: self, action: #selector(self.invitationTapDetected2(_:)))
            invitationTap2.numberOfTapsRequired = 1
            invitationView.isUserInteractionEnabled = true
            invitationView.addGestureRecognizer(invitationTap2)
        }
        

        let settingsTap = UITapGestureRecognizer(target: self, action: #selector(self.settingsTapDetected(_:)))
        settingsTap.numberOfTapsRequired = 1
        settingsView.isUserInteractionEnabled = true
        settingsView.addGestureRecognizer(settingsTap)
        
        
        let logoutTap = UITapGestureRecognizer(target: self, action: #selector(self.logoutTapDetected(_:)))
        logoutTap.numberOfTapsRequired = 1
        logoutView.isUserInteractionEnabled = true
        logoutView.addGestureRecognizer(logoutTap)
        
        
        let profiletap = UITapGestureRecognizer(target: self, action: #selector(self.profileTapDetected(_:)))
        profiletap.numberOfTapsRequired = 1
        profileImage.isUserInteractionEnabled = true
        profileImage.addGestureRecognizer(profiletap)
        
        let termsTap = UITapGestureRecognizer(target: self, action: #selector(self.termsViewTapped(_:)))
        termsTap.numberOfTapsRequired = 1
        termsView.isUserInteractionEnabled = true
        termsView.addGestureRecognizer(termsTap)
        
        let aboutTap = UITapGestureRecognizer(target: self, action: #selector(self.aboutViewTapped(_:)))
        aboutTap.numberOfTapsRequired = 1
        aboutView.isUserInteractionEnabled = true
        aboutView.addGestureRecognizer(aboutTap)
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            self.versionNumLabel.text = "Version " + version
        }

        if let version = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            self.buildNumLabel.text = "build " + version
        }

    }
    
       @objc func termsViewTapped(_ sender: UITapGestureRecognizer){
        
        slideMenuController()?.closeLeft()
        
//         UIApplication.shared.open((NSURL(string:"https://minvitd.com/terms?device=mobile")! as URL), options: [:], completionHandler: nil)
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Storyboard", bundle: nil)
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "termsvc") as! TermsVC
        present(mainVC, animated: true, completion: nil)
        
       }
    
    @objc func aboutViewTapped(_ sender: UITapGestureRecognizer){
        
        slideMenuController()?.closeLeft()
        
//        UIApplication.shared.open((NSURL(string:"https://minvitd.com/about?device=mobile")! as URL), options: [:], completionHandler: nil)
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Storyboard", bundle: nil)
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "aboutvc") as! AboutVC
        present(mainVC, animated: true, completion: nil)
        
    }
    
    
       @objc  func profileTapDetected(_ sender: UITapGestureRecognizer){
        
        slideMenuController()?.closeLeft()
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let storyBoard2 : UIStoryboard = UIStoryboard(name: "Storyboard", bundle: nil)
        
        //let vc = storyBoard.instantiateViewController(withIdentifier: "settingsVC") as! SettingsViewController
        
        let leftDrawerVC = storyBoard.instantiateViewController(withIdentifier: "SlideMenuViewController")
        let mainVC = storyBoard2.instantiateViewController(withIdentifier: "ProfileUpdateVC") as! ProfileUpdateVC
        
        let vc = SlideMenuController(mainViewController:
            UINavigationController(rootViewController:mainVC), leftMenuViewController: leftDrawerVC)
        
        pushPresentAnimation()
        present(vc, animated: false, completion: nil)
        
    }
    
    
    
    @objc func homeTapDetected(_ sender: UITapGestureRecognizer) {
        
        slideMenuController()?.closeLeft()
        
        invite = false
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        //let vc = storyBoard.instantiateViewController(withIdentifier: "invitationVC") as! InvitationViewController
        
        let leftDrawerVC = storyBoard.instantiateViewController(withIdentifier: "SlideMenuViewController")
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "invitationVC") as! InvitationViewController
        
        //mainVC.invitationTag = "0"
        
        let vc = SlideMenuController(mainViewController:
            UINavigationController(rootViewController:mainVC), leftMenuViewController: leftDrawerVC)
        
        pushPresentAnimation()
        present(vc, animated: false, completion: nil)
        
    }
    
    @objc func homeTapDetected2(_ sender: UITapGestureRecognizer) {
        
        slideMenuController()?.closeLeft()

        invite = false
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let leftDrawerVC = storyBoard.instantiateViewController(withIdentifier: "SlideMenuViewController") as! SlideMenuViewController
        leftDrawerVC.menuTag = 1
        
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "invitationVC") as! InvitationViewController
        
        let vc = SlideMenuController(mainViewController:
            UINavigationController(rootViewController:mainVC), leftMenuViewController: leftDrawerVC)
        
        pushPresentAnimation()
        present(vc, animated: false, completion: nil)
    }
    
    @objc func invitationTapDetected(_ sender: UITapGestureRecognizer) {
        
        slideMenuController()?.closeLeft()
        
        //UserDefaults.standard.set(true, forKey: "myInvitations")
        invite = true

        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        //let vc = storyBoard.instantiateViewController(withIdentifier: "invitationVC") as! InvitationViewController
        
        let leftDrawerVC = storyBoard.instantiateViewController(withIdentifier: "SlideMenuViewController") as! SlideMenuViewController
        leftDrawerVC.menuTag = 1
        
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "invitationVC") as! InvitationViewController
        
        let vc = SlideMenuController(mainViewController:
            UINavigationController(rootViewController:mainVC), leftMenuViewController: leftDrawerVC)
        
        pushPresentAnimation()
        present(vc, animated: false, completion: nil)
        
    }
    
    @objc func invitationTapDetected2(_ sender: UITapGestureRecognizer) {
        
        slideMenuController()?.closeLeft()
        
        invite = true

        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        //let vc = storyBoard.instantiateViewController(withIdentifier: "invitationVC") as! InvitationViewController
        
        let leftDrawerVC = storyBoard.instantiateViewController(withIdentifier: "SlideMenuViewController") as! SlideMenuViewController
        leftDrawerVC.menuTag = 1
        
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "invitationVC") as! InvitationViewController
        
        let vc = SlideMenuController(mainViewController:
            UINavigationController(rootViewController:mainVC), leftMenuViewController: leftDrawerVC)
        
        pushPresentAnimation()
        present(vc, animated: false, completion: nil)
    }

    
    @objc func settingsTapDetected(_ sender: UITapGestureRecognizer) {
        
        
        slideMenuController()?.closeLeft()

        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        
        let leftDrawerVC = storyBoard.instantiateViewController(withIdentifier: "SlideMenuViewController")
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "contactUsVC") as! ContactUsViewController
        
        let vc = SlideMenuController(mainViewController:
            UINavigationController(rootViewController:mainVC), leftMenuViewController: leftDrawerVC)
        
        pushPresentAnimation()
        present(vc, animated: false, completion: nil)
        
        
    }
    

    
    func logoutTapDetected(_ sender: UITapGestureRecognizer) {
        
        let manager = FBSDKLoginManager()
        manager.logOut()
        slideMenuController()?.closeLeft()
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        var parameters = Parameters()
        
        if let userToken = UserDefaults.standard.string(forKey: "userToken"){
            parameters = ["userID": userID! , "userToken" : userToken, "device" : "iOS" ]

        }else
        {
            parameters  = ["userID": userID! , "userToken" : "11", "device" : "iOS"]
        }

        apiCallingMethod(apiEndPints: "user/logout", method: .post, parameters: parameters) { (data1:Any?, totalObjects:Int?) in
            
            try! Auth.auth().signOut()

            UserDefaults.standard.removeObject(forKey: "userID")
            UserDefaults.standard.removeObject(forKey: "userToken")
            UserDefaults.standard.removeObject(forKey: "imageUrl")
            UserDefaults.standard.removeObject(forKey: "name")
            UserDefaults.standard.removeObject(forKey: "coverUrl")
            UserDefaults.standard.removeObject(forKey: "eventID")
            UserDefaults.standard.removeObject(forKey: "invitationID")
            UserDefaults.standard.removeObject(forKey: "commentID")
            UserDefaults.standard.removeObject(forKey: "localSocialAccount")
            UserDefaults.standard.set(false, forKey: "isRegister")
            
            UserDefaults.standard.removeObject(forKey: "imageID")
            UserDefaults.standard.removeObject(forKey: "name")
            UserDefaults.standard.removeObject(forKey: "email")
            UserDefaults.standard.removeObject(forKey: "userAvatar")
            UserDefaults.standard.removeObject(forKey: "imageUrl")
            UserDefaults.standard.removeObject(forKey: "dob")


            DispatchQueue.main.async {
                let storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                
                let vc:NewOtpScreen =  (storyboard.instantiateViewController(withIdentifier: "NewOtpScreen") as? NewOtpScreen)!

                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = vc
                
                appDelegate.window?.rootViewController? .dismiss(animated: false, completion: nil)
            
            }
            
        }
        
    }
    
}
