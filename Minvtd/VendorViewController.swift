//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//


import UIKit
import SlideMenuControllerSwift
import Alamofire
import AlamofireImage

class VendorViewController: UIViewController {
    
    var vendors: [MInvitVendor] = [MInvitVendor]()
    
    @objc let placeholderImage = UIImage(named: "eventCoverImage")!
 
    fileprivate let sectionInsets = UIEdgeInsets(top: 5.0, left: 5.0, bottom: 5.0, right: 5.0)
    fileprivate let itemsPerRow: CGFloat = 2
    var titleName : String = ""

    @IBOutlet weak var vendorsCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.navigationItem.title = titleName
        
        vendorsCollectionView.delegate = self
        vendorsCollectionView.dataSource = self
    
        navigationBarSetup(nil)
        
        let button1 = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(back))
        
        self.navigationItem.leftBarButtonItem = button1

        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        vendor()
        
    }
    
    @objc  func back(_ sender: Any) {
        pushpopAnimation()
        dismiss(animated: false, completion: nil)
    }
    
    @objc func vendor() {
        
        let params = [
            "invitationID": UserDefaults.standard.string(forKey: "invitationID")!,
            "type": UserDefaults.standard.string(forKey: "category"),
            "category": UserDefaults.standard.string(forKey: "iType")
        ] as [String : AnyObject]
        //print("-----ParamsVendor-----\(params)")
        
        apiCallingMethod(apiEndPints: "vendor/categories", method: .get, parameters: params ) { (data1:Any?, objcount:Int?) in
            
            if let data = data1 as? [[String : Any]]
            {
                let vendors = MInvitVendor.vendorsFromResults(data as [[String : AnyObject]])
                self.vendors = vendors
                DispatchQueue.main.async {
                    self.vendorsCollectionView.reloadData()
                }
            }
        }
    }
}

// MARK: - InvitationViewController: UICollectionViewDelegate, UICollectionViewDataSource

extension VendorViewController : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let vendor = vendors[(indexPath as NSIndexPath).row]
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "listingVC") as! VendorsListingViewController
        //print(vendor)
        //print(vendor.categoryID)
        mainVC.categoryID = vendor.categoryID!
        
        mainVC.titleName = vendor.name!
        self.navigationController?.pushViewController(mainVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let vendor = vendors[(indexPath as NSIndexPath).row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "vendorCell", for: indexPath) as! VendorsCollectionViewCell
        
        cell.title.text = vendor.name
        cell.image.af_setImage(
            withURL: URL(string: vendor.thumbnailImage!)! ,
            placeholderImage: placeholderImage
        )
        cell.image.contentMode = .scaleAspectFill
        cell.image.clipsToBounds = true
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return vendors.count
    }
  
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}
