//  FeedFooterComment.swift
//  MinvitdFeeds
//  Created by admin on 10/11/17.
//  Copyright © 2017 Sachin. All rights reserved.

import UIKit
import Alamofire

class FeedFooterComment: UIView {
    
    var VC:AllFeedViewController?
    var subCommentEdit: Bool = false
    var feed:MInvtdAllFeed?
    
    var subCommentCasted = MInvtdAllFeed()
    
    var subComment:MInvtdAllFeedSubComment?{
        didSet{
            //print("^^^^^^Test^^^^^^")
            
            if let imgUrl = subComment?.userImageUrl{
                profileImg.af_setImage(withURL: URL(string:imgUrl)!)
            }
            
            let fullString = NSMutableAttributedString(string: "")
            let image1Attachment = NSTextAttachment()
            image1Attachment.image = UIImage(named: "clock-1.png")
            image1Attachment.bounds = CGRect(x: 0, y: 0, width: 13.5, height: 13.5)
            //print(subComment)
            
            let socialTime = subComment?.created?.socialTime()
            
            fullString.append(NSAttributedString(string: " " + socialTime!))
            
            timeLabel.attributedText = fullString

            let subCommentString = NSMutableAttributedString()
            subCommentString.bold((subComment?.userName) ?? "")
                //.normal(" ")
                //.normal((subComment?.created?.socialTime()) ?? "")
                //.normal("\n \n \n").normal((subComment?.comment) ?? "")
            nameLabel.attributedText = subCommentString
            if let mySubComment = subComment?.comment {
                subCommentText.text = "\(String(describing: mySubComment))"
            }
            
            
            if self.subCommentEdit{
                if self.likeButton.tag == 0{
                    self.likeButton.setImage(#imageLiteral(resourceName: "like").withRenderingMode(.alwaysOriginal), for: .normal)
                    self.subCommentEdit = false
                }else if self.likeButton.tag == 1{
                    //print("This like label text in tag = 1\(self.likeLabel.text)")
                    self.likeButton.setImage(#imageLiteral(resourceName: "likeFilled").withRenderingMode(.alwaysOriginal), for: .normal)
                    self.subCommentEdit = false
                }
            }else{
                self.likeLabel.text = " " + (subComment?.likes?.description)!
                self.likeButton.setImage( (subComment?.liked!)! ?  #imageLiteral(resourceName: "likeFilled") .withRenderingMode(.alwaysOriginal):  #imageLiteral(resourceName: "like").withRenderingMode(.alwaysOriginal) , for: .normal)
                self.commentLabel.text = " " + (subComment?.subCommentsCount?.description)!
            }
           
        }
    }
    
    let profileImg = { () -> UIImageView in
        let imgView = UIImageView()
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.backgroundColor = .purple
        imgView.contentMode = .scaleAspectFill
        imgView.layer.cornerRadius = 20
        imgView.clipsToBounds = true
        return imgView
    }()
    
    let clockIcon = { () -> UIImageView in
        let img = UIImageView()
        img.image = #imageLiteral(resourceName: "clock").withRenderingMode(.alwaysOriginal)
        img.clipsToBounds = true
        return img
    }()
    
    let timeLabel = { () -> UILabel in
        let li = UILabel()
        li.text = "7 min ago"
        li.textColor = UIColor.gray
        li.font = UIFont(name: "GothamRounded-Book", size: 12)
        li.translatesAutoresizingMaskIntoConstraints = false
        return li
    }()
    
    let subCommentText = { () -> UILabel in
        let li = UILabel()
        li.text = "This is a subcomment"
        li.textColor = UIColor.black
        li.textAlignment = .left
        li.numberOfLines = 0
        li.font = UIFont(name: "GothamRounded-Book", size: 12)
        li.translatesAutoresizingMaskIntoConstraints = false
        li.sizeToFit()
        return li
    }()
    
    let nameLabel = { () -> UILabel in
        let li = UILabel()
        li.text = "Sachin Yadav Hello there this my first  comment text"
        li.textAlignment = .left
        li.numberOfLines = 0
//      li.font = UIFont.systemFont(ofSize: 15)
        li.font = UIFont(name: "GothamRounded-Book", size: 12)
        li.translatesAutoresizingMaskIntoConstraints = false
        return li
    }()
    
    //FeedFooter Comment Like Addition :
    
    let commentButton = { () -> UIButton in
        let btn = UIButton(type: .system)
        btn.setImage(#imageLiteral(resourceName: "msg").withRenderingMode(.alwaysOriginal), for: .normal)
        //btn.tintColor = UIColor.init(hexString: "#9d9d9d")
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        return btn
    }()
    
    let commentLabel = { () -> UILabel in
        let lab = UILabel()
        lab.text = "234"
        lab.textColor = UIColor.init(hexString: "#9d9d9d")
        lab.font = UIFont(name: "GothamRounded-Book", size: 12)
        return lab
    }()
    
    let commentLblview: UIView = {
        let temp = UIView()
        return temp
    }()
    
    let commentBtnView = { () -> UIView in
        let btnView = UIView()
        return btnView
    }()
    
    private lazy var commentStack = { () -> UIStackView in
        let base = UIStackView(arrangedSubviews: [commentBtnView,commentLblview])
        base.axis = .horizontal
        base.distribution = .fillEqually
        base.spacing = 0
        return base
    }()
    
    let likeBtnView = { () -> UIView in
        let lab = UIView()
        return lab
    }()
    
    let likeButton = { () -> UIButton in
        let btn = UIButton(type: .system)
        let colour1 = UserDefaults.standard.string(forKey: "colour1")
//      btn.tintColor = UIColor.init(hexString: colour1!)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let likeLblview: UIView = {
        let temp = UIView()
        return temp
    }()
    
    let likeLabel = { () -> UILabel in
        let lab = UILabel()
        lab.text = "234"
        lab.textColor = UIColor.init(hexString: "#9d9d9d")
        lab.font = UIFont(name: "GothamRounded-Book", size: 12)
        return lab
    }()
    
    private lazy var likeStack = { () -> UIStackView in
        let base = UIStackView(arrangedSubviews: [likeBtnView,likeLblview])
        base.axis = .horizontal
        base.distribution = .fillEqually
        base.spacing = 0
        return base
    }()
    
    private lazy var baseStack = { () -> UIStackView in
//      let base = UIStackView(arrangedSubviews: [commentButton,likeButton,collectionImg,extraLikes])
        let base = UIStackView(arrangedSubviews: [likeStack,commentStack])
        base.axis = .horizontal
        base.distribution = .fillEqually
        return base
    }()
    
    @objc func likeACell(){
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        let userName = UserDefaults.standard.string(forKey: "name")!
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")!
        let iName = UserDefaults.standard.string(forKey: "invitationTitle")
        
        let parameters: Parameters = ["commentID" : (subComment?.postID!)! as Any , "userID" : userID as AnyObject,"userName":userName,"invitationID":invitationID, "postID" : (subComment?.postID!)! as Any, "type": (subComment?.type!)!, "likeID": (subComment?.likeID!)!,"iName": iName!,"eventID": (subComment?.eventID!)!]

        apiCallingMethod(apiEndPints: "like", method: .post, parameters: parameters) { (data1:Any?, totalobj:Int?) in

            if data1 != nil{
                //print("here is the data \(String(describing: data1))")
            }
        }

        if self.likeButton.image(for: .normal) == #imageLiteral(resourceName: "likeFilled").withRenderingMode(.alwaysOriginal){
            self.likeButton.setImage(#imageLiteral(resourceName: "like").withRenderingMode(.alwaysOriginal), for: .normal)
            self.likeButton.tag = 0
            self.subCommentEdit = true
            subComment?.likes = (subComment?.likes)! - 1
            self.likeLabel.text = "\((subComment?.likes)!)"

        }else{
            self.likeButton.setImage(#imageLiteral(resourceName: "likeFilled").withRenderingMode(.alwaysOriginal), for: .normal)
            self.likeButton.tag = 1
            self.subCommentEdit = true
            subComment?.likes = (subComment?.likes)! + 1
            self.likeLabel.text = "\((subComment?.likes)!)"
        }
    }
    
    func likeCommentTargetSetup(){
        self.likeButton.addTarget(self, action: #selector(self.likeACell), for: .touchUpInside)
        self.commentButton.addTarget(self, action: #selector(self.infoClick), for: .touchUpInside)
    }
    
    @objc func infoClick()  {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        //print(subComment)
        
        subCommentCasted.userID = subComment!.userID
        subCommentCasted.postID = subComment!.postID
        subCommentCasted.comment = subComment!.comment
        subCommentCasted.commentID = subComment!.commentID
        subCommentCasted.created = subComment!.created
        subCommentCasted.date = subComment!.date
        subCommentCasted.description = subComment!.description
        subCommentCasted.eventID = subComment!.eventID
        subCommentCasted.fileThumbnail = subComment!.fileThumbnail
        subCommentCasted.fileType = subComment!.fileType
        subCommentCasted.fileUrl = subComment!.fileUrl
        subCommentCasted.invitationID = subComment!.invitationID
        subCommentCasted.likes = subComment!.likes
        subCommentCasted.liked = subComment!.liked
        subCommentCasted.likeID = subComment!.likeID
        subCommentCasted.subCommentArray = subComment!.subCommentArray
        subCommentCasted.subCommentsCount = subComment!.subCommentsCount
        subCommentCasted.title = subComment!.title
        subCommentCasted.typeName = subComment!.typeName
        subCommentCasted.userImageUrl = subComment!.userImageUrl
        subCommentCasted.userName = subComment!.userName
        
        if subComment?.fileType == "text" || subComment?.fileType == "post"{
            
            //subComment?.parentTypeName == "text" || subComment?.parentTypeName == "post"
            let mainVC = storyBoard.instantiateViewController(withIdentifier: "feedCommentVC") as! FeedCommentVC
            mainVC.allSubComments = [subComment] as? [MInvtdAllFeedSubComment]
            mainVC.origin = "subComment"
            mainVC.paramaters = subCommentCasted
           mainVC.navigationFromSubComment = true
            let currentController = self.getCurrentViewController()
            currentController?.present(mainVC, animated: true, completion: nil)
            
        }else {
            
            let mainVC = storyBoard.instantiateViewController(withIdentifier: "feedImageCommentVC") as! FeedImageCommentVC
            mainVC.allSubComments = [subComment] as? [MInvtdAllFeedSubComment]
            mainVC.origin = "subComment"
            mainVC.navigationFromSubComment = true
            let currentController = self.getCurrentViewController()
            currentController?.present(mainVC, animated: true, completion: nil)
        }
    }
    
    func getCurrentViewController() -> UIViewController? {
        
        if let rootController = UIApplication.shared.keyWindow?.rootViewController {
            var currentController: UIViewController! = rootController
            while( currentController.presentedViewController != nil ) {
                currentController = currentController.presentedViewController
            }
            return currentController
        }
        return nil
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(profileImg)
        addSubview(nameLabel)
        addSubview(timeLabel)
        addSubview(subCommentText)
        addSubview(clockIcon)
        likeBtnView.addSubview(likeButton)
        likeLblview.addSubview(likeLabel)
        commentBtnView.addSubview(commentButton)
        commentLblview.addSubview(commentLabel)
        addSubview(baseStack)
        likeCommentTargetSetup()
        
        
        // like frame here
        likeButton.widthAnchor.constraint(equalToConstant: 18).isActive = true
        likeButton.heightAnchor.constraint(equalToConstant: 18).isActive = true
        likeButton.anchorCenterSuperview()
        likeLabel.feedCommentNumberHeight()
        
        // comment frame here
        commentButton.widthAnchor.constraint(equalToConstant: 18).isActive = true
        commentButton.heightAnchor.constraint(equalToConstant: 18).isActive = true
        commentButton.anchorCenterSuperview()
        commentLabel.feedCommentNumberHeight()
        
        //Changes ended
        
        profileImg.anchor(topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 12, leftConstant: 20, bottomConstant: 0, rightConstant: 0, widthConstant: 40, heightConstant: 40)
        
        nameLabel.leftAnchor.constraint(equalTo: profileImg.rightAnchor, constant: 8).isActive = true
        nameLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: 8).isActive = true
        nameLabel.topAnchor.constraint(equalTo: profileImg.topAnchor, constant: 2).isActive = true
        
        clockIcon.anchor(profileImg.centerYAnchor, left: nameLabel.leftAnchor, bottom: nil, right: nil, topConstant: 2, leftConstant: -2, bottomConstant: 0, rightConstant: 5, widthConstant: 14, heightConstant: 14)
        clockIcon.center.y = timeLabel.center.y
        
        timeLabel.anchor(profileImg.centerYAnchor, left: clockIcon.rightAnchor, bottom: nil, topConstant: 4, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        subCommentText.anchor(profileImg.bottomAnchor, left: profileImg.leftAnchor, bottom: nil, right: rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 0)
        
        baseStack.anchor(nil, left: leftAnchor, bottom: bottomAnchor, right: nil, topConstant: 20, leftConstant: 15, bottomConstant: 15, rightConstant: 0, widthConstant: UIScreen.main.bounds.width / 2, heightConstant: 25)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func apiCallingMethod(apiEndPints:String,method: HTTPMethod, parameters: Parameters?,complition:@escaping (_ data:Any?,_ totalObjects:Int?)->() ){
        
        let url = MInvitdClient.Constants.BaseUrl + apiEndPints
        var prams = parameters
        let colour1 = UserDefaults.standard.string(forKey: "colour1")
        let colour2 = UserDefaults.standard.string(forKey: "colour2")
        
        if let colour = colour1 {
            prams!["colour"] = colour
        }
        if let colour = colour2 {
            prams!["colour_2"] = colour
        }
        
       let authHeader = ["Authorization" : UserDefaults.standard.string(forKey: "userToken")!, "Content-Type" : "application/json" ]
        
        Alamofire.request(url, method: method, parameters: prams, encoding: JSONEncoding.default, headers: authHeader).responseJSON { (response) in
            //print("this is response ->\(response)")
            //debugPrint(response)
            if response.error == nil {
                self.apiHelperMethod(JSONData : response.data!, complition: {
                    data, totalObjects, myString
                    in
                    complition(data, totalObjects)
                })
            }
        }
    }
    
    fileprivate func  apiHelperMethod(JSONData : Data,complition:(_ data:Any?,_ totalObjects:Int?,_ Message:String?)->()) {
        
        var totalObjects:Int?
        var serverData:Any?
        var message:String?
        do{
            let json = try JSONSerialization.jsonObject(with: JSONData, options: .allowFragments)
            
            if let dictionary = json as? [String : Any] {
                let totalObjects1 = dictionary["totalObjects"] as? Int
                
                totalObjects = totalObjects1
                
                if totalObjects == 0 {
                    complition(serverData, totalObjects,nil)
                    return
                }
                
                if let status = dictionary["status"] as? String
                {
                    
                    if status == "success"
                    {
                        let data = dictionary["data"]
                        serverData = data
                        
                        if let mess = (dictionary["messages"] as? [[String : String]])                             {
                            if let messdict = mess.first {
                                message = messdict["message"] ?? ""
                            }}
                        complition(serverData,totalObjects,message)
                        return
                        
                    }else
                    {
                        if let mess = (dictionary["messages"] as? [[String : String]])                             {
                            if let messdict = mess.first {
                                let message = messdict["message"] ?? "Error status returned from server"
                                //print("\(message)")
                            }
                        }
                    }
                }
            }
        }
        catch{
        }
    }
}
