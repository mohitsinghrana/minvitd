//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//


import UIKit
import Alamofire
import Contacts
import AlamofireImage
import UserNotifications
import Contacts

class EventsGuestViewController : UIViewController {
    
    var guests: [MInvitdGuest] = [MInvitdGuest]()
    var filterData = [MInvitdGuest]()
    
    var searchBar:UISearchBar = {
        let srch = UISearchBar()
        srch.placeholder = "Search"
        
        return srch
    }()
    
    var isSearching:Bool = false
    
    let center = UNUserNotificationCenter.current()

    var dotMenuShown: Bool?
    
    @objc var ownerID : Int = 0
    @IBOutlet weak var guestsCollectionView: UITableView!
    
    func setupDotAlert(){
        let alert = UIAlertController(title: "Guest Status", message: nil, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Got It!", style: .destructive) { (action) -> Void in
        }
        
        let lineView1: UIView = {
            let view = UIView()
            view.frame = CGRect(x: 0, y: 59, width: 315, height: 0.5)
            view.backgroundColor = UIColor.black
            view.alpha = 0.1
            return view
        }()
        
        let lineView2: UIView = {
            let view = UIView()
            view.frame = CGRect(x: 0, y: 109, width: 315, height: 0.5)
            view.backgroundColor = UIColor.black
            view.alpha = 0.1
            return view
        }()
        
        let lineView3: UIView = {
            let view = UIView()
            view.frame = CGRect(x: 0, y: 159, width: 315, height: 0.5)
            view.backgroundColor = UIColor.black
            view.alpha = 0.1
            return view
        }()
        
        let imgView1: UIImageView = {
           let img = UIImageView()
            img.backgroundColor = UIColor.green
            img.frame = CGRect(x: 20, y: 70, width: 24, height: 24)
            img.layer.cornerRadius = 12
            return img
        }()
        
        let acceptedLabel: UILabel = {
           let lbl = UILabel()
            lbl.text = "Accepted"
            lbl.frame = CGRect(x: 100, y: 73, width: 100, height: 15)
            return lbl
        }()
        
        let imgView2: UIImageView = {
            let img = UIImageView()
            img.backgroundColor = UIColor.yellow
            img.frame = CGRect(x: 20, y: 120, width: 24, height: 24)
            img.layer.cornerRadius = 12
            return img
        }()
        
        let maybeLabel: UILabel = {
            let lbl = UILabel()
            lbl.text = "Maybe"
            lbl.frame = CGRect(x: 100, y: 123, width: 100, height: 15)
            return lbl
        }()
        
        let imgView3: UIImageView = {
            let img = UIImageView()
            img.backgroundColor = UIColor.red
            img.frame = CGRect(x: 20, y: 170, width: 24, height: 24)
            img.layer.cornerRadius = 12
            return img
        }()
        
        let rejectedLabel: UILabel = {
            let lbl = UILabel()
            lbl.text = "Rejected"
            lbl.frame = CGRect(x: 100, y: 173, width: 100, height: 15)
            return lbl
        }()
        
        alert.view.addSubview(imgView1)
        alert.view.addSubview(acceptedLabel)
        alert.view.addSubview(imgView2)
        alert.view.addSubview(maybeLabel)
        alert.view.addSubview(imgView3)
        alert.view.addSubview(rejectedLabel)
        alert.view.addSubview(lineView1)
        alert.view.addSubview(lineView2)
        alert.view.addSubview(lineView3)
        //alert.view.backgroundColor = UIColor(red: 255, green: 52, blue: 101, alpha: 1)
        
        var height:NSLayoutConstraint = NSLayoutConstraint(item: alert.view, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 255)
        
        alert.view.addConstraint(height);
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guestsCollectionView.delegate = self
        guestsCollectionView.dataSource = self
        navigationBarSetup(nil)
        title = "Guests"
        
        if let myBool = UserDefaults.standard.object(forKey: "dotMenuShown"){
            dotMenuShown = myBool as! Bool
        }else{
            dotMenuShown = true
            UserDefaults.standard.set(dotMenuShown, forKey: "dotMenuShown")
         //   setupDotAlert()
        }
        setupNavBarContent()
    }
    
    func setupNavBarContent(){
        
        
        let button1 = UIBarButtonItem(barButtonSystemItem: .stop,target: self, action: #selector(cancelButton(_:)))
        self.navigationItem.leftBarButtonItem = button1
        
        let button2 = UIBarButtonItem(image: #imageLiteral(resourceName: "groupFilled"), style: .plain, target: self, action: #selector(addGuest))
        self.navigationItem.rightBarButtonItem = button2
    }
    
    func presentSettingsActionSheet() {
        let alert = UIAlertController(title: "Permission to Contacts", message: "This app needs access to your contacts in order to invite guests", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Go to Settings", style: .default) { _ in
            let url = URL(string: UIApplicationOpenSettingsURLString)!
            UIApplication.shared.open(url)
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        present(alert, animated: true)
    }
    
    @objc func addGuest(_ sender: UIBarButtonItem) {
        
        //Sachin code
        let store = CNContactStore()
        
        store.requestAccess(for: .contacts) { (isGranted, err) in
            
            if let  error = err{
                let status = CNContactStore.authorizationStatus(for: .contacts)
                if status == .denied || status == .restricted {
                    self.presentSettingsActionSheet()
                    return
                }
            }
            
            if isGranted {
                
                    let contactStore = CNContactStore()
                    var guests = [MInvitdGuest]()
                    let keysToFetch = [
                        CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
                        CNContactEmailAddressesKey,
                        CNContactPhoneNumbersKey] as [Any]
                    
                    let request = CNContactFetchRequest(keysToFetch:keysToFetch as! [CNKeyDescriptor])
                    
                    do {
                        try contactStore.enumerateContacts(with: request, usingBlock: { (contact:CNContact, stop:UnsafeMutablePointer<ObjCBool>) -> Void in
                            // contacts
                            
                        for each in contact.phoneNumbers{
                                    
                                    var minvitedGuest = MInvitdGuest(name: contact.givenName + " " + contact.familyName, phone: each.value.stringValue, email: nil)
                                    
                                    if let email = contact.emailAddresses.first?.value {
                                        minvitedGuest.email = email as String
                                    }
                                      guests.append(minvitedGuest)
                                }
                        })
                    }catch {
                        //catch
                    }

                let vc = ConfirmationGuestInvitation()
                vc.guestArray = guests
                self.present( UINavigationController(rootViewController: vc), animated: true, completion: nil)
                

            }
        }
    }
    
    
    @objc func cancelButton(_ sender: UIBarButtonItem) {
        
        self.dismiss(animated: true, completion: nil)
    }

    
    
    @objc public func imageLayerForGradientBackgroundSmall() -> UIImage {
        
        let updatedFrame = CGRect(x: 0, y: 0, width: 40, height: 40)
        // take into account the status bar
        let layer = CAGradientLayer.gradientLayerForBounds(bounds: updatedFrame)
        UIGraphicsBeginImageContext(layer.bounds.size)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }


    
    
    @objc func getContacts(){
        
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")
        let eventID = UserDefaults.standard.string(forKey: "eventID")
        let authHeader = ["Authorization" : UserDefaults.standard.string(forKey: "userToken")!, "Content-Type" : "application/json" ]
        
        let url = "\(MInvitdClient.Constants.BaseUrl)guests?invitationID=\(invitationID!)&eventID=\(eventID!)"
        let params = [ "test" : 1, "hello" : 2 ]
        
        Alamofire.request( url, parameters: params, encoding: JSONEncoding.default, headers: authHeader).responseJSON { (response) in
            
            if response.error == nil {
                self.actionResult(JSONData : response.data!)
            }else{
                let alert = UIAlertController(title: "Couldn't connect to server", message: "Check Internet", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: { (action : UIAlertAction) in
                    alert.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    
    @objc func changeStatus(guestID : String , status : String , index : Int){
        
        //delete from array
        if status  == "-1"{
            self.guests.remove(at: index)
            // delete the table view row
            let indexPath = IndexPath(row: index, section: 0)
            guestsCollectionView.deleteRows(at: [indexPath], with: .fade)
            

        }
        let parameters: Parameters = ["guestID" : guestID as AnyObject, "status" : status as AnyObject]
        
        let url = MInvitdClient.Constants.BaseUrl + "guest/status"
        
        let authHeader = ["Authorization" : UserDefaults.standard.string(forKey: "userToken")!, "Content-Type" : "application/json" ]
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: authHeader).responseJSON { (response) in
    
            if response.data != nil{
                
                self.getContacts()
            }else{
                
                let alert = UIAlertController(title: "Couldn't connect to server", message: "Check Internet", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: { (action : UIAlertAction) in
                    alert.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
                
            }
        }
        

    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        searchBar.frame =  CGRect(x: 0, y: 0, width: view.bounds.width - 44.0, height: 44.0)
        searchBar.delegate = self
        guestsCollectionView.tableHeaderView = searchBar
        self.guestsCollectionView.setContentOffset(CGPoint(x: 0, y: 44), animated: true)
        self.getContacts()

    }
    
    @objc func actionResult(JSONData : Data)
    {
        do{
            let json = try JSONSerialization.jsonObject(with: JSONData, options: [])
            
         if let dictionary = json as? [String : Any] {
                
                if let status = dictionary["status"] as? String
                {
                    
                    let totalObjects = dictionary["totalObjects"] as! Int
                    if totalObjects == 0
                    {
                        let alert = UIAlertController(title: "MInvitd", message: "You have no guests", preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: { (action : UIAlertAction) in
                            alert.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                        return
                    }
                    
                    if status == "success"
                    {
                        if let data = dictionary["data"] as? [[String : Any]]
                        {
                            let guests = MInvitdGuest.guestsFromResults(data as [[String : AnyObject]])
                            self.guests = guests
                            print("\(guests)")
                            DispatchQueue.main.async {
                                self.guestsCollectionView.reloadData()
                            }
                        }
                    }else
                    {
                         self.dismissAlert("Error returned by server")
                    }
                }
            }
        }
        catch{}
    }
    
    
    @objc func submitFunc( guests: [[String:String]] )  {
        
      
        let eventID = UserDefaults.standard.string(forKey: "eventID")
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")
        let userToken = UserDefaults.standard.string(forKey: "userToken")
        
        let parameters2 = ["guests" : guests as AnyObject , "invitationID" : invitationID!  as AnyObject , "eventID" : eventID as AnyObject]
        
        let url = MInvitdClient.Constants.BaseUrl + "guests"
        
        // start
        
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "POST"
        
        request.addValue( userToken!, forHTTPHeaderField: "Authorization" )
        request.addValue( "application/json", forHTTPHeaderField: "Content-Type" )
        
        //        let postString = "ID=13&name=Jack"

        request.httpBody = try?  JSONSerialization.data(withJSONObject: parameters2, options: .prettyPrinted)
        
        // parameters2
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                 // this is a check for fundamental networking error
                return
            }
            self.actionResult2(JSONData: data)
        }
        task.resume()
        
    }
    
    @objc func actionResult2(JSONData : Data)
    {
        
        
        do{
            let json = try JSONSerialization.jsonObject(with: JSONData, options: [])
            
            if let dictionary = json as? [String : Any] {
                
                
                if let status = dictionary["status"] as? String
                {
                    if status == "success"
                    {
                        if let data = dictionary["data"] as? [[String : Any]]
                        {
                            
                            self.getContacts()
                            
                            DispatchQueue.main.async {
                                
                                let alert = UIAlertController(title: "MInvitd", message: "Guest Added" , preferredStyle: .alert)
                                
                                alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: { (action : UIAlertAction) in
                                    
                                    alert.dismiss( animated: true, completion: nil)
                                }))
                                self.present(alert, animated: true, completion: nil)
                                
                            }
                        }
                    }else
                    {
                        if let messages = dictionary["messages"] as? [[String : Any]]
                        {
                            let message = messages[0]["message"] as? String
                            
                            DispatchQueue.main.async {
                                
                                let alert = UIAlertController(title: "Alert", message: message , preferredStyle: .alert)
                                
                                alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: { (action : UIAlertAction) in
                                    
                                    alert.dismiss( animated: true, completion: nil)
                                }))
                                self.present(alert, animated: true, completion: nil)
                                
                                
                            }
                        }
                    }
                }
            }
            
        }
        catch{
            
            let alert = UIAlertController(title: "Error", message: "Could not connect to server", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: { (action : UIAlertAction) in
                
                alert.dismiss( animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
    }

    
}

// MARK: - EventsGuestViewController: UICollectionViewDelegate, UICollectionViewDataSource

extension EventsGuestViewController : UITableViewDelegate, UITableViewDataSource {
    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return CGFloat(60)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isSearching ? filterData.count : guests.count

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let guest = isSearching ? filterData[indexPath.row] : guests[indexPath.row]
        print("\(guest)")

        let cell = guestsCollectionView.dequeueReusableCell(withIdentifier: "guestEventListCell", for: indexPath) as! GuestsCollectionViewCell
        
        cell.nameLabel.text = guest.name
        var firstLetter : String?
        if let firstChar = guest.name?.characters.first {
            
            firstLetter = String(describing: firstChar)
            
        }else {
            firstLetter = "?"
        }
        
        cell.userImageView.layer.cornerRadius = cell.userImageView.frame.size.width / 2
        cell.userImage.layer.cornerRadius = cell.userImage.frame.size.width / 2
        
        
        if let imgurl = guest.imageUrl{
            cell.imageLabel.isHidden = true
            cell.userImageView.isHidden = false
            self.downloadImageFrom(urlString: imgurl) { (data) in
                guard let image: UIImage = UIImage(data: data!)else{return}
                DispatchQueue.main.async {
                    cell.userImage.image = image
                }
            }
        }else{
            cell.imageLabel.isHidden = false
            cell.userImageView.isHidden = true
            cell.imageLabel.text = firstLetter
            cell.imageLabel.layer.cornerRadius  = cell.imageLabel.frame.size.width/2
            cell.imageLabel.layer.masksToBounds = true
            cell.imageLabel.clipsToBounds = true
        }
        
        //print(guest.statusName!)
        cell.statusName.text = guest.statusName!
       
        //print(guest)
        
        let bgImage : UIImage = self.imageLayerForGradientBackgroundSmall()
        cell.imageLabel.backgroundColor = UIColor(patternImage: bgImage)
        
        cell.inviteView.backgroundColor = UIColor.init(hexString: guest.statusColour!)
        cell.inviteView.layer.cornerRadius = cell.inviteView.bounds.size.width/2
        return cell
    }
    
    
     func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     
        let ifAdmin = UserDefaults.standard.string(forKey: "userRole")
        if ifAdmin == "admin"{
            return true
        }
        
        return false
        
     }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "GuestProfileVC") as? GuestProfileVC
        vc?.data = isSearching ? filterData[indexPath.row] : guests[indexPath.row]
        //print("^^^^\(guests[indexPath.row])")
        self.present(UINavigationController(rootViewController:vc!), animated: true, completion: nil)
    }
    
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let guest = isSearching ? filterData[indexPath.row] : guests[indexPath.row]

        let editAction = UITableViewRowAction(style: .normal, title: "Arrived") { (rowAction, indexPath) in
            //TODO: edit the row at indexPath here
            let guestID = String(describing : guest.guestID!)
            self.changeStatus(guestID: guestID , status: "4" , index : indexPath.row)
        }
        editAction.backgroundColor = UIColor.init(hexString: "#2C7EC3")
        
        let deleteAction = UITableViewRowAction(style: .normal, title: "Delete") { (rowAction, indexPath) in
            //TODO: Delete the row at indexPath here
            let guestID = String(describing : guest.guestID!)
            self.changeStatus(guestID: guestID , status: "-1" , index : indexPath.row)
        }
        deleteAction.backgroundColor = UIColor.init(hexString: "#DA4C45")
        
        return [editAction,deleteAction]
    }
    
    
    func downloadImageFrom(urlString:String, completion:@escaping(Data?)->()) {
        guard let url = URL(string:urlString) else { return }
        let request = URLRequest(url: url)
        URLSession.shared.dataTask(with: request) { (data, _, err) in
            if err != nil {
                // handle error if any
            }
            if let data = data{
                completion(data)
            }
            
            }.resume()
    }
    
}



extension EventsGuestViewController:UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
        // tableView.reloadData()
        self.guestsCollectionView.setContentOffset(CGPoint(x: 0, y: 44), animated: true)
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
       searchBar.text = ""
        searchBar.resignFirstResponder()
       searchBar.setShowsCancelButton(false, animated: true)
      //  isSearching = false
        guestsCollectionView.reloadData()
        self.guestsCollectionView.setContentOffset(CGPoint(x: 0, y: 44), animated: true)
        
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        //reloading here
        if searchBar.text == nil || searchBar.text == ""{
            isSearching = false
            searchBar.setShowsCancelButton(false, animated: true)
            view.endEditing(true)
            guestsCollectionView.reloadData()
            self.guestsCollectionView.setContentOffset(CGPoint(x: 0, y: 44), animated: true)
            
            return
            
        }else {
            isSearching = true
            filterData = guests.filter({return ($0.name?.lowercased().contains(searchText.lowercased()))!})
            guestsCollectionView.reloadData()
            
        }
        
    }
}
