//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//


import UIKit
import AlamofireImage
import Alamofire
import UITextView_Placeholder
class ContactUsViewController: UIViewController {

    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var subjectTF: UITextField!
    @IBOutlet weak var messageTF: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Contact Us"
        navigationBarSetup(nil)
        self.messageTF.placeholder = "Write a message..."
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dissmisskeyboard)))
        //messageTF.returnKeyType = .send
        setupNavBarContent()
    }
    func setupNavBarContent(){
        let button1 = UIBarButtonItem(image: #imageLiteral(resourceName: "menu"), style: .plain, target: self, action: #selector(menu(_:)))
        self.navigationItem.leftBarButtonItem = button1
    }

    @IBAction func submitBtnClicked(_ sender: UIButton) {
        
        if validate()
        {
            let userID = UserDefaults.standard.string(forKey: "userID")
            let strSubject =  self.subjectTF.text!
            let strMessgae =  self.messageTF.text!
            self.submitFunc(message: strMessgae, subject : strSubject, userID: userID!)
        }
    }
    
    @objc func dissmisskeyboard()
    {
        self.view.endEditing(true)
    }
    
    @IBAction func menu(_ sender: Any) {
        slideMenuController()?.openLeft()
    }
   
    @objc func backBtn(_ sender: UIBarButtonItem) {
        pushpopAnimation()
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func validate() -> Bool
    {
        guard let name = self.subjectTF.text, !name.isEmpty else {
            self.subjectTF.placeholder = "Please enter a subject"
            return false
        }
        guard let email = self.messageTF.text, !email.isEmpty else {
            self.messageTF.text = "Please enter a message"
            return false
        }
        
        return true
    }
    
    @objc func submitFunc(message : String , subject : String, userID : String )  {
        
        let parameters: Parameters = ["message" : message as AnyObject , "subject" : subject  as AnyObject , "userID" : userID as AnyObject]
        
        apiCallingMethod(apiEndPints: "contact/message", method: .post, parameters: parameters) { (data1:Any?, totalObj:Int?) in
            
            if let data = data1 as? [[String : Any]]
            {
                
                DispatchQueue.main.async {
                    
                    let alert = UIAlertController(title: "Minvitd", message: "Your message has been sent successfully." , preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: { (action : UIAlertAction) in
                        
                        alert.dismiss( animated: true, completion: nil)
                        
                        self.dismiss( animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
}
