//
//  ManageGuestListVc.swift
//  Minvtd
//
//  Created by admin on 22/09/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit

class ManageGuestListVc : UIViewController {
    
    var guests: [MInvitdGuest] = [MInvitdGuest]()
    
    @objc var ownerID : Int = 0
    @IBOutlet weak var guestsCollectionView: UITableView!
    var selectionflag = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guestsCollectionView.delegate = self
        guestsCollectionView.dataSource = self
        guestsCollectionView.allowsSelection = false
        guestsCollectionView.tableFooterView = UIView()
        
    }
    
    @objc public func imageLayerForGradientBackgroundSmall() -> UIImage {
        
        let updatedFrame = CGRect(x: 0, y: 0, width: 40, height: 40)
        // take into account the status bar
        let layer = CAGradientLayer.gradientLayerForBounds(bounds: updatedFrame)
        UIGraphicsBeginImageContext(layer.bounds.size)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
}


extension ManageGuestListVc : UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return CGFloat(60)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return guests.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let guest = guests[(indexPath as NSIndexPath).row]
        let cell = guestsCollectionView.dequeueReusableCell(withIdentifier: "guestEventListCell", for: indexPath) as! GuestsCollectionViewCell
        
        
        cell.nameLabel.text = guest.name
        var firstLetter : String?
        if let firstChar = guest.name?.characters.first {
            
            firstLetter = String(describing: firstChar)
            
        }else {
            firstLetter = "?"
        }
        cell.imageLabel.text = firstLetter
        cell.imageLabel.layer.cornerRadius  = cell.imageLabel.frame.size.width/2
        cell.imageLabel.layer.masksToBounds = true
        cell.imageLabel.clipsToBounds = true
        cell.emailLabel.text = guest.statusName
        
        let bgImage : UIImage = self.imageLayerForGradientBackgroundSmall()
        cell.imageLabel.backgroundColor = UIColor(patternImage: bgImage)
        
        cell.inviteView.backgroundColor = UIColor.init(hexString: guest.statusColour!)
        
//        if guest.status == 0 {
//            cell.inviteView.backgroundColor = UIColor.init(hexString: "#F18037")
//        }else if guest.status == 1 {
//            cell.inviteView.backgroundColor = UIColor.init(hexString: "#61BCA7")
//            
//        }else if guest.status == 2{
//            cell.inviteView.backgroundColor = UIColor.init(hexString: "#DA4C45")
//        }else{
//            cell.inviteView.backgroundColor = UIColor.init(hexString: "#2C7EC3")
//        }
        cell.inviteView.layer.cornerRadius = cell.inviteView.bounds.size.width/2
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}








