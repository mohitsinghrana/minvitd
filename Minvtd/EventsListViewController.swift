//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//


import Alamofire
import AlamofireImage
import UIKit

class EventsListViewController : UIViewController{
    
    var events : [MInvitdEvent] = [MInvitdEvent]()
    
    @IBOutlet weak var navigationBar: UINavigationBar!
    @objc let placeholderImage = UIImage(named: "eventCoverImage")!
    
    @IBOutlet weak var myTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myTableView.delegate = self
        myTableView.dataSource = self
        navigationBarSetup(navigationBar)
        
    }
    
    @IBAction func backBtn(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
       
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getEvents()
    }
    
    @objc func getEvents(){
        
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")!
        
        
        apiCallingMethod(apiEndPints: "events", method: .get, parameters: ["invitationID":invitationID]) { (data1:Any?, totalObjects:Int?) in
            
            if totalObjects == 0
            {
                self.CreateAlertPopUp("Minvited", "You have No Events", OnTap: nil)
                return
            }
            
            if let data = data1 as? [[String : Any]]
            {
                let events = MInvitdEvent.eventsFromResults(data as [[String : AnyObject]])
                self.events = events
                DispatchQueue.main.async {
                    self.myTableView.reloadData()
                }
            }
        }
        
    }
}

extension EventsListViewController: UITableViewDelegate, UITableViewDataSource {
   
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let event = events[(indexPath as NSIndexPath).row]
        let cell = myTableView.dequeueReusableCell(withIdentifier: "eventListCell", for: indexPath) as! EventListTableViewCell
        
        cell.eventName.text = event.name
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        

        let event = events[(indexPath as NSIndexPath).row]
        
        UserDefaults.standard.set(event.eventID, forKey: "eventID")
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "eventScheduleVC") as! EventScheduleViewController
        pushPresentAnimation()
        self.present(mainVC, animated: false, completion: nil)
        
    }

}


