//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//


import UIKit
import Alamofire
import AlamofireImage

class VideosViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var videos: [MInvtdAllFeed] = [MInvtdAllFeed]()
    
    @objc let placeholderImage = UIImage(named: "eventCoverImage")!
    
    @IBOutlet weak var navigationBar: UINavigationBar!
    fileprivate let sectionInsets = UIEdgeInsets(top: 5.0, left: 5.0, bottom: 5.0, right: 1.0)
    fileprivate let itemsPerRow: CGFloat = 2
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        navigationBarSetup(nil)
        title = "Videos"
        let button2 = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissVC))
        self.navigationItem.rightBarButtonItem = button2

    }
    
    
    @objc func dismissVC(_ sender: UIBarButtonItem) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         videosLoad()
    }
    
    @objc func videosLoad(){
        
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")!
        let userID = UserDefaults.standard.string(forKey: "userID")!
        
        apiCallingMethod(apiEndPints: "invitation/files", method: .get, parameters: ["invitationID":invitationID,"fileType":"video","userID":userID]) { (data1:Any?, totalobj:Int?) in
            
            if data1 != nil {
                
                if let data = data1 as? [[String : Any]]
                {
                    let videos = MInvtdAllFeed.allFeedsFromResults(data as [[String : AnyObject]])
                    self.videos = videos
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }
            }
        }

    }
    
   
}


extension VideosViewController : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        let height = widthPerItem
        
        return CGSize(width: widthPerItem, height: height)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let video = videos[(indexPath as NSIndexPath).row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "videoCell", for: indexPath) as! VideoCollectionViewCell
        
        cell.image.af_setImage(
            withURL: URL(string: video.fileThumbnail!)! ,
            placeholderImage: placeholderImage
        )
        
        cell.profileImage.af_setImage(withURL: URL(string: video.userImageUrl!)!)
        
        cell.LikeImage.image = nil
        
        if let liked = video.liked{
            cell.LikeImage.image = liked ?  UIImage(named: "likeFilled") : UIImage(named: "like")
        }
        
        //Shadow Effect
        cell.contentView.layer.cornerRadius = 4.0
        cell.contentView.layer.borderWidth = 1.0
        cell.contentView.layer.borderColor = UIColor.clear.cgColor
        cell.contentView.layer.masksToBounds = false
        cell.layer.shadowColor = UIColor(red: 211/255, green: 211/255, blue: 211/255, alpha: 1.0).cgColor
        cell.layer.shadowOffset = CGSize(width: 4, height: 8)
        cell.layer.shadowRadius = 4.0
        cell.layer.shadowOpacity = 1.0
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: (cell.contentView.layer.cornerRadius)).cgPath
        
        //commentTapped
        let commentTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.commentTap(_:)))
        commentTapGesture.numberOfTapsRequired = 1
        cell.commentBtnView.tag = indexPath.row
        cell.commentBtnView.isUserInteractionEnabled = true
        cell.commentBtnView.addGestureRecognizer(commentTapGesture)
        cell.commentButton.isUserInteractionEnabled = false
        
        //imageTapped
        let imageTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(_:)))
        cell.image.tag = indexPath.row
        cell.image.isUserInteractionEnabled = true
        cell.image.addGestureRecognizer(imageTapGesture)
        cell.image.contentMode = .scaleAspectFill
        cell.image.clipsToBounds = true
        
        //like Label
        if let likeCount = video.likes{
            cell.likeLabel.text = "\(likeCount)"
        }
        //comment Label
        if let commentCount = video.subCommentsCount{
            cell.commentLabel.text = "\(commentCount)"
        }
      
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return videos.count
    }
    
    @objc func imageTapped(_ sender: UITapGestureRecognizer){
        
        let currentIndex = sender.view?.tag
        let video = self.videos[currentIndex!]
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "videoPlayVC") as! VideoPlayViewController
        mainVC.url = video.fileUrl!
        
        self.present(UINavigationController(rootViewController:mainVC), animated: true, completion: nil)
        
    }
    
    @objc func commentTap(_ sender: UITapGestureRecognizer) {
        
        let currentIndex = sender.view?.tag
        let feed  = self.videos[currentIndex!]
        if feed.fileType == "text"{
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let mainVC = storyBoard.instantiateViewController(withIdentifier: "feedCommentVC") as! FeedCommentVC
            mainVC.paramaters = feed
            mainVC.origin = "feed"
            self.present( UINavigationController(rootViewController:mainVC), animated: true, completion: nil)

        }else {
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let mainVC = storyBoard.instantiateViewController(withIdentifier: "feedImageCommentVC") as! FeedImageCommentVC
            mainVC.paramaters = feed
            mainVC.origin = "feed"
            self.present( UINavigationController(rootViewController:mainVC), animated: true, completion: nil)

        }
        
        
    }

    
}


