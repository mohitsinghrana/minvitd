//
//  NewRegister.swift
//  Minvtd
//
//  Created by admin on 07/09/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit
import Alamofire
import SlideMenuControllerSwift

class NewRegister: UIViewController,GIDSignInUIDelegate {

    @IBOutlet weak var fullNameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var dobField: UITextField!
    @IBOutlet weak var genderSegment: UISegmentedControl!
    @IBOutlet weak var DisplayPicture: UIButton!
    @objc var firebaseID = ""
    @objc let facebooklogin = FBSDKLoginManager()
    @objc var defaultGender = 0
    @objc var defaultImgUrl:String?
    @objc var myActivityIndicator:UIActivityIndicatorView?
    @objc var defaultName = ""
    @objc var defaultEmail = ""
    @objc var defaultDob = ""
    @objc var serverdate:String?
    @objc let datepicker1 = UIDatePicker()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Register Account"
        
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.setStatusBarBackgroundColor(color: UIColor(hexString: "#691C2F")!)
        datepicker1.datePickerMode = .date

        disssmissKeyboard()
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        self.navigationController?.navigationBar.barStyle = UIBarStyle.black
        self.navigationController?.navigationBar.tintColor = .white
        dobField.inputView = datepicker1
       
        
        datepicker1.addTarget(self, action: #selector(datePickerChanged(datePicker:)), for: .valueChanged)

        let button1 = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(back))
        
        self.navigationItem.leftBarButtonItem = button1
        let button2 = UIBarButtonItem(title: "Next", style: .plain, target: self, action: #selector(manualSignUP(_:)))
        
        self.navigationItem.rightBarButtonItem = button2

        navigationBarSetup(nil)

    }
    
    
    @objc func back(_ sender: Any) {
        
        
        let vc = UIAlertController(title: "Are you sure?", message: "You have to verify OTP", preferredStyle: .alert)
        vc.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (act) in
            
            
            self.pushpopAnimation()
            let presentingViewController = self.presentingViewController
            self.dismiss(animated: false, completion: {
                presentingViewController!.dismiss(animated: false, completion: {})

            
        })
        
            
        }))
        vc.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
        self.present(vc, animated: true, completion: nil)
        
    }
    
    
    
    

    @IBAction func gendeerPickerAction(_ sender: UISegmentedControl) {
        
        if genderSegment.selectedSegmentIndex == 0 {
            defaultGender = 0
        }else {
            defaultGender = 1

        }
        
    }

    
    @IBAction func DisplayImagePicker(_ sender: Any) {
        let imagePicker = UIImagePickerController()

        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
        
        
    }
    
    
    @IBAction func googleSignInAction(_ sender: UIButton) {
        GIDSignIn.sharedInstance().signIn()

    }
    
    @IBAction func facebookSignIN(_ sender: Any) {
        
        
        facebooklogin.logIn(withReadPermissions: ["email","public_profile"], from: self) {
            (result:FBSDKLoginManagerLoginResult?, error:Error?) in
            if error != nil {
                
                self.dismissAlert(error!.localizedDescription)
            }
            else if (result?.isCancelled)!{
                
            }
                
            else  {
                
                Spinner.sharedinstance.startSpinner(viewController: self)
                if((FBSDKAccessToken.current()) != nil){
                    FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, email,gender"]).start(completionHandler: { (connection, result, error) -> Void in
                        if (error == nil){

                            if let dict = result as? [String:Any]{
                                
                                var prams:Parameters = [:]
                                var id = "'"
                                
                                if let FBid = dict["id"] as? String {
                                    id = FBid
                                    let imageUrl = "https://graph.facebook.com/\(FBid)/picture?type=large&return_ssl_resources=1"
                                    prams["imageUrl"] = imageUrl
                                    
                                }
                                if let emailID = dict["email"] as? String {
                                    prams["email"] = emailID

                                    
                                }
                                if let nameID = dict["name"] as? String {
                                    prams["name"] = nameID

                                }
                                self.createProfile(socialtype: "Facebook", firebaseID: self.firebaseID, params: prams, socialID:id )
                            }
                            
                        }else {
                            //print("graph error",error?.localizedDescription)
                            Spinner.sharedinstance.stopSpinner(viewController: self)

                        }
                    })
                }else {
                    Spinner.sharedinstance.stopSpinner(viewController: self)

                    //print("facebook errorin tooken")
                }
                
            }
        }
    }
    
    
    
    @objc func manualSignUP(_ sender: UIBarButtonItem) {
        
        defaultName = fullNameField.text!
        defaultDob  = dobField.text!
        defaultEmail = emailField.text!
        
        
            if validateString(string: defaultName) && validateString(string: defaultDob) && validateString(string: defaultEmail)  {
                
                var params:Parameters = ["name":defaultName,"dob":defaultDob,"email":defaultEmail,"gender":defaultGender]
                
                if defaultImgUrl != nil {
                    params["imageUrl"] = defaultImgUrl
                }
                
                createProfile(socialtype: "Manual", firebaseID: firebaseID, params: params, socialID: nil)
                
                
            }else {
                
                CreateAlertPopUp("Oops", "Please fill all fields to continue", OnTap: nil)

            }
    }
}


extension NewRegister:UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let img = info[UIImagePickerControllerOriginalImage] as? UIImage{
            
            DisplayPicture.clipsToBounds = true
            
            myActivityIndicator?.startAnimating()

            uploadImage(image: img, Complition: { (fid, Furl) in
                
                // sucessfully uploaded
                self.DisplayPicture.setImage(img.withRenderingMode(.alwaysOriginal), for: .normal)
                self.defaultImgUrl = Furl
                self.myActivityIndicator?.stopAnimating()

            })
            picker.dismiss(animated: true, completion: nil)
    }
 }
}


extension NewRegister:GIDSignInDelegate {
    
func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {

    if error != nil {
        return
    }

    var param:Parameters = [:]
    
    let emailID = user.profile.email
    let nameID = user.profile.name
    
    param["name"] = nameID
    param["email"] = emailID

    if (user.profile.hasImage) {
        let googleimg = (user.profile.imageURL(withDimension: 300))
        param["imageUrl"] = googleimg
    }
    
    createProfile(socialtype: "Gmail", firebaseID: firebaseID, params: param, socialID: user.userID)
}
    
    @objc func createProfile(socialtype:String,firebaseID:String,params:Parameters,socialID:String?){
        
        let phoneNO = UserDefaults.standard.string(forKey: "phone")!
        let userID = UserDefaults.standard.string(forKey: "userID")!

        var parameter = params
        
        parameter["userID"] = userID
        parameter["socialAccount"] = socialtype
        parameter["firebaseID"] = firebaseID
        
        if socialID != nil {
            parameter["socialID"] = socialID
        }
        
        if let token = InstanceID.instanceID().token() {
            parameter["fcmToken"] = token
        }

        UserDefaults.standard.removeObject(forKey: "userID")
        //print("prams:",parameter)

        apiCallingMethod(apiEndPints: "user", method: .put, parameters: parameter) { (data1:Any?, totalobj:Int?) in
            Spinner.sharedinstance.stopSpinner(viewController: self)
            
            if let data = data1 as? [[String : Any]]
            {
                let userID = data[0]["userID"]!
                let name = data[0]["name"]!
                let email = data[0]["email"]!
                let phone = data[0]["phone"]!
                let imageUrl = data[0]["imageUrl"]!
                
                UserDefaults.standard.set(userID , forKey : "userID")
                UserDefaults.standard.set(name , forKey : "name")
                UserDefaults.standard.set(email , forKey : "email")
                UserDefaults.standard.set(phone , forKey : "phone")
                UserDefaults.standard.set(imageUrl , forKey : "imageUrl")
                UserDefaults.standard.set(self.defaultGender , forKey : "gender")
                
                self.firebaseDatabaseUpdate(fid: firebaseID, params: ["userID":userID,"name":name,"email":email,"phone":phone,"imageUrl":imageUrl])

                DispatchQueue.main.async {
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)

                    let leftDrawerVC = storyboard.instantiateViewController(withIdentifier: "SlideMenuViewController") as! SlideMenuViewController
                    let mainVC = storyboard.instantiateViewController(withIdentifier: "invitationVC") as! InvitationViewController
                    let leftDrawer = SlideMenuController(mainViewController: mainVC, leftMenuViewController: leftDrawerVC)
                    self.present(leftDrawer, animated: true, completion: nil)
                }
            }
        }
    }
    
    @objc func firebaseDatabaseUpdate(fid:String,params:Parameters) {
        
        let ref = Database.database().reference().child("users").child(fid)
        ref.setValue(params)
    }
    
    @objc func datePickerChanged(datePicker:UIDatePicker) {
        
        serverdate = datePicker.date.description
        dobField.text = serverdate
    }
}
