//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import UIKit

class Message: NSObject {

    @objc var fromId : String?
    @objc var toId : String?
    @objc var text : String?
    @objc var toIdName : String?
    @objc var fromIdName : String?
    
}
