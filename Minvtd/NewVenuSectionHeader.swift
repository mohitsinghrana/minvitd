//
//  VendoreSectionHeader.swift
//  Minvtd
//
//  Created by admin on 13/11/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit

class VenuTableViewHeader: UITableViewHeaderFooterView {
    
    var delegate: CollapsibleTableViewHeaderDelegate?
    @objc var section: Int = 0
    
    private var isSetup = true
    @objc var bottomHeight = 0
    
    @objc var ticketBottom : NSLayoutConstraint?
    
    // uistuff
    @objc let locationLabel = UILabel()
    @objc let locationImageView = UIImageView()
    
    @objc let titleLabel =  { () -> UILabel in
        let lab = UILabel()
        lab.textColor = UIColor.black
        lab.font = UIFont.boldSystemFont(ofSize: 15)
        lab.translatesAutoresizingMaskIntoConstraints = false
        lab.clipsToBounds = true
        return lab
    }()
    @objc let timeLabel = { () -> UILabel in
        let lab = UILabel()
        lab.text = ""
        lab.textColor = UIColor.black
        lab.font = UIFont.systemFont(ofSize: 15)
        
        lab.translatesAutoresizingMaskIntoConstraints = false
        return lab
    }()
    
    @objc let arrowImageView = { () -> UIImageView in
        let lab = UIImageView()
        lab.translatesAutoresizingMaskIntoConstraints = false
        return lab
    }()
    
    @objc let containerView1 = { () -> UIView in
        let vi = UIView()
        vi.backgroundColor = .white
        vi.clipsToBounds = true
        vi.translatesAutoresizingMaskIntoConstraints = false
        
        return vi
        
    }()
    
    
    let detailLabel = { () -> UILabel in
        let  detailLabel = UILabel()
        detailLabel.translatesAutoresizingMaskIntoConstraints = false
        detailLabel.numberOfLines = 1
        detailLabel.font = UIFont.systemFont(ofSize: 12)
        detailLabel.textColor = UIColor.darkGray
        detailLabel.clipsToBounds = true
        detailLabel.lineBreakMode = .byWordWrapping
        detailLabel.text = "fgcfgfgvghfjfghckbmnnbvcxzvbnm,jhgfdsghjklhgfdsghjkhgfdhjklhgfdhjkjhgfdhj"
        
        return detailLabel
    }()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        if isSetup {
            
            SetupUI()
            
            addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CollapsibleTableViewHeader.tapHeader(_:))))
            
            
            
        }
        
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    
    
    
    
    
    
    //
    // Trigger toggle section when tapping on the header
    //
    @objc func tapHeader(_ gestureRecognizer: UITapGestureRecognizer) {
        guard let cell = gestureRecognizer.view as? CollapsibleTableViewHeader else {
            return
        }
        
        delegate?.toggleSection(self, section: cell.section)
    }
    
    @objc func setCollapsed(_ collapsed: Bool) {
        //
        // Animate the arrow rotation
        //
        self.arrowImageView.image = collapsed  ? #imageLiteral(resourceName: "carat"): #imageLiteral(resourceName: "carat-open")
        self.detailLabel.isHidden = collapsed  ? false:true
        
        
        
    }
    
    @objc func SetupUI(){
        
        
        locationLabel.translatesAutoresizingMaskIntoConstraints = false
        locationLabel.numberOfLines = 0
        locationLabel.textAlignment = .left
        
        locationLabel.font = UIFont.systemFont(ofSize: 12)
        locationLabel.textColor = UIColor.lightGray
        locationLabel.text = ""
        locationImageView.image = #imageLiteral(resourceName: "location").withRenderingMode(.alwaysTemplate)
        locationImageView.contentMode = .scaleAspectFit
        let colour1 = UserDefaults.standard.string(forKey: "colour1")
        locationImageView.tintColor = UIColor.init(hexString: colour1!)
        
        //        let marginGuide = contentView.layoutMarginsGuide
        
        isSetup = false
        self.containerView1.backgroundColor = UIColor.white.withAlphaComponent(1.0)
        contentView.addSubview(containerView1)
        contentView.addSubview(arrowImageView)
        contentView.addSubview(timeLabel)
        contentView.addSubview(titleLabel)
        contentView.addSubview(locationLabel)
        contentView.addSubview(locationImageView)
        contentView.addSubview(detailLabel)
        
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        titleLabel.anchor(containerView1.topAnchor, left: leftAnchor, bottom: locationImageView.topAnchor, right: rightAnchor, topConstant: 8, leftConstant: 20, bottomConstant: 8, rightConstant: 40, widthConstant: 0, heightConstant: 0)
        
        // arrow img
        
        arrowImageView.anchor(nil, left: nil, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 20, widthConstant: 25, heightConstant: 25)
        arrowImageView.centerYAnchor.constraint(equalTo: timeLabel.centerYAnchor).isActive = true
        // time label
        timeLabel.anchor(nil, left: nil, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 4, rightConstant: 50, widthConstant: 0, heightConstant: 0)
        // timeLabel.anchorCenterYToSuperview()
        timeLabel.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor).isActive = true
        
        
        // container
        
        containerView1.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0.0, leftConstant: 8.0, bottomConstant: 0.0, rightConstant: 8.0, widthConstant: 0, heightConstant: 0)
        
        
        locationImageView.anchor(titleLabel.bottomAnchor, left: contentView.leftAnchor, bottom:nil , right: nil, topConstant: 4, leftConstant: 20, bottomConstant: 4, rightConstant: 40, widthConstant: 15, heightConstant: 15)
        
        locationLabel.anchor(nil, left: locationImageView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 4, leftConstant: 25, bottomConstant: 4, rightConstant: 40, widthConstant: 0, heightConstant: 0)
        locationLabel.centerYAnchor.constraint(equalTo: locationImageView.centerYAnchor).isActive = true
        
        detailLabel.anchor(locationImageView.bottomAnchor, left: locationImageView.leftAnchor, bottom: contentView.bottomAnchor, right: arrowImageView.rightAnchor, topConstant: 2, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
    }
    
}


