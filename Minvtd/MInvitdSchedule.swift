//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//




import UIKit


struct MInvitdSchedule{
    
    // MARK: Properties
    
    let scheduleID :Int?
    let eventID : Int?
    let invitationID : Int?
    let created : String?
    let updated : String?
    let name : String?
    let time : String?
    let description : String?
    let date : String?
    let eventName : String?
    let header : String?
    let venue : String?
    var collapsed = true
    
    // MARK: Initializers
    
    // construct a dictionary
    init(dictionary: [String:AnyObject]) {
        
        scheduleID = dictionary[MInvitdClient.JSONResponseKeys.scheduleID] as? Int
        invitationID = dictionary[MInvitdClient.JSONResponseKeys.invitationID] as? Int
        eventID = dictionary[MInvitdClient.JSONResponseKeys.eventID] as? Int
        created = dictionary[MInvitdClient.JSONResponseKeys.created] as? String
        updated = dictionary[MInvitdClient.JSONResponseKeys.updated] as? String
        name = dictionary[MInvitdClient.JSONResponseKeys.name] as? String
        time = dictionary[MInvitdClient.JSONResponseKeys.time] as? String
        description = dictionary[MInvitdClient.JSONResponseKeys.description] as? String
        date = dictionary[MInvitdClient.JSONResponseKeys.date] as? String
        eventName = dictionary[MInvitdClient.JSONResponseKeys.eventName] as? String
         header = dictionary[MInvitdClient.JSONResponseKeys.header] as? String
         venue = dictionary[MInvitdClient.JSONResponseKeys.venue] as? String        
    }
    
    static func schedulesFromResults(_ results: [[String:AnyObject]]) -> [MInvitdSchedule] {
        var schedules = [MInvitdSchedule]()
        
        // iterate through array of dictionaries, each Article is a dictionary
        for schedule in results {
            schedules.append(MInvitdSchedule(dictionary: schedule))
        }
        
        return schedules
    }
}
