//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//



import UIKit
import Firebase
import AlamofireImage
import Alamofire
import NVActivityIndicatorView

class ChatList: UITableViewController {
    
    lazy var refresher:UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .gray
        refreshControl.addTarget(self, action: #selector(requestData), for: .valueChanged )
        
        return refreshControl
    }()
    
    @objc let id = "reuseIdentifier"
    @objc let headerID = "reuseid"
    @objc var activityIndicator : NVActivityIndicatorView?
    @objc let layerGradient = CAGradientLayer()
    
    var arr  = [MInvitdchatList]()
    var filterData = [MInvitdchatList]()
    var membersData = [MInvitdchatList]()
   
    @objc let currentEvent = UserDefaults.standard.string(forKey: "invitationID")
    
    @objc let myid = Auth.auth().currentUser?.uid
    @objc var firebaseref : DatabaseReference?
    
    var searchBar:UISearchBar = {
        let srch = UISearchBar()
        srch.placeholder = "Search"
        
        //srch.enablesReturnKeyAutomatically = true
        // srch.setShowsCancelButton(true, animated: true)
        return srch
    }()
    
    var isSearching:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //refresh setup
        tableView.refreshControl = refresher

        self.tableView.tableFooterView = UIView()
        activityIndicator = NVActivityIndicatorView(frame: CGRect(x: view.center.x - CGFloat(25.0), y: view.center.y - CGFloat(25.0), width: 50.0 , height: 50.0 ))
        activityIndicator?.startAnimating()
        
        firebaseref =  Database.database().reference().child("members").child(currentEvent!).child(myid!)
        
        navigationBarSetup(nil)
        
        tableView.register(ChatCell.self, forCellReuseIdentifier: id)
        tableView.rowHeight = 70.0
      
        setupNav()
        
        let colour1 = UserDefaults.standard.string(forKey: "colour1")
        let colour2 = UserDefaults.standard.string(forKey: "colour2")
        
        layerGradient.colors = [UIColor.init(hexString: colour1!)?.cgColor, UIColor.init(hexString: colour2!)?.cgColor]
        layerGradient.startPoint = CGPoint(x: 0, y: 0.5)
        layerGradient.endPoint = CGPoint(x: 1, y: 0.5)
        layerGradient.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height)
        self.tabBarController?.tabBar.layer.insertSublayer(layerGradient, at: 0)
        self.tabBarController?.tabBar.barTintColor  = UIColor.white
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func requestData() {
        //print("request refresh")
      
        lookforApi()
        self.tableView.reloadData()
        
        let deadline = DispatchTime.now() + .milliseconds(500)
        DispatchQueue.main.asyncAfter(deadline: deadline) {
            self.refresher.endRefreshing()
        }
        // Code to refresh table view
    }
    
    
    @IBAction func openMenu(_ sender: UIBarButtonItem) {
        slideMenuController()?.openLeft()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        arr = []
        self.tableView.reloadData()
        lookforApi()
        
        //search
        searchBar.frame =  CGRect(x: 0, y: 0, width: view.bounds.width - 44.0, height: 44.0)
        searchBar.delegate = self
        tableView.tableHeaderView = searchBar
        self.tableView.setContentOffset(CGPoint(x: 0, y: 44), animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    @objc public func imageLayerForGradientBackgroundSmall() -> UIImage {
        
        let updatedFrame = CGRect(x: 0, y: 0, width: 40, height: 40)
        // take into account the status bar
        let layer = CAGradientLayer.gradientLayerForBounds(bounds: updatedFrame)
        UIGraphicsBeginImageContext(layer.bounds.size)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    // tableview code
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isSearching ? filterData.count:  arr.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell1 = tableView.dequeueReusableCell(withIdentifier: id, for: indexPath) as! ChatCell
        let data = isSearching ? filterData[indexPath.row] : arr[indexPath.row]
        
        //print("\(data)")
        
        cell1.NameLabel.text = data.name
        cell1.LastMessage.text = data.message
        
        if data.updated != 0 {
            cell1.TimeLabel.text = data.updated?.socialTime()
        }else {
            cell1.TimeLabel.text = ""
        }
        
        if data.fileUrl != nil {
            cell1.ProfileImg.af_setImage(withURL: URL(string: data.fileUrl!)!)
        }
        
        cell1.accessoryType = .disclosureIndicator
        
        let bgImage : UIImage = self.imageLayerForGradientBackgroundSmall()
        cell1.ProfileImg.backgroundColor = UIColor(patternImage: bgImage)
        
        return cell1
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data = isSearching ? filterData[indexPath.row] : arr[indexPath.row]

        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Chat") as! UINavigationController
        let a = vc.viewControllers[0] as? ChatVC
        
        //print("this is the data ^^^^ \(data)")
        a?.chatlist = data
        
        self.present(vc, animated: true, completion: nil)
    }
    
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
        
        tableView.separatorInset =  UIEdgeInsetsMake(0, 70, 0, 0)
    }
    
    // trying new things
    
       override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
    
        let eventID = UserDefaults.standard.string(forKey: "invitationID")
        let myfirebaseID = Auth.auth().currentUser?.uid
        let userID = UserDefaults.standard.string(forKey: "userID")
        let data = self.arr[indexPath.row]

    
            let deleteAction = UITableViewRowAction(style: .normal, title: "Delete") { (rowAction, indexPath) in
                
                // serving delete
                let url = MInvitdClient.Constants.BaseUrl + "chat/member"
        
                let parameters:Parameters = [
                    "userID":userID!,
                    "invitationID":eventID!,
                    "firebaseID":myfirebaseID!,
                    "chatID":data.chatID!
                ]
                let authHeader = ["Authorization" : UserDefaults.standard.string(forKey: "userToken")!, "Content-Type" : "application/json" ]
                
                Alamofire.request(url, method: .delete, parameters: parameters, encoding: JSONEncoding.default, headers: authHeader).responseJSON { (response) in
                    
                    self.isSearching ? self.arr.remove(at: indexPath.row) : self.arr.remove(at: indexPath.row)
                    tableView.deleteRows(at: [indexPath], with: .fade)

                }

                //TODO: Delete the row at indexPath here
            }
            deleteAction.backgroundColor = .red
    
            return [deleteAction]
        }
    
    @objc func ClearBtnAction() {
        arr = []
        tableView.reloadData()
    }
    
    @objc func EditAction() {
        
        tableView.isEditing = !tableView.isEditing
        
        if tableView.isEditing == true {
            self.navigationItem.leftBarButtonItem?.title = "done"
            self.navigationItem.leftBarButtonItem?.action = #selector(self.EditAction)
            self.navigationItem.rightBarButtonItem =  nil
            tableView.reloadData()
        }
        else {
            self.navigationItem.leftBarButtonItem?.title = "Edit"
            let button1 = UIBarButtonItem(image: #imageLiteral(resourceName: "createNew"), style: .plain, target: self, action: #selector(self.ClearBtnAction))
            self.navigationItem.rightBarButtonItem = button1
            tableView.reloadData()
        }
    }
    
    @objc func setupNav(){
        
        self.title = "Chat"
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        let fontDictionary = [ NSAttributedStringKey.foregroundColor:UIColor.white ]
        self.navigationController?.navigationBar.titleTextAttributes = fontDictionary
    }
    
    @IBAction func MenuBar(_ sender: Any) {
        
        slideMenuController()?.openLeft()
        
    }
    
    
    @IBAction func Compose(_ sender: UIBarButtonItem) {
        
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "guestsVC") as! GuestsViewController
        self.present( UINavigationController(rootViewController:mainVC), animated: true, completion: nil)
        
        
    }
}

extension ChatList {
    
    @objc func lookforApi() {
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")
        let firebaseID = Auth.auth().currentUser?.uid

        let parameters:Parameters = [
            "userID":userID!,
            "invitationID":invitationID!,
        ]
        
        //"firebaseID":firebaseID!
        
        apiCallingMethod(apiEndPints: "chat/members", method: .get, parameters: parameters) { (data1:Any?, objectCount:Int?) in

            self.arr = []
            self.tableView.reloadData()

            if let data12 = data1 as? [[String:AnyObject]]{
                //print("\(data12)")
                self.arr = MInvitdchatList.resultFromServerData(data1: data12)
                //print("\(self.arr)")
                self.arr.sort(by: {
                    
                    return $0.updated! >
                        $1.updated!})
                
                self.tableView.reloadData()
                //print("\(self.arr)")
            }
        }
    }
}

class ChatCell:UITableViewCell {
    
    
    @objc let ProfileImg = { () -> UIImageView in
        let img =  UIImageView()
        img.clipsToBounds = true
        img.layer.cornerRadius = 25.0
        return img
    }()
    
    @objc var NameLabel = { () -> UILabel in
        let lab = UILabel()
        lab.text = ""
        lab.font = UIFont.systemFont(ofSize: 15.0)
        lab.textColor = UIColor.darkText
        return lab
    }()
    
     @objc let TimeLabel = { () -> UILabel in
     let lab = UILabel()
     lab.text = ""
     lab.font = UIFont.systemFont(ofSize: 12.0)
     lab.textColor = UIColor.lightGray
     return lab
     }()
    
     @objc let LastMessage = { () -> UILabel in
     let lab = UILabel()
     lab.text = ""
     lab.font = UIFont.systemFont(ofSize: 12.0)
     lab.textColor = UIColor.lightGray
     return lab
     }()
    
    
    @objc func Setup() {
        
        self.contentView.addSubview(TimeLabel)
        self.contentView.addSubview(NameLabel)
        self.contentView.addSubview(ProfileImg)
        self.contentView.addSubview(LastMessage)
        
        
        ProfileImg.anchor(nil, left: self.contentView.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 8, bottomConstant: 0, rightConstant: 0, widthConstant: 50, heightConstant: 50)
        ProfileImg.anchorCenterYToSuperview()
        
        NameLabel.anchor(ProfileImg.topAnchor, left: ProfileImg.rightAnchor, bottom: nil, right: nil, topConstant: 4, leftConstant: 8, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        
         LastMessage.anchor(nil, left: ProfileImg.rightAnchor, bottom: ProfileImg.bottomAnchor, right: nil, topConstant: 0, leftConstant: 8, bottomConstant: 8, rightConstant: 0, widthConstant: 0, heightConstant: 0)
         
         TimeLabel.anchor(NameLabel.topAnchor, left: nil, bottom: nil, right: self.contentView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 2, widthConstant: 0, heightConstant: 0)
 
    }
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        Setup()
    }
    ///new stuff trying
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // welcome firebase
}

/// extensions

extension ChatList:UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
       // tableView.reloadData()
        self.tableView.setContentOffset(CGPoint(x: 0, y: 44), animated: true)

    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
        isSearching = false

        // remember reloading
        //tableView.layoutIfNeeded()
        tableView.reloadData()
        self.tableView.setContentOffset(CGPoint(x: 0, y: 44), animated: true)

    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        //reloading here
        if searchBar.text == nil || searchBar.text == ""{
            isSearching = false
            searchBar.setShowsCancelButton(false, animated: true)
            view.endEditing(true)
            tableView.reloadData()
            self.tableView.setContentOffset(CGPoint(x: 0, y: 44), animated: true)

            return
        }else {
            isSearching = true
            filterData = arr.filter({
                return ($0.name?.contains(searchText))!})
            tableView.reloadData()
        }
    }
}
