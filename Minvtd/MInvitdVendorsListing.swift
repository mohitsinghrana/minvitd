//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import UIKit


struct MInvitdVendorsListing{
    
    // MARK: Properties
    
    let vendorID : Int?
    let userID : Int?
    let categoryID : Int?
    let title : String?
    let description : String?
    let imageUrl : String?
    let address : String?
    let name : String?
    let contact : Int?
    let profileImageUrl : String?
    let category : String?
    let type : String?
    
    var products:[MInvitdVendorsProducts]? = []
    
    // MARK: Initializers
    
    // construct 
    init(dictionary: [String:AnyObject]) {
        vendorID = dictionary[MInvitdClient.JSONResponseKeys.vendorID] as? Int
        userID = dictionary[MInvitdClient.JSONResponseKeys.userID] as? Int
        categoryID = dictionary[MInvitdClient.JSONResponseKeys.categoryID] as? Int
        title = dictionary[MInvitdClient.JSONResponseKeys.title] as? String
        description = dictionary[MInvitdClient.JSONResponseKeys.description] as? String
        imageUrl = dictionary[MInvitdClient.JSONResponseKeys.imageUrl] as? String
        address = dictionary[MInvitdClient.JSONResponseKeys.address] as? String
        name = dictionary[MInvitdClient.JSONResponseKeys.name] as? String
        contact = dictionary[MInvitdClient.JSONResponseKeys.contact] as? Int
        profileImageUrl = dictionary[MInvitdClient.JSONResponseKeys.profileImageUrl] as? String
        category = dictionary[MInvitdClient.JSONResponseKeys.category] as? String
        type = dictionary[MInvitdClient.JSONResponseKeys.type] as? String
        
        if let produ = dictionary["products"] as? [[String:AnyObject]]{
            products = MInvitdVendorsProducts.productsFromResults(produ)
        }
    }
    
    static func listingsFromResults(_ results: [[String:AnyObject]]) -> [MInvitdVendorsListing] {
        var listings = [MInvitdVendorsListing]()
        
        // iterate through array of dictionaries, each Article is a dictionary
        for listing in results {
            listings.append(MInvitdVendorsListing(dictionary: listing))
        }
        
        return listings
    }
}
