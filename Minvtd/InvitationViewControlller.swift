//  Copyright © 2017 Euroinfotek. All rights reserved.

import UIKit
import SlideMenuControllerSwift
import Alamofire
import AlamofireImage
import Firebase
import FBSDKLoginKit

var invite : Bool = false

class InvitationViewController: UIViewController{
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    //refresh setup 1 of 3
    
    lazy var refresher:UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .gray
        refreshControl.addTarget(self, action: #selector(requestData), for: .valueChanged )
        
        return refreshControl
    }()
    
    var invitations: [MInvitdInvitation] = [MInvitdInvitation]()
    var filterData: [MInvitdInvitation] = [MInvitdInvitation]()
    var superInv: [MInvitdInvitation] = [MInvitdInvitation]()
    
    static var showRsvp: Int?
    @objc let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
    @objc var blurEffectView:UIVisualEffectView!
    var headerID = "djgndgnkds"
    @objc var currentIndex : Int = 0
    var  isSearching = false

    @IBOutlet weak var personalTabBarView: UIView!
    
    @IBAction func actionClicked(_ sender: UIBarButtonItem) {
        
        let alertController = UIAlertController(title: "Add an Event", message: "Enter Invite Code", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Add", style: .default, handler: {
            alert -> Void in
            
            let firstTextField = alertController.textFields![0] as UITextField
            
            if let code = firstTextField.text {
                self.submitFunc(code: code)
            }
            if firstTextField.text == "" || firstTextField.text == nil{
                self.CreateAlertPopUp("MInvitd", "Please enter the code!", OnTap: nil)
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Code"
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc let placeholderImage = UIImage(named: "eventCoverImage")!
    
    @IBOutlet weak var navigationBar: UINavigationBar!
    
    @IBAction func menu(_ sender: UIBarButtonItem) {
        slideMenuController()?.openLeft()
    }

    @IBOutlet weak var invitationsCollectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //refresh setup 2 of 3
        collectionView.refreshControl = refresher
        
        
        nameCheck()
        
        self.getUserData()
        
        let nsObject: AnyObject? = Bundle.main.infoDictionary!["CFBundleVersion"] as AnyObject
        let version = nsObject as! String
        print(version)
        
        invitationsCollectionView.delegate = self
        invitationsCollectionView.dataSource = self
        invitationsCollectionView.register(SearchBarHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerID)
        //navigationBarSetup(nil)
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 255 / 255, green: 52 / 255, blue: 101 / 255, alpha: 1.0)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        setupNavBarContent()
    }
    
    func setupNavBarContent(){
        var myString : String = ""
        let button1 = UIBarButtonItem(image: #imageLiteral(resourceName: "menu"), style: .plain, target: self, action: #selector(menu(_:)))
        let titleLabel = UILabel()
        if invite == false{
            myString = "Invitations"
        }
        else{
            myString = "My Invitations"
        }
        let myAttribute = [NSAttributedStringKey.font: UIFont(name: "GothamRounded-Medium", size: 18)!,NSAttributedStringKey.foregroundColor: UIColor.white] as [NSAttributedStringKey : Any]
        let myAtrString = NSAttributedString(string: myString, attributes: myAttribute)
        titleLabel.attributedText = myAtrString
        titleLabel.sizeToFit()
        
        self.navigationItem.titleView = titleLabel
        self.navigationItem.leftBarButtonItems = [button1]
        if invite == false{
        let button2 = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(actionClicked))
            self.navigationItem.rightBarButtonItem = button2
        }
        else{
            // do nothing
        }
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func requestData() {
        //print("request refresh")
        self.getInvitations()
        let deadline = DispatchTime.now() + .milliseconds(500)
        DispatchQueue.main.asyncAfter(deadline: deadline) {
            self.refresher.endRefreshing()
        }
        // Code to refresh table view
    }

    

    @objc func dissmisskeyboard()
    {
        self.view.endEditing(true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getInvitations()
        
        let swipeDownGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeDown(_:)))
        swipeDownGesture.direction = .down
        self.view.addGestureRecognizer(swipeDownGesture)
        self.invitationsCollectionView.addGestureRecognizer(swipeDownGesture)
        
    }
     func nameCheck(){
        let name = UserDefaults.standard.string(forKey: "name")
        if name == nil || name == ""{
            logout()
        }else{
            //print ("name exist")
        }
    }
    
    override func logout() {
        
        let manager = FBSDKLoginManager()
        manager.logOut()
        slideMenuController()?.closeLeft()
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        var parameters = Parameters()
        
        parameters  = ["userID": userID! , "userToken" : "UnknownToken", "device" : "iOS"]
        if let userToken = UserDefaults.standard.string(forKey: "userToken"){
            parameters["userToken"] = userToken
            //parameters = ["userID": userID! , "userToken" : userToken, "device" : "iOS" ]
        }/*else {
            parameters  = ["userID": userID! , "userToken" : "UnknownToken", "device" : "iOS"]
        }*/
        
        apiCallingMethod(apiEndPints: "user/logout", method: .post, parameters: parameters) { (data1:Any?, totalObjects:Int?) in
            
            try! Auth.auth().signOut()
            
            let defaults = UserDefaults.standard
            let dictionary = defaults.dictionaryRepresentation()
            
            dictionary.keys.forEach {
                key in defaults.removeObject(forKey: key)
            }
            defaults.synchronize()

//            UserDefaults.standard.removeObject(forKey: "userID")
//            UserDefaults.standard.removeObject(forKey: "userToken")
//            UserDefaults.standard.removeObject(forKey: "imageUrl")
//            UserDefaults.standard.removeObject(forKey: "name")
//            UserDefaults.standard.removeObject(forKey: "coverUrl")
//            UserDefaults.standard.removeObject(forKey: "eventID")
//            UserDefaults.standard.removeObject(forKey: "invitationID")
//            UserDefaults.standard.removeObject(forKey: "commentID")
//            UserDefaults.standard.removeObject(forKey: "localSocialAccount")
//            UserDefaults.standard.removeObject(forKey: "colour")
//            UserDefaults.standard.removeObject(forKey: "colour2")
//            UserDefaults.standard.removeObject(forKey: "imageID")
//            UserDefaults.standard.removeObject(forKey: "name")
//            UserDefaults.standard.removeObject(forKey: "email")
//            UserDefaults.standard.removeObject(forKey: "userAvatar")
//            UserDefaults.standard.removeObject(forKey: "imageUrl")
//            UserDefaults.standard.removeObject(forKey: "dob")
//            UserDefaults.standard.set(false, forKey: "isRegister")
            
            DispatchQueue.main.async {
                let storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                
                let vc:NewOtpScreen =  (storyboard.instantiateViewController(withIdentifier: "NewOtpScreen") as? NewOtpScreen)!
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = vc
                appDelegate.window?.rootViewController? .dismiss(animated: false, completion: nil)
            }
        }
    }
    
    @objc func swipeDown(_ sender: UISwipeGestureRecognizer){
        
        switch sender.direction {
        case .down:
            //print("swipedown")
            self.invitationsCollectionView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        default:
            print("nothing")
        }
        
        
    }
    
    @objc func getInvitations(){
        
        if invite == true {
           
            apiCallingMethod(apiEndPints: "invitations", method: .get, parameters: [:]) { (data1:Any?, totalObject:Int?) in
                
                if totalObject == 0 {
                    
                    self.CreateAlertPopUp("MInvitd", "You have no invitations as yet", OnTap: nil)
                    
                    return
                }
                
                if let data = data1 as? [[String : Any]]
                {
                    //print("Invitation Data Response: \(data)")
                    let invitations = MInvitdInvitation.invitationsFromResults(data as [[String : AnyObject]])
                    self.invitations = invitations
                    self.superInv = self.invitations
                    DispatchQueue.main.async {
                        self.invitationsCollectionView.reloadData()
                        self.invitationsCollectionView.setContentOffset(CGPoint(x: 0, y: 44), animated: true)
                    }
                }
                
                
            }
            
        }else {
       
            apiCallingMethod(apiEndPints: "invites", method: .get, parameters: [:]) { (data1:Any?, totalObjects:Int?) in
                if totalObjects == 0 {
                    self.CreateAlertPopUp("MInvitd", "You have no invites as yet", OnTap: nil)
                    return
                }else {
                    
                    if let data = data1 as? [[String : Any]]
                    {
                        //print("Invitation Data Response: \(data)")
                        let invitations = MInvitdInvitation.invitationsFromResults(data as [[String : AnyObject]])
                        self.invitations = invitations
                        self.superInv = self.invitations
                        DispatchQueue.main.async {
                            self.invitationsCollectionView.reloadData()
                            self.invitationsCollectionView.setContentOffset(CGPoint(x: 0, y: 44), animated: true)
                        }
                    } else {
                        //print("oopsss")
                    }
                }
            }
        }
        
    }
}

// MARK: - InvitationViewController: UICollectionViewDelegate, UICollectionViewDataSource

extension InvitationViewController : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout , UISearchDisplayDelegate {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let bounds = UIScreen.main.bounds
        let height = (bounds.height - 100)/3
        let width = bounds.width

        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let invitation = isSearching ? filterData[indexPath.row] : invitations[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "invitationCell", for: indexPath) as! InvitationCollectionViewCell
        
        cell.title.text =  invitation.name
        cell.date.text = invitation.date
        cell.image.af_setImage(
            withURL: URL(string: invitation.imageUrl!)! ,
            placeholderImage: placeholderImage
        )
        
        cell.bellView.layer.cornerRadius = 12.5
        cell.bellView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        
        cell.notiView.layer.cornerRadius = cell.notiView.frame.size.width / 2
        cell.notiView.clipsToBounds = true
        
        if invitation.notificationsCount! > 0 {
            
            cell.notiView.isHidden = false
            
            let noti = String(describing: invitation.notificationsCount!)
            
            if invitation.notificationsCount! < 10{
                cell.notificationLabel.text = noti
                cell.notificationLabel.anchorCenterSuperview()
            }else{
                cell.notificationLabel.text = noti
                cell.notificationLabel.anchorCenterSuperview()
            }
        
        }else {
            
            cell.notiView.isHidden = true
            cell.notificationLabel.text = ""
            
        }
        
        
        cell.notificationButton.tag = indexPath.row
        cell.button.tag = indexPath.row
        
        //Remove Target

        cell.button.removeTarget(nil, action: nil, for: .allEvents)
        cell.notificationButton.removeTarget(nil, action: nil, for: .allEvents)
        
        // Add target
        cell.button.addTarget(self, action: #selector(didSelectInvitation(sender:)), for: .touchUpInside)
        cell.notificationButton.addTarget(self, action: #selector(didClickNotification(sender:)), for: .touchUpInside)

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return isSearching ? filterData.count : invitations.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.bounds.width, height: 44.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let search = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerID, for: indexPath) as! SearchBarHeader
        search.mysearchBar.delegate = self
        return search
    }
    
    @objc func getUserData(){
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        
        apiCallingMethod(apiEndPints: "users", method: .get, parameters: ["userID":userID]) { (data1, totalObject) in
            
            if let data = data1 as? [[String : Any]]
            {
                let name  = data[0]["name"] as? String
                UserDefaults.standard.set(name, forKey: "name")
                let email = data[0]["email"] as? String
                UserDefaults.standard.set(email, forKey: "email")
                let imageUrl = data[0]["imageUrl"] as? String
                //UserDefaults.standard.set(imageUrl, forKey: "imageUrl")
                
                let imageID = data[0]["fileID"] as? Int
                UserDefaults.standard.set(imageID, forKey: "imageID")
                let coverUrl = data[0]["coverUrl"] as? String
                UserDefaults.standard.set(coverUrl, forKey: "coverUrl")
            }
            else{
                //print("naah naah")
            }
        }
    }
    
    @objc func didSelectInvitation(sender : UIButton){
        
        let indexPath =  sender.tag
        let invitation = isSearching ? filterData[indexPath] : invitations[indexPath]
        let userID =  UserDefaults.standard.integer(forKey: "userID")
      //  let ownerID : Int  =  invitation.ownerID!

        UserDefaults.standard.set(invitation.invitationID, forKey: "invitationID")
        UserDefaults.standard.set(invitation.userRole, forKey: "userRole")
        UserDefaults.standard.set(invitation.imageUrl, forKey: "invitationImageUrl")
        UserDefaults.standard.set(invitation.coverUrl, forKey: "invitationCoverUrl")
        UserDefaults.standard.set(invitation.albumBgUrl, forKey: "albumBgUrl")
        UserDefaults.standard.set(invitation.name, forKey: "invitationName")
        UserDefaults.standard.set(invitation.name, forKey: "invitationTitle")
        UserDefaults.standard.set(invitation.colour, forKey: "colour1")
        UserDefaults.standard.set(invitation.colour_2, forKey: "colour2")
        UserDefaults.standard.set(invitation.feedStatus, forKey: "feedStatus")
        UserDefaults.standard.set(invitation.category, forKey: "category")
        UserDefaults.standard.set(invitation.creatorID, forKey: "creatorID")
        UserDefaults.standard.set(invitation.type, forKey: "type")
        UserDefaults.standard.set(invitation.type, forKey: "iType")
        UserDefaults.standard.set(invitation.category, forKey: "iCategory")
        
        //let receiveNotifications = UserDefaults.standard.string(forKey: "receiveNotifications")
        //print("This is receiveNotifications User Default:\(receiveNotifications)")
        
        let showRsvp = invitation.showRsvp
        
        if(showRsvp == false)
        {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let leftDrawerVC = storyBoard.instantiateViewController(withIdentifier: "SlideMenuViewController") as! SlideMenuViewController
            leftDrawerVC.menuTag = 2
            let mainVC = storyBoard.instantiateViewController(withIdentifier: "tabVC")
            let vc = SlideMenuController(mainViewController: mainVC, leftMenuViewController: leftDrawerVC)
            pushPresentAnimation()
            self.present(vc, animated: false, completion: nil)
            
        }else{
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            
            let mainVC = storyBoard.instantiateViewController(withIdentifier: "eventsVC") as! EventsViewController
            
            pushPresentAnimation()
            self.present(UINavigationController(rootViewController:mainVC), animated: false, completion: nil)
            
        }
        
    }
    
    @objc func didClickNotification(sender : UIButton){
        
        let indexPath =  sender.tag
        
        let invitation = isSearching ? filterData[indexPath] : invitations[indexPath]
        let userID =  UserDefaults.standard.integer(forKey: "userID")
        //let ownerID : Int  =  invitation.ownerID!
        
        UserDefaults.standard.set(invitation.invitationID, forKey: "invitationID")
        UserDefaults.standard.set(invitation.userRole, forKey: "userRole")
        UserDefaults.standard.set(invitation.imageUrl, forKey: "invitationImageUrl")
        UserDefaults.standard.set(invitation.albumBgUrl, forKey: "albumBgUrl")
        UserDefaults.standard.set(invitation.name, forKey: "invitationName")
        UserDefaults.standard.set(invitation.colour, forKey: "colour1")
        UserDefaults.standard.set(invitation.colour_2, forKey: "colour2")
        UserDefaults.standard.set(invitation.receiveNotifications, forKey: "receiveNotifications")
        UserDefaults.standard.set(invitation.coverUrl, forKey: "invitationCoverUrl")
        UserDefaults.standard.set(invitation.feedStatus, forKey: "feedStatus")
        UserDefaults.standard.set(invitation.creatorID, forKey: "creatorID")
        UserDefaults.standard.set(invitation.type, forKey: "type")
        UserDefaults.standard.set(invitation.category, forKey: "category")
        UserDefaults.standard.set(invitation.type, forKey: "iType")
        UserDefaults.standard.set(invitation.category, forKey: "iCategory")

        let mainVC = storyboard?.instantiateViewController(withIdentifier: "notificationsVCOuter") as! NotificationOuterViewController
        add(asChildViewController: mainVC)
    }

    @objc func submitFunc(code : String )  {
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        let parameters: Parameters = ["eventID" : code as AnyObject , "userID" : userID  as AnyObject]
        
        apiCallingMethod(apiEndPints: "event/request", method: .post, parameters: parameters) { (data1:Any?, totalObjects) in
            
            if let data = data1 as? [[String : Any]]
            {
                self.getInvitations()
                
                DispatchQueue.main.async {
                    
                self.CreateAlertPopUp("MInvitd", "You have been successfully added to this event", OnTap: nil)
                }
            }
        }
   }
}


// new info here

extension InvitationViewController:UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
        // tableView.reloadData()
        isSearching = true
        filterData = invitations.filter({
            return ($0.name?.contains(searchBar.text!))!})
        //print("\(filterData)")
        invitationsCollectionView.reloadData()
        self.invitationsCollectionView.setContentOffset(CGPoint(x: 0, y: 44), animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
        isSearching = false

        invitationsCollectionView.reloadData()
        self.invitationsCollectionView.setContentOffset(CGPoint(x: 0, y: 44), animated: true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        //reloading here
        if searchBar.text == nil || searchBar.text == ""{
            isSearching = false
            searchBar.setShowsCancelButton(false, animated: true)
            view.endEditing(true)
            invitationsCollectionView.reloadData()
            self.invitationsCollectionView.setContentOffset(CGPoint(x: 0, y: 44), animated: true)
            
            return
        }else {
            
        }
    }
    
    @objc func CreateBlur () {
        blurEffectView = UIVisualEffectView(effect: blurEffect)
        
        blurEffectView.alpha = 0.9
        
        //always fill the view
        blurEffectView.frame = self.view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(blurEffectView)
    }
    
    
    // childViewController
    fileprivate func add(asChildViewController viewController: UIViewController) {
        
        CreateBlur()
        self.addChildViewController(viewController)
        
        viewController.view.frame = CGRect(x: 25, y: 100, width: view.bounds.width - 50, height: view.bounds.height - 150)
        
        view.addSubview(viewController.view)
        viewController.didMove(toParentViewController: self)
        navigationController?.view.setNeedsLayout()
        
    }
}

class SearchBarHeader:UICollectionReusableView{
    var mysearchBar:UISearchBar = {
        let srch = UISearchBar()
            srch.translatesAutoresizingMaskIntoConstraints = false
            srch.placeholder = "Search"

        return srch
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(mysearchBar)
        mysearchBar.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
