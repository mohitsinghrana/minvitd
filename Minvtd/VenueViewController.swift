//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//


import UIKit
import Alamofire
import AlamofireImage
import IDMPhotoBrowser

class VenueViewController: UIViewController , IDMPhotoBrowserDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    
    @objc var photos : [IDMPhoto] = [IDMPhoto]()
    @objc var browser:IDMPhotoBrowser? = nil
    @objc var currentIndex : Int = 0
    @objc var location : String = ""
    var venues: [MInvitdVenue] = [MInvitdVenue]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        navigationBarSetup(nil)
        self.locationBtn.setBackgroundImage(imageLayerForGradientBackground(navBar: self.navigationController?.navigationBar), for: .normal)
        let imageBackgroundUrl =  UserDefaults.standard.string(forKey: "invitationImageUrl")
        self.bgImage.af_setImage(
            withURL: URL(string: imageBackgroundUrl!)! 
        )
        
        browser?.delegate = self

        self.bgImage.contentMode = .scaleAspectFill
        self.bgImage.clipsToBounds = true
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        //always fill the view
        blurEffectView.frame = self.bgImage.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.bgImage.insertSubview(blurEffectView, at: 0)
        let button1 = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(backButtonClicked(_:)))
        self.navigationItem.leftBarButtonItem = button1
        self.navigationItem.rightBarButtonItem = nil
        self.title = "Venues"


    }
    
    @IBOutlet weak var locationBtn: UIButton!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    @IBAction func locationBtnClicked(_ sender: UIButton) {
        
        self.location = venues[self.currentIndex].coordinates!
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "eventLocationVC") as! EventLocationViewController
        mainVC.location = self.location
        self.present(UINavigationController(rootViewController:mainVC), animated: false, completion: nil)
    }

    @objc func backButtonClicked(_ sender: UIBarButtonItem) {
        pushpopAnimation()
        self.dismiss(animated: false, completion: nil)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        VenuView()
        
    }
    
    
    
    @objc func VenuView(){
        
        let invitationID =  UserDefaults.standard.string(forKey: "invitationID")!


        apiCallingMethod(apiEndPints: "venues", method: .get, parameters: ["invitationID":invitationID]) { (data1:Any?, totalObjects:Int?) in
            
            if totalObjects == 0
            {
               self.dismissAlert("there is noting to display")
                return
            }else{
                self.pageControl.numberOfPages = totalObjects!
            }
            if let data = data1  as? [[String : Any]]{
                
                print("This is venue data: \(data)")
                
                    let venues = MInvitdVenue.venuesFromResults(data as [[String : AnyObject]])
                    self.venues = venues
                    DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let center = CGPoint(x: scrollView.contentOffset.x + (scrollView.frame.width / 2), y: (scrollView.frame.height / 2))
        if let ip = self.collectionView.indexPathForItem(at: center) {
            self.currentIndex = ip.row
            self.pageControl.currentPage =  self.currentIndex
            self.location = venues[self.currentIndex].coordinates!
        }
    }

    
}



extension VenueViewController : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout   {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let bounds = collectionView.bounds
        let width = bounds.width
        let height = bounds.height
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let venue = venues[(indexPath as NSIndexPath).row]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "venueCell", for: indexPath) as! VenueCollectionViewCell
        
        let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "venueCellNoImage", for: indexPath) as! VenueNoImageCollectionViewCell
        
       
        if venue.gallery == nil {
            
            cell2.mainView.layer.cornerRadius = 10
            cell2.mainView.clipsToBounds = true
            cell2.title.text =  venue.eventName
            cell2.address.text = venue.address
            cell2.contactNumber.text = venue.phone
            cell2.venue.text = venue.name
            cell2.date.text = venue.eventDate

            
            let contactTap = UITapGestureRecognizer(target: self, action: #selector(self.contactTapFunc(_:)))
            contactTap.numberOfTapsRequired = 1
            cell2.contactNumber.isUserInteractionEnabled = true
            cell2.contactNumber.addGestureRecognizer(contactTap)
            cell2.contactEmail.text = venue.email
            cell2.contactPerson.text = venue.contactPerson


            return cell2

            
        }else{
            
            cell.mainView.layer.cornerRadius = 10
            cell.mainView.clipsToBounds = true
            cell.title.text =  venue.eventName
            cell.date.text = venue.eventDate
            cell.address.text = venue.address
            cell.contactNumber.text = venue.phone
            cell.venue.text = venue.name
            
            cell.image.isHidden = false
            cell.image.af_setImage(withURL: URL(string: venue.gallery![0].fileUrl!)!)
            
            let imageTap = UITapGestureRecognizer(target: self, action: #selector(self.imageTap(_:)))
            imageTap.numberOfTapsRequired = 1
            cell.image.isUserInteractionEnabled = true
            cell.image.addGestureRecognizer(imageTap)
            
            let contactTap = UITapGestureRecognizer(target: self, action: #selector(self.contactTapFunc(_:)))
            contactTap.numberOfTapsRequired = 1
            cell.contactNumber.isUserInteractionEnabled = true
            cell.contactNumber.addGestureRecognizer(contactTap)
            cell.contactEmail.text = venue.email
            cell.contactPerson.text = venue.contactPerson

            return cell

        }
    }
    
    @objc func imageTap(_ sender: UITapGestureRecognizer) {
    
        photos = [IDMPhoto]()
        
        for photo in venues[self.currentIndex].gallery! {
            let currentPhoto = IDMPhoto(url: URL(string : photo.fileUrl!))
            photos.append(currentPhoto!)
        }
        
        browser = IDMPhotoBrowser.init(photos: photos)
        browser?.displayCounterLabel = true
        browser?.displayActionButton = false
        browser?.doneButtonRightInset = CGFloat(5.0)
        browser?.doneButtonTopInset = CGFloat(30.0)
        browser?.doneButtonImage = UIImage(named: "cross")
        present(browser!, animated: true, completion: nil)
        
    }
    
    
    @objc func contactTapFunc(_ sender: UITapGestureRecognizer) {
        
        let venue = self.venues[self.currentIndex]
        let number : String = venue.phone!
        if let url = URL(string: "tel://\(number))"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    



    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return venues.count
    }
    
    
}



