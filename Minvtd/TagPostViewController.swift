//
//  TagPostViewController.swift
//  Minvtd
//
//  Created by admin on 03/12/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.

import UIKit

class TagPostViewController: UITableViewController {
    
    var filterData = [MInvitdGuest]()
    var guests:[MInvitdGuest] = []
    var tempSelectedGuests = [MInvitdGuest]()
    var removeGuest : MInvitdGuest?
    var tagID : [Int] = []
    
    var searchBar:UISearchBar = {
        let srch = UISearchBar()
        srch.placeholder = "Search"
        return srch
    }()
    var isSearching:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = 60
        tableView.register(guestCell.self, forCellReuseIdentifier: "reuseIdentifier")

        navigationBarSetup(nil)
        title = "Tag"
        let button1 = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(back))
        self.navigationItem.leftBarButtonItem = button1
        let button2 = UIBarButtonItem(title:"Next", style: .plain, target: self, action: #selector(nextVC))
        self.navigationItem.rightBarButtonItem = button2
        tableView.allowsMultipleSelection = true
        tableView.tableFooterView = UIView()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         getMYGuests()
        searchBar.frame =  CGRect(x: 0, y: 0, width: view.bounds.width - 44.0, height: 44.0)
        searchBar.delegate = self
        tableView.tableHeaderView = searchBar
        tableView.setContentOffset(CGPoint(x: 0, y: 44), animated: true)
        
        navigationBarSetup(nil)
        title = "Tag"
        let button1 = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(back))
        self.navigationItem.leftBarButtonItem = button1
        let button2 = UIBarButtonItem(title:"Next", style: .plain, target: self, action: #selector(nextVC))
        self.navigationItem.rightBarButtonItem = button2
        
        tagID = []
    }
    
    func getMYGuests(){
        let invitationID:Int = UserDefaults.standard.integer(forKey: "invitationID")
        let userID:Int = UserDefaults.standard.integer(forKey:"userID")
        
        apiCallingMethod(apiEndPints: "guests", method: .get, parameters: ["invitationID":invitationID,"userID":userID]) { (data, total) in
            if let data1 = data as? [[String : Any]]{
                if let guestsDict = data1 as? [[String:AnyObject]]{
                    self.guests = MInvitdGuest.guestsFromResults(guestsDict)
                    self.tableView.reloadData()
                }
            }
        }
    }

   @objc func back(){
        //tages = []
      //  NotificationCenter.default.post(name: NSNotification.Name.init("tags"), object: tages)

        dismiss(animated: true, completion: nil)
    }
    
    @objc func nextVC(){
        
        for tag in tempSelectedGuests{
            tagID.append(tag.userID!)
        }
        
        NotificationCenter.default.post(name: NSNotification.Name.init("tagID"), object: tagID)
        
        NotificationCenter.default.post(name: NSNotification.Name.init("tags"), object: tempSelectedGuests)
        
        dismiss(animated: true, completion: nil)
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isSearching ? filterData.count : guests.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let guest = isSearching ? filterData[indexPath.row]:  guests[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as? guestCell
        cell?.guest = guest
        
        if guest.isSelected {
            cell?.accessoryType = .checkmark

        }else{
            cell?.accessoryType = .none
        }
        
        for gg in tempSelectedGuests{
            if gg.phone == guest.phone{
                cell?.accessoryType = .checkmark
            }
        }
        
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
         let guest = isSearching ? filterData[indexPath.row] : guests[indexPath.row]
        
        let cell = tableView.cellForRow(at: indexPath) as? guestCell

        if isSearching {
            if cell?.accessoryType == .none {
                self.filterData[indexPath.row].isSelected = true
                self.tempSelectedGuests.append(guest)
                
                searchBar.text = ""
                searchBar.resignFirstResponder()
                searchBar.setShowsCancelButton(false, animated: true)
                isSearching = false
                self.tableView.reloadData()
            }
            else{
                self.filterData[indexPath.row].isSelected = false
                removeGuest = guest
                //print(" THIS IS DE-Select tempSelectedGuests : \n \(removeGuest)")
                
                var filter = [MInvitdGuest]()
                for guest in tempSelectedGuests {
                    if guest.phone != removeGuest?.phone{
                        filter.append(guest)
                    }
                }
                tempSelectedGuests = filter
                cell?.accessoryType = .none
                searchBar.text = ""
                searchBar.resignFirstResponder()
                searchBar.setShowsCancelButton(false, animated: true)
                isSearching = false
                self.tableView.reloadData()
            }
        }
        else{
            if cell?.accessoryType == .none {
                
                self.guests[indexPath.row].isSelected = true
                self.tempSelectedGuests.append(guest)
                cell?.accessoryType = .checkmark
                self.tableView.reloadData()
                
            }
            else{
                self.guests[indexPath.row].isSelected = false
                removeGuest = guest
                print(" THIS IS DE-Select tempSelectedGuests : \n \(removeGuest)")
                
                var filter = [MInvitdGuest]()
                for guest in tempSelectedGuests {
                    if guest.phone != removeGuest?.phone{
                        filter.append(guest)
                    }
                }
                tempSelectedGuests = filter
                cell?.accessoryType = .none
                self.tableView.reloadData()
            }
        }
        //print(" THIS IS filter Select tempSelectedGuests : \n \(self.tempSelectedGuests)")
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let guest = isSearching ? filterData[indexPath.row] : guests[indexPath.row]
        
        let cell = tableView.cellForRow(at: indexPath) as? guestCell
        
        if isSearching {
            
            if cell?.accessoryType == .checkmark {
                self.filterData[indexPath.row].isSelected = false
                removeGuest = guest
                //print(" THIS IS DE-Select tempSelectedGuests : \n \(removeGuest)")
                
                var filter = [MInvitdGuest]()
                for guest in tempSelectedGuests {
                    if guest.phone != removeGuest?.phone{
                        filter.append(guest)
                    }
                }
                tempSelectedGuests = filter
                cell?.accessoryType = .none
                searchBar.text = ""
                searchBar.resignFirstResponder()
                searchBar.setShowsCancelButton(false, animated: true)
                isSearching = false
                self.tableView.reloadData()
                
            }
            else{
                
                self.filterData[indexPath.row].isSelected = true
                self.tempSelectedGuests.append(guest)
                
                searchBar.text = ""
                searchBar.resignFirstResponder()
                searchBar.setShowsCancelButton(false, animated: true)
                isSearching = false
                self.tableView.reloadData()
            }
            
        }
        else{
            if cell?.accessoryType == .checkmark {
                self.guests[indexPath.row].isSelected = false
                removeGuest = guest
                //print(" THIS IS DE-Select tempSelectedGuests : \n \(removeGuest)")
                
                var filter = [MInvitdGuest]()
                for guest in tempSelectedGuests {
                    if guest.phone != removeGuest?.phone{
                        filter.append(guest)
                    }
                }
                tempSelectedGuests = filter
                cell?.accessoryType = .none
                self.tableView.reloadData()
            }
            else{
                
                
                self.guests[indexPath.row].isSelected = true
                self.tempSelectedGuests.append(guest)
                cell?.accessoryType = .checkmark
                self.tableView.reloadData()
            }
            
        }
    }
}

class guestCell:UITableViewCell{
    
    
    var guest:MInvitdGuest?{
        didSet{
            if let img = guest?.imageUrl{
                contentImg.af_setImage(withURL: URL(string:img)!)
            }
            nameLabel.text = guest?.name
        }
    }
    
    let contentImg = { () -> UIImageView in
        let imgView = UIImageView()
        imgView.clipsToBounds = true
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.backgroundColor = .black
        imgView.isUserInteractionEnabled = true
        imgView.contentMode = .scaleAspectFill
        imgView.clipsToBounds = true
        imgView.layer.cornerRadius = 20.0
        return imgView
    }()

    let nameLabel = { () -> UILabel in
        let li = UILabel()
        li.textAlignment = .left
        li.font = UIFont.boldSystemFont(ofSize: 18)
        li.translatesAutoresizingMaskIntoConstraints = false
        return li
    }()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(contentImg)
        contentView.addSubview(nameLabel)
        
        contentImg.anchor(topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 8, leftConstant: 20, bottomConstant: 0, rightConstant: 0, widthConstant: 40, heightConstant: 40)
        nameLabel.anchorCenterYToSuperview(constant: -10)
        nameLabel.leftAnchor.constraint(equalTo: contentImg.rightAnchor, constant: 12).isActive = true
        selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        contentImg.image = nil
    }
}

extension TagPostViewController : UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
        // tableView.reloadData()
        tableView.setContentOffset(CGPoint(x: 0, y: 44), animated: true)
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
        isSearching = false
        tableView.reloadData()
       tableView.setContentOffset(CGPoint(x: 0, y: 44), animated: true)
        
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        //reloading here
        if searchBar.text == nil || searchBar.text == ""{
            isSearching = false
            searchBar.setShowsCancelButton(false, animated: true)
            view.endEditing(true)
            tableView.reloadData()
            tableView.setContentOffset(CGPoint(x: 0, y: 44), animated: true)
            return
        }
        else {
            isSearching = true
            filterData = guests.filter({
                return ($0.name?.contains(searchText))!})
           tableView.reloadData()
            
        }
        
    }
}
