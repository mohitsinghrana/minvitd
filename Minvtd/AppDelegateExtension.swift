//  AppDelegateExtension.swift
//  Minvtd
//
//  Created by admin on 22/10/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.

import UIKit
import SlideMenuControllerSwift
import Firebase
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import Alamofire
import UserNotifications

extension AppDelegate{
    
    func changeUserStatus( major : String?  , minor : String? , uuid : String?,arrive:Bool){
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        let parameters: Parameters = ["userID" : userID as AnyObject , "majorID" : major as AnyObject , "minorID" : minor as AnyObject ]
        let test = arrive ? "guest/arrive":"guest/leave"
        
        let url = MInvitdClient.Constants.BaseUrl + test
        
        let authHeader = ["Authorization" : UserDefaults.standard.string(forKey: "userToken")!, "Content-Type" : "application/json" ]
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: authHeader).responseJSON { (response) in
            
            print("Api Response", response.result)
            
        }
    }
    
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        let handled = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String?, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        
        GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String?, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        
        return handled
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        Auth.auth().setAPNSToken(deviceToken, type: AuthAPNSTokenType.prod)
    }
}

// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        completionHandler([.alert,.sound,.badge])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo as! [String: Any]
        if let messageID = userInfo[gcmMessageIDKey] {
        }
        
        
        if let dictionary = userInfo as? [String : Any] {
            
            if let data = dictionary["data"] as? String{
                print("hellow data",data)
                let data1: Data = data.data(using: .utf8)!
                let anyObj = try! JSONSerialization.jsonObject(with: data1, options: .allowFragments)
                let notiObj = NotificationData(with: anyObj as! [String : Any])
                print("hellow noti ", notiObj)
                let userID = UserDefaults.standard.string(forKey: "userID")!
                var notiArray: [MInvtdAllFeed] = []
                
                if let module = notiObj.module{
                    if module == "chat"{
                        
                        let invitationID = UserDefaults.standard.integer(forKey: "invitationID")
                        
                        guard let str = notiObj.chatID as? String else {
                            return}
                        
                        let chatroom =  MInvitdchatList(chatID: str, name: notiObj.userName!, invitationID: invitationID)
                        
                        let storyBoard : UIStoryboard = UIStoryboard(name: "ChatRefectored", bundle: nil)
                        let vc = storyBoard.instantiateViewController(withIdentifier: "Chat") as! UINavigationController
                        let a = vc.viewControllers[0] as? ChatVC
                        a?.chatlist = chatroom
                        a?.notiDirection = true
                        
                        self.window =  UIWindow(frame: UIScreen.main.bounds)
                        self.window?.rootViewController = vc
                        self.window?.makeKeyAndVisible()
                        
                    }else if module == "likes" || module == "comment" || module == "gallery" || module == "image" || module == "video" || module == "story" || module == "album" || module == "post" || module == "guests" {
                        
                        let url = MInvitdClient.Constants.BaseUrl + "comments"
                        let prams: Parameters = ["invitationID":notiObj.invitationID!,"postID":notiObj.postID ,"userID":userID]
                        
                        print("hellow",prams)
                        let authHeader = ["Authorization" : UserDefaults.standard.string(forKey: "userToken")!, "Content-Type" : "application/json" ]
                        
                        Alamofire.request(url, method: .get, parameters: prams, encoding: JSONEncoding.default, headers: authHeader).responseJSON { (response) in
                            
                            if response.error == nil {
                                self.apiHelperMethod(JSONData : response.data!, complition: {
                                    data, totalObjects
                                    in
                                    print("hello",data)
                                    let tempArray = MInvtdAllFeed.allFeedsFromResults(data as! [[String : AnyObject]])
                                    
                                    notiArray = tempArray
                                    print("Hello",notiArray)
                                    if notiArray[0] != nil{
                                        
                                        let feed = notiArray[0]
                                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                        
                                        if feed.fileType == "text"{
                                            
                                            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                            let mainVC = storyBoard.instantiateViewController(withIdentifier: "feedCommentVC") as! FeedCommentVC
                                            mainVC.paramaters = feed
                                            mainVC.origin = "feed"
                                            mainVC.notificationRedirection = true
                                            
                                            self.window =  UIWindow(frame: UIScreen.main.bounds)
                                            self.window?.rootViewController = mainVC
                                            self.window?.makeKeyAndVisible()
                                            
                                        }else {
                                            
                                            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                            let mainVC = storyBoard.instantiateViewController(withIdentifier: "feedImageCommentVC") as! FeedImageCommentVC
                                            mainVC.paramaters = feed
                                            mainVC.origin = "feed"
                                            mainVC.notificationRedirection = true
                                            
                                            self.window =  UIWindow(frame: UIScreen.main.bounds)
                                            self.window?.rootViewController = mainVC
                                            self.window?.makeKeyAndVisible()
                                        }
                                    }
                                })
                            }else {
                                
                            }
                        }
                    }else{
                        
                    }
                }
            }
        }
        
        completionHandler()
    }
    
    fileprivate func  apiHelperMethod(JSONData : Data,complition:(_ data:Any?,_ totalObjects:Int?)->()) {
        
        var totalObjects:Int?
        var serverData:Any?
        
        do{
            let json = try JSONSerialization.jsonObject(with: JSONData, options: .allowFragments)
            
            if let dictionary = json as? [String : Any] {
                let totalObjects1 = dictionary["totalObjects"] as? Int
                
                totalObjects = totalObjects1
                if totalObjects == 0 {
                    
                    complition(serverData, totalObjects)
                    return
                }
                
                if let status = dictionary["status"] as? String
                {
                    if status == "success"
                    {
                        let data = dictionary["data"]
                        serverData = data
                        
                        complition(serverData, totalObjects)
                        return
                        
                    }else
                    {
                        if let mess = (dictionary["messages"] as? [[String : String]])                             {
                            if let messdict = mess.first {
                                
                                let message = messdict["message"] ?? "Error  Status return  from server"
                                
                                DispatchQueue.main.async {
                                    
                                    //self.CreateAlertPopUp(status, message, OnTap: nil)
                                }
                            }
                        }
                    }
                }
            }
        }
        catch{
            // errorrr
            //elf.CreateAlertPopUp("Error", "Error in JSON Serialization", OnTap: nil)
        }
    }
    
    func application(_ application: UIApplication,
                     didReceiveRemoteNotification notification: [AnyHashable : Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if Auth.auth().canHandleNotification(notification) {
            completionHandler(UIBackgroundFetchResult.noData)
            return
        }
    }
}

extension AppDelegate : MessagingDelegate {
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
    }
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
}

struct NotificationData {
    
    var commentID: String?
    var invitationID: String?
    var module: String?
    var rid: String?
    var userName: String?
    var postID:String?
    var chatID : String?
    
    init(with dictionary: [String: Any]?) {
        guard let dictionary = dictionary else { return }
        commentID = dictionary["commentID"] as? String
        invitationID = dictionary["invitationID"] as? String
        module = dictionary["module"] as? String
        rid = dictionary["rid"] as? String
        userName = dictionary["userName"] as? String
        postID = dictionary["postID"] as? String
        chatID = dictionary["chatID"] as? String
    }
}
