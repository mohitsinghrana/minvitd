//
//  NewOtpScreen.swift
//  Minvtd
//
//  Created by admin on 05/09/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit
import Firebase
import CTKFlagPhoneNumber


class NewOtpScreen: UIViewController {
    
    @IBOutlet weak var phoneField:UITextField?
    @IBOutlet weak var bottomImage: UIImageView!
    @IBOutlet weak var countryCode: CTKFlagPhoneNumberTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        countryCode.borderStyle = .none
        countryCode.backgroundColor = UIColor.clear
        
        //setupCountryCode()
        
        // Do any additional setup after loading the view.
    }
    
    var countryCodeTF: CTKFlagPhoneNumberTextField = {
        
        var pickerC = CTKFlagPhoneNumberTextField()
        pickerC.backgroundColor = UIColor.clear
        pickerC.layer.cornerRadius = 4
        pickerC.borderStyle = .none
        return pickerC
    }()
    
    func setupCountryCode(){
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        phoneField?.text = ""
        countryCode.setFlag(for: "IN")
        disssmissKeyboard()
    }
    
    @IBAction func sendOTPNotification(sender:UIButton){
        
        Spinner.sharedinstance.startSpinner(viewController: self)
        
        
        let selectedCountryCode = countryCode.getCountryPhoneCode()
        print("\(selectedCountryCode!)")
        print("\(phoneField?.text!)")

        UserDefaults.standard.set(selectedCountryCode!, forKey: "countryCode")
        UserDefaults.standard.set(phoneField?.text!, forKey: "phoneNo")
        
        PhoneAuthProvider.provider().verifyPhoneNumber(("\(selectedCountryCode!)"+(phoneField?.text!)!), uiDelegate: nil) { (verificationID, error) in
            
            print("This is the phone no with country code to verify :\(selectedCountryCode!)\(self.phoneField?.text!)")
            
            Spinner.sharedinstance.stopSpinner(viewController: self)

            if let error = error {
                
                self.dismissAlert(error.localizedDescription)
                return
                
            }
            
            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
            UserDefaults.standard.set(self.phoneField?.text, forKey: "phone")

            
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "newOTPConfirmationScreen") as? newOTPConfirmationScreen
            vc?.varificationID = verificationID!
            
            
            // just animation
                self.pushPresentAnimation()
            self.present(UINavigationController(rootViewController: vc!), animated: false, completion: nil)
        }
    }
}

