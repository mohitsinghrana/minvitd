//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//


import UIKit

class VenueNoImageCollectionViewCell : UICollectionViewCell {
    
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var venue: UILabel!
    
    @IBOutlet weak var contactEmail: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var contactPerson: UILabel!
    @IBOutlet weak var contactNumber: UILabel!
}
