//
//  VenuBottomBar.swift
//  Experiment
//
//  Created by admin on 15/11/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit


class VenuBottomBar: UIView {
    
    var cellID = "hhhhhhh"
    var vc:VenueVC?
    var venus = [Venue]()

    @objc let appsCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.clear
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        appsCollectionView.delegate = self
        appsCollectionView.dataSource = self
        appsCollectionView.isPagingEnabled = true
        appsCollectionView.register(VenueTextCell.self, forCellWithReuseIdentifier: cellID)
        //appsCollectionView.backgroundColor = UIColor.white
        addSubview(appsCollectionView)
        
        appsCollectionView.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
        appsCollectionView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        appsCollectionView.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        appsCollectionView.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension VenuBottomBar: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return venus.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! VenueTextCell
        let dateStamp = venus[indexPath.row].eventTimestamp
        let date = Date(timeIntervalSince1970: Double(dateStamp!))
        cell.label.text = date.venueDate()
        
        if indexPath.row == vc?.currentIndex {
            cell.label.backgroundColor = UIColor.darkGray
            cell.label.textColor = UIColor.lightGray
       }else {
            cell.label.textColor = UIColor.white
            cell.label.backgroundColor = UIColor.black
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 2*frame.height, height: frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        let first = collectionView.cellForItem(at: IndexPath(item: 0, section: 0)) as! VenueTextCell
        first.label.textColor = UIColor.white
        first.label.backgroundColor = UIColor.black
        
        vc?.currentIndex = indexPath.row
        let cell = collectionView.cellForItem(at: indexPath) as! VenueTextCell

        cell.label.backgroundColor = UIColor.darkGray
        cell.label.textColor = UIColor.lightGray
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! VenueTextCell

        cell.label.textColor = UIColor.white
        cell.label.backgroundColor = UIColor.black
    }
}

class VenueTextCell:UICollectionViewCell{
    
    let label = { () -> UILabel in
        let img = UILabel()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.textAlignment = .center
        img.numberOfLines = 2
        img.textColor = .white
        return img
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(label)
        
        label.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
        label.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        label.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        label.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        label.backgroundColor = isSelected ? .white : .black        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
