
//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//


import UIKit
import SlideMenuControllerSwift
import Alamofire
import AlamofireImage
import IDMPhotoBrowser

class OurStoryViewController : UIViewController , IDMPhotoBrowserDelegate {
    
    //@objc var photos : [IDMPhoto] = [IDMPhoto]()
    @objc var browser:IDMPhotoBrowser? = nil
    var photos : [IDMPhotoCustom2] = [IDMPhotoCustom2]()
    
    var stories: [MInvtdAllFeed] = [MInvtdAllFeed]()
    @objc var invitationID : String = ""
    @objc let placeholderImage = UIImage(named: "eventCoverImage")!
    
    @IBOutlet weak var storyCollectionView: UITableView!
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        browser?.delegate = self
        
        storyCollectionView.delegate = self
        storyCollectionView.dataSource = self
        storyCollectionView.allowsSelection = true
        
        self.title = "Our Story"
        // Do any additional setup after loading the view, typically from a nib.
        navigationBarSetup(nil)

        let button1 = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(back))
        
        self.navigationItem.leftBarButtonItem = button1
    }
    
    @objc  func back(_ sender: Any) {
        pushpopAnimation()
        dismiss(animated: false, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getStories()
    }

    @objc func getStories(){
        
        invitationID = UserDefaults.standard.string(forKey: "invitationID")!

        apiCallingMethod(apiEndPints: "storytimeline", method: .get, parameters: [ "invitationID":invitationID ]) { (data1:Any?, totalobj:Int?) in
            
            if let data = data1 as? [[String : Any]] {
                let stories = MInvtdAllFeed.allFeedsFromResults(data as [[String : AnyObject]])
                self.stories = stories
                DispatchQueue.main.async {
                    self.storyCollectionView.reloadData()
                }
            }
        }
    }
}

// MARK: - InvitationViewController: UICollectionViewDelegate, UICollectionViewDataSource

extension OurStoryViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stories.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat((UIScreen.main.bounds.height - 60) / 4)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let story = stories[indexPath.row]
        let cell = storyCollectionView.dequeueReusableCell(withIdentifier: "ourStoryCell", for: indexPath) as! OurStoryCollectionViewCell
        cell.data = story
        cell.likedLabel.text = story.likes?.description
        cell.controler = self
        cell.storyImage.tag = indexPath.row
        let imageTap = UITapGestureRecognizer(target: self, action: #selector(self.imageTapFunc(_:)))
        imageTap.numberOfTapsRequired = 1
        cell.storyImage.isUserInteractionEnabled = true
        cell.storyImage.addGestureRecognizer(imageTap)


        if( indexPath.row % 2 == 0 ){
            cell.backgroundColor = UIColor.white
        }else {
            cell.backgroundColor = UIColor(red: 245 / 255, green: 245 / 255, blue: 245 / 255, alpha: 1.0)
        }
        
        if indexPath.row == 0 || (indexPath.row) % 2 == 0{
            
            cell.roundImageWidth.constant = 26
            cell.roundImageHeight.constant = 26
            cell.roundImage.layer.cornerRadius = 13
            cell.roundImage.layer.borderWidth = 4.0
            cell.roundImage.layer.borderColor = UIColor(red: 181 / 255, green: 181 / 255, blue: 181 / 255, alpha: 1.0).cgColor
        
        }else{

            cell.roundImageWidth.constant = 16
            cell.roundImageHeight.constant = 16
            cell.roundImage.layer.cornerRadius = 8
            cell.roundImage.layer.borderWidth = 4.0
            cell.roundImage.layer.borderColor = UIColor(red: 181 / 255, green: 181 / 255, blue: 181 / 255, alpha: 1.0).cgColor
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = self.storyCollectionView.cellForRow(at: indexPath) as! OurStoryCollectionViewCell
        let story = stories[(indexPath as NSIndexPath).row]
        self.likeAction(postID : story.postID!, type: story.type!, likeID: story.likeID!, eventID: story.eventID!)
        if story.liked == true{
            cell.likeImage.image = #imageLiteral(resourceName: "likeFilled")
        }else{
            cell.likeImage.image = #imageLiteral(resourceName: "like")
        }
    }

    @objc func imageTapFunc(_ sender: UITapGestureRecognizer) {
        
        let rowInt  = sender.view?.tag
        let story  = stories[rowInt!]
        
        photos = [IDMPhotoCustom2]()
        let tag = sender.view?.tag
        
        var counter  = 1
        let total = self.stories.count
        
        for photo in self.stories {
            let currentPhoto = IDMPhotoCustom2(counter : "\(counter) of \(String(describing: total))"   ,imageUrl: URL(string : photo.imageUrl!)!, detail: photo.description, title: photo.title)
            photos.append(currentPhoto)
            counter  = counter + 1
        }
        
        browser = IDMPhotoBrowser.init(photos: photos)
        browser?.delegate = self
        browser?.displayCounterLabel = true
        browser?.displayActionButton = false
        browser?.doneButtonRightInset = CGFloat(5.0)
        browser?.doneButtonTopInset = CGFloat(30.0)
        browser?.doneButtonImage = UIImage(named: "cross")
        browser?.autoHideInterface = false
        browser?.setInitialPageIndex(UInt(rowInt!))
        present(browser!, animated: true, completion: nil)
    }
    
    @objc func likeAction(postID : Int, type: Int, likeID: Int, eventID: Int){
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")
        let userName = UserDefaults.standard.string(forKey: "name")!
        let iName = UserDefaults.standard.string(forKey: "invitationTitle")
        
        let parameters: Parameters = [
            "postID" : postID as AnyObject,
            "invitationID" : invitationID as AnyObject,
            "userID" : userID as AnyObject,
            "userName":userName,
            "type" : type,
            "likeID": likeID,
            "iName": iName!,
            "eventID": eventID
        ]
        
        apiCallingMethod(apiEndPints: "like", method: .post, parameters: parameters) { (data1:Any?, totalobj:Int?) in
            if data1 != nil {
                self.getStories()
            }
        }
    }
}
