//
//  NotificationOuterViewController.swift
//  Minvtd
//
//  Created by Tushar Aggarwal on 30/07/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class NotificationOuterViewController: UIViewController {
    
    var notis : [MInvitdNoti] = [MInvitdNoti]()
    @IBOutlet weak var myTableView: UITableView!
    var allFeeds: [MInvtdAllFeed] = [MInvtdAllFeed]()
    var allSubComments: [MInvtdAllFeedSubComment]? = [MInvtdAllFeedSubComment]()
    var paramaters : MInvtdAllFeed?
    @objc var origin : String = "feed"
    
    var invitationsObj: InvitationViewController?
    
    override func viewDidLoad() {
    
        super.viewDidLoad()
        myTableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine

        myTableView.delegate = self
        myTableView.dataSource = self
        myTableView.estimatedRowHeight = 60
        self.view.layer.cornerRadius = 10.0
        self.view.layer.masksToBounds = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.layoutIfNeeded()
    }
    
    @IBAction func dismissVC(_ sender: UIBarButtonItem) {
        // new stuff
               if let parentVC =  self.parent {
            
                
            if let InvitationViewController = parentVC as? InvitationViewController {
                
                InvitationViewController.blurEffectView.removeFromSuperview()
                self.view.removeFromSuperview()
                self.removeFromParentViewController()
            }
        }
        invitationsObj?.invitationsCollectionView.reloadData()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        notis = []
        myTableView.reloadData()
        gettingAllNotification()
    }
    
    
    @objc func gettingAllNotification() {
        
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")!
        let userID = UserDefaults.standard.string(forKey: "userID")!
        
        
        apiCallingMethod(apiEndPints: "notifications", method: .get, parameters: ["invitationID":invitationID,"userID":userID]) { (data1:Any?, totalobj:Int?) in
            
            if let data = data1 as? [[String : Any]]
            {
                let notis = MInvitdNoti.notiFromResults(data as [[String : AnyObject]])
                self.notis = notis
                
                //print("This is Notification list :\n \(self.notis)")
                DispatchQueue.main.async {
                    
                    self.myTableView.reloadData()
                    
                }
            }
        }
    }
}

extension NotificationOuterViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return notis.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

            let noti = notis[(indexPath as NSIndexPath).row]
            let cell = myTableView.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath) as! NotificationTableViewCell
            
            cell.title.text = noti.message
            cell.userImage.af_setImage(withURL: URL(string : noti.fileUrl!)!)
            
            cell.userImage.contentMode = .scaleAspectFill
            cell.userImage.layer.cornerRadius  =  cell.userImage.frame.size.width/2
            cell.userImage.layer.masksToBounds = true
            cell.userImage.clipsToBounds = true
            
//            let dateFormater = DateFormatter()
//            dateFormater.dateFormat = "MM/dd/yy"
//            let date = dateFormater.string(from: NSDate(timeIntervalSince1970: noti.created!) as Date)
//            let string = "🕒  " + date
//            let string1 = " " + date
        
            cell.date.text = noti.createdTimeAgo
            
            let moduleType : String = noti.module!
            if moduleType == "chat"{
                cell.typeImage.image = UIImage(named : "chatActive")
            }else if moduleType == "comment"{
                cell.typeImage.image = UIImage(named : "comment")
            }else if moduleType == "post"{
                cell.typeImage.image = UIImage(named : "feedsActive")
            }else if moduleType == "image"{
                cell.typeImage.image = UIImage(named : "camera")
            }else if moduleType == "video"{
                cell.typeImage.image = UIImage(named : "videoPlayIcon")
            }else if moduleType == "gallery"{
                cell.typeImage.image = UIImage(named : "photoGalleryIcon")
            }else if moduleType == "story"{
                cell.typeImage.image = UIImage(named : "photoGalleryIcon")
            }else if moduleType == "album"{
                cell.typeImage.image = UIImage(named : "photoGalleryIcon")
            }else if moduleType == "likes"{
                cell.typeImage.image = UIImage(named : "heartLiked")
            }else if moduleType == "invitation"{
                cell.typeImage.image = UIImage(named : "globesActive")
            }else if moduleType == "event"{
                cell.typeImage.image = UIImage(named : "eventsInactive")
            }else if moduleType == "vendor"{
                cell.typeImage.image = UIImage(named : "globesActive")
            }else{
                cell.typeImage.image = UIImage(named : "homeActive")
            }
            cell.typeImage.image = cell.typeImage.image?.withRenderingMode(.alwaysTemplate)
            let colour1 = UserDefaults.standard.string(forKey: "colour1")
            cell.typeImage.tintColor = UIColor(hexString: colour1!)
            
            return cell
        }
    

    
    @objc func submitFunc(status : String )  {
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")!
        
        let parameters: Parameters = ["status" : status  , "userID" : userID  as AnyObject,"invitationID": invitationID]
 
        apiCallingMethod(apiEndPints: "invitation/notify", method: .post, parameters: parameters) { (data1:Any?, ObjCount:Int?) in
            if data1 != nil {
            }
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let noti = notis[indexPath.row]
        
        var commentID = noti.pid
        //print("This is notification data:\n \(noti)")
        
        if noti.module == "likes" || noti.module == "comment" || noti.module == "gallery" || noti.module == "image" || noti.module == "video" || noti.module == "story" || noti.module == "guests" || noti.module == "album" || noti.module == "post"{
            
          self.getFeedsRequest( invitationID: noti.invitationID!, postID: noti.postID! )
        }
        else if noti.module == "chat"  {
            
            //print(noti)
            
            let invitationID = UserDefaults.standard.integer(forKey: "invitationID")
            
            guard let str = noti.rid as? String else {
                return}
            
            let chatroom =  MInvitdchatList(chatID: str, name: noti.creatorName!, invitationID: invitationID)
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Chat") as! UINavigationController
            let a = vc.viewControllers[0] as? ChatVC
            a?.chatlist = chatroom
            self.present(vc, animated: true, completion: nil)
            
        }
        else if noti.module == "vendor"{
            //print("vendor need to be added")
        }
    }
    
    @objc func getFeedsRequest(invitationID : Int, postID : Int){
        
        Spinner.sharedinstance.startSpinner(viewController: self)
        // Activity Incator
//        let invitationID = UserDefaults.standard.string(forKey: "invitationID")!
//        let userID = UserDefaults.standard.string(forKey: "userID")!
        
        var params = Parameters()
        
//        if self.origin == "feed"{
//            params = ["commentID":commentID,"invitationID":invitationID,"userID":userID,"limitSubComment":"1000"]
//
//        }
        params = [
            "postID": postID,
            "invitationID": invitationID,
            "limitSubComment": 1000
        ]
        
        //print("These are params \n\(params)")
        
        apiCallingMethod(apiEndPints: "comments", method: .get, parameters: params) { (data:Any?, totalobj:Int?) in
            
            Spinner.sharedinstance.stopSpinner(viewController: self)
            
            if let data1 = data as? [[String : AnyObject]]{
                //print("These are params \n\(data1)")
                
                if let allFeeds1 = MInvtdAllFeed.allFeedsFromResults(data1 ) as? [MInvtdAllFeed] {
                    self.allFeeds = allFeeds1
                    self.paramaters = allFeeds1[0]
                    if allFeeds1[0].subCommentArray != nil
                    {
                        self.allSubComments = allFeeds1[0].subCommentArray!
                    }
                    
                    // Close Activity Indicator
                    
                    if self.paramaters?.fileType == "text"{
                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let mainVC = storyBoard.instantiateViewController(withIdentifier: "feedCommentVC") as! FeedCommentVC
                        mainVC.paramaters = self.paramaters
                        mainVC.origin = "feed"
                        self.present( UINavigationController(rootViewController:mainVC), animated: true, completion: nil)
                    }else {
                        
                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let mainVC = storyBoard.instantiateViewController(withIdentifier: "feedImageCommentVC") as! FeedImageCommentVC
                        mainVC.paramaters = self.paramaters
                        mainVC.origin = "feed"
                        self.present( UINavigationController(rootViewController:mainVC), animated: true, completion: nil)

                    }
                }
            }
        }
    }
}
