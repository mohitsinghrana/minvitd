//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

// MARK: - MInvitdInvitation

struct MInvitdInvitation{
    
    // MARK: Properties
    
    let invitationID : Int?
    let creatorID : Int?
    let userID : Int?
    let category : String?
    let type : String?
    let name : String?
    let description : String?
    let imageUrl : String?
    let status : String?
    let created : Double?
    let updated : String?
    let date : String?
    let showRsvp : Bool?
    let colour : String?
    let colour_2 : String?
    let notificationsCount : Int?
    let albumBgUrl:String?
    let receiveNotifications : Int?
    let coverUrl : String?
    let userRole:String?
    let feedStatus:String?
    // MARK: Initializers
    
    // construct a dictionary
    init(dictionary: [String:AnyObject]) {
        invitationID = dictionary["invitationID"] as? Int
        creatorID = dictionary["creatorID"] as? Int
        userID = dictionary["userID"] as? Int
        category = dictionary["category"] as? String
        type = dictionary["type"] as? String
        name = dictionary["name"] as? String
        description = dictionary["description"] as? String
        imageUrl = dictionary["imageUrl"] as? String
        status = dictionary["status"] as? String
        created = dictionary["created"] as? Double
        updated = dictionary["updated"] as? String
        date = dictionary["date"] as? String
        showRsvp = dictionary["showRsvp"] as? Bool
        colour = dictionary["colour"] as? String
        colour_2 = dictionary["colour_2"] as? String
        albumBgUrl = dictionary["albumBgUrl"] as? String
        receiveNotifications = dictionary["receiveNotifications"] as? Int
        notificationsCount = dictionary["notificationsCount"] as? Int
        coverUrl = dictionary["coverUrl"] as? String
        userRole = dictionary["userRole"] as? String
        feedStatus = dictionary["feedStatus"] as? String
    }
    
    static func invitationsFromResults(_ results: [[String:AnyObject]]) -> [MInvitdInvitation] {
        var invitations = [MInvitdInvitation]()
        
        // iterate through array of dictionaries, each Article is a dictionary
        for invitation in results {
            invitations.append(MInvitdInvitation(dictionary: invitation))
        }
        
        return invitations
    }
}
