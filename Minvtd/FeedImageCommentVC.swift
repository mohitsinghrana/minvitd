//
//  FeedImageCommentVC.swift
//  Minvtd
//
//  Created by Tushar Aggarwal on 06/06/17.
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage
import Alamofire
import IDMPhotoBrowser
import SlideMenuControllerSwift

class FeedImageCommentVC : UIViewController , IDMPhotoBrowserDelegate{
    
    @objc var photos : [IDMPhoto] = [IDMPhoto]()
    @objc var browser: IDMPhotoBrowser? = nil
    
    var navigationFromSubComment: Bool = false
    var showDataFlag : Bool = false

    var tagesVC:GuestListProfileVC?
    
    @objc var origin : String = ""
    var allFeeds: [MInvtdAllFeed] = [MInvtdAllFeed]()
    var allSubComments: [MInvtdAllFeedSubComment]? = [MInvtdAllFeedSubComment]()
    
    var paramaters : MInvtdAllFeed?
    @objc let placeholderImage = UIImage(named: "eventCoverImage")!
    @objc var currentIndex : Int = 0
    @objc var headerSuper = AFICommentHeaderCell()
    @objc var cellSuper = AFICommentCVC()
    fileprivate let sectionInsets = UIEdgeInsets(top: 5.0, left: 10.0, bottom: 5.0, right: 10.0)
    fileprivate let itemsPerRow: CGFloat = 1
    var subCommentCasted = MInvtdAllFeed()
    
    var newSubComments: [MInvtdAllFeedSubComment]? = [MInvtdAllFeedSubComment]()
    
    var notificationRedirection: Bool = false
    
    @IBOutlet weak var myCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        browser?.delegate = self
        
        myCollectionView.delegate = self
        myCollectionView.dataSource = self
        
        if self.navigationFromSubComment || self.notificationRedirection{
            
            let navBar: UINavigationBar = UINavigationBar(frame: CGRect(x: 0, y: 20, width: UIScreen.main.bounds.width, height: 44))
            navBar.isTranslucent = false
            self.view.addSubview(navBar)
            let navItem = UINavigationItem(title: "Comments")
            let doneItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissVC));
            navItem.rightBarButtonItem = doneItem;
            navBar.setItems([navItem], animated: false)
            navigationBarSetup(navBar)
            myCollectionView.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: 0, right: 0)
            
            //statusbarSetup
            let layer = CAGradientLayer()
            layer.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 20)
            let colour1 = UserDefaults.standard.string(forKey: "colour1") ?? "#E12459"
            let colour2 = UserDefaults.standard.string(forKey: "colour2") ?? "#E12459"
            layer.colors = [UIColor.init(hexString: colour1)?.cgColor, UIColor.init(hexString: colour2)?.cgColor]
            layer.startPoint = CGPoint(x: 0.0, y: 0.5)
            layer.endPoint = CGPoint(x: 1.0, y: 0.5)
            
            let view = UIView()
            view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 20)
            view.layer.addSublayer(layer)
            self.view.addSubview(view)
            
        }else{
            navigationBarSetup(nil)
            title = "Comments"
            let button2 = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissVC))
            self.navigationItem.rightBarButtonItem = button2
        }
    }
    @IBAction func TagTapped(_ sender: Any) {
        
        let users = allFeeds.first?.users
        
        if !(users?.isEmpty)!{
            tagesVC =  GuestListProfileVC()
            tagesVC?.guests = users!
            self.present(UINavigationController(rootViewController: tagesVC!), animated: true, completion: nil)
        }
        
    }
    
    @objc func dismissVC(_ sender: UIBarButtonItem) {
        
        if self.notificationRedirection{
            
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let leftDrawerVC = storyBoard.instantiateViewController(withIdentifier: "SlideMenuViewController")
            let mainVC = storyBoard.instantiateViewController(withIdentifier: "invitationVC") as! InvitationViewController
            let leftDrawer = SlideMenuController(mainViewController: UINavigationController(rootViewController:mainVC), leftMenuViewController: leftDrawerVC)
            //getUserData()
            appdelegate.window =  UIWindow(frame: UIScreen.main.bounds)
            appdelegate.window?.rootViewController = leftDrawer
            appdelegate.window?.makeKeyAndVisible()
            
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func dissmisskeyboard(){
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getFeedsRequest()
    }
    
    @objc func getFeedsRequest(){
        
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")!
        
        var params:Parameters = [:]
     
        
//        if self.origin == "feed"{
//            params = ["invitationID":invitationID,"userID":userID,"limitSubComment":"10000"]
//        }else if self.origin == "subComment"{
//            params = ["invitationID":invitationID,"userID":userID,"limitSubComment":"10000"]
//        }else{
//            let postID = (self.paramaters?.postID!) ?? 0
//            let eventID = UserDefaults.standard.string(forKey: "eventID")!
//            params = ["commentID":postID,"invitationID":invitationID,"limitSubComment":"1000","eventID":eventID]
//        }
        
        var postID : Int = 0
        var eventID : String = "0"
        
        if self.origin == "feed"{
            postID = (self.paramaters?.postID!)!
        }
        else if self.origin == "subComment"{
            postID = (self.allSubComments?.first?.postID)!
            eventID = UserDefaults.standard.string(forKey: "eventID")!
        }
        else{
            postID = (self.allSubComments?.first?.postID)!
            eventID = UserDefaults.standard.string(forKey: "eventID")!
        }
        params = [
            "invitationID"  : invitationID,
            "eventID"       : eventID,
            "postID"        : postID,
            "limitSubComment":1000,
        ]
        
        print("these are the params ->\(params)")
        
        apiCallingMethod(apiEndPints: "comments", method: .get, parameters: params) { (data1:Any?, totalobj:Int?) in
            
            if let data = data1 as? [[String : Any]]
            {
                
                print("This is data in comments API:*** \n\(data)")
                
                let allFeeds = MInvtdAllFeed.allFeedsFromResults(data as [[String : AnyObject]])
                self.allFeeds = allFeeds
                if self.navigationFromSubComment{
                    self.paramaters = allFeeds.first
                }
                self.newSubComments = (allFeeds.first?.subCommentArray)
                DispatchQueue.main.async {
                    self.myCollectionView.reloadData()
                }
                self.showDataFlag = true
                
            }else {
                self.allFeeds  = []
                self.myCollectionView.reloadData()
            }
        }
    }
}

extension FeedImageCommentVC : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout , UITextFieldDelegate {
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if self.newSubComments == nil{
            
            let subComment = allFeeds[indexPath.row]
            
            var additionalSize : Int  = Int()
            if ( subComment.comment ?? "").isEmpty{
                additionalSize = 0
            }else{
                
                let width = UIScreen.main.bounds.width
                var maxChars = 50
                if width == CGFloat(320) {
                    maxChars = 45
                }else if width == CGFloat(375){
                    maxChars = 50
                }else{
                    maxChars = 55
                }
                let countLetter =  subComment.comment?.characters.count
                additionalSize  = (countLetter! / maxChars) * 18
                let labelTokens =  subComment.comment?.components(separatedBy: "\n")
                additionalSize  += ((labelTokens?.count)! - 1 ) * 18
            }
            
            let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
            let availableWidth = view.frame.width - paddingSpace
            let widthPerItem = availableWidth / itemsPerRow
            let height:CGFloat = 80 + 51
            
            return CGSize(width : widthPerItem, height: height + CGFloat(additionalSize) )
            
        }else{
            let subComment = self.newSubComments![indexPath.row]
            
            var additionalSize : Int  = Int()
            if ( subComment.comment ?? "").isEmpty{
                additionalSize = 0
            }else{
                
                let width = UIScreen.main.bounds.width
                var maxChars = 50
                if width == CGFloat(320) {
                    maxChars = 45
                }else if width == CGFloat(375){
                    maxChars = 50
                }else{
                    maxChars = 55
                }
                let countLetter =  subComment.comment?.characters.count
                additionalSize  = (countLetter! / maxChars) * 18
                let labelTokens =  subComment.comment?.components(separatedBy: "\n")
                additionalSize  += ((labelTokens?.count)! - 1 ) * 18
            }
            
            let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
            let availableWidth = view.frame.width - paddingSpace
            let widthPerItem = availableWidth / itemsPerRow
            let height:CGFloat = 80 + 51
            
            return CGSize(width : widthPerItem, height: height + CGFloat(additionalSize) )
        }
        
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if(!showDataFlag){
            return 0
        }
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        

            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "afiCommentCVC", for: indexPath) as! AFICommentCVC
        
            if self.newSubComments == nil{
                cell.isHidden = true
                
                return cell
            }else{
                let subComment = self.newSubComments
                    //allFeeds[indexPath.row]
                
            //    print("This is allFeeds : \n\(allFeeds)")
                
                cell.subComment.text = subComment![indexPath.row].comment
                cell.subUserName.text = subComment![indexPath.row].userName
                
                cell.likeLabel.text = " " + (subComment![indexPath.row].likes?.description)!
                cell.likeButton.setImage( (subComment![indexPath.row].liked!) ?  #imageLiteral(resourceName: "likeFilled") .withRenderingMode(.alwaysOriginal):  #imageLiteral(resourceName: "like").withRenderingMode(.alwaysOriginal) , for: .normal)
                cell.commentLabel.text = " " + (subComment![indexPath.row].subCommentsCount?.description)!
                cell.commentButton.tag = indexPath.row
                cell.likeButton.tag = indexPath.row
                cell.likeButton.addTarget(self, action: #selector(likeMeCell(sender:)), for: .touchUpInside)
                cell.commentButton.addTarget(self, action: #selector(commentAction(sender:)), for: .touchUpInside)
                
        
                let imageAttachment =  NSTextAttachment()
                imageAttachment.image = UIImage(named:"clock-1")
                let imageOffsetY:CGFloat = -5.0;
                imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: imageAttachment.image!.size.width, height: imageAttachment.image!.size.height)
                let attachmentString = NSAttributedString(attachment: imageAttachment)
                var completeText = NSMutableAttributedString(string: "")
                completeText.append(attachmentString)

                
                let myString =  subComment![indexPath.row].created!.socialTime()
                cell.date.text = myString
                
                
                
                let filter = AspectScaledToFillSizeWithRoundedCornersFilter(
                    size: cell.subUserImage.frame.size,
                    radius: cell.subUserImage.bounds.height/2 + 5
                )
                
                cell.subUserImage.af_setImage(
                    withURL: URL(string: (subComment![indexPath.row].userImageUrl!))! ,
                    placeholderImage: placeholderImage,
                    filter : filter
                )
                
                cell.layer.shadowColor = UIColor.gray.cgColor
                cell.layer.shadowOffset = CGSize(width: 0, height: 1)
                cell.layer.shadowOpacity = 1
                cell.layer.shadowRadius = 1.0
                cell.clipsToBounds = false
                cell.layer.masksToBounds = false
                cell.layer.cornerRadius = 2.5
                
                return cell
            }
    }
    
    
    @objc func likeMeCell(sender:UIButton){
        // let subComment = allFeeds[sender.tag]
        
        let index = sender.tag
        
        if  self.newSubComments![index].liked == true {
            self.newSubComments![index].likes! -= 1
            self.newSubComments![index].liked = false
        }else {
            self.newSubComments![index].likes! += 1
            self.newSubComments![index].liked = true
        }
        
        self.myCollectionView.reloadItems(at: [IndexPath(item: index, section: 0)])
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        let userName = UserDefaults.standard.string(forKey: "name")!
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")!
        
        let parameters: Parameters = ["postID" : self.allFeeds[index].postID as Any ,
                                      "userID" : userID as AnyObject,
                                      "userName":userName,
                                      "invitationID":invitationID,
                                      "type": self.newSubComments![index].type!,
                                      "likeID": self.newSubComments![index].likeID!
                                       ]
        
        apiCallingMethod(apiEndPints: "like", method: .post, parameters: parameters) { (data1:Any?, totalobj:Int?) in
            
            if data1 != nil{
                
            }
            
        }
        
    }
    
    @objc func commentAction( sender:UIButton){
        
     //   print("Clicked")
        let subComment = self.newSubComments![sender.tag]
            //allFeeds[sender.tag]
        
        subCommentCasted.comment = subComment.comment
        subCommentCasted.commentID = subComment.commentID
        subCommentCasted.created = subComment.created
        subCommentCasted.date = subComment.date
        subCommentCasted.description = subComment.description
        subCommentCasted.eventID = subComment.eventID
        subCommentCasted.fileThumbnail = subComment.fileThumbnail
        subCommentCasted.fileType = subComment.fileType
        subCommentCasted.fileUrl = subComment.fileUrl
        subCommentCasted.invitationID = subComment.invitationID
        subCommentCasted.likes = subComment.likes
        subCommentCasted.liked = subComment.liked
        subCommentCasted.postID = subComment.postID
        subCommentCasted.userID = subComment.userID
        subCommentCasted.subCommentArray = subComment.subCommentArray
        subCommentCasted.subCommentsCount = subComment.subCommentsCount
        subCommentCasted.title = subComment.title
        subCommentCasted.typeName = subComment.typeName
        subCommentCasted.userImageUrl = subComment.userImageUrl
        subCommentCasted.userName = subComment.userName
        
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "feedCommentVC") as! FeedCommentVC
        mainVC.paramaters = subCommentCasted
        mainVC.origin = "feed"
        mainVC.allFeeds = [subCommentCasted]
        self.present(UINavigationController(rootViewController: mainVC), animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        //return allFeeds.count
        
        if self.newSubComments == nil{
            return allFeeds.count
        }else{
            return self.newSubComments!.count
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
      //  print("Clicked")
        
        let subComment = self.newSubComments![(indexPath as NSIndexPath).row]
        //allFeeds[(indexPath as NSIndexPath).row]
        subCommentCasted.comment = subComment.comment
        subCommentCasted.commentID = subComment.commentID
        subCommentCasted.created = subComment.created
        subCommentCasted.date = subComment.date
        subCommentCasted.description = subComment.description
        subCommentCasted.eventID = subComment.eventID
        subCommentCasted.fileThumbnail = subComment.fileThumbnail
        subCommentCasted.fileType = subComment.fileType
        subCommentCasted.fileUrl = subComment.fileUrl
        subCommentCasted.invitationID = subComment.invitationID
        subCommentCasted.postID = subComment.postID
        subCommentCasted.userID = subComment.userID
        subCommentCasted.likes = subComment.likes
        subCommentCasted.liked = subComment.liked
        subCommentCasted.subCommentArray = subComment.subCommentArray
        subCommentCasted.subCommentsCount = subComment.subCommentsCount
        subCommentCasted.title = subComment.title
        subCommentCasted.typeName = subComment.typeName
        subCommentCasted.userImageUrl = subComment.userImageUrl
        subCommentCasted.userName = subComment.userName
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "feedCommentVC") as! FeedCommentVC
        mainVC.paramaters = subCommentCasted
        mainVC.allFeeds = [subCommentCasted]
        mainVC.origin = "feed"
        self.present(UINavigationController(rootViewController: mainVC), animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let cell = self.myCollectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerImageCommentCell", for: indexPath) as! AFICommentHeaderCell
 
        
        
        cell.users = (self.paramaters?.users)!
        
        cell.userName.text = self.paramaters?.userName

        cell.userImage.af_setImage(
            withURL: URL(string: (self.paramaters?.userImageUrl!)!)! ,
            placeholderImage: placeholderImage
        )
        
        cell.userImage.layer.cornerRadius = cell.userImage.frame.width / 2
        cell.userImage.clipsToBounds = true
        cell.userImage.contentMode = .scaleAspectFill
        
        if self.paramaters?.liked! == true{
            
            var image = UIImage(named: "likeFilled")
            image = image?.withRenderingMode(.alwaysOriginal)
            let colour1 = UserDefaults.standard.string(forKey: "colour1")
            cell.heartBtn.setImage(image, for: .normal)
            //cell.tintColor = UIColor(hexString: colour1!)
            
        }else{
            
            var image = UIImage(named: "like")
            image = image?.withRenderingMode(.alwaysOriginal)
            let colour1 = UserDefaults.standard.string(forKey: "colour1")
            cell.heartBtn.setImage(image, for: .normal)
            //cell.tintColor = UIColor(hexString: colour1!)
            
        }
        
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "MM/dd/yy"
        
        if let myString = self.paramaters?.created!.socialTime(){
            cell.created.text = myString
        }
        
        cell.comment.text =  self.paramaters?.comment
        
        cell.likedLabel.text = "\(String(describing: (self.paramaters?.likes)!))"
        cell.subCommentCount.text = "\(String(describing: (self.paramaters?.subCommentsCount)!))"
        cell.postSubCommentTF.layer.borderWidth = 0.5
        cell.postSubCommentTF.layer.borderColor = UIColor.gray.cgColor
        cell.postSubCommentTF.placeholder = "Write a comment..."
        cell.postSubCommentTF.delegate = self
        cell.heartBtn.tag = indexPath.row
        cell.commentBtn.tag  = indexPath.row
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 1)
        cell.layer.shadowOpacity = 1
        cell.layer.shadowRadius = 1.0
        cell.clipsToBounds = false
        cell.layer.masksToBounds = false
        cell.layer.cornerRadius = 2.5
        cell.image.tag = indexPath.row
        
        
        
        if self.paramaters?.typeName == "image" {
            
            cell.playIcon.isHidden = true
            cell.galleryButton.isHidden = true
            
          //  print("This is self.parameters.fileUrl in feedcommentVC headerview *** \n\(self.paramaters?.fileUrl!)")
            
            // Remove all existing targets (in case cell is being recycled)
            cell.playIcon.removeTarget(nil, action: nil, for: .allEvents)
            cell.image.af_setImage(
                withURL: URL(string: (self.paramaters?.fileUrl!)!)! ,
                placeholderImage: placeholderImage
            )
            
            
            let imageTap = UITapGestureRecognizer(target: self, action: #selector(self.imageTapFunc(_:)))
            imageTap.numberOfTapsRequired = 1
            cell.image.isUserInteractionEnabled = true
            cell.image.addGestureRecognizer(imageTap)
            
        }else if self.paramaters?.typeName == "video"{
            
          
            cell.playIcon.isHidden = false
            cell.galleryButton.isHidden = true
            
      //      print("This is self.parameters.fileUrl in feedcommentVC headerview *** \n\(self.paramaters?.fileUrl!)")
            
            cell.image.af_setImage(
                withURL: URL(string: (self.paramaters?.fileThumbnail!)!)! ,
                placeholderImage: placeholderImage
            )
            
            let videoTap = UITapGestureRecognizer(target: self, action: #selector(self.videoTapFunc(_:)))
            videoTap.numberOfTapsRequired = 1
            cell.image.isUserInteractionEnabled = true
            cell.image.addGestureRecognizer(videoTap)
            
        }else if self.paramaters?.typeName == "gallery"{
            
            cell.playIcon.isHidden = true
            cell.galleryButton.isHidden = false
            cell.image.af_setImage(
                withURL: URL(string: (self.paramaters?.fileUrl!)!)! ,
                placeholderImage: placeholderImage
            )
            
            let galleryTap = UITapGestureRecognizer(target: self, action: #selector(self.galleryTapFunc(_:)))
            galleryTap.numberOfTapsRequired = 1
            cell.image.isUserInteractionEnabled = true
            cell.image.addGestureRecognizer(galleryTap)
            
        }else{
            
            cell.playIcon.isHidden = true
            cell.galleryButton.isHidden = true
            
            // Remove all existing targets (in case cell is being recycled)
            cell.playIcon.removeTarget(nil, action: nil, for: .allEvents)
            cell.image.af_setImage(
                withURL: URL(string: (self.paramaters?.fileUrl!)!)! ,
                placeholderImage: placeholderImage
            )
            
            
            let imageTap = UITapGestureRecognizer(target: self, action: #selector(self.imageTapFunc(_:)))
            imageTap.numberOfTapsRequired = 1
            cell.image.isUserInteractionEnabled = true
            cell.image.addGestureRecognizer(imageTap)
        }
        
        
        // Remove all existing targets (in case cell is being recycled)
        cell.heartBtn.removeTarget(nil, action: nil, for: .allEvents)
        cell.commentBtn.removeTarget(nil, action: nil, for: .allEvents)
        
        
        // Add target
        cell.heartBtn.addTarget(self, action: #selector(likeBtnTap(sender:)), for: .touchUpInside)
        cell.commentBtn.addTarget(self, action: #selector(commentBtnTapCell(sender:)), for: .touchUpInside)
        
        self.headerSuper = cell
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        var additionalSize : Int  = Int()
        if ( self.paramaters?.comment ?? "").isEmpty{
            additionalSize = 0
        }else{
            let width = UIScreen.main.bounds.width
            var maxChars = 50
            if width == CGFloat(320) {
                maxChars = 45
            }else if width == CGFloat(375){
                maxChars = 50
            }else{
                maxChars = 55
            }
            let countLetter =   self.paramaters?.comment?.characters.count
            additionalSize  = (countLetter! / maxChars) * 18
            let labelTokens =  self.paramaters?.comment?.components(separatedBy: "\n")
            additionalSize  += ((labelTokens?.count)! - 1 ) * 18
        }
        
        return CGSize(width: self.myCollectionView.bounds.width, height: 452 + CGFloat(additionalSize))
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        let postID = self.paramaters?.postID!
        let string =  textField.text!
        if validateString(string: string){
            
            let invitaionID = UserDefaults.standard.string(forKey: "invitationID")
            let userID = UserDefaults.standard.string(forKey: "userID")
            let userName = UserDefaults.standard.string(forKey: "name")!
            let iName = UserDefaults.standard.string(forKey: "invitationTitle")
            let eventID =  self.paramaters?.eventID!
            
            let parameters : Parameters            
            
            parameters  = ["comment" : string as AnyObject,
                           "userID" : userID as AnyObject,
                           "invitationID" : invitaionID as AnyObject,
                           "commentID" : postID as AnyObject,
                           "userName":userName,
                           "iName":iName!,
                           "eventID" : eventID as AnyObject]
            
            apiCallingMethod(apiEndPints: "comment", method: .post, parameters: parameters, complition: { (data1:Any?, totalobj:Int?) in
                
                self.headerSuper.postSubCommentTF.text = ""
                
                self.getFeedsRequest()
            })
            
        }else{
            
            let alert = UIAlertController(title: "Minvitd", message: "Field cannot be empty", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: { (action : UIAlertAction) in
                alert.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }
        return true
    }
    
    @objc func validateString(string : String) -> Bool{
        
        let charactersNew = String.trimmingCharacters(string)
        let stringNew = String(describing: charactersNew)
        if string == ""{
            return false
        }
        if stringNew == ""{
            return false
        }
        return true
    }
    
    
    
    @objc func likeBtnTap(sender : UIButton){
        
        let currentIndex = sender.tag
        let userID = UserDefaults.standard.string(forKey: "userID")
        let userName = UserDefaults.standard.string(forKey: "name")!
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")!
        let iName = UserDefaults.standard.string(forKey: "invitationTitle")

        
        let parameters: Parameters = [
                                      "postID" : self.paramaters!.postID as AnyObject,
                                      "userID" : userID as AnyObject,
                                      "userName":userName,
                                      "invitationID":invitationID ,
                                      "type": self.allFeeds[currentIndex].type!,
                                      "likeID": self.allFeeds[currentIndex].likeID!,
                                      "iName": iName!,
                                      "eventID": self.paramaters?.eventID!
                                       ]
        
        if paramaters?.liked == true {
            self.paramaters?.likes! -= 1
            self.paramaters?.liked = false
        }else {
            self.paramaters?.likes! += 1
            self.paramaters?.liked = true
        }
        
        self.myCollectionView.reloadData()
        
        apiCallingMethod(apiEndPints: "like", method: .post, parameters: parameters) { (data1:Any?, totalobj:Int?) in
            
            if data1 != nil{
                self.getFeedsRequest()
            }
        }
    }
    
    @objc func playButtonTap(sender: UIButton) {
        
        let currentIndex = sender.tag
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "videoPlayVC") as! VideoPlayViewController
        mainVC.url = allFeeds[currentIndex].fileUrl!
        self.present(UINavigationController(rootViewController:mainVC), animated: true, completion: nil)
    }
    
    @objc func commentBtnTapCell(sender : UIButton) {
        
        let currentIndex =  sender.tag
    }
    
    @objc func commentBtnTapImageCell(sender : UIButton) {
        
        let currentIndex =  sender.tag
    }
    
    
    @objc func imageTapFunc(_ sender: UITapGestureRecognizer) {
        
        let currentIndex  = sender.view?.tag
        
        photos = [IDMPhoto]()
        let currentPhoto = IDMPhoto(url: URL(string : (self.paramaters?.fileUrl!)!))
        photos.append(currentPhoto!)
        browser = IDMPhotoBrowser.init(photos: photos)
        browser?.displayCounterLabel = false
        browser?.displayActionButton = false
        browser?.doneButtonRightInset = CGFloat(5.0)
        browser?.doneButtonTopInset = CGFloat(30.0)
        browser?.doneButtonImage = UIImage(named: "cross")
        present(browser!, animated: true, completion: nil)
    }
    
    @objc func videoTapFunc(_ sender: UITapGestureRecognizer) {
        
        let currentIndex  = sender.view?.tag
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "videoPlayVC") as! VideoPlayViewController
        mainVC.url = (self.paramaters?.fileUrl)!
        self.present(UINavigationController(rootViewController:mainVC), animated: true, completion: nil)
    }
    
    @objc func galleryTapFunc(_ sender: UITapGestureRecognizer) {
        
        let rowInt  = sender.view?.tag
        let feed  = allFeeds[0].gallery
   
        if feed?.count != 0{
            
            photos = [IDMPhotoCustom]()
            
            var counter = 1
            let total = feed?.count
            
            var filteredFeed: [MInvitdAllFeedTwo] = [MInvitdAllFeedTwo]()
                //[MInvtdAllFeed] = [MInvtdAllFeed]()
            
            
            
            for object in feed!{
                
                if object.fileType == "image"{
                    filteredFeed.append(object)
                }
               
            }
            
            for object in filteredFeed {
                print("this is the no of object \(object)")
                let currentPhoto = IDMPhotoCustom(counter : "\(counter) of \(String(describing: total))" , imageUrl: URL(string : object.fileUrl!)!)
                photos.append(currentPhoto)
                counter  = counter + 1
            }
            
            browser = IDMPhotoBrowser.init(photos: photos)
            browser?.displayCounterLabel = false
            browser?.displayActionButton = false
            browser?.doneButtonRightInset = CGFloat(5.0)
            browser?.doneButtonTopInset = CGFloat(30.0)
            browser?.doneButtonImage = UIImage(named: "cross")
            present(browser!, animated: true, completion: nil)
        }
    }
}
