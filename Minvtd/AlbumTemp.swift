//
//  Copyright © 2017 TheAppGuruz. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class AlbumTemp: UIViewController {

    @IBOutlet weak var imageBack: UIImageView!
    var gallery: [MInvtdAllFeed] = [MInvtdAllFeed]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getAlbum()
    }
    
    @objc func getAlbum(){
      
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")!
        //let userID = UserDefaults.standard.string(forKey: "userID")!
        let prams:Parameters? = [ "invitationID":invitationID ]
        
        apiCallingMethod(apiEndPints: "albums", method: .get, parameters: prams) { (data1:Any?, total:Int?) in
            
            if let data = data1 as? [[String : Any]] {
                let gallery = MInvtdAllFeed.allFeedsFromResults(data as [[String : AnyObject]])
                self.gallery = gallery
                var title = [String]()
                var photoUrl = [String]()
                
                for photo in self.gallery {
                    title.append(photo.comment!)
                    photoUrl.append(photo.fileUrl!)
                }
                
                let vc  = self.storyboard?.instantiateViewController(withIdentifier: "PageStart") as! Alubam
                if (title.count > 0 && photoUrl.count > 0 ){
                    vc.arrPageTitle = title
                    vc.arrPagePhoto = photoUrl
                    vc.gallery = self.gallery
                    
                    self.present(vc, animated: true, completion: nil)
                }
            }
        }
    }
}
