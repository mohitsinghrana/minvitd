//
//  MInvtdAllFeedSubComment.swift
//  Minvtd
//
//  Created by Tushar Aggarwal on 06/06/17.
//  Copyright © 2017 Euroinfotek. All rights reserved.
//



struct MInvtdAllFeedSubComment{
    
    // MARK: Properties
    
    let postID : Int?
    let userID : Int?
    let invitationID : Int?
    let eventID : Int?
    let commentID : Int?
    let fileID : Int?
    let comment : String?
    let created : Double?
    let userName : String?
    let userImageUrl : String?
    var likes : Int?
    var liked : Bool?
    var subCommentsCount : Int?
    let fileUrl : String?
    let fileThumbnail : String?
    let fileType : String?
    var typeName : String?
    var title : String?
    var description : String?
    var date : String?
    var subCommentArray : [MInvtdAllFeedSubComment]?
    var parentTypeName: String?
    var type: Int?
    var likeID: Int?

    // MARK: Initializers
    
    init(postID : Int, userID : Int , invitationID : Int , eventID : Int, commentID : Int , comment : String , created : Double , userName : String , userImageUrl : String, likes : Int , liked : Bool , subCommentsCount : Int , fileUrl : String , fileThumbnail : String , fileType : String , parentTypeName : String , type : Int, likeID: Int, fileID: Int){
        
        self.postID = postID
        self.userID = userID
        self.invitationID = invitationID
        self.eventID = eventID
        self.commentID = commentID
        self.fileID = fileID
        self.comment = comment
        self.created = created
        self.userName = userName
        self.userImageUrl = userImageUrl
        self.likes = likes
        self.liked = liked
        self.subCommentsCount = subCommentsCount
        self.fileUrl = fileUrl
        self.fileThumbnail = fileThumbnail
        self.fileType = fileType
        self.parentTypeName = parentTypeName
        self.type = type
        self.likeID = likeID
    }
    
    init(dictionary: [String:AnyObject]) {
        
        if let subCommentArray = dictionary["subComments"] as? [AnyObject], subCommentArray.isEmpty == false {
            var temp1 = [MInvtdAllFeedSubComment]()
            for i in 0...subCommentArray.count-1 {
                let temp = MInvtdAllFeedSubComment(dictionary: subCommentArray[i] as! [String : AnyObject])
                temp1.append(temp)
            }
            self.subCommentArray = temp1
        } else {
            self.subCommentArray = nil
        }
        
        typeName = dictionary["typeName"] as? String
        description = dictionary[MInvitdClient.JSONResponseKeys.description] as? String
        title = dictionary[MInvitdClient.JSONResponseKeys.title] as? String
        date = dictionary[MInvitdClient.JSONResponseKeys.date] as? String
        postID = dictionary[MInvitdClient.JSONResponseKeys.postID] as? Int
        userID = dictionary[MInvitdClient.JSONResponseKeys.userID] as? Int
        invitationID = dictionary[MInvitdClient.JSONResponseKeys.invitationID] as? Int
        eventID = dictionary[MInvitdClient.JSONResponseKeys.eventID] as? Int
        commentID = dictionary[MInvitdClient.JSONResponseKeys.commentID] as? Int
        fileID = dictionary[MInvitdClient.JSONResponseKeys.fileID] as? Int
        comment = dictionary[MInvitdClient.JSONResponseKeys.comment] as? String
        created = dictionary[MInvitdClient.JSONResponseKeys.created] as? Double
        userImageUrl = dictionary[MInvitdClient.JSONResponseKeys.userImageUrl] as? String
        userName = dictionary[MInvitdClient.JSONResponseKeys.userName] as? String
        likes = dictionary[MInvitdClient.JSONResponseKeys.likes] as? Int
        liked = dictionary[MInvitdClient.JSONResponseKeys.liked] as? Bool
        subCommentsCount = dictionary[MInvitdClient.JSONResponseKeys.subCommentsCount] as? Int
        fileUrl = dictionary[MInvitdClient.JSONResponseKeys.fileUrl] as? String
        fileThumbnail = dictionary[MInvitdClient.JSONResponseKeys.fileThumbnail] as? String
        fileType = dictionary[MInvitdClient.JSONResponseKeys.fileType] as? String
        parentTypeName = dictionary[MInvitdClient.JSONResponseKeys.parentTypeName] as? String
        type = dictionary[MInvitdClient.JSONResponseKeys.type] as? Int
        likeID = dictionary[MInvitdClient.JSONResponseKeys.likeID] as? Int
    }
}

