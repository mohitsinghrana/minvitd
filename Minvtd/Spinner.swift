//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView

class Spinner {
    
    
    static let sharedinstance = Spinner()
    
    private var view1 = NVActivityIndicatorView(frame: CGRect(x: UIScreen.main.bounds.width/2 - 20.0 , y: UIScreen.main.bounds.height/2 - 120 , width: 40, height: 40) , type: .ballTrianglePath, color: UIColor.black, padding: 0.0)
    
    private let blurEffectView = UIVisualEffectView(effect:  UIBlurEffect(style: UIBlurEffectStyle.light))
    
    
    func startSpinner(viewController:UIViewController){
        
        if !blurEffectView.isDescendant(of: viewController.view) {
            
            blurEffectView.frame = UIScreen.main.bounds
            viewController.view.isUserInteractionEnabled = false
            viewController.navigationController?.navigationBar.isUserInteractionEnabled = false
            viewController.view.addSubview(blurEffectView)
            viewController.view.addSubview(view1)
            view1.startAnimating()
            
            
        }
        
        
    }
    
    func  stopSpinner(viewController:UIViewController)  {
        
        if blurEffectView.isDescendant(of: viewController.view) {
            blurEffectView.removeFromSuperview()
            
        }
        viewController.view.isUserInteractionEnabled = true
        viewController.navigationController?.navigationBar.isUserInteractionEnabled = true
        
        
        view1.stopAnimating()
    }
    
    
}

