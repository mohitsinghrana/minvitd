//
//  ConfirmationGuestInvitation.swift
//  Minvtd
//
//  Created by admin on 10/08/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.

import UIKit 

class ConfirmationGuestInvitation: UITableViewController {
    
    @objc let referenceCode = "GuestsCollectionViewCell"
    var guestArray = [MInvitdGuest]()
    var filterData = [MInvitdGuest]()
    var tempSelectedGuests = [MInvitdGuest]()
    var removeGuest : MInvitdGuest?
    
    var searchBar:UISearchBar = {
        let srch = UISearchBar()
        srch.placeholder = "Search"
        return srch
    }()
    
    var isSearching:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.allowsMultipleSelection = true
        tableView.register(ConfirmGuestCell.self, forCellReuseIdentifier: referenceCode)
        
        navigationBarSetup(nil)

        setup()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        searchBar.frame =  CGRect(x: 0, y: 0, width: view.bounds.width - 44.0, height: 44.0)
        searchBar.delegate = self
        tableView.tableHeaderView = searchBar
    }

    override func scrollViewDidScrollToTop(_ scrollView: UIScrollView) {
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
   
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return isSearching ? filterData.count : guestArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let guest = isSearching ? filterData[indexPath.row] : guestArray[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: referenceCode, for: indexPath) as? ConfirmGuestCell
            cell?.guest = guest
        
        if guest.isSelected {
            cell?.accessoryType = .checkmark
        }else{
            cell?.accessoryType = .none
        }
        
        for gg in tempSelectedGuests{
                        if gg.phone == guest.phone{
                            cell?.accessoryType = .checkmark
                        }
                    }
        if tempSelectedGuests.count == 0{
            self.title = "Contacts"
        }
        else{
        self.title = "Selected Contacts" + " (\(tempSelectedGuests.count))"
        }
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return CGFloat(60)
    }

    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        let guest = isSearching ? filterData[indexPath.row] : guestArray[indexPath.row]
        
        let cell = tableView.cellForRow(at: indexPath) as? ConfirmGuestCell
        
        if isSearching {
            if cell?.accessoryType == .none {
                self.filterData[indexPath.row].isSelected = true
                self.tempSelectedGuests.append(guest)
                
                searchBar.text = ""
                searchBar.resignFirstResponder()
                searchBar.setShowsCancelButton(false, animated: true)
                isSearching = false
                self.tableView.reloadData()
            }
            else{
            self.filterData[indexPath.row].isSelected = false
            removeGuest = guest
            print(" THIS IS DE-Select tempSelectedGuests : \n \(removeGuest)")
            
            var filter = [MInvitdGuest]()
            for guest in tempSelectedGuests {
                if guest.phone != removeGuest?.phone{
                    filter.append(guest)
                }
            }
            tempSelectedGuests = filter
            cell?.accessoryType = .none
            searchBar.text = ""
            searchBar.resignFirstResponder()
            searchBar.setShowsCancelButton(false, animated: true)
            isSearching = false
            self.tableView.reloadData()
            }
        }
        else{
            if cell?.accessoryType == .none {
                
                self.guestArray[indexPath.row].isSelected = true
                self.tempSelectedGuests.append(guest)
                cell?.accessoryType = .checkmark
                self.tableView.reloadData()
                
            }
            else{
            self.guestArray[indexPath.row].isSelected = false
            removeGuest = guest
            print(" THIS IS DE-Select tempSelectedGuests : \n \(removeGuest)")
            
            var filter = [MInvitdGuest]()
            for guest in tempSelectedGuests {
                if guest.phone != removeGuest?.phone{
                    filter.append(guest)
                }
            }
            tempSelectedGuests = filter
            cell?.accessoryType = .none
            self.tableView.reloadData()
            }
        }
                print(" THIS IS filter Select tempSelectedGuests : \n \(self.tempSelectedGuests)")
    }

   override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        let guest = isSearching ? filterData[indexPath.row] : guestArray[indexPath.row]
    
        let cell = tableView.cellForRow(at: indexPath) as? ConfirmGuestCell
    
    if isSearching {
        
        if cell?.accessoryType == .checkmark {
            self.filterData[indexPath.row].isSelected = false
            removeGuest = guest
            print(" THIS IS DE-Select tempSelectedGuests : \n \(removeGuest)")
            
            var filter = [MInvitdGuest]()
            for guest in tempSelectedGuests {
                if guest.phone != removeGuest?.phone{
                    filter.append(guest)
                }
            }
            tempSelectedGuests = filter
            cell?.accessoryType = .none
            searchBar.text = ""
            searchBar.resignFirstResponder()
            searchBar.setShowsCancelButton(false, animated: true)
            isSearching = false
            self.tableView.reloadData()

        }
        else{
        
        self.filterData[indexPath.row].isSelected = true
        self.tempSelectedGuests.append(guest)
        
                    searchBar.text = ""
                    searchBar.resignFirstResponder()
                    searchBar.setShowsCancelButton(false, animated: true)
                    isSearching = false
                    self.tableView.reloadData()
        }
        
    }
    else{
        if cell?.accessoryType == .checkmark {
            self.guestArray[indexPath.row].isSelected = false
            removeGuest = guest
            print(" THIS IS DE-Select tempSelectedGuests : \n \(removeGuest)")
            
            var filter = [MInvitdGuest]()
            for guest in tempSelectedGuests {
                if guest.phone != removeGuest?.phone{
                    filter.append(guest)
                }
            }
            tempSelectedGuests = filter
            cell?.accessoryType = .none
            self.tableView.reloadData()
        }
        else{
            
        
        self.guestArray[indexPath.row].isSelected = true
        self.tempSelectedGuests.append(guest)
        cell?.accessoryType = .checkmark
            self.tableView.reloadData()
        }
        
    }
print(" THIS IS list of Select tempSelectedGuests : \n \(self.tempSelectedGuests)")
    
    }
    
    @objc func setup(){
        
        let button1 = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(self.back))
        
        self.navigationItem.leftBarButtonItem = button1
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Invite", style: .plain, target: self, action:#selector(self.invite))
        self.title = "Contacts"
        
    }
    
    @objc func back(){
        self.dismiss(animated: true, completion: nil)
        
    }

    @objc func invite() {
        
        print("this self.tempSelectedGuests in inviteBtnPressed \n\(self.tempSelectedGuests)")
        
        submitFunc(guests: tempSelectedGuests )
    }
}

extension ConfirmationGuestInvitation {
    
    func submitFunc( guests: [MInvitdGuest] )  {
        
        let eventID = UserDefaults.standard.string(forKey: "eventID")
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")
        let userID = UserDefaults.standard.string(forKey: "userID")!
        
        let parameters2:[String:Any] = ["guests" : MInvitdGuest.dictionaryFromGuests(guests),"invitationID" : invitationID!  as AnyObject , "eventID" : eventID as AnyObject,"userID": userID]
        
        print("\(parameters2)")
        
        let url = MInvitdClient.Constants.BaseUrl + "guests"
        
        // start
        
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = ["Authorization" : UserDefaults.standard.string(forKey: "userToken")!,
                                       "Content-Type" : "application/json"
                                       ]
       request.httpBody = try?  JSONSerialization.data(withJSONObject: parameters2, options: .prettyPrinted)
        
        // parameters2
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                return
            }
            
            print("data response : \n \(response)")
            self.actionResult2(JSONData: data)
        }
        task.resume()
    }
    
    @objc func actionResult2(JSONData : Data)
    {
        do{
            let json = try JSONSerialization.jsonObject(with: JSONData, options: [])
            
            print("\(json)")
            
            if let dictionary = json as? [String : Any] {
                print("\(dictionary)")
                
                if let status = dictionary["status"] as? String
                {
                    if status == "success"
                    {
                        if let data = dictionary["data"] as? [[String : Any]]
                        {
                            DispatchQueue.main.async {
                                
                                let alert = UIAlertController(title: "Minvitd", message: "Guest Added" , preferredStyle: .alert)
                                
                                alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: { (action : UIAlertAction) in
                                    self.dismiss( animated: true, completion: nil)
                                }))
                                self.present(alert, animated: true, completion: nil)
                                
                            }
                        }
                    }else
                    {
                        if let messages = dictionary["messages"] as? [[String : Any]]
                        {
                            let message = messages[0]["message"] as? String
                            
                            DispatchQueue.main.async {
                                
                                let alert = UIAlertController(title: "Alert", message: message , preferredStyle: .alert)
                                
                                alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: { (action : UIAlertAction) in
                                    
                                    alert.dismiss( animated: true, completion: nil)
                                }))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }
                }
            }
        }
        catch{
        }
    }
}

extension ConfirmationGuestInvitation:UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
        // tableView.reloadData()
        self.tableView.setContentOffset(CGPoint(x: 0, y: 44), animated: true)
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
        isSearching = false
        self.tableView.reloadData()
        self.tableView.setContentOffset(CGPoint(x: 0, y: 44), animated: true)
        
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        //reloading here
        if searchBar.text == nil || searchBar.text == ""{
            isSearching = false
            searchBar.setShowsCancelButton(false, animated: true)
            view.endEditing(true)
            self.tableView.reloadData()
            self.tableView.setContentOffset(CGPoint(x: 0, y: 44), animated: true)
            return
        }
        else {
            isSearching = true
            filterData = self.guestArray.filter({
                return ($0.name?.contains(searchText))!})
            self.tableView.reloadData()
        }
    }
}

