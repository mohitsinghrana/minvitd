//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import UIKit

class MessageCollectionViewCell : UICollectionViewCell {
    
    @IBOutlet weak var imageLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
}
