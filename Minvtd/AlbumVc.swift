//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//


import UIKit
import Alamofire


class Alubam:UIPageViewController, UIPageViewControllerDataSource,AlbumContainerDelegate{
    
    
    @objc var arrPageTitle  = [String]()
    @objc var arrPagePhoto =  [String]()
    
    var currentPageIndex = 0
    var gallery: [MInvtdAllFeed] = [MInvtdAllFeed]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dataSource = self
        self.delegate = self
        currentPageIndex = 0
        self.setViewControllers([getViewControllerAtIndex(0)] as [UIViewController], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    // MARK:- UIPageViewControllerDataSource Methods
    
    func didpressmovebtn(btn: UIButton, tag: Int) {
        print("my",currentPageIndex)

        if tag == 0 {
            if currentPageIndex > 0{
                currentPageIndex = currentPageIndex - 1

                self.setViewControllers([getViewControllerAtIndex(currentPageIndex)] as [UIViewController], direction: UIPageViewControllerNavigationDirection.reverse, animated: true, completion: nil)

            }
            //let index
        }else{

            if currentPageIndex < gallery.count - 1{
                currentPageIndex = currentPageIndex + 1
                self.setViewControllers([getViewControllerAtIndex(currentPageIndex)] as [UIViewController], direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: nil)
                
            }
        }
    }

    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        let pageContent: AlbumContentVC = viewController as! AlbumContentVC
        
        var index = pageContent.pageIndex
        
        if ((index == 0) || (index == NSNotFound))
        {
            return nil
        }
        
        index -= 1;
        return getViewControllerAtIndex(index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        let pageContent: AlbumContentVC = viewController as! AlbumContentVC
        
        var index = pageContent.pageIndex
        
        if (index == NSNotFound)
        {
            return nil;
        }
        
        index += 1;
        if (index == arrPageTitle.count)
        {
            return nil;
        }
        return getViewControllerAtIndex(index)
    }
    
    // MARK:- Other Methods
    @objc func getViewControllerAtIndex(_ index: NSInteger) -> AlbumContentVC
    {
        // Create a new view controller and pass suitable data.
        let AlbumContentVC = self.storyboard?.instantiateViewController(withIdentifier: "AlbumContentVC") as! AlbumContentVC
        
        AlbumContentVC.strTitle = "\(arrPageTitle[index])"
        AlbumContentVC.strPhotoName = "\(arrPagePhoto[index])"
        AlbumContentVC.pageIndex = index
        AlbumContentVC.gallery = self.gallery
        AlbumContentVC.delegate = self
        return AlbumContentVC
    }
    
}


extension Alubam:UIPageViewControllerDelegate{
    
    func  pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
                    self.currentPageIndex = (pageViewController.viewControllers!.first! as? AlbumContentVC)!.pageIndex
                    
        
    }
    
}
