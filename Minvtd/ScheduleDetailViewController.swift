//
//  ScheduleDetailViewController.swift
//  Minvtd
//
//  Created by Tushar Aggarwal on 01/08/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit

class ScheduleDetailViewController: UIViewController {

    @IBOutlet weak var imageLocation: UIImageView!
    @IBOutlet weak var imageClock: UIImageView!
    @IBOutlet weak var imageDate: UIImageView!
    @IBOutlet weak var header: UILabel!
    @IBOutlet weak var scheduleTitleLabel: UILabel!
    @IBOutlet weak var scheduleTimeLabel: UILabel!
    @IBOutlet weak var shceduleLocationLabel: UILabel!
    @IBOutlet weak var sheduleDescriptionLabel: UITextView!
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var colorView: UIView!
    @IBAction func dismissVC(_ sender: UIBarButtonItem) {
        
        pushpopAnimation()
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBOutlet weak var myNavigationBar: UINavigationBar!
    var schedule : MInvitdSchedule?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBarSetup(myNavigationBar)
        
        if let navTitle = schedule?.eventName{
            self.navigationItem.title = navTitle
            self.navigationController?.navigationItem.title = navTitle
            self.myNavigationBar.topItem?.title = navTitle
        }
        
        let color = UserDefaults.standard.string(forKey: "colour1")
        colorView.backgroundColor = UIColor.init(hexString: color!)
        colorView.layer.cornerRadius = colorView.bounds.width/2
        colorView.layer.masksToBounds = true
        
        if let name = schedule?.name {
            scheduleTitleLabel.text = name
        }
        
        if let name = schedule?.time {
            scheduleTimeLabel.text = name
        }
        
        if let name = schedule?.description {
            sheduleDescriptionLabel.text = name
        }
        
        if let place = schedule?.venue {
            shceduleLocationLabel.text = place
        }
        
        if let headerString = schedule?.header {
            header.text = "  \(headerString)"
        }
        
        if let dateString = schedule?.date {
            dateLabel.text = dateString
        }
        
        self.imageDate.image = imageDate.image?.withRenderingMode(.alwaysTemplate)
        self.imageDate.tintColor = UIColor.gray
        self.imageClock.image = imageClock.image?.withRenderingMode(.alwaysTemplate)
        self.imageClock.tintColor = UIColor.gray

        self.imageLocation.image = imageLocation.image?.withRenderingMode(.alwaysTemplate)
        self.imageLocation.tintColor = UIColor.gray

        // Do any additional setup after loading the view.
    }
}
