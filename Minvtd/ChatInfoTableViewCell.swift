//
//  ChatInfoTableViewCell.swift
//  Minvtd
//
//  Created by Naved on 07/09/18.
//  Copyright © 2018 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit

class ChatInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var memberImage: UIImageView!
    @IBOutlet weak var memberName: UILabel!
    @IBOutlet weak var memberPhone: UILabel!
    

}
