
//  PostViewController.swift
//  Minvtd
//
//  Created by Tushar Aggarwal on 21/07/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved

import UIKit
import Photos
import Alamofire
import AVKit
import AVFoundation
import ImagePicker

class PostViewController: UIViewController , UINavigationControllerDelegate, UIImagePickerControllerDelegate , URLSessionDelegate , URLSessionTaskDelegate , URLSessionDataDelegate, UITextViewDelegate, ImagePickerDelegate {
    
    @IBOutlet weak var userStopView: UIView!
    @IBOutlet weak var videoUploadImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var photoUploadImage: UIImageView!
    @IBOutlet weak var commentTV: UITextView!
    @IBOutlet weak var imageUploadProgressView: UIProgressView!
    
    @IBOutlet weak var tagCollectionView: UICollectionView!
    //@IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var myCollectionView: UICollectionView!
    let vc = TagPostViewController()
    
    @objc let imagePicker = UIImagePickerController()
    let imagePickerController = ImagePickerController()
    
    var files = [FileUpload]()
    @objc var origin = "feed"
    @objc var uploadVideoUrl  :  URL?
    @objc var uploadImage  : UIImage?
    @objc var indexUpload : Int = 0
    @objc var boolUpload = false
    var tagedPeople:[Int] = []
    var taged : [MInvitdGuest] = []
    var observer:NSObjectProtocol?
    @IBOutlet weak var addImageVideoView: UIView!
    
    
    internal func textViewDidBeginEditing(_ textView: UITextView) {
        
        UIView.animate(withDuration: 0, delay: 0.1, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            let myMaxY = self.addImageVideoView.frame.maxY - 350
            self.addImageVideoView.frame = CGRect(x: 0, y: myMaxY, width: self.view.bounds.width, height: 50)
            
        }, completion: nil)
    }
    
    internal func textViewDidEndEditing(_ textView: UITextView) {
        
        UIView.animate(withDuration: 0, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            let myMaxY = self.view.frame.height - 50
            self.addImageVideoView.frame = CGRect(x: 0, y: myMaxY, width: self.view.bounds.width, height: 50)
        }, completion: nil)
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePickerController.delegate = self
        imagePickerController.imageLimit = 5
        commentTV.delegate = self
        
        //self.myTableView.isHidden = true
        
        setup()
        let left = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(dismissVC))
        self.navigationItem.leftBarButtonItem = left
        
        let rigtht = UIBarButtonItem(title: "Post", style: .done, target: self, action: #selector(postComment))
        self.navigationItem.rightBarButtonItem = rigtht
        self.title = "Update Status"

        observer =  NotificationCenter.default.addObserver(forName: NSNotification.Name.init("tags"), object: nil, queue: nil) { (notification) in
            print("lookhere")
            if let tag =  notification.object as? [MInvitdGuest] {

                self.taged = tag
                self.tagCollectionView.reloadData()
                print("tag ->>>",self.taged)
                print("tag count->>>", self.taged.count)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    @IBAction func tagAction(_ sender: Any) {
        present( UINavigationController(rootViewController:vc), animated: true, completion: nil)
    }
    
    @objc func uploadImageTapDetected(_ sender: UITapGestureRecognizer) {
        
        self.files = []
        self.myCollectionView.reloadData()
        imagePickerController.imageLimit = 10
        self.present(imagePickerController, animated: true, completion: nil)
        
    }
    
    @objc func uploadVideoTapDetected(_ sender: UITapGestureRecognizer) {
        
        self.files = []
        self.myCollectionView.reloadData()
        
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        imagePicker.mediaTypes = ["public.movie"]
        imagePicker.navigationBar.tintColor = .white //Text Color
        imagePicker.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        imagePicker.navigationItem.title = "Videos"
        present(imagePicker, animated: true, completion: nil)
        
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]){
        print("Wrapper Pressed")
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]){
        
        print("\(images)")
        self.files = []
        self.myCollectionView.reloadData()
        
        if images != []
        {
            for image in images{
                let file = FileUpload.init(fid: nil, image: image, videoUrl: nil, fileType: nil, type: true)
                files.append(file)
                //myTableView.reloadData()
                self.myCollectionView.reloadData()
            }
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    func cancelButtonDidPress(_ imagePicker: ImagePickerController){
        self.files = []
        self.myCollectionView.reloadData()
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func dismissVC(_ sender: UIBarButtonItem) {
        print("tap")
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func dissmisskeyboard(){
        self.view.endEditing(true)
    }

    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        self.files = []
        self.myCollectionView.reloadData()
        
            if let videourl = info[UIImagePickerControllerMediaURL] as? URL
        {
            
            let data = try? Data(contentsOf: videourl)
    
            let file = FileUpload.init(fid: nil, image: nil, videoUrl: videourl, fileType: nil, type: false)
            files.append(file)
            
            if self.files[0].type == false{
                self.videoUploadImage.isUserInteractionEnabled = false
            }
            self.myCollectionView.reloadData()
        }
        //collectionView.reloadData()
        self.dismiss(animated: true, completion: nil)
    }
    
    func setup(){
        
        self.myCollectionView.dataSource = self
        self.myCollectionView.delegate = self
        
        tagCollectionView.delegate = self
        tagCollectionView.dataSource = self
        
        imagePicker.delegate = self
        navigationBarSetup(nil)
        if let name = UserDefaults.standard.string(forKey: "name"){
            
            self.nameLabel.text = name
        }
        
        if let imageUrl = UserDefaults.standard.string(forKey: "imageUrl"){
            
            self.userImage.af_setImage(withURL: URL(string: imageUrl)!)
            self.userImage.contentMode = .scaleAspectFill
            self.userImage.layer.cornerRadius  = self.userImage.frame.size.width/2
            self.userImage.layer.masksToBounds = true
            self.userImage.clipsToBounds = true
        }
        
        let uploadImageTap = UITapGestureRecognizer(target: self, action: #selector(self.uploadImageTapDetected(_:)))
        uploadImageTap.numberOfTapsRequired = 1
        let uploadVideoTap = UITapGestureRecognizer(target: self, action: #selector(self.uploadVideoTapDetected(_:)))
        uploadVideoTap.numberOfTapsRequired = 1
        
        photoUploadImage.isUserInteractionEnabled = true
        photoUploadImage.addGestureRecognizer(uploadImageTap)
        videoUploadImage.isUserInteractionEnabled = true
        videoUploadImage.addGestureRecognizer(uploadVideoTap)
        
        photoUploadImage.image = photoUploadImage.image?.withRenderingMode(.alwaysTemplate)
        photoUploadImage.tintColor = UIColor(hexString: "#89be49")
        
        videoUploadImage.image = videoUploadImage.image?.withRenderingMode(.alwaysTemplate)
        videoUploadImage.tintColor = UIColor(hexString: "#fa121e")
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dissmisskeyboard)))
    }

    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction  func postComment(_ sender: UIBarButtonItem) {
        
        print("\(files)")
        
        if !files.isEmpty{
            self.uploadFiles(file: files[0])
            indexUpload = 0
        }else {
            
            // Post Normal
            
            let string = self.commentTV.text
            self.commentTV.placeholder = "Write Something"
            if validateString(string: string!){
                
                let invitaionID = UserDefaults.standard.string(forKey: "invitationID")
                let userID = UserDefaults.standard.string(forKey: "userID")
                let userName = UserDefaults.standard.string(forKey: "name")
                let iName = UserDefaults.standard.string(forKey: "invitationTitle")!
                

                var parameters : Parameters
                
                if self.origin == "event" {
                    
                    let eventID = UserDefaults.standard.string(forKey: "eventID")
                    let commentID = UserDefaults.standard.string(forKey: "commentID")
                    
                    parameters = ["comment" : string! as AnyObject,
                                  "userID" : userID as AnyObject,
                                  "invitationID" : invitaionID as AnyObject ,
                                  "eventID" : eventID as AnyObject ,
                                  "commentID" : commentID as AnyObject,
                                  "userName":userName!,"iName":iName ]
                }else{
                    parameters = ["comment" : string! as AnyObject,
                                  "userID" : userID as AnyObject,
                                  "invitationID" : invitaionID as AnyObject ,
                                  "userName":userName! ]
                }
                for tag in taged{
                    tagedPeople.append(tag.userID!)
                }
                
                print(tagedPeople)
                parameters["users"] = tagedPeople
                parameters["iName"] = iName
                print(iName)
                
                apiCallingMethod(apiEndPints: "comment", method: .post, parameters: parameters, complition: { (data1:Any?, totalobj:Int?) in
                    print("^^^^^^^^^^^^^^^^^^^")
                    print(data1)
                    
                        self.commentTV.text = ""
                        self.userStopView.isHidden = true
                        
                    let presentingViewController = self.presentingViewController
                    self.dismiss(animated: true, completion: {
                        if let vc =  presentingViewController as? AllFeedViewController{
                            
                        }
                    })
                })
            }else{
                
                self.commentTV.placeholder = "Write Something"
                self.dismissAlert("Write Something")
            }
            return
        }
    }
    
    
    func deleteFile(_ filePath:URL) {
        guard FileManager.default.fileExists(atPath: filePath.path) else {
            return
        }
        
        do {
            try FileManager.default.removeItem(atPath: filePath.path)
        }catch{
            fatalError("Unable to delete file: \(error) : \(#function).")
        }
    }
    
    func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality) else {
            handler(nil)
            
            return
        }
        
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mp4
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }
    
    private func uploadFiles(file : FileUpload ){
        
        var imgData  : Data!
        var mimeType : String!
        var fileName : String!
        
        if file.type == true {
            imgData =  UIImageJPEGRepresentation(file.image!, 0.05)
            mimeType = "image/jpg"
            fileName = "file.jpg"
            
        }else{
            
            imgData = try? Data(contentsOf: file.videoUrl!)
            mimeType = "video/mp4"
            fileName = "file.mp4"
        }
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        let parameters = ["file_input" : "files" , "file_module": "\(self.origin)" , "userID" : userID]
        let authHeader = ["Authorization" : UserDefaults.standard.string(forKey: "userToken")!, "Content-Type" : "application/json" ]
        
        self.imageUploadProgressView.progress = 0.0
        self.userStopView.isHidden = false
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in parameters {
                multipartFormData.append((value?.data(using: String.Encoding.utf8)!)!, withName: key)
            }
            
            multipartFormData.append(imgData, withName: "files",fileName: fileName!, mimeType: mimeType!)

        },
                         to:MInvitdClient.Constants.BaseUrl + "files/upload",
                         method : .post,
                         headers : authHeader) {
            (result) in
            
            //print("\(result)")
            switch result
            {
                
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    self.imageUploadProgressView.setProgress(Float(progress.fractionCompleted), animated: true)
                })
                
                upload.responseJSON { response in
                    
                    //print("\(response)")
                    //print("\(self.files.count)")
                    
                    if let result = response.result.value {
                        let JSON = result as! NSDictionary
                        if let status = JSON["status"] as? String
                        {
                            if status == "success"
                            {
                                if let data = JSON["data"] as? [[String : Any]]
                                {
                                    let fid = data[0]["fid"] as! Int
                                    guard let fileType = data[0]["fileType"]else {
                                        return
                                    }
                                    
                                    self.files[self.indexUpload].fid = fid
                                    self.files[self.indexUpload].fileType = fileType as! String
                                    self.indexUpload += 1
                                    //print("\(self.indexUpload)")
                                    if self.indexUpload < self.files.count{
                                        // Doubt here
                                        self.uploadFiles(file: self.files[self.indexUpload])
                                    }else if self.indexUpload == self.files.count   {
                                        self.userStopView.isHidden = true
                                        self.sendFidsToServer()
                                    }
                                }
                            }
                        }
                    }
                }
                
            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }
    
    @objc func sendFidsToServer(){
        
        let string = self.commentTV.text + " "
        if validateString(string: string){
            
            let invitaionID = UserDefaults.standard.string(forKey: "invitationID")
            let userID = UserDefaults.standard.string(forKey: "userID")
            
            var parameters : Dictionary<String , AnyObject>
            var i = 0
            var requestFiles = [[String : Any?]]()
            for file in self.files {
                i += 1
                let object:[String : Any?] = ["fileID" : file.fid , "fileType" : file.fileType]
                requestFiles.append(object)
            }
            
            
            let userName = UserDefaults.standard.string(forKey: "name")!
            let iName = UserDefaults.standard.string(forKey: "invitationTitle")!

            if self.origin == "event" {
                
                let eventID = UserDefaults.standard.string(forKey: "eventID")
                let commentID = UserDefaults.standard.string(forKey: "commentID")

                
                parameters = ["comment" : string as AnyObject, "userID" : userID as AnyObject, "invitationID" : invitaionID as AnyObject , "eventID" : eventID as AnyObject , "commentID" : commentID as AnyObject ,"files" : requestFiles as AnyObject,"userName":userName as AnyObject,"iName":iName as AnyObject]
            }else{
                
                parameters = ["comment" : string as AnyObject, "userID" : userID as AnyObject, "invitationID" : invitaionID as AnyObject ,"files" : requestFiles as AnyObject,"userName":userName as AnyObject,"iName":iName as AnyObject]
            }
            
            let url = MInvitdClient.Constants.BaseUrl + "comment"
            
            Spinner.sharedinstance.startSpinner(viewController: self)
            
            for tag in taged{
                tagedPeople.append(tag.userID!)
            }
            
            print(tagedPeople)

            
            parameters["users"] = tagedPeople as AnyObject
            
           let authHeader = ["Authorization" : UserDefaults.standard.string(forKey: "userToken")!, "Content-Type" : "application/json" ]
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: authHeader).responseJSON { (response) in
                
                Spinner.sharedinstance.stopSpinner(viewController: self)

                if response.data != nil{
                    self.commentTV.text = ""
                    self.userStopView.isHidden = true
                    self.dismiss(animated: true, completion: nil)
                }else{
                    
                    self.userStopView.isHidden = true
                    
                    let alert = UIAlertController(title: "Couldn't connect to server", message: "Check Internet", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: { (action : UIAlertAction) in
                        alert.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            
        }else{
            
            self.commentTV.placeholder = "Write Something"
            let alert = UIAlertController(title: "Couldn't connect to server", message: "Check Internet", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: { (action : UIAlertAction) in
                alert.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
    }

    
}

extension PostViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == myCollectionView{
          return self.files.count
        }
        else{
            if taged.count < 6{
                return taged.count
            }else{
                return 5
            }
            
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == myCollectionView{
        return 0
        }
        else{
            return -13
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 5
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == myCollectionView{
        
        if self.files[0].type == false{
            return CGSize(width: UIScreen.main.bounds.width - 32, height: 200)
        }else{
            return CGSize(width: (UIScreen.main.bounds.width - 42) / 3, height: 100)
        }
        }
        else{
         return CGSize(width: 30, height: 30)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == myCollectionView{
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "gridCell", for: indexPath) as! WritePostCollectionCell
    
        cell.cross.tag = indexPath.row
        
        //Remove Target
        
        cell.cross.removeTarget(nil, action: nil, for: .allEvents)
        
        // Add target
        cell.cross.addTarget(self, action: #selector(self.deleteCell(sender:)), for: .touchUpInside)
        
        
        cell.videoPlayBtn.image = nil
        cell.videoPlayBtn.image = UIImage(named: "videoPlayIcon")
        
        if files[indexPath.row].type == true {
            cell.videoPlayBtn.isHidden = true
            cell.selectedImage.image = files[indexPath.row].image
        }else{
            cell.videoPlayBtn.isHidden = false
            cell.selectedImage.image = thumbnailForVideoAtURL(url: files[indexPath.row].videoUrl!)
        }
        
        return cell
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tagCommentsCollectionCell", for: indexPath) as! CommentTagCollectionViewCell
            
            let guest = taged[indexPath.row]
            cell.userImage.isHidden = false
            cell.moreLabel.isHidden = true
            if let imgUrl = guest.imageUrl {
                cell.userImage.af_setImage(withURL: URL(string:imgUrl)!)
            }
            if indexPath.row == 4{
                cell.userImage.isHidden = true
                cell.moreLabel.isHidden = false
            }
            return cell
        }
    }
    
     @objc func deleteCell(sender: UIButton){
        let currentIndex = sender.tag
        
        print(" **** Current Index is \(currentIndex)")
            self.files.remove(at: currentIndex)
            self.myCollectionView.reloadData()
          //  self.myCollectionView.deleteItems(at: [IndexPath(row: currentIndex, section: 0)])
        print("no of files left is \(self.files.count)")
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {if collectionView == myCollectionView{
        print("collection view")
        }
        
    else{
        print("open TagsVC")
        }
    }
}


struct FileUpload {
    
    var fid : Int?
    var image : UIImage?
    var videoUrl : URL?
    var fileType : String?
    var type : Bool?
}
