//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import UIKit

class EventScheduleCollectionViewCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var colorView: UIView!

}
