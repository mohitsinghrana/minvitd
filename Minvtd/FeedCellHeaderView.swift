//
//  FeedCellHeader.swift
//  MinvitdFeeds
//
//  Created by admin on 03/11/17.
//  Copyright © 2017 Sachin. All rights reserved.

import UIKit

class FeedCellHeaderView: UIView {
    
    var feed:MInvtdAllFeed? {
        
        didSet{
            if let imgUrl = feed?.userImageUrl{
                profileImg.af_setImage(withURL: URL(string:imgUrl)!)
            }
            nameLabel.text = feed?.userName
            textLabel.text = feed?.comment
            //
            let fullString = NSMutableAttributedString(string: "")
            let image1Attachment = NSTextAttachment()
            image1Attachment.image = UIImage(named: "clock-1.png")
            image1Attachment.bounds = CGRect(x: 0, y: 0, width: 13.5, height: 13.5)
            
            fullString.append(NSAttributedString(string: " " + (feed?.created?.socialTime())!))
            
            timeLabel.attributedText = fullString
    
            let userID = UserDefaults.standard.integer(forKey: "userID")
            deleteButton.isHidden = feed?.userID == userID ? false: true
        }
    }
    
    let profileImg = { () -> UIImageView in
        let imgView = UIImageView()
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.backgroundColor = .purple
        imgView.layer.cornerRadius = 20.0
        imgView.contentMode = .scaleAspectFill
        imgView.clipsToBounds = true
        return imgView
    }()
    
    let clockIcon = { () -> UIImageView in
        let img = UIImageView()
        img.image = #imageLiteral(resourceName: "clock").withRenderingMode(.alwaysTemplate)
        img.tintColor = UIColor.lightGray
        img.clipsToBounds = true
        return img
    }()
    
    let timeLabel = { () -> UILabel in
        let li = UILabel()
        li.text = "7 min ago"
        li.textColor = UIColor.init(hexString: "#979797")
        li.font = UIFont(name: "GothamRounded-Book", size: 12)
        li.translatesAutoresizingMaskIntoConstraints = false
        return li
    }()
    
    let nameLabel = { () -> UILabel in
        let li = UILabel()
        li.text = "Sachin Yadav"
        li.textColor = UIColor.init(hexString: "#494949")
        li.textAlignment = .left
        li.font = UIFont(name: "GothamRounded-Medium", size: 14)
        li.translatesAutoresizingMaskIntoConstraints = false
        return li
    }()
    
    let deleteButton = { () -> UIButton in
        let btn = UIButton(type: .system)
        btn.tintColor = UIColor.init(hexString: "#494949")
        btn.setImage(#imageLiteral(resourceName: "Delete Invitation"), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let textLabel = { () -> UILabel in
        let li = UILabel()
        li.font = UIFont(name: "GothamRounded-Book", size: 14)
        li.text = "dghzxnbfvdhjsmgbajkDbvjashbvSbajkDbvjashbvSdghzxnbfvdhjsmgbajkDbvjashbvSdghzxnbfvdhjsmgbajkDbvjashbvS"
        li.textAlignment = .left
        li.numberOfLines = 0
        li.textColor = UIColor.init(hexString: "#131313")
        li.translatesAutoresizingMaskIntoConstraints = false
        return li
    }()
    
    let dull = { () -> UIView in
    let vi = UIView()
        vi.backgroundColor = UIColor(red: 231/255, green: 231/255, blue: 231/255, alpha: 1.0)
        vi.translatesAutoresizingMaskIntoConstraints = false
        return vi
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup(){
        
        addSubview(dull)
        addSubview(profileImg)
        addSubview(deleteButton)
        addSubview(nameLabel)
        addSubview(timeLabel)
        addSubview(textLabel)
        addSubview(clockIcon)
        
        dull.anchor(topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 12)
        
        profileImg.anchor(dull.bottomAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 20, leftConstant: 20, bottomConstant: 0, rightConstant: 0, widthConstant: 40, heightConstant: 40)
        
        deleteButton.anchor(nil, left: nil, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 20, widthConstant: 18, heightConstant: 18)
        deleteButton.centerYAnchor.constraint(equalTo: profileImg.centerYAnchor, constant: 0).isActive = true
        
        nameLabel.bottomAnchor.constraint(equalTo: profileImg.centerYAnchor, constant: -2).isActive = true
        nameLabel.leftAnchor.constraint(equalTo: profileImg.rightAnchor, constant: 8).isActive = true
        nameLabel.rightAnchor.constraint(equalTo: deleteButton.leftAnchor, constant: -8).isActive = true
        
        clockIcon.anchor(profileImg.centerYAnchor, left: nameLabel.leftAnchor, bottom: nil, right: nil, topConstant: 2, leftConstant: -2, bottomConstant: 0, rightConstant: 0, widthConstant: 14, heightConstant: 14)
        clockIcon.center.y = timeLabel.center.y
        
        timeLabel.anchor(profileImg.centerYAnchor, left: clockIcon.rightAnchor, bottom: nil, right: deleteButton.leftAnchor, topConstant: 4, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        textLabel.anchor(profileImg.bottomAnchor, left: profileImg.leftAnchor, bottom: bottomAnchor, right: deleteButton.rightAnchor, topConstant: 12, leftConstant: 0, bottomConstant: 4, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    // height calculation   16 + 90 + textHeight
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

