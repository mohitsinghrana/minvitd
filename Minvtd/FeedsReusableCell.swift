//
//  FeedsReusableCell.swift
//  Minvtd
//
//  Created by Tushar Aggarwal on 20/07/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit

class FeedsReusableCell: UICollectionReusableView {
    
    @IBOutlet weak var mainImage: UIImageView!
    
    @IBOutlet weak var smallImage: UIImageView!
        
    @IBOutlet weak var uploadButton: UIButton!
    @IBOutlet weak var postFeedTV: UITextView!
    @IBOutlet weak var addFeedView: UIView!
    
    @IBOutlet weak var photosTapView: UIView!
    
    @IBOutlet weak var photosBtnImage: UIImageView!
    
    @IBOutlet weak var photosBtn: UIButton!
    
    @IBOutlet weak var videosTapView: UIView!
    
    @IBOutlet weak var videosBtnImage: UIImageView!
    
    @IBOutlet weak var videosBtn: UIButton!
    
    @IBOutlet weak var updateOnlyBtn: UIButton!
    
    @IBOutlet weak var allbtn: UIButton!
    
    @IBOutlet weak var updateOnlyBottom: UIView!
    
    @IBOutlet weak var allbtm: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        postFeedTV.returnKeyType = UIReturnKeyType.send
    }
    
    
    @IBAction func actionswitch(_ sender: Any) {
        let btn = sender as? UIButton
        
        let colour1 = UserDefaults.standard.string(forKey: "colour1")!
        
        if btn == allbtn {
            allbtm.backgroundColor = UIColor.init(hexString: colour1)
            updateOnlyBottom.backgroundColor = .clear
        }else {
            updateOnlyBottom.backgroundColor = UIColor.init(hexString: colour1)
            allbtm.backgroundColor = .clear
        }
    }
    
    @objc func configure(num:Int){
        
        let colour1 = UserDefaults.standard.string(forKey: "colour1")!
        
        if num == 0 {
            allbtm.backgroundColor = UIColor.init(hexString: colour1)
            updateOnlyBottom.backgroundColor = .clear
        }else {
            updateOnlyBottom.backgroundColor = UIColor.init(hexString: colour1)
            allbtm.backgroundColor = .clear
        }
    }

}
