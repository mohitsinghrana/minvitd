//
//  GalleryCollectionViewCell.swift
//  Minvtd
//
//  Created by Mohit Singh on 18/10/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.
//

import Foundation
import UIKit
class GalleryCollectionViewCell : UICollectionViewCell
{
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var videoPlayIcon: UIImageView!
    
    @objc let profileImage = { () -> UIImageView in
        
        let img = UIImageView()
        img.layer.cornerRadius = 18.0
        img.clipsToBounds = true
        img.layer.borderWidth = 2.0
        img.layer.borderColor = UIColor.white.cgColor
        return img
    }()
    
    @objc let LikeImage = { () -> UIImageView in
        
        let img = UIImageView()
        img.image = #imageLiteral(resourceName: "likeFilled")
        return img
    }()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    
    
    
    @objc func setup() {
        
        self.addSubview(profileImage)
        self.addSubview(LikeImage)
        
        
        profileImage.anchor(self.topAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 15.0, leftConstant: 15.0, bottomConstant: 0, rightConstant: 0.0, widthConstant: 36.0, heightConstant: 36.0)
        
        LikeImage.anchor(self.topAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 15.0, leftConstant: 0, bottomConstant: 0, rightConstant: 15.0, widthConstant: 30.0, heightConstant: 30.0)
    }
    
}
