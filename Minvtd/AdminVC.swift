//
//  AdminVC.swift
//  Minvtd
//
//  Created by admin on 08/09/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit
import VBPieChart

class AdminVC: UIViewController  {


    @IBOutlet weak var message: UITextField!
    @IBOutlet weak var pieChartFrame: UIView!
    
    @IBOutlet weak var manageGuest: UIButton!
    
    @IBOutlet weak var totalGuest: UILabel!
    
    @IBOutlet weak var yesLabel: UILabel!
    
    @IBOutlet weak var noLabel: UILabel!
    @IBOutlet weak var unDecidedLabel: UILabel!
    
    var Event:MInvitdEvent = MInvitdEvent(dictionary: [:])
    @objc let pieChart = VBPieChart()
    
    var guests:[MInvitdGuest]? = []{
        didSet{
            
            configureData(guests: self.guests!)
        }
    
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBarSetup(nil)
        disssmissKeyboard()
        setup()
        
        
        let button1 = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(dissmissVc))
        self.navigationItem.leftBarButtonItem  = button1

      message.returnKeyType = .send
        


    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getContacts(eventID: Event.eventID!)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    

    
    @IBAction func manageGuest(_ sender: UIButton) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Storyboard", bundle: nil)
        
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "AdminManageGuestBaseVC") as! AdminManageGuestBaseVC
        mainVC.guests = self.guests!
        mainVC.eventID = Event.eventID!
        //self.present( UINavigationController(rootViewController:mainVC), animated: true, completion: nil)
        self.navigationController?.pushViewController(mainVC, animated: true)
        
        
    }
    
    
    
     func dissmissVC(_ sender: Any) {
        
        pushpopAnimation()
        self.dismiss(animated: false, completion: nil)

    }
    
    @IBAction func pushAdminNotification(_ sender: UITextField) {

        let userID = UserDefaults.standard.string(forKey: "userID")
        let invitationID = UserDefaults.standard.integer(forKey: "invitationID")
        
        let userName = UserDefaults.standard.string(forKey: "name")!
        guard let iName = UserDefaults.standard.string(forKey: "invitationTitle") else {return}
        guard let eventID =  UserDefaults.standard.string(forKey: "eventID") else {return}


        let message = sender.text
        var prams:[String:AnyObject] = ["userID":userID as AnyObject,"invitationID":invitationID as AnyObject,"userName":userName as AnyObject,"iName":iName as AnyObject,"eventID":eventID as AnyObject]
        
        if validateString(string: message!){
           
            prams["body"] = message as AnyObject
            
            apiCallingMethod(apiEndPints: "notify/guests", method: .post, parameters: prams) { (data1, totalobj) in
                
                if data1 != nil {
                   self.message.text = nil
                    
                    self.CreateAlertPopUp("Success", "Notification  sent successfully", OnTap: nil)
                    
                }else {
                    self.dismissAlert("Unable to send Notification")
                }
            }

        }else {
            
            CreateAlertPopUp("Oops", "textfield is empty", OnTap: nil)
        }
    }
    
    
    @objc func getContacts(eventID:Int){
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")!
        
        apiCallingMethod(apiEndPints: "guests", method: .get, parameters: ["invitationID":invitationID,"eventID":eventID]) { (data1:Any?, totalobj:Int?) in
            
            if totalobj == 0  {
             
                
                self.CreateAlertPopUp("MInvitd", "You have no guests", OnTap:nil)
                return
            }
            
            if let data = data1 as? [[String : Any]]{
                
                    self.guests = MInvitdGuest.guestsFromResults(data as [[String : AnyObject]])
                
            }
            
            
        }
        
        
        
        
    }

    
    @objc func setup() {
        
        
        self.title = Event.name

        pieChartFrame.addSubview(pieChart)

        pieChart.frame = CGRect(x: 0, y: 0, width: pieChartFrame.bounds.height/2, height: pieChartFrame.bounds.height/2 )
//        pieChart.center = CGPoint(x: pieChartFrame.bounds.width  / 2,
//                                     y: pieChartFrame.bounds.height / 2)
        
        pieChart.centerYAnchor.constraint(equalTo: pieChartFrame.centerYAnchor).isActive = true
        pieChart.centerXAnchor.constraint(equalTo: pieChartFrame.centerXAnchor).isActive = true

        totalGuest.centerYAnchor.constraint(equalTo: pieChartFrame.centerYAnchor).isActive = true
        totalGuest.centerXAnchor.constraint(equalTo: pieChartFrame.centerXAnchor).isActive = true

        pieChart.holeRadiusPrecent = 0.75
        pieChart.startAngle = 0.0
        let yescolour = UIColor(red: 79/255, green: 135/255, blue: 64/255, alpha: 1.0)
        manageGuest.layer.borderColor = yescolour.cgColor

        
       
        
    }
    
    func configureData(guests: [MInvitdGuest]){
        
        
        
        let  yesCount = guests.filter({return $0.status == 1}).count
        let  noCount = guests.filter({return $0.status == 2}).count
        let  maybeCount = guests.filter({return $0.status == 0}).count

        
        totalGuest.text = "Total Guest " + guests.count.description
        yesLabel.text = "Yes - " + yesCount.description
        noLabel.text = "No - " + noCount.description
        unDecidedLabel.text = "Undecided - " + maybeCount.description

        
        
        let yescolour = UIColor(red: 79/255, green: 135/255, blue: 64/255, alpha: 1.0)
        let noColour = UIColor(red: 246/255, green: 160/255, blue: 156/255, alpha: 1.0)
        let undecidedColour = UIColor(red: 182/255, green: 182/255, blue: 182/255, alpha: 1.0)
        
        let chartValues = [ ["name":"yes", "value": yesCount, "color":yescolour],
                            ["name":"No", "value": noCount, "color":noColour],
                            ["name":"Undecided", "value": maybeCount, "color":undecidedColour]]
        
        pieChart.setChartValues(chartValues as [AnyObject], animation:false)

        
        
    }
    
}
