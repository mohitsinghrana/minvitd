//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//


import UIKit

class ListingCollectionViewCell : UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var titile: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var number: UILabel!
    @IBOutlet weak var arrowAccessory: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        arrowAccessory.transform = arrowAccessory.transform.rotated(by: CGFloat.pi)
        
    }
    
    
}
