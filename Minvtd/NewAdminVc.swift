//
//  NewAdminVc.swift
//  Minvtd
//
//  Created by admin on 27/09/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit
import Alamofire

class NewAdminVc:UIViewController {
    
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var phoneField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "New Admin"
        navigationBarSetup(nil)
        disssmissKeyboard()

        let button1 = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(dissmissVc))
        self.navigationItem.leftBarButtonItem  = button1
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        nameField.text = ""
        emailField.text = ""
        phoneField.text = ""
    }
    
    
    @IBAction func newAdmin(_ sender: Any) {
        
        let name = nameField.text!
        let phone = phoneField.text!
        let email = emailField.text!
        let userName = UserDefaults.standard.string(forKey: "name")
        
        if ((validateString(string: name))&&(validateString(string: email))&&(validateString(string: phone))) {
            
            
            let invitationID = UserDefaults.standard.string(forKey: "invitationID")!
            let userID = UserDefaults.standard.string(forKey: "userID")!

            
            let prams:Parameters = ["name":name,
                                    "email":email,
                                    "mobile":phone,
                                    "invitationID":invitationID,
                                    "userID":userID,
                                    "userName":userName as Any
                                    ]
            
            apiCallingMethod(apiEndPints: "invitation/admin", method: .post, parameters: prams, complition: { (data1, totalobj) in
                
                if data1 != nil {
                    
                    
                    self.CreateAlertPopUp("Success", "New admin has been added", OnTap: {
                        self.dismiss(animated: true, completion: nil)
                    })
                }
                
            })
            
        }else{
            
            dismissAlert("All fields aren't filled")
        }

        
        
    }
    
    
    
    
    
    
   

}
