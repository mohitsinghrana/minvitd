//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import UIKit

class VendorsCollectionViewCell : UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var title: UILabel!
}
