//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import UIKit

class ImageEventCell: UICollectionViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var descript: UILabel!
    
    @IBOutlet weak var heartBtn: UIButton!
    @IBOutlet weak var likedLabel: UILabel!
    @IBOutlet weak var commentBtn: UIButton!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var scheduleBtn: UIButton!
    @IBOutlet weak var locationBtn: UIButton!
    
    @IBOutlet weak var eventName: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var venue: UILabel!
    @IBOutlet weak var time: UILabel!
    
    @IBOutlet weak var statusButton: UIButton!
    
    @IBOutlet weak var statusChangeBtn: UIButton!
    
    //Changes;
    @IBOutlet weak var circleImage: UIImageView!
    @IBOutlet weak var imageEventName: UILabel!
    @IBOutlet weak var venueCircle: UILabel!
    
}
