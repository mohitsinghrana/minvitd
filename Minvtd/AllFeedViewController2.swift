//
//  File.swift
//  Minvtd
//
//  Created by admin on 20/09/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import UITextView_Placeholder
import SlideMenuControllerSwift
import IDMPhotoBrowser

extension AllFeedViewController{
    
    func showGuests(cell: BaseClass){
        
        previous = tableView.indexPath(for: cell)
        let rowint = (tableView.indexPath(for: cell)?.row)! - 1
        let users = self.allFeeds[rowint].users
        
        if !users.isEmpty{
            tagesVC =  GuestListProfileVC()
            tagesVC?.guests = users
            self.present(UINavigationController(rootViewController: tagesVC!), animated: true, completion: nil)
        }
    }
    
    @objc func segmentFilter(_ sender: UIButton) {
        
        if sender.tag == selectedSegmentIndex1 {
            return
        }
        
        if sender.tag == 0 {
            selectedSegmentIndex1 = 0
            allFeeds = nonfilteredArray
            var indexpaths:[IndexPath] = []
            
            //print("\(allFeeds.indices)")
            for x in allFeeds.indices {
                let indexpath = IndexPath(row: x+1, section: 0)
                indexpaths.append(indexpath)
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.tableView.reloadRows(at: indexpaths, with: .left)
            }
            
        }else  {
            
            selectedSegmentIndex1 = 1
            allFeeds = self.adminFeedArray
            //filteredArray
            var indexpaths:[IndexPath] = []
            
            for x in filteredArray.indices {
                let index = IndexPath(row: x+1, section: 0)
                indexpaths.append(index)
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.tableView.reloadRows(at: indexpaths, with: .right)
            }
        }
    }
    
    @objc func sendSubComment (cell: BaseClass , string : String ){
        
        let rowint = (tableView.indexPath(for: cell)?.row)! - 1
        let postID = self.allFeeds[rowint].postID!
        let invitaionID = UserDefaults.standard.string(forKey: "invitationID")
        let userID = UserDefaults.standard.string(forKey: "userID")
        let userName = UserDefaults.standard.string(forKey: "name")!
        let eventID = self.paramaters?.eventID!
        guard let iName = UserDefaults.standard.string(forKey: "invitationTitle") else {return}
        
        let parameters: Parameters = ["comment" : string as AnyObject, "userID" : userID as AnyObject, "invitationID" : invitaionID as AnyObject, "commentID" : postID as AnyObject,"userName":userName,"iName":iName,"eventID":eventID]
        
        let url = MInvitdClient.Constants.BaseUrl + "comment"
    
        let authHeader = ["Authorization" : UserDefaults.standard.string(forKey: "userToken")!, "Content-Type" : "application/json" ]
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: authHeader).responseJSON { (response) in
          
            if response.data != nil{
                
                self.getFeedsRequest(indexpath: (self.tableView.indexPath(for: cell)))
            }else{
                
                let alert = UIAlertController(title: "Couldn't connect to server", message: "Check Internet", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: { (action : UIAlertAction) in
                    alert.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    @objc func commentBtnTap(sender : BaseClass) {
        
        previous = tableView.indexPath(for: sender)
        let currentIndex = previous!.row - 1
        
        let feed  = allFeeds[currentIndex]        
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
      
        //print("^^^^^")
        //print(feed)
        
        if feed.fileType == "text"{
            
            let mainVC = storyBoard.instantiateViewController(withIdentifier: "feedCommentVC") as! FeedCommentVC
           
            mainVC.paramaters = feed
            mainVC.origin = "feed"
            self.present( UINavigationController(rootViewController:mainVC), animated: true, completion: nil)
            
        }else {
            
            let mainVC = storyBoard.instantiateViewController(withIdentifier: "feedImageCommentVC") as! FeedImageCommentVC
            mainVC.paramaters = feed
            mainVC.origin = "feed"
            self.present( UINavigationController(rootViewController:mainVC), animated: true, completion: nil)
            
        }
    }
    
    func likeACell(cell:BaseClass){
        
        let indexpath = tableView.indexPath(for: cell)
        let index = (indexpath?.row)! - 1
        
        if self.allFeeds[index].liked == true {
            
            self.allFeeds[index].likes! -= 1
            self.allFeeds[index].liked = false
        }else {
            self.allFeeds[index].likes! += 1
            self.allFeeds[index].liked = true
        }
        
        if let txt = cell as? TextONLY {
            txt.feed = allFeeds[index]
        }
            
        else if let im = cell as? ImageCell{
            im.feed = allFeeds[index]
            
        }else if let im = cell as? VideoCell{
            im.feed = allFeeds[index]
            
        }else {
        }
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        let userName = UserDefaults.standard.string(forKey: "name")!
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")!
        let iName = UserDefaults.standard.string(forKey: "invitationTitle")
        
        let parameters: Parameters = [
            "postID" : self.allFeeds[index].postID! as Any ,
            "userID" : userID as AnyObject,
            "userName":userName,
            "invitationID":invitationID,
            "type": self.allFeeds[index].type!,
            "likeID": self.allFeeds[index].likeID!,
            "iName": iName!,
            "eventID": self.allFeeds[index].eventID!
        ]
        
        apiCallingMethod(apiEndPints: "like", method: .post, parameters: parameters) { (data1:Any?, totalobj:Int?) in
            if data1 != nil{
            }
        }
    }
    
    func getFeedsRequest(indexpath:IndexPath?){
        
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")!
        
        let prams : Parameters = ["invitationID":invitationID,"limit":10000,"admin":0]
        //print("======params------\(prams)")
        apiCallingMethod(apiEndPints: "comments", method: .get, parameters: prams) { (data1:Any?, totalobj:Int?) in
            //debugPrint(data1)
            if let data = data1 as? [[String : Any]]
            {
                self.nonfilteredArray = []
                self.filteredArray = []
                let tempArray = MInvtdAllFeed.allFeedsFromResults(data as [[String : AnyObject]])
                
                let userID = UserDefaults.standard.integer(forKey: "userID")
                self.nonfilteredArray = tempArray
                
                self.filteredArray = tempArray.filter({  return $0.userID! == userID})
                
                if self.selectedSegmentIndex1 == 0 {
                    self.allFeeds = self.nonfilteredArray
                }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    if indexpath != nil {
                        self.tableView.scrollToRow(at: indexpath! , at: .middle, animated: false)
                    }
                }
            }
        }
        
        let prams2 : Parameters = ["invitationID":invitationID,"limit":10000,"admin":1]
       
        //print("======params2------\(prams2)")
        apiCallingMethod(apiEndPints: "comments", method: .get, parameters: prams2) { (data1:Any?, totalobj:Int?) in
            //print("------debug2------")
            //debugPrint(data1)
            if let data = data1 as? [[String : Any]]
            {
                self.adminFeedArray = []
                self.adminFeedArray = MInvtdAllFeed.allFeedsFromResults(data as [[String : AnyObject]])
     
                if self.selectedSegmentIndex1 != 0 {
                    self.allFeeds = self.adminFeedArray
                }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    if indexpath != nil {
                        self.tableView.scrollToRow(at: indexpath! , at: .middle, animated: false)
                    }
                }
            }
        }
    }
    
    @objc func moreButtonTap(sender : UITableViewCell) {
        
        let currentIndex = (tableView.indexPath(for: sender)?.row)! - 1
        let feed  = allFeeds[currentIndex]
        
        let parameters: Parameters = [ "postID" : feed.postID! as AnyObject,"type": feed.type as AnyObject]
        
        let url = MInvitdClient.Constants.BaseUrl + "comment"
        
        let alert = UIAlertController(title: "Alert", message: "Are you sure you want to delete this post", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: { (action : UIAlertAction) in
            alert.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action : UIAlertAction) in
            
            self.allFeeds.remove(at: currentIndex)
            self.tableView.deleteRows(at: [self.tableView.indexPath(for: sender)!], with: .none)
            let authHeader = ["Authorization" : UserDefaults.standard.string(forKey: "userToken")!, "Content-Type" : "application/json" ]
            
            Alamofire.request(url, method: .delete, parameters: parameters, encoding: JSONEncoding.default, headers: authHeader).responseJSON { (response) in
             
                if response.data != nil{
                    self.getFeedsRequest(indexpath:self.tableView.indexPath(for: sender))
                }else{
                    
                    let alert = UIAlertController(title: "Couldn't connect to server", message: "Check Internet", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: { (action : UIAlertAction) in
                        alert.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                }
            }
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func imageTapSmall() {
        
        photos = []
        let imageBackgroundUrl =  UserDefaults.standard.string(forKey: "invitationImageUrl")
        let currentPhoto = IDMPhoto(url: URL(string : imageBackgroundUrl!))
        photos.append(currentPhoto!)
        
        browser = IDMPhotoBrowser.init(photos: photos)
        browser?.displayCounterLabel = true
        browser?.displayActionButton = false
        browser?.doneButtonRightInset = CGFloat(5.0)
        browser?.doneButtonTopInset = CGFloat(30.0)
        browser?.doneButtonImage = UIImage(named: "cross")
        present(browser!, animated: true, completion: nil)
    }
    
    @objc func imageTapMain() {
        
        photos = []
        let imageBackgroundUrl =  UserDefaults.standard.string(forKey: "invitationCoverUrl")
        let currentPhoto = IDMPhoto(url: URL(string : imageBackgroundUrl!))
        photos.append(currentPhoto!)
        
        browser = IDMPhotoBrowser.init(photos: photos)
        browser?.displayCounterLabel = true
        browser?.displayActionButton = false
        browser?.doneButtonRightInset = CGFloat(5.0)
        browser?.doneButtonTopInset = CGFloat(30.0)
        browser?.doneButtonImage = UIImage(named: "cross")
        present(browser!, animated: true, completion: nil)
    }
    
    func videoTapFunc(_ sender: BaseClass) {
        
        let rowInt  = (tableView.indexPath(for: sender)?.row)! - 1
        let feed  = allFeeds[rowInt]
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "videoPlayVC") as! VideoPlayViewController
        mainVC.url = feed.fileUrl!
        
        self.present(UINavigationController(rootViewController:mainVC), animated: true, completion: nil)
    }
    
    func imageTapFunc(_ sender: BaseClass) {
        
        let rowInt  = (tableView.indexPath(for: sender)?.row)! - 1
        let feed  = allFeeds[rowInt]
    
        if feed.gallery != nil{
            
            photos = [IDMPhotoCustom]()
            
            var counter  = 1
            let total = feed.galleryCount
            
            for photo in feed.gallery! {
                let currentPhoto = IDMPhotoCustom(counter : "\(counter) of \(String(describing: total!))" , imageUrl: URL(string : photo.fileUrl!)!)
                photos.append(currentPhoto)
                counter  = counter + 1
            }
        
            browser = IDMPhotoBrowser.init(photos: photos)
            browser?.displayCounterLabel = true
            browser?.displayActionButton = false
            browser?.doneButtonRightInset = CGFloat(5.0)
            browser?.doneButtonTopInset = CGFloat(30.0)
            browser?.doneButtonImage = UIImage(named: "cross")
            present(browser!, animated: true, completion: nil)
            
        }else{
            
            photos = [IDMPhoto]()
            let currentPhoto = IDMPhoto(url: URL(string : feed.fileUrl!))
            photos.append(currentPhoto!)
            browser = IDMPhotoBrowser.init(photos: photos)
            browser?.displayCounterLabel = true
            browser?.displayActionButton = false
            browser?.doneButtonRightInset = CGFloat(5.0)
            browser?.doneButtonTopInset = CGFloat(30.0)
            browser?.doneButtonImage = UIImage(named: "cross")
            present(browser!, animated: true, completion: nil)
            
        }
    }
}



