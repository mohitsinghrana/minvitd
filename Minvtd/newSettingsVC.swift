
//  newSettingsVC.swift
//  Minvtd
//
//  Created by admin on 08/09/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.


import UIKit
import Alamofire
import SlideMenuControllerSwift
import AlamofireImage

class newSettingsVC: UITableViewController {

    @objc var arr:[String] = []
    var myEvents:[MInvitdEvent] = []
    var venus:[Venue] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let bottonView = UIView()
        bottonView.backgroundColor = UIColor.init(hexString: "E3E3E3")
        tableView.tableFooterView = bottonView
        tableView.isScrollEnabled = false
        tableView.backgroundColor = UIColor(red: 245 / 255, green: 245 / 255, blue: 245 / 255, alpha: 1.0)
        settingsapi()
        navigationBarSetup(nil)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
   
    @IBAction func menu(_ sender: Any) {
        slideMenuController()?.openLeft()

    }
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! newSettingTableViewCell
        cell.contextText.text = arr[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selected = arr[indexPath.row]
        
        switch selected {
            
        case "Delete Invitation":
            deleteAction()
            break
        
        case "Album":
            albumAction()
            break
        
        case "Vendors":
            venuAction()
            break
    
        case "Our story":
            ourStoryAction()
            break
        case "Admin":
            adminAction()
            
            break
        case "Recommended for you" :
            recommandationAction()
            break

        default:
            break;
        }
        
    }
    
    
    @objc func adminAction(){
        
        slideMenuController()?.closeLeft()
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Storyboard", bundle: nil)
        
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "AdminEventVC") as! AdminEventVC
        mainVC.myEvents = myEvents
        self.navigationController?.pushViewController( mainVC, animated: true)

    }

    func recommandationAction () {
         slideMenuController()?.closeLeft()
        let layout = UICollectionViewFlowLayout()
        let featuredAppsController = FeaturedAppsController(collectionViewLayout: layout)
                
        pushPresentAnimation()
        present(UINavigationController(rootViewController:featuredAppsController), animated: false, completion: nil)

        
    }
    @objc func ourStoryAction(){
        
        slideMenuController()?.closeLeft()
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "ourStoryVC") as! OurStoryViewController
        pushPresentAnimation()
       present(UINavigationController(rootViewController:mainVC), animated: false, completion: nil)
    }
    
    @objc func venuAction(){
        
        slideMenuController()?.closeLeft()
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "vendorsVC") as! VendorViewController
        mainVC.titleName = "Vendors"
        pushPresentAnimation()
        present(UINavigationController(rootViewController:mainVC), animated: false, completion: nil)

        
    }
    
    
    func InfoAction(){
        
        slideMenuController()?.closeLeft()
        
        let mainVC = VenueVC()
        mainVC.arr = venus
//        mainVC.currentIndex =
        pushPresentAnimation()
        present(mainVC, animated: false, completion: nil)
        
        
    }
    
    @objc func albumAction(){
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "galleryVC") as! AlbumTemp
        self.present(mainVC, animated: true, completion: nil)
    }
    
    
    @objc func deleteAction(){
        
        slideMenuController()?.closeLeft()
        
        let alert = UIAlertController(title: "Alert", message: "Are you sure you want to delete this invitation", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: { (action : UIAlertAction) in
            
            alert.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action : UIAlertAction) in
            self.actionDeleteInvitation()
        }))
        self.present(alert, animated: true, completion: nil)
        

    }
    
    
    @objc func settingsapi(){
        
        let userID = UserDefaults.standard.string(forKey: "userID")!
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")!

        apiCallingMethod(apiEndPints: "invitation", method: .get, parameters: ["userID":userID,"invitationID":invitationID]) { (data1:Any?, total:Int?) in
            if let data  = data1 as? [[String:Any]]  {
                
                
                self.arr = []
                let mysettings = data[0]
                
                let albumCount = mysettings["albumsCount"] as! Int
                let storylineCount = mysettings["storytimelineCount"] as! Int
                let vendorCount = mysettings["vendorsCount"] as! Int
                let vendorRecommendationsCount = mysettings["vendorRecommendationsCount"] as! Int
                let venuesCount = mysettings["venuesCount"] as! Int
   
                
                 self.myEvents = MInvitdEvent.eventsFromResults(mysettings["events"] as! [[String : AnyObject]])

                if storylineCount != 0 {
                    self.arr.append("Our story")
                }

                if albumCount != 0 {
                    self.arr.append("Album")
                }
                
                if vendorRecommendationsCount != 0 {
                    self.arr.append("Recommended for you")
                    
                }

                if vendorCount != 0 {
                    self.arr.append("Vendors")
                }
                
                let ifAdmin = UserDefaults.standard.string(forKey: "userRole")
                
                if ifAdmin == "admin"{
                    self.arr.append("Admin")
                }
                
                self.arr.append("Delete Invitation")
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    @objc func actionDeleteInvitation(){
        
        let invitaionID = UserDefaults.standard.string(forKey: "invitationID")!
        let userID = UserDefaults.standard.string(forKey: "userID")!
        
        let parameters: Parameters = ["invitationID" : invitaionID , "userID" : userID]
        
        apiCallingMethod(apiEndPints: "invitation/events", method: .delete, parameters: parameters) { (data1:Any?, totalobj:Int?) in
            
            if data1 != nil {
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let leftDrawerVC = storyBoard.instantiateViewController(withIdentifier: "SlideMenuViewController")
                let mainVC = storyBoard.instantiateViewController(withIdentifier: "invitationVC") as! InvitationViewController
                let leftDrawer = SlideMenuController(mainViewController: UINavigationController(rootViewController:mainVC), leftMenuViewController: leftDrawerVC)
                self.pushPresentAnimation()
                self.present(leftDrawer, animated: false, completion: nil)
            }
        }
    }
}
