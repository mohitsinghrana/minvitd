
//  Copyright © 2017 Euroinfotek. All rights reserved.

import UIKit
import Alamofire
import AlamofireImage

class GalleryFeedViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var videos: [MInvitdAllFeedTwo] = [MInvitdAllFeedTwo]()
    var allFeeds: [MInvtdAllFeed] = [MInvtdAllFeed]()
    var allSubComments: [MInvtdAllFeedSubComment]? = [MInvtdAllFeedSubComment]()
    var paramaters : MInvtdAllFeed?
    @objc var origin : String = "feed"
    @objc let placeholderImage = UIImage(named: "eventCoverImage")!
    
    fileprivate let sectionInsets = UIEdgeInsets(top: 5.0, left: 5.0, bottom: 5.0, right: 1.0)
    fileprivate let itemsPerRow: CGFloat = 2
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        navigationBarSetup(nil)
        
        title = "Gallery"
        let button2 = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissVC))
        self.navigationItem.rightBarButtonItem = button2
    }
    
    @objc func dismissVC(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
}


extension GalleryFeedViewController : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        let height = widthPerItem
        
        return CGSize(width: widthPerItem, height: height)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let video = videos[(indexPath as NSIndexPath).row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "galleryCell", for: indexPath) as! GalleryCollectionViewCell
        
        cell.image.af_setImage(
            withURL: URL(string: video.fileThumbnail!)! ,
            placeholderImage: placeholderImage
        )
        if(video.fileType == "video"){
            cell.videoPlayIcon.isHidden = false
        }else{
            cell.videoPlayIcon.isHidden = true
        }
        cell.profileImage.isHidden = true
        cell.LikeImage.isHidden = true
        cell.image.tag = indexPath.row
        let commentTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.commentTap(_:)))
        commentTapGesture.numberOfTapsRequired = 1
        cell.image.isUserInteractionEnabled = true
        cell.image.addGestureRecognizer(commentTapGesture)
        cell.image.contentMode = .scaleAspectFill
        cell.image.clipsToBounds = true
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return videos.count
    }
    
    @objc func commentTap(_ sender: UITapGestureRecognizer) {
        
        let currentIndex = sender.view?.tag
        let feed  = self.videos[currentIndex!]
        getFeedsRequest(commentID: feed.commentID!)
    }
    
    @objc func getFeedsRequest(commentID : Int){
        
        // Activity Incator
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")!
        let userID = UserDefaults.standard.string(forKey: "userID")!
        
        var params = Parameters()
        
        if self.origin == "feed"{
            params = ["commentID" : commentID , "invitationID":invitationID,"userID":userID,"limitSubComment":"1000"]
            
        }
        
        apiCallingMethod(apiEndPints: "comments", method: .get, parameters: params) { (data:Any?, totalobj:Int?) in
            
            if let data1 = data as? [[String : AnyObject]]{
                if let allFeeds1 = MInvtdAllFeed.allFeedsFromResults(data1 ) as? [MInvtdAllFeed] {
                    self.allFeeds = allFeeds1
                    self.paramaters = allFeeds1[0]
                    if allFeeds1[0].subCommentArray != nil
                    {
                        self.allSubComments = allFeeds1[0].subCommentArray!
                    }
                    
                    // Close Activity Indicator
                    
                    if self.paramaters?.fileType == "text"{
                        
                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let mainVC = storyBoard.instantiateViewController(withIdentifier: "feedCommentVC") as! FeedCommentVC
                        mainVC.paramaters = self.paramaters
                        mainVC.origin = "feed"
                        self.present( UINavigationController(rootViewController:mainVC), animated: true, completion: nil)

                    }else {
                        
                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let mainVC = storyBoard.instantiateViewController(withIdentifier: "feedImageCommentVC") as! FeedImageCommentVC
                        mainVC.paramaters = self.paramaters
                        mainVC.origin = "feed"
                        self.present( UINavigationController(rootViewController:mainVC), animated: true, completion: nil)

                    }
                    
                }
                
            }
        }
    }
}
