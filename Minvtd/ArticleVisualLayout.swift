//
//  ArticleVisualLayout.swift
//  Minvtd
//
//  Created by Vivek Aggarwal on 20/05/18.
//  Copyright © 2018 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit
class ArticleVisualLayout: UICollectionViewLayout {
    
    fileprivate var contentWidth:CGFloat!
    fileprivate var contentHeight:CGFloat!
    fileprivate var xOffset:CGFloat = 0
    
    var maxAlpha:CGFloat = 1
    var minAlpha:CGFloat = 0
    
    var widthOffset:CGFloat = 35
    var heightOffset:CGFloat = 35
    
    fileprivate var cache = [UICollectionViewLayoutAttributes]()
    
    fileprivate var itemWidth:CGFloat{
        return (collectionView?.bounds.width)!
    }
    fileprivate var itemHeight:CGFloat{
        return (collectionView?.bounds.height)!
    }
    fileprivate var collectionViewHeight:CGFloat{
        return (collectionView?.bounds.height)!
    }
    fileprivate var collectionViewWidth:CGFloat{
        return (collectionView?.bounds.width)!
    }
    
    fileprivate var numberOfItems:Int{
        return (collectionView?.numberOfItems(inSection: 0))!
    }
    
    
    fileprivate var dragOffset:CGFloat{
        return (collectionView?.bounds.width)!
    }
    fileprivate var currentItemIndex:Int{
        return max(0, Int(collectionView!.contentOffset.x / collectionViewWidth))
    }
    
    var nextItemBecomeCurrentPercentage:CGFloat{
        return (collectionView!.contentOffset.x / (collectionViewWidth)) - CGFloat(currentItemIndex)
    }
    
    override func prepare() {
        cache.removeAll(keepingCapacity: false)
        xOffset = 0
        
        for item in 0 ..< numberOfItems{
            
            let indexPath = IndexPath(item: item, section: 0)
            let attribute = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            attribute.zIndex = -indexPath.row
            
            if (indexPath.item == currentItemIndex+1) && (indexPath.item < numberOfItems){
                
                attribute.alpha = minAlpha + max((maxAlpha-minAlpha) * nextItemBecomeCurrentPercentage, 0)
                let width = itemWidth - widthOffset + (widthOffset * nextItemBecomeCurrentPercentage)
                let height = itemHeight - heightOffset + (heightOffset * nextItemBecomeCurrentPercentage)
                
                let deltaWidth =  width/itemWidth
                let deltaHeight = height/itemHeight
                
                attribute.frame = CGRect(x: xOffset, y: 0, width: itemWidth, height: itemHeight)
                attribute.transform = CGAffineTransform(scaleX: deltaWidth, y: deltaHeight)
                
                attribute.center.y = (collectionView?.center.y)! +  (collectionView?.contentOffset.y)!
                attribute.center.x = (collectionView?.center.x)! + (collectionView?.contentOffset.x)!
                xOffset += collectionViewWidth
                
            }else{
                attribute.frame = CGRect(x: xOffset, y: 0, width: itemWidth, height: itemHeight)
                attribute.center.y = (collectionView?.center.y)!
                attribute.center.x = (collectionView?.center.x)! + xOffset
                xOffset += collectionViewWidth
            }
            cache.append(attribute)
        }
    }
    
    //Return the size of ContentView
    override var collectionViewContentSize : CGSize {
        contentHeight = (collectionView?.bounds.height)!
        contentWidth = CGFloat(numberOfItems) * (collectionView?.bounds.width)!
        return CGSize(width: contentWidth, height: contentHeight)
    }
    
    //Return Attributes  whose frame lies in the Visible Rect
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var layoutAttributes = [UICollectionViewLayoutAttributes]()
        for attribute in cache{
            if attribute.frame.intersects(rect){
                layoutAttributes.append(attribute)
            }
        }
        return layoutAttributes
    }
    
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        let itemIndex = round(proposedContentOffset.x / (dragOffset))
        let xOffset = itemIndex * (collectionView?.bounds.width)!
        return CGPoint(x: xOffset, y: 0)
    }
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        
        // Logic that calculates the UICollectionViewLayoutAttributes of the item
        // and returns the UICollectionViewLayoutAttributes
        return UICollectionViewLayoutAttributes(forCellWith: indexPath)
    }
    
}

