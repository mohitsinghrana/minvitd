//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import Foundation
import UIKit

class InvitationCollectionViewCell : UICollectionViewCell {
    
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var notificationLabel: UILabel!
    @IBOutlet weak var notiView: UIView!
    @IBOutlet weak var bellView: UIView!
    @IBOutlet weak var gradientView: UIView!
    
    @IBOutlet weak var special: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let layer = CAGradientLayer()
        layer.colors = [UIColor.clear.cgColor,UIColor.init(white: 0, alpha: 0.5).cgColor]
        layer.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.gradientView.bounds.height)
        //special.bounds.height
        self.gradientView.layer.insertSublayer(layer, at: 0)
    }
}
