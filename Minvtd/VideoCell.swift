//
//  VideoCell.swift
//  Minvtd
//
//  Created by admin on 01/12/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit

class VideoCell: BaseClass {
    
    var  topheightConst:NSLayoutConstraint?
    var height:CGFloat = 94

    var feed:MInvtdAllFeed? {
        didSet {
            cellheader.feed = feed!
            cellFooter.feed = feed!
            contentImg.af_setImage(withURL: URL(string:(feed?.fileThumbnail)!)!)

            if let comment = feed?.comment {
                let size = CGSize(width: UIScreen.main.bounds.width - 40, height: 1000)
                
                let options:NSStringDrawingOptions =  NSStringDrawingOptions.usesFontLeading.union(NSStringDrawingOptions.usesLineFragmentOrigin)
                
                let estimatedFrame = NSString(string: comment).boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font:UIFont(name: "GothamRounded-Book", size: 12)!], context: nil)
                
                topheightConst?.constant = height + estimatedFrame.height + 8
            }
        }
    }
    
    let contentImg = { () -> UIImageView in
        let imgView = UIImageView()
        imgView.clipsToBounds = true
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.backgroundColor = .black
        imgView.isUserInteractionEnabled = true
        return imgView
    }()
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(cellheader)
        addSubview(cellFooter)
        addSubview(contentImg)
        smallSetup()
        
        contentImg.anchor(cellheader.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: heightImg)
        
        cellheader.anchor(topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant:
            0)
        
        topheightConst = NSLayoutConstraint(item: cellheader, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: height)
        addConstraint(topheightConst!)

        cellFooter.anchor(contentImg.bottomAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant:0 )
        contentImg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(playThisVideo)))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func playThisVideo(){
    
        VC?.videoTapFunc(self)
    }
    
}


