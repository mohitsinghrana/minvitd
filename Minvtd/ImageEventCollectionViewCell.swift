//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import Foundation
import UIKit

class ImageEventCollectionViewCell : UICollectionViewCell {
    @IBOutlet weak var responseLabel: UILabel!
    
    @IBOutlet weak var changeButton: UIButton!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var descript: UILabel!
    
    @IBOutlet weak var eventName: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var venue: UILabel!
    @IBOutlet weak var time: UILabel!
    
    @IBOutlet weak var heartBtn: UIButton!
    @IBOutlet weak var likedLabel: UILabel!
    @IBOutlet weak var commentBtn: UIButton!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var scheduleBtn: UIButton!
    @IBOutlet weak var locationBtn: UIButton!
    @IBOutlet weak var statusButton: UIButton!
    
    @IBOutlet weak var statusChangeBtn: UIButton!
    
    @IBOutlet weak var circleImage: UIImageView!
    @IBOutlet weak var circleEventName: UILabel!
    @IBOutlet weak var circleVenue: UILabel!
    
    @IBOutlet weak var bottomView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
}
