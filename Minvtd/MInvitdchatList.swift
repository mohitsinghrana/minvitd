//
//  File.swift
//  Minvtd
//
//  Created by admin on 12/08/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.
//

import Foundation


struct MInvitdchatList {
    
    let chatID:String?
    let created:Double?
    let fileID: Int?
    let fileThumbnail:String?
    let fileUrl: String?
    let firebaseID:String?
    let invitationID: Int?
    let message:String?
    let name:String?
    let updated: Double?
    let userID:Int?
    let members:[MInvitdchatList]?
    let group:Int?
    init(dictionary: [String:AnyObject]) {
        
        self.chatID = dictionary["chatID"] as? String
        self.fileThumbnail = dictionary["fileThumbnail"] as? String
        self.fileUrl = dictionary["fileUrl"] as? String
        self.firebaseID = dictionary["firebaseID"] as? String
        self.message = dictionary["message"] as? String
        self.name = dictionary["name"] as? String
        
        
        self.created = dictionary["created"] as? Double
        self.updated = dictionary["updated"] as? Double
        
        self.fileID = dictionary["fileID"] as? Int
        self.invitationID = dictionary["invitationID"] as? Int
        self.userID = dictionary["userID"] as? Int
        self.members = dictionary["members"] as? [MInvitdchatList]
        self.group =  dictionary["group"] as? Int
        
    }
    
    
    init(chatID:String,name:String,invitationID:Int,group:Int = 0) {
        
        
        self.name = name
        self.invitationID = invitationID
        self.chatID = chatID

        self.created = nil
        self.fileID = nil
        self.fileThumbnail = nil
        self.fileUrl = nil
        self.firebaseID = nil
        self.message = nil
        self.updated = nil
        self.userID = nil
        self.members = nil
        self.group = group
    }
    
    static func resultFromServerData(data1: [[String:AnyObject]])->[MInvitdchatList]{
        
        var response = [MInvitdchatList]()
        for each in data1 {
            
            let newChat = MInvitdchatList(dictionary: each)
            
            response.append(newChat)
            
        }
        return response
        
    }
    
}






