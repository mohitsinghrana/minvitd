//
//  VenueModel.swift
//  Experiment
//
//  Created by admin on 13/11/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

struct VenueTab {
    
    var icon:String?
    var title:String?
    var body:String?
    var collapsed = true
    var arrtibutedString:NSAttributedString? = nil

    init(dictionary: [String:AnyObject]) {
        icon = dictionary["icon"] as? String
        title = dictionary["title"] as? String
        body = dictionary["body"] as? String
    }
    
    init(arrtibutedString:NSAttributedString) {
        self.arrtibutedString = arrtibutedString
        self.title = "Location"
        self.icon = "location"
    }

    static func venueTabFromResults(_ results: [[String:AnyObject]] , arrtibutedString:NSAttributedString) -> [VenueTab] {
        
        var venueTabs = [VenueTab]()
        let first = VenueTab(arrtibutedString: arrtibutedString)
        venueTabs.append(first)
        
        for venueTab in results {
            venueTabs.append(VenueTab(dictionary: venueTab))
        }
        
        return venueTabs
    }

}

struct Venue{
        
        // MARK: Properties
        
        let address : String?
        let contactPerson : String?
        let coordinates : String?
        let eventDate : String?
        let phone : Int64?
        let eventName : String?
        let eventTimestamp : Int64?
        let venueID : Int?
        let eventID : Int?
        var gallery:[MInvitdPhotos] = []
        var tabs:[VenueTab] = []

        // MARK: Initializers
        
        init(dictionary: [String:AnyObject]) {
            address = dictionary["address"] as? String
            contactPerson = dictionary["address"] as? String
            phone = dictionary["contactPerson"] as? Int64
            coordinates = dictionary["coordinates"] as? String
            eventDate = dictionary["eventDate"] as? String
            eventName = dictionary["eventName"] as? String
            eventTimestamp = dictionary["eventTimestamp"] as? Int64
            venueID = dictionary["venueID"] as? Int
            eventID = dictionary["eventID"] as? Int
            if let data =  dictionary["gallery"]  as? [[String:AnyObject]]{
                gallery = MInvitdPhotos.photosFromResults(data)
            }
            
            let attribute = NSMutableAttributedString()
            if let data =  dictionary["tabs"]  as? [[String:AnyObject]]{
                if let address = self.address{
                    let addressAtt = NSMutableAttributedString(string: address + "\n" )
                    attribute.append(addressAtt)
                }
                
                let coordinateAtt = NSMutableAttributedString(string: "Click to show maps")
                let myRange = NSRange(location: 0, length: 18) // range of "Swift"
                //print( "coordinates--\( self.coordinates )")
                if let coordinates = self.coordinates{
                    coordinateAtt.addAttributes(
                        [NSAttributedStringKey.foregroundColor:UIColor.blue,
                         NSAttributedStringKey.link:"custom://" +  coordinates
                        ],range:myRange )
                    attribute.append(coordinateAtt)
                }
                
                tabs = VenueTab.venueTabFromResults(data, arrtibutedString: attribute)
            }            
        }
        
        static func venueFromResults(_ results: [[String:AnyObject]]) -> [Venue] {
            
            var venues = [Venue]()

            for venue in results {
                venues.append(Venue(dictionary: venue))
            }
            return venues
        }
}
