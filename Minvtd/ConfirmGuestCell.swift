//
//  ConfirmGuestCell.swift
//  Minvtd
//
//  Created by admin on 10/08/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit

class ConfirmGuestCell: UITableViewCell {

    @objc var firstLetter = ""
    
    @objc let nameLabel = { () -> UILabel in 
        let lab = UILabel()
        lab.text = ""
        lab.textColor = .black
        lab.font = UIFont.systemFont(ofSize: 17.0)
        return lab
    }()
    
    @objc let imageLabel = { () -> UILabel in
        let lab = UILabel()
        lab.text = ""
        lab.textColor = .white
        lab.font = UIFont.systemFont(ofSize: 20.0)
        lab.textAlignment = .center
        
        
        return lab
    }()
    
    @objc let image1 = { () -> UIImageView in
    let lab = UIImageView()
    let layer1 = CAGradientLayer.gradientLayerForBoundsSmall(bounds: CGRect(x: 0, y: 0, width: 40, height: 40))
        lab.layer.insertSublayer(layer1, at: 0)
        lab.layer.cornerRadius = 20.0
        lab.clipsToBounds = true
    
    return lab
    }()

    @objc let phoneNo = { () -> UILabel in
        let lab = UILabel()
        lab.text = ""
        lab.textColor = .black
        lab.font = UIFont.systemFont(ofSize: 13.0)
        return lab
    }()


    var guest:MInvitdGuest?{
        
        didSet {
            nameLabel.text = guest?.name
            phoneNo.text = guest?.phone
            
            if let firstChar = guest?.name?.characters.first {
                
                firstLetter = String(describing: firstChar)
                imageLabel.text = firstLetter
                
            }else {
                firstLetter = "?"
                imageLabel.text = firstLetter
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        imageLabel.layer.cornerRadius  = imageLabel.frame.size.width/2
        imageLabel.clipsToBounds = true
        // Initialization code
    }

    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
        selectionStyle = .none
    }

    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }

    @objc func setup(){
        
        contentView.addSubview(image1)
        contentView.addSubview(imageLabel)
        contentView.addSubview(nameLabel)
        contentView.addSubview(phoneNo)

        imageLabel.anchor(contentView.topAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: nil, topConstant: 10, leftConstant: 10, bottomConstant: 10, rightConstant: 0, widthConstant: 40, heightConstant: 0)
        
                image1.anchor(contentView.topAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: nil, topConstant: 10, leftConstant: 10, bottomConstant: 10, rightConstant: 0, widthConstant: 40, heightConstant: 0)
        nameLabel.anchor(contentView.topAnchor, left: imageLabel.rightAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 10, leftConstant: 20.0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
         phoneNo.anchor(nameLabel.bottomAnchor, left: imageLabel.rightAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 10, leftConstant: 20.0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        imageLabel.clipsToBounds = true
        
        
        

        
    }
    
    
}
