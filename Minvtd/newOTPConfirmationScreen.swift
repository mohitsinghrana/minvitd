
//  newOTPConfirmationScreen.swift
//  Minvtd
//  Created by admin on 05/09/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.


import UIKit
import Firebase
import SlideMenuControllerSwift

class newOTPConfirmationScreen: UIViewController {

    @objc var varificationID = ""

    @IBOutlet weak var t1: UITextField!
    @IBOutlet weak var t2: UITextField!
    @IBOutlet weak var t3: UITextField!
    @IBOutlet weak var t4: UITextField!
    @IBOutlet weak var t5: UITextField!
    @IBOutlet weak var t6: UITextField!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        disssmissKeyboard()
        navigationBarSetup(nil)
        
        t1.layer.masksToBounds = true
        t2.layer.masksToBounds = true
        t3.layer.masksToBounds = true
        t4.layer.masksToBounds = true
        t5.layer.masksToBounds = true
        t6.layer.masksToBounds = true
        
        t1.layer.cornerRadius = 15.0
        t2.layer.cornerRadius = 15.0
        t3.layer.cornerRadius = 15.0
        t4.layer.cornerRadius = 15.0
        t5.layer.cornerRadius = 15.0
        t6.layer.cornerRadius = 15.0
        
        t1.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        t2.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        t3.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        t4.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        t5.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
        t6.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: .editingChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        t1.becomeFirstResponder()
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @objc func textFieldDidChange(textField: UITextField){
        
        let text = textField.text
        
        if text?.utf16.count == 1{
            switch textField{
            case t1:
                t2.becomeFirstResponder()
            case t2:
                t3.becomeFirstResponder()
            case t3:
                t4.becomeFirstResponder()
            case t4:
                t5.becomeFirstResponder()
            case t5:
                t6.becomeFirstResponder()
            case t6:
                t6.becomeFirstResponder()
            default:
                break
            }
        }
        else{}
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func ResendOTP(_ sender: Any) {
        
        Spinner.sharedinstance.startSpinner(viewController: self)
        
        let selectedCountryCode = UserDefaults.standard.string( forKey : "countryCode")
        let phoneNo = UserDefaults.standard.string( forKey: "phoneNo")
        //print("\(selectedCountryCode!)")
        //print("\(phoneNo!)")
        
        PhoneAuthProvider.provider().verifyPhoneNumber(("\(selectedCountryCode!)"+(phoneNo)!), uiDelegate: nil) { (verificationID, error) in
            
            //print("This is the phone no with country code to verify :\(selectedCountryCode!)\(phoneNo!)")
            
            Spinner.sharedinstance.stopSpinner(viewController: self)
            
            if let error = error {
                self.dismissAlert(error.localizedDescription)
                return
            }
            UserDefaults.standard.set(verificationID!, forKey: "authVerificationID")
            UserDefaults.standard.set(phoneNo!, forKey: "phone")
            self.varificationID = verificationID!
            self.CreateAlertPopUp("OTP Send", "A new OTP has been sent to: \(phoneNo!)", OnTap: nil)
        }
    }
    
    
    @IBAction func createNewAccount () {
        
        let confirmOtp = "\(t1.text!)\(t2.text!)\(t3.text!)\(t4.text!)\(t5.text!)\(t6.text!)"
        
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: varificationID,
            verificationCode: (confirmOtp))
        
        
        Spinner.sharedinstance.startSpinner(viewController: self)

        Auth.auth().signIn(with: credential) { (user, error) in
            Spinner.sharedinstance.stopSpinner(viewController: self)

            if let error = error {
                self.dismissAlert(error.localizedDescription)
                return
            }
            else{
                
                //print("\(user?.phoneNumber)")
                
                let phoneNo = user?.phoneNumber?.replacingOccurrences(of: "+", with: "")
                let firebaseID = user?.uid
                
                UserDefaults.standard.setValue(firebaseID, forKey: "firebaseID")
                UserDefaults.standard.setValue(phoneNo, forKey: "phone")
                
                self.checkUserExist(phoneNo: phoneNo!, fid: (user?.uid)!)
                // User is signed in
            }
        }
    }
    
    @objc func checkUserExist(phoneNo:String,fid:String){
        
        let code = UserDefaults.standard.object(forKey: "countryCode")
        let phone = UserDefaults.standard.object(forKey: "phoneNo")
        let deviceID = UIDevice.current.identifierForVendor?.uuidString
        let modelName = UIDevice.modelName
        let systemVersion = UIDevice.current.systemVersion
        let device = "iOS"
        let nsObject: AnyObject? = Bundle.main.infoDictionary!["CFBundleVersion"] as AnyObject
        let version = nsObject as! String
        let firebaseID =  UserDefaults.standard.string(forKey: "firebaseID")
//      let version = "18.10.25"
        
        let params = ["phone" : phone!,
                      "code": code! ,
                      "deviceID":deviceID!,
                      "device": device,
                      "model" : modelName,
                      "os" : systemVersion,
                      "version" : version,
                      "firebaseID" : firebaseID!,
                      "manufacturer": "apple"
        ]
        
        //print(params)
        
        //print("This is UserDefaults Countrycode\(code!) and phoneNo \(phone!))")
            
        apiCallingMethod(apiEndPints: "user/account/check", method: .post, parameters: params) { (data1:Any?, totalobj:Int?) in
            
            //debugPrint(data1)
            
            if let data = data1 as? [[String : Any]]
            {
                //print(data)

                let userID = data[0]["userID"] as? Int
                let userToken = data[0]["userToken"] as? String
                let newUser = data[0]["newUser"] as? Bool

                //print("user token is \(userToken)")

                UserDefaults.standard.set(userID , forKey : "userID")
                UserDefaults.standard.set(userToken , forKey : "userToken")
                UserDefaults.standard.set(newUser , forKey : "newUser")

                if newUser == false{

                    //  User already exists
                    let name  = data[0]["userName"] as? String
                    let userAvatar = data[0]["userAvatar"] as? String
                    let email = data[0]["userEmail"] as? String
                    let dob = data[0]["userDob"] as? String
                    let imageID = data[0]["imageID"] as? String
                    let fileID = data[0]["fileID"] as? String
                    
                    UserDefaults.standard.set(imageID, forKey: "imageID")
                    UserDefaults.standard.set(name, forKey: "name")
                    UserDefaults.standard.set(email, forKey: "email")
                    UserDefaults.standard.set(userAvatar, forKey: "userAvatar")
                    UserDefaults.standard.set(userAvatar, forKey: "imageUrl")
                    UserDefaults.standard.set(dob, forKey: "dob")
                    
                    if fileID == nil{
                        UserDefaults.standard.set("0", forKey: "fileID")
                    }else{
                        UserDefaults.standard.set(fileID, forKey: "fileID")
                    }

                    // Instantiate InvitationViewController
                    DispatchQueue.main.async {
                        let story = UIStoryboard(name: "Main", bundle: nil)

                        let leftDrawerVC = story.instantiateViewController(withIdentifier: "SlideMenuViewController") as! SlideMenuViewController
                        let mainVC = story.instantiateViewController(withIdentifier: "invitationVC") as! InvitationViewController
                        let leftDrawer = SlideMenuController(mainViewController: UINavigationController(rootViewController:mainVC), leftMenuViewController: leftDrawerVC)
                        
                        self.present(leftDrawer, animated: true, completion: nil)
                        tokkenUpdate()
                    }
                }
                else{
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileUpdateVC") as? ProfileUpdateVC
                    vc?.firebaseID = fid
                    vc?.register = 1

                    self.present(UINavigationController(rootViewController:vc!), animated: true, completion: nil)
                    tokkenUpdate()
                }
            }
            else{
                print("Unable to check account details...")
            }
        }
    }
}
