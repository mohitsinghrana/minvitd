//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//


import UIKit

class PostHeaderViewCell: UICollectionReusableView {
    
    @IBOutlet weak var postTextView: UITextView!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var uploadPhotoBtn: UIButton!

}

