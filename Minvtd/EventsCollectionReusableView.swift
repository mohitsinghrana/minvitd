//
//  EventsCollectionReusableView.swift
//  Minvtd
//
//  Created by Tushar Aggarwal on 25/07/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit

class EventsCollectionReusableView: UICollectionReusableView {
        
    @IBOutlet weak var addFeedView: UIView!
    
    @IBOutlet weak var postFeedTV: UITextView!
    
    @IBOutlet weak var uploadButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        postFeedTV.returnKeyType = UIReturnKeyType.send
        
    }

    
}
