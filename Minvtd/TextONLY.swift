//
//  TextONLY.swift
//  MinvitdFeeds
//
//  Created by admin on 10/11/17.
//  Copyright © 2017 Sachin. All rights reserved.
//

import UIKit


class BaseClass: UITableViewCell{
    
    
    var VC:AllFeedViewController?
    let date = Date()

    var cellheader = { () -> FeedCellHeaderView in
        let head = FeedCellHeaderView()
        head.clipsToBounds = true
        // height calculation   16 + 90 + textHeight
        
        return head
    }()
    
    var cellFooter = { () -> FeedCellFooterView in
        let head = FeedCellFooterView()
        head.clipsToBounds = true
        
  //      head.feed = feed
        return head
    }()
    
    @objc func deleteThisCell(){
        
        VC?.moreButtonTap(sender:  self)
    }
    
    @objc func commentThisCell(){
        
        VC?.commentBtnTap(sender:  self)
    }
    
    @objc func likeThisCell(){
        
        VC?.likeACell(cell: self)
    }
    
    @objc func commentonThisCell(sender:UITextField){
        
        VC?.sendSubComment(cell: self, string: sender.text!)
        sender.text = ""
    }
    
    @objc func showGuests(){
        
        VC?.showGuests(cell: self)
    }

    func smallSetup(){
        
        cellFooter.likeButton.addTarget(self, action: #selector(likeThisCell), for: .touchUpInside)
        cellFooter.commentButton.addTarget(self, action: #selector(commentThisCell), for: .touchUpInside)
        cellheader.deleteButton.addTarget(self, action: #selector(deleteThisCell), for: .touchUpInside)
        cellFooter.textField.addTarget(self, action: #selector(commentonThisCell(sender:)),  for: .editingDidEndOnExit)
        cellFooter.tagVcOpen.addTarget(self, action: #selector(showGuests),  for: .touchUpInside)
    }
}


class TextONLY: BaseClass {
    
    var  topheightConst:NSLayoutConstraint?
    var height:CGFloat = 94

    var feed:MInvtdAllFeed? {
        didSet {
            cellheader.feed = feed!
            cellFooter.feed = feed!
            //print("text only feed-> \(feed)")
            if let comment = feed?.comment {
                let size = CGSize(width: UIScreen.main.bounds.width - 40, height: 1000)
                
                let options:NSStringDrawingOptions =  NSStringDrawingOptions.usesFontLeading.union(NSStringDrawingOptions.usesLineFragmentOrigin)
                
                let estimatedFrame = NSString(string: comment).boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font:UIFont(name: "GothamRounded-Book", size: 12)!], context: nil)
                
                topheightConst?.constant = height + estimatedFrame.height + 8
            }
        }
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(cellheader)
        addSubview(cellFooter)
        
        cellheader.anchor(topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant:0)
        
        cellFooter.anchor(cellheader.bottomAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant:0 )
        
        topheightConst = NSLayoutConstraint(item: cellheader, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: height)
        
        addConstraint(topheightConst!)

        smallSetup()


    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func prepareForReuse() {
        cellFooter.comment.isHidden = false
    }
}

class firstCell:UITableViewCell,UITextFieldDelegate{
    
    var VC:AllFeedViewController?

    let textField = { () -> PaddedTextField in
        let ti = PaddedTextField()
        ti.textColor = UIColor.gray
        ti.layer.borderColor = UIColor.black.cgColor
        ti.translatesAutoresizingMaskIntoConstraints = false
        ti.layer.borderWidth = 0.5
        ti.layer.cornerRadius = 5.0
        ti.clipsToBounds = true
        ti.rightViewMode = .always
        ti.placeholder = "  Write a comment ..."
        let img = UIImageView()
        img.image = #imageLiteral(resourceName: "camera")
        img.contentMode = .scaleAspectFit
        ti.rightView = img
        
        return ti
    }()
    
    let whiteBorder = { () -> UIView in
        let ti = UIView()
        ti.translatesAutoresizingMaskIntoConstraints = false
        ti.backgroundColor = .white
        return ti
    }()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(whiteBorder)
        addSubview(textField)
        
        textField.anchorCenterYToSuperview()
        textField.anchorCenterXToSuperview()
        textField.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width - 40).isActive = true
        textField.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        whiteBorder.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 5, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        textField.delegate = self
        //addTarget(self, action: #selector(writePostTap), for: .touchUpInside)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        VC?.previous = IndexPath(item: 0, section: 0)
        writePostTap()
    }
    
    
        @objc func writePostTap() {
            textField.endEditing(true)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainVC = storyboard.instantiateViewController(withIdentifier: "writePost") as! PostViewController
            VC?.present( UINavigationController(rootViewController:mainVC), animated: true, completion: nil)
        }
}

class PaddedTextField: UITextField {
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        let rightBounds = CGRect(x: bounds.size.width-30, y: 10, width: 25, height: bounds.size.height - 20)
        
        return rightBounds
    }
}

