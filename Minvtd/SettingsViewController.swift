//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//


import Foundation
import UIKit
import Alamofire
import AlamofireImage

class SettingsViewController : UIViewController {
    
    @IBOutlet weak var contactUsView: UIView!
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var versionLabel: UILabel!
    
    @IBAction func menu(_ sender: UIBarButtonItem) {
        slideMenuController()?.openLeft()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
       
        let contactUsTap = UITapGestureRecognizer(target: self, action: #selector(self.contactUsTapDetected(_:)))
        contactUsTap.numberOfTapsRequired = 1
        contactUsView.isUserInteractionEnabled = true
        contactUsView.addGestureRecognizer(contactUsTap)
        
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            self.versionLabel.text = version
        }
    }
    
    @objc func contactUsTapDetected(_ sender: UITapGestureRecognizer) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "contactUsVC") as! ContactUsViewController
        
        pushPresentAnimation()
        self.present(mainVC, animated: false, completion: nil)

        
    }
    
    

}
