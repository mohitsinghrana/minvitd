//
//  BasicFunction.swift
//  Minvtd
//
//  Created by Tushar Aggarwal on 09/06/17.
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import Foundation
import Alamofire
import AVFoundation
import Firebase


class GetProfile {
    
    
    static let sharedinstance = GetProfile()
    
    func doSomethingAsync(Imageurl:String,completion: @escaping (ChatStruct) -> ()) {
        
   //     let url  = "\(MInvitdClient.Constants.BaseUrl)users?Imageurl=" + Imageurl
   //     let authHeader = ["Authorization" : UserDefaults.standard.string(forKey: "userToken")!, "Content-Type" : "application/json" ]
        
        let url = MInvitdClient.Constants.BaseUrl + "users"
        let parameters: Parameters = ["Imageurl" : Imageurl]
        let authHeader = ["Authorization" : UserDefaults.standard.string(forKey: "userToken")!, "Content-Type" : "application/json" ]
        
        Alamofire.request(url, method: .get, parameters: parameters, encoding: JSONEncoding.default, headers: authHeader).responseJSON { (response) in
     
            if response.error == nil {
                let user1 = self.actionUserData(JSONData : response.data!)
                completion(user1!)
            }else{
                print("Unable to fetch user details.")
            }
            
        }
    }
    
    
    func actionUserData(JSONData : Data)-> ChatStruct?
    {
        do{
            let json = try JSONSerialization.jsonObject(with: JSONData, options: [])
            
            if let dictionary = json as? [String : Any] {
                if let status = dictionary["status"] as? String
                {
                    if status == "success"
                    {
                        if let data = dictionary["data"] as? [[String : Any]]
                        {
                            if let name  = data[0]["name"] as? String,
                                let imageUrl = data[0]["imageUrl"] as? String
                            {
                                UserDefaults.standard.set(name, forKey: "name")
                                UserDefaults.standard.set(imageUrl, forKey: "imageUrl")
                                let user1 = ChatStruct(name: name, imgUrl:imageUrl )
                                return user1
                            }
                        }
                    }
                }
            }
            
        }
        catch{
            
        }
        return nil
    }
    
}





//// chat Structure
//
struct ChatStruct {
    
    var name : String
    var imgUrl : String
    
    init(name:String,imgUrl:String) {
        self.name = name
        self.imgUrl = imgUrl
    }
    
}



// trying



func thumbnailForVideoAtURL(url: URL) -> UIImage? {
    
    let asset = AVAsset(url: url)
    let assetImageGenerator = AVAssetImageGenerator(asset: asset)
    
    var time = asset.duration
    time.value = min(time.value, 2)
    
    do {
        let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
        return UIImage(cgImage: imageRef)
    } catch {
        return nil
    }
}





func validateString(string : String) -> Bool{
    
    let charactersNew = String.trimmingCharacters(string)
    let stringNew = String(describing: charactersNew)
    if string == ""{
        return false
    }
    if stringNew == ""{
        return false
    }
    return true
}


func tokkenUpdate() {
    if let token = InstanceID.instanceID().token()
    {
        let userID = UserDefaults.standard.string(forKey: "userID")
        UserDefaults.standard.set(token, forKey: "fcmToken")

        let parameters: Parameters = [
            "userID" : userID as AnyObject,
            "fcmToken" : token as AnyObject,
            "login" : NSDate().timeIntervalSince1970
        ]

        let url = MInvitdClient.Constants.BaseUrl + "user"

        let authHeader = ["Authorization" : UserDefaults.standard.string(forKey: "userToken")!, "Content-Type" : "application/json" ]
        
        Alamofire.request(url, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: authHeader).responseJSON { (response) in
            if response.data != nil{
                //print("This is the reponse of tokenUpdate url user : \(response)")
            }
        }
    }
}






