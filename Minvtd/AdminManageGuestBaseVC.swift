//
//  AdminManageGuestBaseVC.swift
//  Minvtd
//
//  Created by admin on 21/09/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit
import PageMenu


class AdminManageGuestBaseVC: UIViewController ,CAPSPageMenuDelegate{
    
    
    @objc var pageMenu : CAPSPageMenu?
    @objc var eventID : Int = 0
    private var nature = true
    var guests:[MInvitdGuest] = []
    var vc4:ManageGuestUndecided?
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBarSetup(nil)
        setup()
    }
    
    @objc func setup() {
        
        self.title = "Guests"
        let button1 = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(dissmissVc))
        self.navigationItem.leftBarButtonItem  = button1
        
        var controllerArray : [UIViewController] = []
        
       // All
        let vc1 = createVC(tittle: "ALL", guests: guests)
        controllerArray.append(vc1)
        // Yes
        let guestYes =  self.guests.filter({return $0.status == 1})
        
        let vc2 = createVC(tittle: "YES", guests: guestYes)
        controllerArray.append(vc2)


        let guestNO =  self.guests.filter({return $0.status == 2})

        let vc3 = createVC(tittle: "NO", guests: guestNO)
        controllerArray.append(vc3)
        
        let guestUndecided =  self.guests.filter({return $0.status == 0})

        vc4 = self.storyboard?.instantiateViewController(withIdentifier: "ManageGuestUndecided") as? ManageGuestUndecided
        vc4!.guests = guestUndecided
        vc4!.orignalguest = guestUndecided
        vc4!.title = "UNDECIDED"
        vc4!.eventID  = eventID
        controllerArray.append(vc4!)
   
        let colour1 = UserDefaults.standard.string(forKey: "colour1")

        let parameters: [CAPSPageMenuOption] = [
            .useMenuLikeSegmentedControl(true),
            .menuItemFont(UIFont.systemFont(ofSize: 15.0)),
            .unselectedMenuItemLabelColor(UIColor.gray),
            .selectedMenuItemLabelColor (UIColor.black),
            .scrollMenuBackgroundColor (UIColor.white),
            .selectionIndicatorColor(UIColor.init(hexString: colour1!)!),
            .menuHeight(45.0),
            .addBottomMenuHairline (true),
            .bottomMenuHairlineColor(UIColor.gray),
            .menuItemSeparatorWidth(0.0),
            .menuItemSeparatorPercentageHeight(0.0),
            .menuItemWidthBasedOnTitleTextWidth (true)
        ]
        
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: view.frame
            //CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height)
            
            , pageMenuOptions: parameters)
        
        // Lastly add page menu as subview of base view controller view
        pageMenu?.delegate = self
        self.view.addSubview(pageMenu!.view)

    }
    
    func didMoveToPage(_ controller: UIViewController, index: Int) {
        if index == 3 {
            let btn = UIBarButtonItem(title: "Resend", style: .plain, target: self, action: #selector(invoke))
            let btn1 = UIBarButtonItem(image: #imageLiteral(resourceName: "Select"), style: .plain, target: self, action: #selector(invoke2))

            self.navigationItem.rightBarButtonItems = [btn,btn1]
        }else{
            self.navigationItem.rightBarButtonItems = []
        }
        
    }
   
    @objc func invoke (){
     vc4?.sendAgainAction()
        
    }
    
    @objc func invoke2 (){
        if nature == true {
            vc4?.selectAllAction(nature: nature)
            nature = false

        }else{
            vc4?.selectAllAction(nature: nature)
            nature = true
        }
    }

    
    func createVC( tittle:String, guests:[MInvitdGuest])->ManageGuestListVc{
        
        let mainVC = self.storyboard?.instantiateViewController(withIdentifier: "ManageGuestListVc") as! ManageGuestListVc
        mainVC.title = tittle
        mainVC.guests = guests
        
        return mainVC
    }
    
}
