//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import UIKit

class ImageOnlyEventCell: UICollectionViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var descript: UILabel!
    @IBOutlet weak var heartBtn: UIButton!
    @IBOutlet weak var likedLabel: UILabel!
    @IBOutlet weak var commentBtn: UIButton!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var scheduleBtn: UIButton!
    @IBOutlet weak var locationBtn: UIButton!
    
    @IBOutlet weak var statusChangeBtn: UIButton!
    @IBOutlet weak var statusButton: UIButton!
    
    //new entries
    @IBOutlet weak var circleImage: UIImageView!
    @IBOutlet weak var venueCircle: UILabel!
    @IBOutlet weak var imageOnlyEventName: UILabel!
    
    
}
