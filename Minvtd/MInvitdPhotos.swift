//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import UIKit


struct MInvitdPhotos{
    
    // MARK: Properties
    
    let fileUrl : String?
    let title : String?
    let fileThumbnail : String?

    // MARK: Initializers
    
    // construct a dictionary
    init(dictionary: [String:AnyObject]) {
        fileUrl = dictionary[MInvitdClient.JSONResponseKeys.fileUrl] as? String
        title = dictionary[MInvitdClient.JSONResponseKeys.filename] as? String
        fileThumbnail = dictionary["fileThumbnail"] as? String
    }
    
    static func photosFromResults(_ results: [[String:AnyObject]]) -> [MInvitdPhotos] {
        var photos = [MInvitdPhotos]()
        
        // iterate through array of dictionaries, each Article is a dictionary
        for photo in results {
            
            photos.append(MInvitdPhotos(dictionary: photo))
        }
        
        return photos
    }
}

