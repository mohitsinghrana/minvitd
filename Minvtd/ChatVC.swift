
//  Copyright © 2017 Euroinfotek. All rights reserved.


import UIKit
import JSQMessagesViewController
import MobileCoreServices
import AVKit
import Firebase
import Alamofire
import AlamofireImage
import SlideMenuControllerSwift

var chatIdGlobal : String = ""


class ChatVC: JSQMessagesViewController,ChangeNameDelegate{
    
    lazy var refresher:UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .gray
        refreshControl.addTarget(self, action: #selector(requestData), for: .valueChanged )
        
        return refreshControl
    }()
    
    
    var currentUser: String?
        //Auth.auth().currentUser?.uid
    var eventID:Int!
    @objc var messageRef:DatabaseReference!
    @objc var chatID:String!
    var isGroup:Int!
    var selectedImage: UIImage?
    @objc var gpName:String?{
        didSet{
            self.title = self.gpName
        }
    }
   
    let button =  UIButton(type: .custom)
    var notiDirection = false
    
    var chatlist:MInvitdchatList?{
        didSet{
            self.gpName = self.chatlist?.name
            self.eventID = (self.chatlist?.invitationID)!
            let str = self.chatlist?.invitationID!.description
            self.messageRef = Database.database().reference().child("chatrooms").child(str!).child((self.chatlist?.chatID!)!)
            self.chatID = self.chatlist?.chatID!
            self.isGroup = self.chatlist?.group!
        }
    }
    
    fileprivate var sourceType = ""
    
    @objc let date = Date()
    @objc var messages = [JSQMessage]()
    
    @objc let incomingBubble = JSQMessagesBubbleImageFactory().incomingMessagesBubbleImage(with:UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1.0) )
    
    @objc let outgoingBubble =  JSQMessagesBubbleImageFactory().outgoingMessagesBubbleImage(with:UIColor.init(hexString: UserDefaults.standard.string(forKey: "colour1")!) )
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // swipe back feature
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        view.addGestureRecognizer(edgePan)
        
        //refresh setup
        collectionView.refreshControl = refresher
        
        navigationBarSetup(nil)
        collectionView!.collectionViewLayout.incomingAvatarViewSize = CGSize(width: 30, height: 30)
        collectionView!.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero

        
        let userID = UserDefaults.standard.object(forKey: "userID")
        print("\(userID)")
        self.currentUser = String(describing: userID!)
        print("This is currentUser with userID: \n\(self.currentUser!)")
        
        self.navigationController?.navigationBar.setBackgroundImage(imageLayerForGradientBackground(navBar: (self.navigationController?.navigationBar)!), for: UIBarMetrics.default)
        
        let rightNav1 = UIBarButtonItem(image: #imageLiteral(resourceName: "icons8-old_time_camera"),  style: .plain, target: self, action: #selector(rightNavAction))
        
        let rightNav2 = UIBarButtonItem(image: #imageLiteral(resourceName: "icons8-text"), style: .plain, target: self, action: #selector(rightNavAction2))

        self.navigationItem.setRightBarButtonItems([rightNav2,rightNav1], animated: true)
        
        //ChatInfo
        
//      let button =  UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 100, height: 40)
        button.backgroundColor = UIColor.clear

        if let name = self.chatlist?.name{
            print(chatlist)
            button.setTitle("\(name)", for: .normal)
        }
        
        button.addTarget(self, action: #selector(self.chatInfoTapped), for: .touchUpInside)
        self.navigationItem.titleView = button
        self.setup()
        self.reloadMessagesView()
        self.observerMessage()
        self.collectionView.backgroundColor = UIColor.clear
        self.inputToolbar.contentView.leftBarButtonItem = nil
    
    }
    
    
    @objc func chatInfoTapped(){
        
        if self.isGroup == 0{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "GuestProfileVC") as! GuestProfileVC
            
            vc.data.name = self.chatlist?.name
            vc.data.imageUrl = self.chatlist?.fileUrl
            
            self.present(UINavigationController(rootViewController:vc), animated: true, completion: nil)
            
            
        }
        else{
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "chatInfo") as! UINavigationController
        let a = vc.viewControllers[0] as? ChatInfoVC
        a?.chatlist = self.chatlist
        chatIdGlobal = chatID
        a?.delegate = self
        self.present(vc, animated: true, completion: nil)
        
        }
    }
    
    // update the name:
    
    func userEnteredNewName(name: String){
        self.title = name
        //print("i m here \(name)")
        setButtonName(name: name)
    }
    func setButtonName(name:String){
        button.setTitle("\(name)", for: .normal)
    }
    
    @objc func requestData() {
        //print("request refresh")
        self.reloadMessagesView()
      //  self.observerMessage()
        
        let deadline = DispatchTime.now() + .milliseconds(500)
        DispatchQueue.main.asyncAfter(deadline: deadline) {
            self.refresher.endRefreshing()
        }
        // Code to refresh table view
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

            self.navigationItem.rightBarButtonItems = nil
        
    }
    
    @IBAction func backBtn(_ sender: UIBarButtonItem) {
        
        if notiDirection{
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let leftDrawerVC = storyBoard.instantiateViewController(withIdentifier: "SlideMenuViewController")
            let mainVC = storyBoard.instantiateViewController(withIdentifier: "invitationVC") as! InvitationViewController
            let leftDrawer = SlideMenuController(mainViewController: UINavigationController(rootViewController:mainVC), leftMenuViewController: leftDrawerVC)
            //getUserData()
            appdelegate.window =  UIWindow(frame: UIScreen.main.bounds)
            appdelegate.window?.rootViewController = leftDrawer
            appdelegate.window?.makeKeyAndVisible()
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            //print("Screen edge swiped!")
            if notiDirection{
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let leftDrawerVC = storyBoard.instantiateViewController(withIdentifier: "SlideMenuViewController")
                let mainVC = storyBoard.instantiateViewController(withIdentifier: "invitationVC") as! InvitationViewController
                let leftDrawer = SlideMenuController(mainViewController: UINavigationController(rootViewController:mainVC), leftMenuViewController: leftDrawerVC)
                //getUserData()
                appdelegate.window =  UIWindow(frame: UIScreen.main.bounds)
                appdelegate.window?.rootViewController = leftDrawer
                appdelegate.window?.makeKeyAndVisible()
            }else{
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    override func didPressAccessoryButton(_ sender: UIButton!) {
        
        let sheet = UIAlertController(title: "Upload", message: "", preferredStyle: .actionSheet)
        let action1 = UIAlertAction(title: "Photo", style: .default) { (action:UIAlertAction) in
            self.gettingMedia(kUTTypeImage)
        }

        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction) in
        }
        
        sheet.addAction(action1)
        //sheet.addAction(action2)
        sheet.addAction(cancel)
        self.present(sheet, animated: true, completion: nil)
    }
    
    @objc func gettingMedia (_ send:CFString) {
        
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.mediaTypes = [send as String ]
        self.present(picker, animated: true, completion: nil)
    }
    
    override func textViewDidEndEditing(_ textView: UITextView) {
        textView.text = ""
    }
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        
            let myDate = Int64(date.timeIntervalSince1970)
            let userID = UserDefaults.standard.string(forKey: "userID")
            let dict = ["senderId":userID!,"displayName":senderDisplayName! , "date":myDate, "message": text ?? "" , "type" : "message"] as [String : Any]
        
            //print("\(dict)")
        
            messageRef.childByAutoId().setValue(dict)
            
            // try different logic for notification
            
            handleChatNotification(message:text, group: (chatlist?.group)!)
            
            self.view.endEditing(true)
            JSQSystemSoundPlayer.jsq_playMessageSentSound()
            finishSendingMessage()
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        let data = messages[indexPath.item]
        return data
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didDeleteMessageAt indexPath: IndexPath!) {
        
        let message1 = messages[indexPath.item]
        if  message1.isMediaMessage {
            if let videourl = message1.media as? JSQVideoMediaItem{
                
                let url = videourl.fileURL as URL
                let player = AVPlayer(url: url)
                let mediacontroller = AVPlayerViewController()
                mediacontroller.player = player
                present(mediacontroller, animated: true, completion: nil)
            }
            
            else if (message1.media as? JSQPhotoMediaItem) != nil {
                //let image1 = photo.image as! UIImage
                
            }
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let data = messages[indexPath.row]
//        print("this is messages in row: \n\(messages[indexPath.row])")
//        print("this is data.senderId in row: \n\(data.senderId)")
//        print("this is self.senderId\(self.senderId)")
//
//        print("data.senderId! = \(data.senderId!) & self.senderId! = \(self.currentUser!)")

            if data.senderId! == self.currentUser!{
                return self.outgoingBubble
            }else{
                return self.incomingBubble
            }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        return nil
    }
    
    
    
    @objc func reloadMessagesView()
    {
        self.collectionView!.reloadData()
    }
    
    
    @objc func setup() {
        
        self.senderId = currentUser
        let name = UserDefaults.standard.string(forKey: "name")
        //print(name)
        
        self.senderDisplayName = name
    }
    
    @objc func MediaUploading(_ image:UIImage?, video:URL?) {
        
        let path = "\(currentUser!)"
        let ref = Storage.storage().reference().child(path)
        
        if image != nil {
            
            let data1 = UIImageJPEGRepresentation(image!, 0.1)
            let metadata = StorageMetadata()
            metadata.contentType = "image/jpg"
            
            let date = Date().timeIntervalSince1970
            
            ref.child(date.description).putData(data1!, metadata: metadata, completion: { (meta:StorageMetadata?, error:Error?) in
                
                if error == nil {

                }
                else {
                    print(error!.localizedDescription)
                }
            })
        }
        
        
        if video != nil {
            
            let date = Date().timeIntervalSince1970

            let data1 = try? Data(contentsOf: video!)
            let metadata = StorageMetadata()
            metadata.contentType = "video/mp4"
            
            ref.child("YourData"+"\(date)").putData(data1!, metadata: metadata, completion: { (meta:StorageMetadata?,err: Error?) in
                
                if err == nil {

                }
                else {
                    print(err!.localizedDescription)
                }
            }
            )
        }
    }
    
    @objc func ThumbnailCreate (image:UIImage) ->UIImage  {
        
        let size = CGSize(width: 400, height: 400)
        
        // Define rect for thumbnail
        let scale = max(size.width/image.size.width, size.height/image.size.height)
        let width = image.size.width * scale
        let height = image.size.height * scale
        let x = (size.width - width) / CGFloat(2)
        let y = (size.height - height) / CGFloat(2)
        let thumbnailRect = CGRect(x: x, y: y, width:width, height: height)
        
        // Generate thumbnail from image
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        image.draw(in: thumbnailRect)
        let thumbnail = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    //today new
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let message = messages[indexPath.row]
        
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell

            let userRef = Database.database().reference().child("users").child(message.senderId)
            userRef.observeSingleEvent(of: .value, with: { (data:DataSnapshot) in
                if let profiledata = data.value as? [String:Any] {
                    if let imagurl = profiledata["imageUrl"] as? String {
                        
                        cell.avatarImageView.af_setImage(withURL: URL(string:imagurl)!)
                        cell.avatarImageView.layer.cornerRadius = 15.0
                        cell.avatarImageView.clipsToBounds = true
                    }
                }
            })
        
       
        cell.messageBubbleTopLabel.textInsets = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 62)
       

        if message.senderId == self.senderId {
            cell.textView?.textColor = .white
        } else {
            cell.textView?.textColor = .black
        }
        
        
        return cell
    }
    
    

    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath) -> NSAttributedString {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.left


        let message: JSQMessage? = messages[indexPath.item]
        let date = message?.date

        if indexPath.item - 1 > 0 {
            let previousMessage: JSQMessage? = messages[indexPath.item - 1]
            if (previousMessage?.senderId == message?.senderId) {
                return NSAttributedString(string: "")
            }
        }
            if (message?.senderId == senderId) {
                    paragraphStyle.alignment = NSTextAlignment.right

                    let str =   (date?.convert())!
                    return NSAttributedString(string: str, attributes: [NSAttributedStringKey.paragraphStyle : paragraphStyle])
                    
                }else{
                    
                    paragraphStyle.alignment = NSTextAlignment.left

                    let str = (message?.senderDisplayName)! + "  " + (date?.convert())!
                    return NSAttributedString(string: str, attributes: [NSAttributedStringKey.paragraphStyle : paragraphStyle])
        }
    }
    // height of labels
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellBottomLabelAt indexPath: IndexPath!) -> CGFloat{

        return 8.0
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat {
        let currentMessage = self.messages[indexPath.item]
        
        if (indexPath.item - 1 > 0) {
            let previousMessage = self.messages[indexPath.item - 1]
            if (previousMessage.senderId == currentMessage.senderId) {
                
                return 0.0
            }
        }
        
        return kJSQMessagesCollectionViewCellLabelHeightDefault
        
    }
    
    
}


extension ChatVC : UIImagePickerControllerDelegate ,UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        
        if sourceType == "group"{
            
            if let img = info[UIImagePickerControllerOriginalImage] as? UIImage {
                
                let destImg = ThumbnailCreate(image: img)
                
                // upload image
                
                self.dismiss(animated: true, completion: {
                    
                    self.sourceType = "no"
                    self.uploadImage(image: destImg, Complition: {
                        (fileID, fileUrl) in
                        self.updateGroupInfo(name: self.gpName, fileUrl: fileUrl, fileID: fileID, group: self.chatlist!.group!)

                    })
                })
            }
        }else {
        if let img = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            
            let dest = ThumbnailCreate(image: img)
           
            self.MediaUploading(dest, video: nil)
            
        }else if let videourl = info[UIImagePickerControllerMediaURL] as? URL
        {

            self.MediaUploading(nil, video: videourl)
            
        }
        
        self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapMessageBubbleAt indexPath: IndexPath!) {
        if let test = self.getImage(indexPath: indexPath) {
            selectedImage = test
            //print("this image",selectedImage)
        }
    }
    
    func getImage(indexPath: IndexPath) -> UIImage? {
        let message = self.messages[indexPath.row]
        if message.isMediaMessage == true {
            let mediaItem = message.media
            if mediaItem is JSQPhotoMediaItem {
                let photoItem = mediaItem as! JSQPhotoMediaItem
                if let test: UIImage = photoItem.image {
                    let image = test
                    return image
                }
            }
        }
        return nil
    }
    
    @objc func observerMessage() {
        let query = messageRef.queryLimited(toLast: 100)
        
        query.observe(.childAdded) { (snap:DataSnapshot) in
            
            if let data = snap.value as? [String:Any]{

                let data = snap.value! as! NSDictionary
                let senderId = data["senderId"] as! String
                let displayName = data["displayName"] as! String
                let datestr = data["date"] as! Double?
                let date = datestr?.dateconvert()
                let type = data["type"] as! String?
                let pthurl = data["pathurl"] as? String
                let mess = data["message"] as? String

                if type == "Photo" {

                    let url = URL(string: pthurl!)
                    if let imgdata = try? Data(contentsOf: url!){

                        let image = UIImage(data: imgdata)
                        let jsqmedia = JSQPhotoMediaItem(image: image)

                        if senderId == self.currentUser {
                            jsqmedia?.appliesMediaViewMaskAsOutgoing = true
                        }else
                        {
                            jsqmedia?.appliesMediaViewMaskAsOutgoing = false
                        }
                        let message = JSQMessage(senderId: senderId, senderDisplayName: displayName, date: date, media: jsqmedia)
                        self.messages.append(message!)
                        self.collectionView.reloadData()
                    }
                }

                if type == "Video" {

                    let url = URL(string: pthurl!)
                    let jsqvideo = JSQVideoMediaItem(fileURL: url!, isReadyToPlay: true)
                    let message = JSQMessage(senderId: senderId, senderDisplayName: displayName, date: date, media: jsqvideo)
                    self.messages.append(message!)
                    self.collectionView.reloadData()

                    //extra

                }
                if type == "message" {

                    let message = JSQMessage(senderId: senderId, senderDisplayName: displayName, date: date, text: mess)
                    if let createdDate = (self.chatlist?.created) as? Int {

                        if createdDate < Int(datestr!) {
                            self.messages.append(message!)
                            self.collectionView.reloadData()

                    }

                    }else {

                        self.messages.append(message!)
                        self.collectionView.reloadData()

                    }
                }
            }
        }
    }
    
    @objc func handleChatNotification(message:String, group:Int){
        let userID = UserDefaults.standard.string(forKey: "userID")
        guard let myName = UserDefaults.standard.string(forKey: "name") else {return}
        let eventID = UserDefaults.standard.string(forKey: "invitationID")
//        let myfirebaseID = Auth.auth().currentUser?.uID
        let iName = UserDefaults.standard.string(forKey: "invitationTitle")
        
        let parameters:Parameters = [
            "userID":userID!,
            "invitationID":eventID!,
//            "firebaseID":myfirebaseID!,
            "chatID":chatID!,
            "message":message,
            "group":group,
            "iName": iName!,
            "userName": myName,
        ]
        
        apiCallingMethod(apiEndPints: "chat", method: .post, parameters: parameters) { (data, total) in
        }
 }
    
    @objc func rightNavAction(){
        
        let controller = UIImagePickerController()
            controller.delegate = self
            sourceType = "group"
        self.present(controller, animated: true, completion: nil)
    }
    
    @objc func rightNavAction2(){
        
        let alert = UIAlertController(title: "Group Name", message: "", preferredStyle: .alert)
        
            alert.addTextField { (fild1) in
                fild1.placeholder = "Enter name of the group"
            }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
                
                // update Name
                let text1 = alert.textFields?[0].text!
                self.updateGroupInfo(name: text1, fileUrl: nil, fileID: nil, group: self.chatlist!.group!)
            }))
        
        present(alert, animated: true, completion: nil)
    }

    @objc func updateGroupInfo(name:String?,fileUrl:String?,fileID:String?, group:Int){
        
        let urlStr = MInvitdClient.Constants.BaseUrl + "chat/member"
        
        guard let myName = UserDefaults.standard.string(forKey: "name") else {return}
        guard let iName = UserDefaults.standard.string(forKey: "invitationTitle") else {return}
        
        var parameters:Parameters = ["userID":chatlist!.userID!,
                      "invitationID":chatlist!.invitationID!,
                      "chatID":chatlist!.chatID!,
                      "group":group,
                      "userName":myName,
                      "iName":iName
                        ]
        
        
        if let name1 = name {
            parameters["name"] = name1
        }
        
        if let fileUrl1 = fileUrl {
            parameters["fileUrl"] = fileUrl1
        }
        
        if let fileID1 = fileID {
            parameters["fileID"] = fileID1
        }
        let authHeader = ["Authorization" : UserDefaults.standard.string(forKey: "userToken")!, "Content-Type" : "application/json" ]
        
        Alamofire.request(urlStr, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: authHeader).responseJSON { (response) in
           
            if response.error == nil {
                
                //print("\(response)")
                self.gpName = name
            }
        }
    }
}




