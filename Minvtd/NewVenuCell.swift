//
//  VendoreCell.swift
//  Minvtd
//
//  Created by admin on 13/11/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit
class VenueTableViewCell: UITableViewCell {
    
    
    @objc let detailLabel = UILabel()
    //    @objc let locationLabel = UILabel()
    //    @objc let locationImageView = UIImageView()
    
    @objc let containerView1 = { () -> UIView in
        let vi = UIView()
        
        
        vi.backgroundColor = UIColor.white.withAlphaComponent(1.0)
        //vi.layer.cornerRadius = 8.0
        vi.clipsToBounds = true
        
        vi.translatesAutoresizingMaskIntoConstraints = false
        
        return vi
        
    }()
    
    
    // MARK: Initalizers
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        //        let marginGuide = contentView.layoutMarginsGuide
        
        detailLabel.translatesAutoresizingMaskIntoConstraints = false
        detailLabel.numberOfLines = 0
        detailLabel.font = UIFont.systemFont(ofSize: 12)
        detailLabel.textColor = UIColor.darkGray
        
        //
        //        locationLabel.translatesAutoresizingMaskIntoConstraints = false
        //        locationLabel.numberOfLines = 0
        //        locationLabel.textAlignment = .left
        //
        //        locationLabel.font = UIFont.systemFont(ofSize: 15)
        //        locationLabel.textColor = UIColor.lightGray
        //        locationLabel.text = ""
        //        locationImageView.image = #imageLiteral(resourceName: "location").withRenderingMode(.alwaysTemplate)
        //        locationImageView.contentMode = .scaleAspectFit
        //        let colour1 = UserDefaults.standard.string(forKey: "colour1")
        //        locationImageView.tintColor = UIColor.init(hexString: colour1!)
        //        let blurEffect = UIBlurEffect(style: .dark)
        //        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        //        contentView.addSubview(blurredEffectView)
        
        contentView.addSubview(containerView1)
        
        contentView.addSubview(detailLabel)
        //contentView.addSubview(locationLabel)
        //contentView.addSubview(locationImageView)
        
        
        
        //        locationImageView.anchor(contentView.topAnchor, left: contentView.leftAnchor, bottom:nil , right: nil, topConstant: 4, leftConstant: 20, bottomConstant: 4, rightConstant: 40, widthConstant: 15, heightConstant: 15)
        
        detailLabel.anchor(contentView.topAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, topConstant: 4, leftConstant: 25, bottomConstant: 4, rightConstant: 20, widthConstant: 0, heightConstant: 0)
        //
        
        //        locationLabel.anchor(nil, left: locationImageView.leftAnchor, bottom: nil, right: contentView.rightAnchor, topConstant: 4, leftConstant: 25, bottomConstant: 4, rightConstant: 40, widthConstant: 0, heightConstant: 0)
        //        locationLabel.centerYAnchor.constraint(equalTo: locationImageView.centerYAnchor).isActive = true
        //
        
        
        
        containerView1.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0.0, leftConstant: 8.0, bottomConstant: 0.0, rightConstant: 8.0, widthConstant: 0, heightConstant: 0)
        //
        //        blurredEffectView.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: -8.0, leftConstant: 12.0, bottomConstant: 0.0, rightConstant: 12.0, widthConstant: 0, heightConstant: 0)
        
        
        //        let subLayer = CALayer()
        //        subLayer.backgroundColor  = UIColor.white.withAlphaComponent(0.9).cgColor
        //        subLayer.frame = CGRect(x: 0, y: 12.0, width: bounds.width + 40.0, height:bounds.height )
        //
        //        containerView1.layer.addSublayer(subLayer)
        //
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
