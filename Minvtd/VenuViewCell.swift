//
//  VenuViewCell.swift
//  Experiment
//
//  Created by admin on 15/11/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit

class VenuViewCell: UITableViewCell ,UITextViewDelegate{
    
    var vc:VenueVC?
    
    var textView = { () -> UITextView in
        let ti = UITextView()
        ti.translatesAutoresizingMaskIntoConstraints = false
        ti.isSelectable = true
        ti.isEditable = false
        ti.isScrollEnabled = false
        ti.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.light)
        ti.dataDetectorTypes = [.phoneNumber,.link,.address]
        ti.text = "text"
        
        return ti
    }()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(){
        
        contentView.addSubview(textView)
        textView.delegate = self
        
        textView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8).isActive = true
        textView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8).isActive = true
        textView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 12).isActive = true
        textView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -20).isActive = true
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        
        if (URL.scheme == "custom"){
            let cordinate = URL.absoluteString.dropFirst(9)
            
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            alert.addAction(UIAlertAction(title: "Open in Maps", style: .default, handler: { (act) in
                let url =  NSURL(string:"http://maps.apple.com/maps?saddr=Current%20Location&daddr=" + cordinate) as URL?
                UIApplication.shared.open( url!, options: [:], completionHandler: nil)
            }))
            
            alert.addAction(UIAlertAction(title: "Open in Google Maps", style: .default, handler: { (act) in
                let url = NSURL(string:"comgooglemaps://?saddr=&daddr=" + cordinate  + "&directionsmode=driving") as URL?
                UIApplication.shared.open( url!, options: [:], completionHandler: nil)
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

            vc?.present(alert, animated: true, completion: nil)
        }
        return true
    }
}
