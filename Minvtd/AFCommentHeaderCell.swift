//
//  AFCommentHeaderCell.swift
//  Minvtd
//
//  Created by Tushar Aggarwal on 06/06/17.
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import Foundation
import UIKit

class AFCommentHeaderCell: UICollectionReusableView , UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    var users:[MInvitdGuest] = []
    var tagesVC:GuestListProfileVC?
    
    @IBOutlet weak var openBtn: UIButton!
    @IBOutlet weak var userImage: UIImageView!
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var created: UILabel!
    @IBOutlet weak var comment: UILabel!

    @IBOutlet weak var heartBtn: UIButton!
    @IBOutlet weak var commentBtn: UIButton!
   
    @IBOutlet weak var subCommentCount: UILabel!
    @IBOutlet weak var likedLabel: UILabel!

    @IBOutlet weak var postSubCommentTF: UITextField!
    @IBOutlet weak var sendComment: UIButton!
    
    @IBOutlet weak var clockIcon: UIImageView!
    
    @IBOutlet weak var tagCollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        postSubCommentTF.returnKeyType = UIReturnKeyType.send
        
        clockIcon.image = #imageLiteral(resourceName: "clock").withRenderingMode(.alwaysTemplate)
        clockIcon.tintColor = UIColor.lightGray
        
        tagCollectionView.delegate = self
        tagCollectionView.dataSource = self
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if users.count < 6{
            return users.count
        }else{
            return 5
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tagCommentsCollectionCell", for: indexPath) as! CommentTagCollectionViewCell
        let guest = users[indexPath.row]
        cell.userImage.isHidden = false
        cell.moreLabel.isHidden = true
        if let imgUrl = guest.imageUrl {
            cell.userImage.af_setImage(withURL: URL(string:imgUrl)!)
        }
        if indexPath.row == 4{
            cell.userImage.isHidden = true
            cell.moreLabel.isHidden = false
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 30, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return -13
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if !users.isEmpty {
            tagesVC =  GuestListProfileVC()
            tagesVC?.guests = users
            
            let appdelegate = UIApplication.shared.delegate
            if let myWindow = appdelegate?.window{
                myWindow?.rootViewController?.present(tagesVC!, animated: true, completion: nil)
            }
        }
    }
    
}
