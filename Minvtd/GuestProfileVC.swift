//
//  GuestProfileVC.swift
//  Minvtd
//
//  Created by admin on 11/11/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit
import AlamofireImage
class GuestProfileVC: UIViewController {
    
    
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var phoneLabel: UILabel!
    
    @IBOutlet weak var emailLabel: UILabel!
    
    var data : MInvitdGuest = MInvitdGuest(name: nil, phone: nil, email: nil)
 
    private var guest:MInvitdGuest?{
        
        didSet{
            
            title = guest?.name
        
            if let url =  guest?.imageUrl{
                profileImageView.af_setImage(withURL:URL(string:url)!)
            }
            
//             nameLabel.text = guest?.name
            
//            if let phone = guest?.phone{
//                phoneLabel.text = phone
//            }else {
//                phoneLabel.text = ""
//            }
//            if let email = guest?.email{
//                emailLabel.text = email
//            }else{
//                emailLabel.text = ""
//
//            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationBarSetup(nil)
        let button2 = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissVC))
        self.navigationItem.rightBarButtonItem = button2
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if guest == nil {
            guest = data
            print("this is the data ^^^^^^^^\(data)" )

        }

    }
  @objc  func dismissVC(_ sender: UIBarButtonItem) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
}



