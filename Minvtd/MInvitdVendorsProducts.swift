//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import UIKit

/*
 "categoryId": 8,
 "title": "Cake 2",
 "description": "Cake2",
 "link": "http://euroinfotek.com",
 "rating": 5,
 "featured": 5,
 "imageUrl": "",
 "weight": 1000,
 "created": 1509963628,
 "updated": 0,
 "vendorID": 4
 */

struct MInvitdVendorsProducts{
    
    // MARK: Properties
    let productID : Int?
    let userID : Int?
    let vendorID : Int?
    let categoryID: Int?
    let title : String?
    let description : String?
    let imageUrl : String?
    let weight : Int?
    let created  : String?
    let updated : String?
    let category : String?
    // MARK: Initializers
    
    // construct dictionary
    init(dictionary: [String:AnyObject]) {
        productID = dictionary[MInvitdClient.JSONResponseKeys.productID] as? Int
        userID = dictionary[MInvitdClient.JSONResponseKeys.userID] as? Int
        vendorID = dictionary[MInvitdClient.JSONResponseKeys.vendorID] as? Int
        weight = dictionary[MInvitdClient.JSONResponseKeys.weight] as? Int
        title = dictionary[MInvitdClient.JSONResponseKeys.title] as? String
        description = dictionary[MInvitdClient.JSONResponseKeys.description] as? String
        imageUrl = dictionary[MInvitdClient.JSONResponseKeys.imageUrl] as? String
        created = dictionary[MInvitdClient.JSONResponseKeys.created] as? String
        updated = dictionary[MInvitdClient.JSONResponseKeys.updated] as? String
        categoryID = dictionary[MInvitdClient.JSONResponseKeys.categoryID] as? Int
        category = dictionary[MInvitdClient.JSONResponseKeys.category] as? String
    }
    
    static func productsFromResults(_ results: [[String:AnyObject]]) -> [MInvitdVendorsProducts] {
        var products = [MInvitdVendorsProducts]()
     
        for product in results {
            products.append(MInvitdVendorsProducts(dictionary: product))
        }
        
        return products
    }
}
