//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import Alamofire
import AlamofireImage
class VendorsListingViewController: UIViewController {
    
   
    var listings: [MInvitdVendorsListing] = [MInvitdVendorsListing]()
    var categoryID : Int?
    @objc let placeholderImage = UIImage(named: "eventCoverImage")!
    var titleName : String = " "
    @IBOutlet weak var listingsCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        listingsCollectionView.delegate = self
        listingsCollectionView.dataSource = self
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = titleName

        vendorListing()
        
    }
    
    @objc func vendorListing() {
        
        //print("this is categoryID->\(categoryID!)")
        
        apiCallingMethod(apiEndPints: "vendor/listings", method: .get, parameters: ["categoryID":"\(categoryID!)", "device":"iOS"]) { (data1:Any?, totalCount:Int?) in
            
            //print("thsi is the daya -> /")
            
            if let data = data1 as? [[String : Any]]
            {
                let listings = MInvitdVendorsListing.listingsFromResults(data as [[String : AnyObject]])
                self.listings = listings
                DispatchQueue.main.async {
                    self.listingsCollectionView.reloadData()
                }
            }
        }
    }    
}

// MARK: - InvitationViewController: UICollectionViewDelegate, UICollectionViewDataSource

extension VendorsListingViewController : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let bounds = UIScreen.main.bounds
        let width = bounds.width
        let height = bounds.height/8
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let listing = listings[(indexPath as NSIndexPath).row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "listingCell", for: indexPath) as! ListingCollectionViewCell
        
        cell.titile.text = listing.title
        cell.name.text = listing.name
        cell.address.text = listing.address
        cell.number.text = "\(listing.contact!)"
        
        cell.image.layer.cornerRadius = cell.image.frame.width / 2
        cell.image.clipsToBounds = true
        cell.image.contentMode = .scaleAspectFill
        cell.image.af_setImage(
            withURL: URL(string: listing.imageUrl!)! ,
            placeholderImage: placeholderImage)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listings.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let listing = listings[(indexPath as NSIndexPath).row]

        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "productsVC") as! VendorsProductsViewController
        mainVC.titleName = listing.title!
        mainVC.vendorID = listing.vendorID!
        mainVC.listing = listing
        self.navigationController?.pushViewController(mainVC, animated: true)
    }
}
