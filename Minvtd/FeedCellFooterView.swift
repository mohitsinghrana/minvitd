//
//  FeedCellFooterView.swift
//  MinvitdFeeds
//
//  Created by admin on 03/11/17.
//  Copyright © 2017 Sachin. All rights reserved.
//

import UIKit

class FeedCellFooterView: UIView {
    
    var users:[MInvitdGuest] = []
    let reuseId = "textggcv"
    
    var feed:MInvtdAllFeed? {
        didSet{
            likeLabel.text = " " + (feed?.likes?.description)!
            likeButton.setImage( (feed?.liked!)! ?  #imageLiteral(resourceName: "likeFilled") .withRenderingMode(.alwaysOriginal):  #imageLiteral(resourceName: "like").withRenderingMode(.alwaysOriginal) , for: .normal)
            commentLabel.text = " " + (feed?.subCommentsCount?.description)!
            
            if feed?.subCommentsCount! == 0 {
                comment.isHidden = true
            }else {
                comment.subComment = feed?.subCommentArray?.first
                comment.isHidden = false
            }
            users = (feed?.users)!
            
            if users.count > 5 {
                extraLikes.text = "+" + (users.count - 5).description
                extraLikes.isHidden = false
            }else{
                extraLikes.isHidden = true
            }
            collectionImg.reloadData()
        }
    }

    let textField = { () -> UITextField in
        let ti = UITextField()
        ti.textColor = UIColor.gray
        ti.translatesAutoresizingMaskIntoConstraints = false
        ti.layer.borderWidth = 1.5
        ti.layer.cornerRadius = 8
        ti.font = UIFont(name: "GothamRounded-Book", size: 12)
        ti.layer.borderColor = UIColor.black.cgColor
        ti.placeholder = "  Write a comment ..."
        ti.returnKeyType = UIReturnKeyType.send
        return ti
    }()

    //Feed Like:
    
    let likeBtnView = { () -> UIView in
        let lab = UIView()
        
        return lab
    }()
    
    let likeButton = { () -> UIButton in
        let btn = UIButton(type: .system)
        let colour1 = UserDefaults.standard.string(forKey: "colour1")
        //btn.tintColor = UIColor.init(hexString: colour1!)
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        return btn
    }()
    
    let likeLblview: UIView = {
        let temp = UIView()
        return temp
    }()
    
    let likeLabel = { () -> UILabel in
        let lab = UILabel()
        lab.text = "234"
        lab.textColor = UIColor.init(hexString: "#9d9d9d")
        lab.font = UIFont(name: "GothamRounded-Book", size: 12)
        return lab
    }()
    
    private lazy var likeStack = { () -> UIStackView in
        let base = UIStackView(arrangedSubviews: [likeBtnView,likeLblview])
        base.axis = .horizontal
        base.distribution = .fillEqually
        base.spacing = -20
        return base
    }()

    
    let commentButton = { () -> UIButton in
        let btn = UIButton(type: .system)
        btn.setImage(#imageLiteral(resourceName: "msg").withRenderingMode(.alwaysOriginal), for: .normal)
        //btn.tintColor = UIColor.init(hexString: "#9d9d9d")
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        return btn
    }()
    
    let commentLabel = { () -> UILabel in
        let lab = UILabel()
        lab.text = "234"
        lab.textColor = UIColor.init(hexString: "#9d9d9d")
        lab.font = UIFont(name: "GothamRounded-Book", size: 12)
        return lab
    }()
    
    let commentLblview: UIView = {
        let temp = UIView()
        return temp
    }()
    
    let commentBtnView = { () -> UIView in
        let btnView = UIView()
        return btnView
    }()
    
    private lazy var commentStack = { () -> UIStackView in
        let base = UIStackView(arrangedSubviews: [commentBtnView,commentLblview])
        base.axis = .horizontal
        base.distribution = .fillEqually
        base.spacing = -20
        return base
    }()
    
    //Tags:
    
    let collectionImg = { () -> UICollectionView in
        let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
            layout.minimumLineSpacing = -12
            layout.minimumInteritemSpacing = 0.0
        let col = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        col.backgroundColor = UIColor.clear
        col.translatesAutoresizingMaskIntoConstraints = false
        
        return col
    }()
    
    let extraLikes = { () -> UILabel in
        let lab = UILabel()
        lab.translatesAutoresizingMaskIntoConstraints = false
        lab.text = "234 more"
        lab.font = UIFont(name: "GothamRounded-Medium", size: 12)
        lab.textColor = UIColor.init(hexString: "#9d9d9d")
        return lab
    }()
    
    private lazy var baseStack = { () -> UIStackView in
//      let base = UIStackView(arrangedSubviews: [commentButton,likeButton,collectionImg,extraLikes])
        let base = UIStackView(arrangedSubviews: [leftStack,rightStack])
        base.axis = .horizontal
        base.distribution = .fillEqually
        return base
    }()
    
    private lazy var leftStack = { () -> UIStackView in
//      let base = UIStackView(arrangedSubviews: [commentButton,likeButton,collectionImg,extraLikes])
        let base = UIStackView(arrangedSubviews: [likeStack,commentStack,collectionImg,extraLikes])
        base.axis = .horizontal
        base.distribution = .fillEqually
        return base
    }()
    
    private lazy var rightStack = { () -> UIStackView in
//        let base = UIStackView(arrangedSubviews: [commentButton,likeButton,collectionImg,extraLikes])
        let base = UIStackView(arrangedSubviews: [collectionImg,extraLikes])
        base.axis = .horizontal
        base.distribution = .fill
        return base
    }()
    
    let deviser = {() -> UIView in
        let lab = UIView()
        lab.backgroundColor = UIColor(red: 157/255, green: 157/255, blue: 157/255, alpha: 0.4)
        return lab
    }()
    
   lazy var comment = { () -> FeedFooterComment in
        let vi = FeedFooterComment()
        vi.clipsToBounds = true
        vi.backgroundColor = UIColor(red: 243 / 255, green: 242 / 255, blue: 242 / 255, alpha: 1.0)
        vi.feed = feed
        vi.translatesAutoresizingMaskIntoConstraints = false
        return vi
    }()
    
    let tagVcOpen = { () -> UIButton in
        let vi = UIButton()
        vi.translatesAutoresizingMaskIntoConstraints = false
        return vi
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
        collectionImg.delegate = self
        collectionImg.dataSource = self
        collectionImg.register(VenueimageCell.self, forCellWithReuseIdentifier: reuseId)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupUI(){
        
        addSubview(textField)
        addSubview(baseStack)
        addSubview(comment)
        addSubview(deviser)
       // addSubview(deviser2)
        addSubview(tagVcOpen)
        likeBtnView.addSubview(likeButton)
        likeLblview.addSubview(likeLabel)
        commentLblview.addSubview(commentLabel)
        commentBtnView.addSubview(commentButton)
        
        // like frame here
        likeButton.widthAnchor.constraint(equalToConstant: 18).isActive = true
        likeButton.heightAnchor.constraint(equalToConstant: 18).isActive = true
        likeButton.anchorCenterSuperview()
        likeLabel.commentNumberHeight()

        // comment frame here
        commentButton.widthAnchor.constraint(equalToConstant: 18).isActive = true
        commentButton.heightAnchor.constraint(equalToConstant: 18).isActive = true
        commentButton.anchorCenterSuperview()
        commentLabel.commentNumberHeight()
        
        baseStack.anchor(topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 8, leftConstant: 29, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 35)
        
        textField.anchor(baseStack.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 8, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 35)
        
        comment.anchor(textField.bottomAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
       
        
        tagVcOpen.anchor(rightStack.topAnchor, left: rightStack.leftAnchor, bottom: rightStack.bottomAnchor, right: rightStack.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
}

extension FeedCellFooterView :UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return users.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let guest = users[indexPath.row]

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:reuseId, for: indexPath) as? VenueimageCell
            cell?.imageView.layer.cornerRadius = 14
            cell?.imageView.clipsToBounds = true
            cell?.imageView.backgroundColor = UIColor.black
        if let   imgUrl = guest.imageUrl {
            cell?.imageView.af_setImage(withURL: URL(string:imgUrl)!)
        }

        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 28, height: 28)
    }
}
