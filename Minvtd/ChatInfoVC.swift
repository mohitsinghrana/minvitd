
//  ChatInfoVC.swift
//  Minvtd
//
//  Created by Vivek Aggarwal on 29/08/18.
//  Copyright © 2018 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import MobileCoreServices
import AVKit
import Firebase
import Alamofire
import AlamofireImage
import SlideMenuControllerSwift
import SwiftyJSON
import SVProgressHUD

class MembersOfChat {
    
    var imageUrl:String?
    var name:String?
    var phone:String?
    
    init(dictionary: [String:String]) {
        name = dictionary["name"]
        imageUrl = dictionary["image"]
        phone = dictionary["phone"]
    }
}
protocol ChangeNameDelegate {
    func userEnteredNewName(name: String)
}

class ChatInfoVC: UIViewController ,UITableViewDataSource, UITableViewDelegate , AddedMemberDelegate {
   
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var totalUsers: UILabel!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var middleView: UIView!
    @IBOutlet weak var AddMemberButton: UIButton!
    
    var membersList = [MembersOfChat]()
  
    var guests: [MInvitdGuest] = [MInvitdGuest]()
     var filterGuests: [MInvitdGuest] = [MInvitdGuest]()
    
    var fileID : String?
    var fileUrl : String?
    var arrayOfUsersID : [Int] = []
    var newName : String = ""
    var ImageURL : String?
    var groupName : String?
    
    var delegate : ChangeNameDelegate?

    
    var eventID:Int!
    @objc var messageRef:DatabaseReference!
    @objc var chatID:String!
    var isGroup:Int!
    var selectedImage: UIImage?
    var currentUser: String?
    
    fileprivate var sourceType = ""
    
    @objc var messages = [JSQMessage]()
    
    @objc var gpName:String?{
        didSet{
            self.title = self.gpName
        }
    }
    
    var chatlist:MInvitdchatList?{
        didSet{
            self.gpName = self.chatlist?.name
            self.eventID = (self.chatlist?.invitationID)!
            let str = self.chatlist?.invitationID!.description
            self.messageRef = Database.database().reference().child("chatrooms").child(str!).child((self.chatlist?.chatID!)!)
            self.chatID = self.chatlist?.chatID!
            self.isGroup = self.chatlist?.group!
        }
    }
    
    override func viewDidLoad() {
        
         super.viewDidLoad()
        
        navigationBarSetup(nil)
        
        let userID = UserDefaults.standard.object(forKey: "userID")
        self.currentUser = String(describing: userID!)
        
        self.navigationController?.navigationBar.setBackgroundImage(imageLayerForGradientBackground(navBar: (self.navigationController?.navigationBar)!), for: UIBarMetrics.default)
        
        let leftNav1 = UIBarButtonItem(image: #imageLiteral(resourceName: "back").withRenderingMode(.alwaysTemplate),  style: .plain, target: self, action: #selector(backBtn))
        leftNav1.tintColor = UIColor.white
        
        self.navigationItem.setLeftBarButton(leftNav1, animated: true)
        
        let rightNav1 = UIBarButtonItem(image: #imageLiteral(resourceName: "icons8-old_time_camera"),  style: .plain, target: self, action: #selector(rightNavAction))
        
        let rightNav2 = UIBarButtonItem(image: #imageLiteral(resourceName: "icons8-text"), style: .plain, target: self, action: #selector(rightNavAction2))
        
        self.navigationItem.setRightBarButtonItems([rightNav2,rightNav1], animated: true)
        
        print("this is the chatID\(chatIdGlobal)")
        
        lookforApi()
        
        table.delegate = self
        table.dataSource = self
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guestlist()
        
        if chatlist?.group == 0 {
            self.navigationItem.rightBarButtonItems = nil
            self.AddMemberButton.isHidden = true
            self.AddMemberButton.isEnabled = false
        }
    }
    @IBAction func addMemberTapped(_ sender: Any) {
       
        let vc = storyboard?.instantiateViewController(withIdentifier: "GroupChatCreate") as? GroupChatCreate
        vc?.guests = self.filterGuests
        vc?.chatID = self.chatlist?.chatID
        vc?.chatName = self.chatlist?.name
        vc?.fileID = self.fileID
        vc?.fileUrl = self.fileUrl
        vc?.delegate = self
        self.present(UINavigationController(rootViewController:vc!), animated: true, completion: nil)
        self.filterGuests = []
        
        
    }
    @IBAction func imageTaped(_ sender: Any) {

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "GuestProfileVC") as! GuestProfileVC
        
        vc.data.name = "Group : " + groupName!
        vc.data.imageUrl = ImageURL
        
        self.present(UINavigationController(rootViewController:vc), animated: true, completion: nil)
        
        
    }
    
    
    
    func userAddedMembers() {
        print("added members ")
        self.membersList = []
        lookforApi()
        
    }
    
    @objc func guestlist() {
        
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")
        let eventID = UserDefaults.standard.string(forKey: "eventID")
        let userID = UserDefaults.standard.integer(forKey: "userID")
        
        apiCallingMethod(apiEndPints: "guests", method: .get, parameters: ["invitationID":invitationID!,"guestID":userID,"chat":1]) { (data1:Any?, count:Int?) in
           
            if let data = data1 as? [[String : Any]]
            {
                var guests = MInvitdGuest.guestsFromResults(data as [[String : AnyObject]])
                
                let userID = UserDefaults.standard.integer(forKey: "userID")
                var i = 0
                for guest in guests {
                    
                    if self.arrayOfUsersID.contains(guest.userID){
                        print("match")
                    }
                    else {
                        self.filterGuests.append(guest)
                    }
//                   else if guest.userID == userID{
//                        guests.remove(at: i)
//                    }
                    i += 1
                }
                print("this is data ->\(data)")
                self.guests = guests
                
                print("Value of guest - time Api get call^^^^^^^^-\(guests)")
                
            }
        }
    }
    
    func lookforApi() {
        
        let url = MInvitdClient.Constants.BaseUrl + "chat/members"
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")
        
        let parameters:Parameters = [
            "userID":userID!,
            "invitationID":invitationID!,
            "chatID":chatIdGlobal,
            ]
        
        let authHeader = ["Authorization" : UserDefaults.standard.string(forKey: "userToken")!, "Content-Type" : "application/json" ]
        
        Alamofire.request(url, method: .get, parameters: parameters, encoding: JSONEncoding.default, headers: authHeader).responseJSON { (response) in
           
            if response.result.isSuccess {
                
                print("Success! Got the data")
                
                let memberJSON : JSON = JSON(response.result.value!)
                self.updateInfo(json: memberJSON)
            
            }
              
            else {
                print("Error \(String(describing: response.result.error))")
                
            }
        }
    }
    
    func updateInfo(json:JSON){
        
        let ImageURL = URL(string: json["data"][0]["fileUrl"].stringValue)
        self.ImageURL = json["data"][0]["fileUrl"].stringValue
        if json["data"][0]["fileUrl"].stringValue.isEmpty{
            // do nothingotp
        }
        else{
            self.headerImage.af_setImage(withURL: ImageURL!)
        }
        
        self.title = json["data"][0]["name"].stringValue
        self.groupName = json["data"][0]["name"].stringValue
        self.fileID = json["data"][0]["fileID"].stringValue
        self.fileUrl = json["data"][0]["fileUrl"].stringValue
        
        
        let memberArray = json["data"][0]["members"].arrayValue
        
        //print(memberArray)
        
        for aMember in memberArray{
            
            let image : String = aMember["imageUrl"].stringValue
            let name : String = aMember["name"].stringValue
            let phone : String = aMember["phone"].stringValue
            
            arrayOfUsersID.append(aMember["userID"].intValue)
            
            let mymember : MembersOfChat = MembersOfChat(dictionary: ["image":image,"name":name,"phone":phone])
            membersList.append(mymember)
        
         
        }
        table.reloadData()
        
    }
   
    
    
    @objc func MediaUploading(_ image:UIImage?, video:URL?) {
        
        let path = "\(currentUser!)"
        let ref = Storage.storage().reference().child(path)
        
        if image != nil {
            
            let data1 = UIImageJPEGRepresentation(image!, 0.1)
            let metadata = StorageMetadata()
            metadata.contentType = "image/jpg"
            
            let date = Date().timeIntervalSince1970
            
            ref.child(date.description).putData(data1!, metadata: metadata, completion: { (meta:StorageMetadata?, error:Error?) in
                
                if error == nil {
                    
                }
                else {
                    print(error!.localizedDescription)
                }
            })
        }
        
        
        if video != nil {
            
            let date = Date().timeIntervalSince1970
            
            let data1 = try? Data(contentsOf: video!)
            let metadata = StorageMetadata()
            metadata.contentType = "video/mp4"
            
            ref.child("YourData"+"\(date)").putData(data1!, metadata: metadata, completion: { (meta:StorageMetadata?,err: Error?) in
                
                if err == nil {
                    
                }
                else {
                    print(err!.localizedDescription)
                }
            }
            )
        }
    }
    
    @objc func backBtn(){
        if newName == ""{
            delegate?.userEnteredNewName(name: self.title!)
            self.dismiss(animated: true, completion: nil)
        }
        else {

            delegate?.userEnteredNewName(name: newName)
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
   //table Confiuration-
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.totalUsers.text = "Members (\(membersList.count))"
        return membersList.count
    }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        let mymember = membersList[(indexPath as NSIndexPath).row]
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "GuestProfileVC") as! GuestProfileVC
        
        vc.data.name = mymember.name
        vc.data.phone = mymember.phone
        vc.data.imageUrl = mymember.imageUrl
        
        self.present(UINavigationController(rootViewController:vc), animated: true, completion: nil)
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = table.dequeueReusableCell(withIdentifier: "guestListViewCell", for: indexPath) as! ChatGuestsCollectionViewCell
        let mymember = membersList[(indexPath as NSIndexPath).row]
        
        
        cell.nameLabel?.text = mymember.name
        cell.emailLabel?.text = mymember.phone
        
        let imageUrl : String = mymember.imageUrl!
       
        if imageUrl.isEmpty{
            print("no user Image")
            
            if let firstChar = mymember.name?.characters.first {
                let firstString = String(describing: firstChar)
                cell.imageLabel.text = firstString
            }else {
                cell.imageLabel.text = "?"
            }
            
        }
        else{
        cell.guestImageView.af_setImage( withURL: URL(string: imageUrl)!)
        }
        
        cell.guestImageView.layer.cornerRadius  = cell.imageLabel.frame.size.width/2
        cell.guestImageView.layer.masksToBounds = true
        cell.guestImageView.clipsToBounds = true
        
        cell.imageLabel.layer.cornerRadius  = cell.imageLabel.frame.size.width/2
        cell.imageLabel.layer.masksToBounds = true
        cell.imageLabel.clipsToBounds = true

        return cell

    }
    
}

extension ChatInfoVC : UIImagePickerControllerDelegate ,UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
          SVProgressHUD.show()
//        Spinner.sharedinstance.startSpinner(viewController: self)
        
        if sourceType == "group"{
            
            if let img = info[UIImagePickerControllerOriginalImage] as? UIImage {
                
                let destImg = ThumbnailCreate(image: img)
                
                // upload image
                
                self.dismiss(animated: true, completion: {
                    
                    self.sourceType = "no"
                    self.uploadImage(image: destImg, Complition: {
                        (fileID, fileUrl) in
                        
                        print("name: \(self.gpName), fileUrl: \(fileUrl), fileID: \(fileID), group: \(self.chatlist!.group!)")
                        
                        let ImageURL = URL(string: fileUrl)
                        self.headerImage.af_setImage(withURL: ImageURL!)
                        
                          SVProgressHUD.dismiss()
                    //    Spinner.sharedinstance.stopSpinner(viewController: self)
                        
                        self.updateGroupInfo(name: self.gpName, fileUrl: fileUrl, fileID: fileID, group: self.chatlist!.group!)
                        
                    })
                })
            }
        }else {
            if let img = info[UIImagePickerControllerOriginalImage] as? UIImage
            {
                
                let dest = ThumbnailCreate(image: img)
                
                self.MediaUploading(dest, video: nil)
                
            }else if let videourl = info[UIImagePickerControllerMediaURL] as? URL
            {
                self.MediaUploading(nil, video: videourl)
                
            }
            //collectionView.reloadData()
            self.dismiss(animated: true, completion: nil)
        }

        
    }
    
    func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapMessageBubbleAt indexPath: IndexPath!) {
        if let test = self.getImage(indexPath: indexPath) {
            selectedImage = test
        //    print("this image",selectedImage)
        }
    }
    
    func getImage(indexPath: IndexPath) -> UIImage? {
        let message = self.messages[indexPath.row]
        if message.isMediaMessage == true {
            let mediaItem = message.media
            if mediaItem is JSQPhotoMediaItem {
                let photoItem = mediaItem as! JSQPhotoMediaItem
                if let test: UIImage = photoItem.image {
                    let image = test
                    return image
                }
            }
        }
        return nil
    }
    
    @objc func ThumbnailCreate (image:UIImage) ->UIImage  {
        
        let size = CGSize(width: 400, height: 400)
        
        // Define rect for thumbnail
        let scale = max(size.width/image.size.width, size.height/image.size.height)
        let width = image.size.width * scale
        let height = image.size.height * scale
        let x = (size.width - width) / CGFloat(2)
        let y = (size.height - height) / CGFloat(2)
        let thumbnailRect = CGRect(x: x, y: y, width:width, height: height)
        
        // Generate thumbnail from image
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        image.draw(in: thumbnailRect)
        let thumbnail = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    @objc func observerMessage() {
        let query = messageRef.queryLimited(toLast: 100)

        query.observe(.childAdded) { (snap:DataSnapshot) in

            if let data = snap.value as? [String:Any]{

                let data = snap.value! as! NSDictionary
                let senderid = data["senderId"] as! String
                let displayName = data["displayName"] as! String
                let datestr = data["date"] as! Double?
                let date = datestr?.dateconvert()
                let type = data["type"] as! String?
                let pthurl = data["pathurl"] as? String
                let mess = data["message"] as? String

                if type == "Photo" {

                    let url = URL(string: pthurl!)
                    if let imgdata = try? Data(contentsOf: url!){

                        let image = UIImage(data: imgdata)
                        let jsqmedia = JSQPhotoMediaItem(image: image)

                        if senderid == self.currentUser {
                            jsqmedia?.appliesMediaViewMaskAsOutgoing = true
                        }else
                        {
                            jsqmedia?.appliesMediaViewMaskAsOutgoing = false
                        }
                        let message = JSQMessage(senderId: senderid, senderDisplayName: displayName, date: date, media: jsqmedia)
                        self.messages.append(message!)
                        
                    }
                }

                if type == "Video" {

                    let url = URL(string: pthurl!)
                    let jsqvideo = JSQVideoMediaItem(fileURL: url!, isReadyToPlay: true)
                    let message = JSQMessage(senderId: senderid, senderDisplayName: displayName, date: date, media: jsqvideo)
                    self.messages.append(message!)

                    //extra

                }
                if type == "message" {

                    let message = JSQMessage(senderId: senderid, senderDisplayName: displayName, date: date, text: mess)
                    if let createdDate = (self.chatlist?.created) as? Int {

                        if createdDate < Int(datestr!) {
                            self.messages.append(message!)
                    

                        }

                    }else {

                        self.messages.append(message!)
                        

                    }
                }
            }
        }
    }
    
    @objc func handleChatNotification(message:String, group:Int){
        let userID = UserDefaults.standard.string(forKey: "userID")
        guard let myName = UserDefaults.standard.string(forKey: "name") else {return}
        let eventID = UserDefaults.standard.string(forKey: "invitationID")
        //        let myfirebaseID = Auth.auth().currentUser?.uID
        let iName = UserDefaults.standard.string(forKey: "invitationTitle")
        
        let parameters:Parameters = [
            "userID":userID!,
            "invitationID":eventID!,
            //            "firebaseID":myfirebaseID!,
            "chatID":chatID!,
            "message":message,
            "group":group,
            "iName": iName!,
            "userName": myName,
            ]
        
        apiCallingMethod(apiEndPints: "chat", method: .post, parameters: parameters) { (data, total) in
        }
    }
    
    @objc func rightNavAction(){
        
        let controller = UIImagePickerController()
        controller.delegate = self
        sourceType = "group"
        controller.navigationBar.tintColor = .white //Text Color
        controller.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        self.present(controller, animated: true, completion: nil)
        
    }
    
    @objc func rightNavAction2(){
        
        let alert = UIAlertController(title: "Group Name", message: "", preferredStyle: .alert)
        
        alert.addTextField { (fild1) in
            fild1.placeholder = "Enter name of the group"
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
            
            // update Name
            let text1 = alert.textFields?[0].text!
            self.newName = text1!
            self.updateGroupInfo(name: text1, fileUrl: nil, fileID: nil, group: self.chatlist!.group!)
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    @objc func updateGroupInfo(name:String?,fileUrl:String?,fileID:String?, group:Int){
        
        let urlStr = MInvitdClient.Constants.BaseUrl + "chat/member"
        
        guard let myName = UserDefaults.standard.string(forKey: "name") else {return}
        guard let iName = UserDefaults.standard.string(forKey: "invitationTitle") else {return}
        
        var parameters:Parameters = ["userID":chatlist!.userID!,
                                     "invitationID":chatlist!.invitationID!,
                                     "chatID":chatlist!.chatID!,
                                     "group":group,
                                     "userName":myName,
                                     "iName":iName
        ]
        
        if let name1 = name {
            parameters["name"] = name1
        }
        
        if let fileUrl1 = fileUrl {
            parameters["fileUrl"] = fileUrl1
        }
        
        if let fileID1 = fileID {
            parameters["fileID"] = fileID1
        }
        let authHeader = ["Authorization" : UserDefaults.standard.string(forKey: "userToken")!, "Content-Type" : "application/json" ]
        
        Alamofire.request(urlStr, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: authHeader).responseJSON { (response) in
          
            if response.error == nil {
                
                print("\(response)")
                self.gpName = name
            }
        }
    }
}
