
//  ProfileUpdateVC.swift
//  Minvtd
//
//  Created by admin on 17/10/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.


import UIKit
import Firebase
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit
import Alamofire
import SlideMenuControllerSwift
import AlamofireImage

class ProfileUpdateVC:UIViewController,GIDSignInUIDelegate  {

    @IBOutlet weak var DisplayPicture: UIImageView!
    @IBOutlet weak var changePhotoBtn: UIButton!
    @IBOutlet weak var fullNameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var dobField: UITextField!
    @IBOutlet weak var maleBtn: UIButton!
    @IBOutlet weak var femaleBtn: UIButton!
    @IBOutlet weak var mobNoLabel: UILabel!
    @IBOutlet weak var emptyFieldError: UILabel!
    
    @objc var firebaseID = Auth.auth().currentUser?.uid
    @objc let facebooklogin = FBSDKLoginManager()
    @objc var defaultGender = 1
    @objc var defaultImgUrl:String?
    @objc var myActivityIndicator:UIActivityIndicatorView?
    @objc var defaultName = ""
    @objc var defaultEmail = ""
    @objc var defaultDob = ""
    @objc var serverdate: String?
    @objc let datepicker1 = UIDatePicker()
    var userAvatar = UserDefaults.standard.string(forKey: "userAvatar")
    
    @objc let placeholderImage = UIImage(named: "eventCoverImage")!
    
    var gradientLayer: CAGradientLayer!
    var register:Int?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if register == 1{
            self.title = "Create Profile"
        }
        else{
          self.title = "Update Profile"
        }
        
        datepicker1.datePickerMode = .date

        disssmissKeyboard()
        self.navigationController?.navigationBar.barStyle = UIBarStyle.black
        self.navigationController?.navigationBar.tintColor = .white
        dobField.inputView = datepicker1
        
        datepicker1.addTarget(self, action: #selector(datePickerChanged(datePicker:)), for: .valueChanged)
        
        let button1 = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(back))
        
        self.navigationItem.leftBarButtonItem = button1
        let button2 = UIBarButtonItem(title: "Update", style: .plain, target: self, action: #selector(manualSignUP(_:)))
        
        self.navigationItem.rightBarButtonItem = button2
        
        self.DisplayPicture.layer.cornerRadius = self.DisplayPicture.frame.size.width / 2
        self.DisplayPicture.clipsToBounds = true
        self.DisplayPicture.layer.borderWidth = 2.0
        self.DisplayPicture.layer.borderColor = UIColor(red: 255 / 255, green: 255 / 255, blue: 255 / 255, alpha: 1.0).cgColor
        
        self.DisplayPicture.isUserInteractionEnabled = true
        let tapGestureYes = UITapGestureRecognizer(target: self, action: #selector(self.displayImagePicker(_:)))
        tapGestureYes.numberOfTapsRequired = 1
        self.DisplayPicture.addGestureRecognizer(tapGestureYes)
        
        print("\(self.DisplayPicture.frame.size)")
        
        self.emptyFieldError.isHidden = true
//        navigationBarSetup(nil)
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 255 / 255, green: 52 / 255, blue: 101 / 255, alpha: 1.0)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //adding prefilled values to UI
        
        if let userID = UserDefaults.standard.object(forKey: "userID"){
            if let name = UserDefaults.standard.object(forKey: "name"){
                self.fullNameField.text = name as! String
            }
            if let email = UserDefaults.standard.object(forKey: "email"){
                self.emailField.text = email as! String
            }
            if let myMobNo = UserDefaults.standard.object(forKey: "phone"){
                print("\(myMobNo)")
                self.mobNoLabel.text = String(describing: myMobNo)
            }
            if let gender = UserDefaults.standard.object(forKey: "gender"){
                self.defaultGender = gender as! Int
                if self.defaultGender == 1{
                    self.maleBtn.titleLabel?.textColor = UIColor.black
                    self.femaleBtn.titleLabel?.textColor = UIColor.lightGray
                    self.maleBtn.isSelected = true
                    self.femaleBtn.isSelected = false
                }
                else{
                    self.femaleBtn.titleLabel?.textColor = UIColor.black
                    self.maleBtn.titleLabel?.textColor = UIColor.lightGray
                    self.maleBtn.isSelected = false
                    self.femaleBtn.isSelected = true
                }
            }
            
            if let dob = UserDefaults.standard.object(forKey: "dob"){
                self.dobField.text = dob as! String
            }
            
            if let imageUrl = UserDefaults.standard.object(forKey: "imageUrl"){
                
                self.DisplayPicture.af_setImage(
                    withURL: URL(string: imageUrl as! String)!,
                    placeholderImage: placeholderImage
                )
                self.DisplayPicture.layer.cornerRadius = self.DisplayPicture.frame.size.width / 2
                self.DisplayPicture.clipsToBounds = true
            }
        }
    }
    
    @objc func back(_ sender: Any) {
        self.pushpopAnimation()
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func nameValueChanged(_ sender: UITextField) {
        self.emptyFieldError.isHidden = true
    }
    
    @IBAction func emailValueChanged(_ sender: UITextField) {
        self.emptyFieldError.isHidden = true
    }
    
    @IBAction func maleBtnTapped(_ sender: Any) {
        
        defaultGender = 1
        self.maleBtn.titleLabel?.textColor = UIColor.black
        self.femaleBtn.titleLabel?.textColor = UIColor.lightGray
        self.maleBtn.isSelected = true
        self.femaleBtn.isSelected = false
    }
    
    @IBAction func femaleBtnTapped(_ sender: Any) {
        
        defaultGender = 0
        self.femaleBtn.titleLabel?.textColor = UIColor.black
        self.maleBtn.titleLabel?.textColor = UIColor.lightGray
        self.maleBtn.isSelected = false
        self.femaleBtn.isSelected = true
    }
    
    @objc func displayImagePicker(_ sender: UITapGestureRecognizer){
        
        let imagePicker = UIImagePickerController()
        imagePicker.navigationBar.tintColor = .white //Text Color
        imagePicker.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func changePhotoImagePicker(_ sender: Any) {
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.navigationBar.tintColor = .white //Text Color
        imagePicker.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    @objc func manualSignUP(_ sender: UIBarButtonItem) {
        
        if fullNameField.text == "" || dobField.text == "" || emailField.text == ""{
            
            self.emptyFieldError.isHidden = false
        }
        else if emailField.text!.isValidEmail() == false{
            
            CreateAlertPopUp("Invalid Email", "Add a valid email Address", OnTap: nil)
            
        }
        else{
            
            self.emptyFieldError.isHidden = true
            
            defaultName = fullNameField.text!
//          defaultDob  = dobField.text!
            defaultEmail = emailField.text!
        
            if self.dobField.text != nil{
                self.defaultDob = self.dobField.text!
            }
            var params:Parameters
                
            if let fileID = UserDefaults.standard.object(forKey: "fileID"){
                params = ["name":defaultName,"dob":defaultDob,"email":defaultEmail,"gender":defaultGender,"fileID":fileID]
            }
            else{
                params = ["name":defaultName,"dob":defaultDob,"email":defaultEmail,"gender":defaultGender,"fileID":0]
            }
            if defaultImgUrl != nil {
                params["imageUrl"] = defaultImgUrl
            }
                createProfile(socialtype: "iOS", firebaseID: firebaseID!, params: params, socialID: nil)

        }
    }
}


extension ProfileUpdateVC:UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let img = info[UIImagePickerControllerOriginalImage] as? UIImage{
            
            DisplayPicture.clipsToBounds = true
            
            myActivityIndicator?.startAnimating()
            
            uploadImage(image: img, Complition: { (fid, Furl) in
                
                // sucessfully uploaded
                self.DisplayPicture.image = img.withRenderingMode(.alwaysOriginal)
                self.DisplayPicture.clipsToBounds = true
                //setImage(img.withRenderingMode(.alwaysOriginal), for: .normal)
                self.defaultImgUrl = Furl
                self.myActivityIndicator?.stopAnimating()
                print("This is fid when profile uplaoded:\(fid)")
                UserDefaults.standard.set(fid, forKey: "fid")
            })
            picker.dismiss(animated: true, completion: nil)
        }
    }
}

extension ProfileUpdateVC {
    
    @objc func createProfile(socialtype:String,firebaseID:String,params:Parameters,socialID:String?){
        
       
        if userAvatar == "" || userAvatar == nil {
             let fUrl = self.defaultImgUrl
                UserDefaults.standard.set(fUrl, forKey: "imageUrl")
                userAvatar = fUrl
        }else{
            print("no change in image")
            
        }
        
        if let fUrl = self.defaultImgUrl{
            UserDefaults.standard.set(fUrl, forKey: "imageUrl")
            userAvatar = fUrl
        }
        var parameter = params

        parameter["userID"] = UserDefaults.standard.string(forKey: "userID")!
        if let fileID = UserDefaults.standard.object(forKey: "fid"){
            parameter["fileID"] = fileID
        }
        
        
        print("These are parameters\(parameter)")
        
        apiCallingMethod(apiEndPints: "user", method: .put, parameters: parameter) { (data1, totalobj) in
            
            print("this is data ->\(data1)")
            
            // please update
            
            Spinner.sharedinstance.stopSpinner(viewController: self)

            var data : Parameters?
            
            
            if let data = data1 as? [[String : Any]]
            {
                print("here")
            }
            else{
                data = parameter
                data!["userAvatar"] = self.userAvatar
                print("Parameters hack appleid-----> \(data)")
            }
            
            
            let name = data?["name"]!
            let email = data?["email"]!
//            let imageUrl = data?["userAvatar"]!
            let dob = data?["dob"]!
            let fileID = data?["fileID"]!
            let gender = data?["gender"]!
            
            UserDefaults.standard.set(name , forKey : "name")
            UserDefaults.standard.set(email , forKey : "email")
            UserDefaults.standard.set(gender , forKey : "gender")
            UserDefaults.standard.set(self.userAvatar , forKey : "imageUrl")
            UserDefaults.standard.set(dob, forKey: "dob")
            UserDefaults.standard.set(fileID, forKey: "fileID")
            UserDefaults.standard.set(self.userAvatar,forKey: "userAvatar")
            
            DispatchQueue.main.async {
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let leftDrawerVC = storyboard.instantiateViewController(withIdentifier: "SlideMenuViewController") as! SlideMenuViewController
                let mainVC = storyboard.instantiateViewController(withIdentifier: "invitationVC") as! InvitationViewController
                let leftDrawer = SlideMenuController(mainViewController: UINavigationController(rootViewController:mainVC), leftMenuViewController: leftDrawerVC)
                self.present(leftDrawer, animated: true, completion: nil)
            }
        }
    }
    
    @objc func firebaseDatabaseUpdate(fid:String,params:Parameters) {
        
        print("firebase reference",fid)
        
        let ref = Database.database().reference().child("users").child(fid)
        ref.setValue(params)
    }
    
    @objc func datePickerChanged(datePicker:UIDatePicker) {
        
        self.emptyFieldError.isHidden = true
        
        datepicker1.datePickerMode = UIDatePickerMode.date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        serverdate = dateFormatter.string(from: datepicker1.date)
        
        dobField.text = serverdate
        defaultDob = serverdate!

    }
}
extension String {
    func isValidEmail() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
}
