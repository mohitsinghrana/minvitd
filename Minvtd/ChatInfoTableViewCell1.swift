//
//  ChatInfoTableViewCell.swift
//  Minvtd
//
//  Created by Vivek Aggarwal on 29/08/18.
//  Copyright © 2018 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit

class ChatInfoTableViewCell1: UITableViewCell {

    @IBOutlet var memberImage: UIImageView?
    @IBOutlet var memberName: UILabel?
    @IBOutlet var memberMob: UILabel?
    
}
