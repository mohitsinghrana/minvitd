//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//


import UIKit
import SlideMenuControllerSwift
import Alamofire
import AlamofireImage
class AddEventCodeViewController: UIViewController {

    @IBOutlet weak var myNavigationBar: UINavigationBar!
    @IBOutlet weak var codeTF: UITextField!
    @IBOutlet weak var addBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dissmisskeyboard)))

//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.setStatusBarBackgroundColor(color: UIColor(hexString: "#691C2F")!)


    }
    
    @objc func dissmisskeyboard()
    {
        self.view.endEditing(true)
    }
    

    @IBAction func menuBtn(_ sender: UIBarButtonItem) {
        
        slideMenuController()?.openLeft()

    }
    
    
    @IBAction func addBtnClicked(_ sender: UIButton) {
        
        let strCode =  self.codeTF.text!
        self.submitFunc(code: strCode)
    }

    
    
    @objc func validate() -> Bool
    {
        guard let code = self.codeTF.text, !code.isEmpty else {
            self.codeTF.placeholder = "Please Enter Field"
            return false
        }
       
        return true
    }
    
    @objc func submitFunc(code : String )  {
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        let parameters: Parameters = ["eventID" : code as AnyObject , "userID" : userID  as AnyObject]
      
    
        
        
        apiCallingMethod(apiEndPints: "event/request", method: .post, parameters: parameters) { (data1, totalObjectCount) in
            
            if let data = data1 as? [[String : Any]]
            {
                
                DispatchQueue.main.async {
                    
                    self.codeTF.text = ""
                    let alert = UIAlertController(title: "MInvitd", message: "You have been successfully added to this event" , preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "Go to Event", style: .default, handler: { (action : UIAlertAction) in
                        
                        let leftDrawerVC = self.storyboard?.instantiateViewController(withIdentifier: "SlideMenuViewController") as! SlideMenuViewController
                        let mainVC = self.storyboard?.instantiateViewController(withIdentifier: "invitationVC") as! InvitationViewController
                        let leftDrawer = SlideMenuController(mainViewController: mainVC, leftMenuViewController: leftDrawerVC)
                        self.present(leftDrawer, animated: true, completion: nil)
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                    
                }
            }
            
        }
        
    }
    
   
}
