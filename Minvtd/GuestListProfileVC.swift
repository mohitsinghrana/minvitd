//
//  GuestListProfileVC.swift
//  Minvtd
//
//  Created by admin on 04/12/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit

class GuestListProfileVC: UITableViewController {
    
    var guests:[MInvitdGuest] = []
    
    override func viewDidLoad() {
    super.viewDidLoad()
    tableView.rowHeight = 60
    tableView.register(guestCell.self, forCellReuseIdentifier: "reuseIdentifier")
    
    navigationBarSetup(nil)
    title = "Tag"
    let button1 = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(back))
    self.navigationItem.leftBarButtonItem = button1
    self.navigationItem.rightBarButtonItem = nil
    tableView.tableFooterView = UIView()
    
    }
    
    @objc func back(){
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
    // #warning Incomplete implementation, return the number of sections
    return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return guests.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as? guestCell
    cell?.guest = guests[indexPath.row]
    return cell!
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "GuestProfileVC") as! GuestProfileVC
            vc.data = guests[indexPath.row]

        self.present(UINavigationController(rootViewController:vc), animated: true, completion: nil)

    
    
    }
    
}
