//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import UIKit

class ProductsCollectionViewCell : UICollectionViewCell {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var descript: UILabel!
    @IBOutlet weak var weight: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        image.layer.cornerRadius = image.frame.width / 2
        image.clipsToBounds = true
    }
    
}
