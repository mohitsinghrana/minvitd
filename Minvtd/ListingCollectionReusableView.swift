//
//  ListingCollectionReusableView.swift
//  Minvtd
//
//  Created by Tushar Aggarwal on 24/07/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit

class ListingCollectionReusableView: UICollectionReusableView {
        
    @IBOutlet weak var image: UIImageView!
    
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var contactLabel: UILabel!
    
    @IBOutlet weak var contact: UILabel!
    
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var address: UILabel!
    
    @IBOutlet weak var descLabel: UILabel!
    
    @IBOutlet weak var desc: UILabel!
    
    @IBOutlet weak var vni: UIImageView!
    
    @IBOutlet weak var vci: UIImageView!
    
    @IBOutlet weak var vai: UIImageView!
    
    @IBOutlet weak var vdi: UIImageView!
    
    @IBOutlet weak var descriptionView: UIView!
    
    @IBOutlet weak var descriptionViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var descriptionViewExpandBtn: UIButton!
    
    @IBOutlet weak var view1: UIView!
    
    @IBOutlet weak var view2: UIView!
    
    @IBOutlet weak var descLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var callButton: UIButton!
    
}
