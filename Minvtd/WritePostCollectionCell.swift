//
//  WritePostCollectionCell.swift
//  Minvtd
//
//  Created by Vineet Singh on 07/08/18.
//  Copyright © 2018 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit

class WritePostCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var selectedImage: UIImageView!
    @IBOutlet weak var videoPlayBtn: UIImageView!
    @IBOutlet weak var cross: UIButton!
    
}
