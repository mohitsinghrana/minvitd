//
//  IDMPhotoCustom2.swift
//  Minvtd
//
//  Created by Vineet Singh on 03/07/18.
//  Copyright © 2018 Euro Infotek Pvt Ltd. All rights reserved.
//

import Foundation
import IDMPhotoBrowser


class IDMPhotoCustom2:IDMPhoto {
    
    var counter:String?
    var detail:String?
    var title:String?
    
    init(counter : String? ,imageUrl:URL?,detail:String?, title:String?) {
        
        self.counter = counter
        super.init(url: imageUrl)
        self.photoURL = imageUrl
        self.detail = detail
        self.title = title
    }
}
