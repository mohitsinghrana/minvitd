//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import UIKit

class VideoCollectionViewCell : UICollectionViewCell
{
    
    @IBOutlet weak var image: UIImageView!
   
    @objc let profileImage = { () -> UIImageView in
        
        let img = UIImageView()
        img.layer.cornerRadius = 18.0
        img.clipsToBounds = true
        img.layer.borderWidth = 2.0
        img.layer.borderColor = UIColor.white.cgColor
        return img
    }()
    
//    @objc let LikeImage = { () -> UIImageView in
//
//        let img = UIImageView()
//        img.image = #imageLiteral(resourceName: "likeFilled")
//        return img
//    }()
    
    
    //Comment View :
    
    let commentButton = { () -> UIButton in
        let btn = UIButton(type: .system)
        btn.setImage(#imageLiteral(resourceName: "msg").withRenderingMode(.alwaysOriginal), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        return btn
    }()
    
    let commentLabel = { () -> UILabel in
        let lab = UILabel()
        lab.text = "234"
        lab.textColor = UIColor.init(hexString: "#9d9d9d")
        lab.font = UIFont(name: "GothamRounded-Book", size: 12)
        return lab
    }()
    
    let commentLblview: UIView = {
        let temp = UIView()
        return temp
    }()
    
    let commentBtnView = { () -> UIView in
        let btnView = UIView()
        return btnView
    }()
    
    private lazy var commentStack = { () -> UIStackView in
        let base = UIStackView(arrangedSubviews: [commentBtnView,commentLblview])
        base.axis = .horizontal
        base.distribution = .fillEqually
        base.spacing = 0
        return base
    }()
    
    //like View:
    
    let likeBtnView = { () -> UIView in
        let lab = UIView()
        return lab
    }()
    
    @objc let LikeImage = { () -> UIImageView in
        
        let img = UIImageView()
        img.image = #imageLiteral(resourceName: "likeFilled")
        return img
    }()
    
    
    let likeLblview: UIView = {
        let temp = UIView()
        return temp
    }()
    
    let likeLabel = { () -> UILabel in
        let lab = UILabel()
        lab.text = "234"
        lab.textColor = UIColor.init(hexString: "#9d9d9d")
        lab.font = UIFont(name: "GothamRounded-Book", size: 12)
        return lab
    }()
    
    private lazy var likeStack = { () -> UIStackView in
        let base = UIStackView(arrangedSubviews: [likeBtnView,likeLblview])
        base.axis = .horizontal
        base.distribution = .fillEqually
        base.spacing = 0
        return base
    }()
    
    //baseView
    
    private lazy var baseStack = { () -> UIStackView in
        //      let base = UIStackView(arrangedSubviews: [commentButton,likeButton,collectionImg,extraLikes])
        let base = UIStackView(arrangedSubviews: [likeStack,commentStack])
        base.axis = .horizontal
        base.distribution = .fillEqually
        return base
    }()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }

    
    
    
    @objc func setup() {
        
        self.addSubview(profileImage)
        self.likeBtnView.addSubview(LikeImage)
        self.likeLblview.addSubview(likeLabel)
        self.commentBtnView.addSubview(commentButton)
        self.commentLblview.addSubview(commentLabel)
        self.addSubview(baseStack)
        
        LikeImage.widthAnchor.constraint(equalToConstant: 18).isActive = true
        LikeImage.heightAnchor.constraint(equalToConstant: 18).isActive = true
        LikeImage.anchorCenterSuperview()
        likeLabel.feedvideosNumberHeight()
        
        commentButton.widthAnchor.constraint(equalToConstant: 18).isActive = true
        commentButton.heightAnchor.constraint(equalToConstant: 18).isActive = true
        commentButton.anchorCenterSuperview()
        commentLabel.feedvideosNumberHeight()
        
        
        profileImage.anchor(self.topAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 15.0, leftConstant: 15.0, bottomConstant: 0, rightConstant: 0.0, widthConstant: 36.0, heightConstant: 36.0)
        
        //LikeImage.anchor(self.topAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 15.0, leftConstant: 0, bottomConstant: 0, rightConstant: 15.0, widthConstant: 30.0, heightConstant: 30.0)
        
        baseStack.anchor(self.image.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 15.0, leftConstant: 15.0, bottomConstant: 0.0, rightConstant: 0.0, widthConstant: 0.0, heightConstant: 25.0)
        
        
        
    }

}
