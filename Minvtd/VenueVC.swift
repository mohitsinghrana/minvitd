//
//  VenuVC.swift
//  Minvtd
//
//  Created by admin on 13/11/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit

class VenueVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var tableheader:VenuHeaderView?
    var arr = [Venue]()
    var currentIndex = 0{
        didSet{
            
            // reload tableview
            tableView.reloadData()
            // reload header and headerview
            
            //print("\(arr[currentIndex].gallery)") 
            
            tableheader?.images = arr[currentIndex].gallery
            tableheader?.appsCollectionView.reloadData()
        }
    }

    let tableView = { () -> UITableView in
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
        
    }()

    let venuBottomBar = { () -> VenuBottomBar in
        let tableView = VenuBottomBar()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
        
    }()
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        Setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        currentIndex = 0
        
        tableView.reloadData()
        tableheader?.images = arr[currentIndex].gallery
        tableheader?.appsCollectionView.reloadData()
        
        venuBottomBar.appsCollectionView.reloadData()
    }

    @objc func Setup() {
        view.addSubview(tableView)
        view.addSubview(venuBottomBar)
    
        //tableview
        tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -50).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        tableView.delegate = self
        tableView.dataSource = self
        tableView.bounces = false
        
        // bottom bar
        venuBottomBar.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        venuBottomBar.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        venuBottomBar.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        venuBottomBar.heightAnchor.constraint(equalToConstant: 50).isActive = true
        venuBottomBar.venus = self.arr
        venuBottomBar.vc = self
        
        //end
        tableView.register(VenueSectionHeader.self, forHeaderFooterViewReuseIdentifier: "header")
        tableView.register(VenuViewCell.self, forCellReuseIdentifier: "cell")
        
        //header
        tableheader = VenuHeaderView(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.width*9/16))
        tableheader?.vc = self
        tableView.tableHeaderView = tableheader
        tableView.tableFooterView = UIView()
        
        tableView.estimatedRowHeight = 45
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.estimatedSectionFooterHeight = 0
        tableView.sectionFooterHeight = UITableViewAutomaticDimension
        
        tableView.estimatedSectionHeaderHeight = 100
        tableView.sectionHeaderHeight = UITableViewAutomaticDimension        
    }
}

//
// MARK: - View Controller DataSource and Delegate
//
extension VenueVC {
    
     func numberOfSections(in tableView: UITableView) -> Int {
        return arr[currentIndex].tabs.count
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr[currentIndex].tabs[section].collapsed ? 0:1
    }
    
    // Cell
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? VenuViewCell
       let current = arr[currentIndex].tabs
        //print("\(current)")
        if  indexPath.section == 0 {
            cell?.textView.attributedText = current[indexPath.section].arrtibutedString
            cell?.textView.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.light)

        }else{
            cell?.textView.text = current[indexPath.section].body
            cell?.textView.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.light)
        }

        cell?.vc = self
        
        return cell!
    }
    
    // Header
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header") as! VenueSectionHeader 

        //print("\(arr)")
        //print("\(arr[currentIndex].tabs)")
        
        let current = arr[currentIndex].tabs
        header.setCollapsed(current[section].collapsed)
        header.section = section
        header.textView.text = current[section].title
        header.delegate = self
//        header.contentView.backgroundColor = UIColor.white
        header.imageView.image = UIImage(named: "locationWhite")
        let vl:CGFloat = CGFloat(section)/CGFloat(arr.count)

        header.vi.alpha = vl
        header.vi2.alpha = 1 - vl
        header.vc = self
        
        return header
    }
    
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

//
// MARK: - Section Header Delegate
//
extension VenueVC: VenueSectionHeaderDelegate {
    
    @objc func toggleSection(_ header: VenueSectionHeader, section: Int) {
        let collapsed = !arr[currentIndex].tabs[section].collapsed

        // Toggle collapse
        arr[currentIndex].tabs[section].collapsed = collapsed
        header.setCollapsed(collapsed)

        let   index1 =  NSIndexSet(index: section) as IndexSet;
        tableView.reloadSections(index1, with: .automatic)
        //tableView.reloadData()
    }
}
