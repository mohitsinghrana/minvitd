//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import Firebase
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import Alamofire
import UserNotifications
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    //, ESTBeaconManagerDelegate
    
    var window: UIWindow?
    @objc let gcmMessageIDKey = "gcm.message_id"
    @objc let beaconManager = ESTBeaconManager()
    let region = CLBeaconRegion(proximityUUID: UUID(uuidString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D")!, major: 1986, minor: 18612, identifier: "d10b48b45d13")
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        Fabric.sharedSDK().debug = true
   //     UIApplication.shared.statusBarStyle = .lightContent
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        IQKeyboardManager.shared.enable = true

        UINavigationBar.appearance().barStyle = .blackOpaque
        UINavigationBar.appearance().barTintColor = UIColor(red: 1.0, green: 36/255, blue: 89/255, alpha: 1.0)
        UINavigationBar.appearance().isOpaque = false
        UINavigationBar.appearance().tintColor = UIColor.white
        
        UITabBar.appearance().tintColor = UIColor.white
        if #available(iOS 10.0, *) {
            UITabBar.appearance().unselectedItemTintColor = UIColor.white
        }
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        // menu setup
        SlideMenuOptions.contentViewScale = 1.0
        SlideMenuOptions.leftViewWidth = (self.window?.screen.bounds.size.width)! * 0.75
        
        if let token = InstanceID.instanceID().token(){
            //print("This is FCM Token *** : \(token)")
        }
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        application.applicationIconBadgeNumber = 0
        
        if userID == nil
        {
            // changing here
            self.window =  UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc =  storyboard.instantiateViewController(withIdentifier: "introScreens") as? IntroScreensVC
            self.window?.rootViewController = vc
            self.window?.makeKeyAndVisible()
        }
        else
        {
            // already loged in ; go to Main Screen
            tokkenUpdate()
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let leftDrawerVC = storyBoard.instantiateViewController(withIdentifier: "SlideMenuViewController")
            let mainVC = storyBoard.instantiateViewController(withIdentifier: "invitationVC") as! InvitationViewController
            let leftDrawer = SlideMenuController(mainViewController: UINavigationController(rootViewController:mainVC), leftMenuViewController: leftDrawerVC)
            //getUserData()
            self.window =  UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = leftDrawer
            self.window?.makeKeyAndVisible()
        }
        
        return true
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
        let deviceID = UIDevice.current.identifierForVendor?.uuidString
        let fcmToken = UserDefaults.standard.string(forKey: "fcmToken")
        let modelName = UIDevice.modelName
        let systemVersion = UIDevice.current.systemVersion
        if let userID = UserDefaults.standard.string(forKey: "userID"){
            
            SessionTracker.sharedInstance().apiCallingMethod(apiEndPints: "session", method: .post, parameters: ["userID":userID,"status":2,"deviceID":deviceID!,"device": "iOS","token":fcmToken!,"model" : modelName,"os" : systemVersion]) { (data1:Any?, totalObjects:Int?) in
                if totalObjects == 0 {
                    return
                }/*else {
                    if let data = data1 as? [[String : Any]]
                    {
                        //print("this is data -> \(data)")
                    }
                }*/
            }
        }
    }
    
    
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        let deviceID = UIDevice.current.identifierForVendor?.uuidString
        let fcmToken = UserDefaults.standard.string(forKey: "fcmToken")
        let modelName = UIDevice.modelName
        let systemVersion = UIDevice.current.systemVersion
        if let userID = UserDefaults.standard.string(forKey: "userID"){
            
            SessionTracker.sharedInstance().apiCallingMethod(apiEndPints: "session", method: .post, parameters: ["userID":userID,"status":1,"deviceID":deviceID!,"device": "iOS","token":fcmToken!,"model" : modelName, "os" : systemVersion]) { (data1:Any?, totalObjects:Int?) in
                if totalObjects == 0 {
                    return
                }/*else {
                    if let data = data1 as? [[String : Any]]
                    {
                        //print("\(data)")
                    }
                }*/
            }
        }
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        let deviceID = UIDevice.current.identifierForVendor?.uuidString
        let fcmToken = UserDefaults.standard.string(forKey: "fcmToken")
        let modelName = UIDevice.modelName
        let systemVersion = UIDevice.current.systemVersion
        if let userID = UserDefaults.standard.string(forKey: "userID"){
            
            SessionTracker.sharedInstance().apiCallingMethod(apiEndPints: "session", method: .post, parameters: ["userID":userID,"status":1,"deviceID":deviceID!,"device": "iOS","token":fcmToken!,"model" : modelName, "os" : systemVersion,"manufacturer": "Apple"]) { (data1:Any?, totalObjects:Int?) in
                if totalObjects == 0 {
                    return
                }/*else {
                    if let data = data1 as? [[String : Any]]
                    {
                        //print("\(data)")
                    }
                }*/
            }
        }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        
        let deviceID = UIDevice.current.identifierForVendor?.uuidString
        let fcmToken = UserDefaults.standard.string(forKey: "fcmToken")
        let modelName = UIDevice.modelName
        let systemVersion = UIDevice.current.systemVersion
        if let userID = UserDefaults.standard.string(forKey: "userID"){
            
            SessionTracker.sharedInstance().apiCallingMethod(apiEndPints: "session", method: .post, parameters: ["userID":userID,"status":2,"deviceID":deviceID!,"device": "iOS","token":fcmToken!,"model" : modelName, "os" : systemVersion]) { (data1:Any?, totalObjects:Int?) in
                if totalObjects == 0 {
                    return
                }/*else {
                    if let data = data1 as? [[String : Any]]
                    {
                        //print("the data is -> \(data)")
                    }
                }*/
            }
        }
    }
}
