//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

// MARK: - MInvitdEvents

struct MInvitdEvent{
    
    // MARK: Properties
    
    let eventID : Int?
    let invitationID : Int?
    let adminID : Int?
    let category : String?
    let name : String?
    let description : String?
    let date : String?
    let time : String?
    let fileUrl : String?
    let venue : String?
    let coordinates : String?
    let status : String?
    let created : String?
    let updated : String?
    let guestID : Int?
    var guestStatus : Int?
    var guestStatusName : String?
    var liked : Bool?
    let commentID : Int?
    var commentsCount : Int?
    var likesCount : Int?
    var type: Int?
    var likeID: Int?
    var personThumbnail: String?
    
    // MARK: Initializers
    init(dictionary: [String:AnyObject]) {
        eventID = dictionary[MInvitdClient.JSONResponseKeys.eventID] as? Int
        invitationID = dictionary[MInvitdClient.JSONResponseKeys.invitationID] as? Int
        adminID = dictionary[MInvitdClient.JSONResponseKeys.adminID] as? Int
        category = dictionary[MInvitdClient.JSONResponseKeys.category] as? String
        name = dictionary[MInvitdClient.JSONResponseKeys.name] as? String
        description = dictionary[MInvitdClient.JSONResponseKeys.description] as? String
        date = dictionary[MInvitdClient.JSONResponseKeys.date] as? String
        time = dictionary[MInvitdClient.JSONResponseKeys.time] as? String
        fileUrl = dictionary[MInvitdClient.JSONResponseKeys.fileUrl] as? String
        venue = dictionary[MInvitdClient.JSONResponseKeys.venue] as? String
        coordinates = dictionary[MInvitdClient.JSONResponseKeys.coordinates] as? String
        status = dictionary[MInvitdClient.JSONResponseKeys.status] as? String
        created = dictionary[MInvitdClient.JSONResponseKeys.created] as? String
        updated = dictionary[MInvitdClient.JSONResponseKeys.updated] as? String
        guestID = dictionary[MInvitdClient.JSONResponseKeys.guestID] as? Int
        guestStatus = dictionary[MInvitdClient.JSONResponseKeys.guestStatus] as? Int
        guestStatusName = dictionary[MInvitdClient.JSONResponseKeys.guestStatusName] as? String
        liked = dictionary[MInvitdClient.JSONResponseKeys.liked] as? Bool
        commentID = dictionary[MInvitdClient.JSONResponseKeys.commentID] as? Int
        commentsCount = dictionary[MInvitdClient.JSONResponseKeys.commentsCount] as? Int
        likesCount = dictionary[MInvitdClient.JSONResponseKeys.likesCount] as? Int
        type = dictionary[MInvitdClient.JSONResponseKeys.type] as? Int
        likeID  = dictionary[MInvitdClient.JSONResponseKeys.likeID] as? Int
        personThumbnail = dictionary[MInvitdClient.JSONResponseKeys.personThumbnail] as? String
    }
    
    static func eventsFromResults(_ results: [[String:AnyObject]]) -> [MInvitdEvent] {
        var events = [MInvitdEvent]()
        
        // iterate through array of dictionaries, each Article is a dictionary
        for event in results {
            events.append(MInvitdEvent(dictionary: event))
        }
        
        return events
    }
   
    
    
}
