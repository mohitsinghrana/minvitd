//
//  Copyright © 2017 Euroinfotek. All rights reserved.

import UIKit
import Firebase
import Alamofire

class GuestsViewController : UIViewController {
    
    @IBOutlet weak var navigationBar: UINavigationBar!
    var guests: [MInvitdGuest] = [MInvitdGuest]()
    var filterData = [MInvitdGuest]()
    
    var searchBar:UISearchBar = {
        let srch = UISearchBar()
        srch.placeholder = "Search"
      
        return srch
    }()
    
    var isSearching:Bool = false
    
    @IBOutlet weak var guestsCollectionView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guestsCollectionView.delegate = self
        guestsCollectionView.dataSource = self
        
        navigationBarSetup(nil)
        setupNavBarContent()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func setupNavBarContent(){
        
        let button1 = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(cancelButton(_:)))
        self.navigationItem.leftBarButtonItem = button1
        self.title = "Guests"
        let button2 = UIBarButtonItem(image: #imageLiteral(resourceName: "groupFilled"), style: .plain, target: self, action: #selector(groupButton))
        self.navigationItem.rightBarButtonItem = button2
    }
    
    @IBAction func groupButton(_ sender: UIBarButtonItem) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "GroupChatCreate") as? GroupChatCreate
        vc?.guests = self.guests
        vc?.chatID = ""
        self.present(UINavigationController(rootViewController:vc!), animated: true, completion: nil)
    }
    
    @IBAction func cancelButton(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        guestlist()
        
        searchBar.frame =  CGRect(x: 0, y: 0, width: view.bounds.width - 44.0, height: 44.0)
        searchBar.delegate = self
        guestsCollectionView.tableHeaderView = searchBar
        self.guestsCollectionView.setContentOffset(CGPoint(x: 0, y: 44), animated: true)
    }
    
    @objc func guestlist() {
            
            let invitationID = UserDefaults.standard.string(forKey: "invitationID")
            let eventID = UserDefaults.standard.string(forKey: "eventID")
            let userID = UserDefaults.standard.integer(forKey: "userID")
        
        apiCallingMethod(apiEndPints: "guests", method: .get, parameters: ["invitationID":invitationID!,"guestID":userID,"chat":1]) { (data1:Any?, count:Int?) in
           
            if let data = data1 as? [[String : Any]]
            {
                var guests = MInvitdGuest.guestsFromResults(data as [[String : AnyObject]])
                
                let userID = UserDefaults.standard.integer(forKey: "userID")
                var i = 0
                for guest in guests {
                    
                    if guest.userID == userID{
                        guests.remove(at: i)
                    }
                    i += 1
                }
                //print("this is data ->\(data)")
                self.guests = guests
                
                //print("Value of guest - time Api get call^^^^^^^^-\(guests)")
                
                DispatchQueue.main.async {
                    self.navigationItem.rightBarButtonItem?.isEnabled = true
                    self.guestsCollectionView.reloadData()
                }
            }
        }
    }
    
    
    @objc public func imageLayerForGradientBackgroundSmall() -> UIImage {
        
        let updatedFrame = CGRect(x: 0, y: 0, width: 40, height: 40)
        // take into account the status bar
        let layer = CAGradientLayer.gradientLayerForBounds(bounds: updatedFrame)
        UIGraphicsBeginImageContext(layer.bounds.size)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}

// MARK: - GuestsViewController: UICollectionViewDelegate, UICollectionViewDataSource

extension GuestsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(60)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let guest = isSearching ? filterData[indexPath.row]:  guests[indexPath.row]
        let cell = self.guestsCollectionView.dequeueReusableCell(withIdentifier: "guestListViewCell", for: indexPath) as! ChatGuestsCollectionViewCell
        
        cell.nameLabel.text = guest.name
        var firstLetter : String?
        if let firstChar = guest.name?.characters.first {
            let firstString = String(describing: firstChar)
            cell.imageLabel.text = firstString
        }else {
            cell.imageLabel.text = "?"
        }
        
        cell.guestImageView.layer.cornerRadius = cell.guestImageView.frame.size.width / 2
        cell.guestImageView.layer.cornerRadius = cell.guestImageView.frame.size.width / 2
        
        if let imgurl = guest.imageUrl{
            cell.imageLabel.isHidden = true
            cell.guestImageView.isHidden = false
            self.downloadImageFrom(urlString: imgurl) { (data) in
                guard let image: UIImage = UIImage(data: data!)else{return}
                DispatchQueue.main.async {
                    cell.guestImageView.image = image
                }
            }
        }else{
            cell.imageLabel.isHidden = false
            cell.guestImageView.isHidden = true
            cell.imageLabel.text = firstLetter
            cell.imageLabel.layer.cornerRadius  = cell.imageLabel.frame.size.width/2
            cell.imageLabel.layer.masksToBounds = true
            cell.imageLabel.clipsToBounds = true
        }
        
     //   cell.emailLabel.text = guest.email
        
        if guest.userID == 0{
            //(guest.firebaseId ?? "").isEmpty || guest.firebaseId == ""
            //Fade UI
            cell.imageLabel.alpha = 0.5
            cell.nameLabel.alpha = 0.5
        //    cell.emailLabel.alpha = 0.5
            cell.guestImageView.alpha = 0.5
        }else{
            cell.imageLabel.alpha = 1.0
            cell.nameLabel.alpha = 1.0
      //      cell.emailLabel.alpha = 1.0
            cell.guestImageView.alpha = 1.0
        }
        
        let bgImage : UIImage = self.imageLayerForGradientBackgroundSmall()
        cell.imageLabel.backgroundColor = UIColor(patternImage: bgImage)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isSearching ? filterData.count : guests.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let guest = isSearching ? filterData[indexPath.row]: guests[indexPath.row]
        
        if guest.userID == 0{
            // Dont do anything
        }else{
            
            //  let guest = guests[(indexPath as NSIndexPath).row]
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Chat") as! UINavigationController
            
            var params:Parameters?
            //var users = [[String: Any]]()
            
            // setting up data here Don't try to mess here
            guard let eventID = UserDefaults.standard.string(forKey: "invitationID") else {return}
            let ref  = Database.database().reference().child("chatrooms").child(eventID)
            let messageRef = ref.childByAutoId()
            
            
            let myfirebaseID = Auth.auth().currentUser?.uid ?? UserDefaults.standard.string(forKey: "firebaseID")
           // print("myfirebaseID",myfirebaseID)
            
            let urlStr = MInvitdClient.Constants.BaseUrl + "chat/members"
            
            guard let guestUserID = guest.userID else {return}
            guard let guestName = guest.name else {return}
            guard let guestFileID = guest.fileID else {return}
            guard let img2 = guest.imageUrl else {return}
            let myName = UserDefaults.standard.string(forKey: "name")!
            //let myImage = UserDefaults.standard.string(forKey: "userAvatar")!
            let myImage = UserDefaults.standard.string(forKey: "userAvatar") ??  "https://minvitd.com/assets/images/user.jpg"
            let myUserID = UserDefaults.standard.string(forKey: "userID")!
            let iName = UserDefaults.standard.string(forKey: "invitationTitle")!
            let fileID = UserDefaults.standard.string(forKey: "fileID")!
            
            var roomID = messageRef.key as String
            
            guard let chatID = guest.chatID else {return}
            //print(guest)
            if chatID != ""{
                roomID = chatID
                //print("chatID")
            }
            //print("this is the roomID\(roomID)")
            //print("this is the chatID\(chatID)")
            
            //Single Chat Parameters
            let mem1 = "{\"userID\":\"\(myUserID)\",\"name\":\"\(myName)\",\"fileID\":\"\(fileID)\",\"fileUrl\":\"\(myImage)\"}"
            
            
            let mem2 = "{\"userID\":\"\(guestUserID)\",\"name\":\"\(guestName)\",\"fileID\":\"\(guestFileID)\",\"fileUrl\":\"\(img2)\"}"
                       
            params = ["invitationID":eventID,
                      "chatID" : roomID,
                      "guestID": myUserID,
                      "users": [mem1, mem2],
                      "iName": iName
                      ]
            //print("the params > ",params)
            
            let authHeader = ["Authorization" : UserDefaults.standard.string(forKey: "userToken")!, "Content-Type" : "application/json" ]
            
            Alamofire.request(urlStr, method: .post, parameters: params, encoding: JSONEncoding.default, headers: authHeader).responseJSON { (response) in
                    if let dataZ = response.data {
                        
                        //print("^^^^^^^^^^^^^^^^^^\(response)")
                        //debugPrint(response)
                        
                        do {
                            let data1 = try JSONSerialization.jsonObject(with: dataZ, options: JSONSerialization.ReadingOptions.allowFragments)
                            if let dict = data1 as? [String:Any]{
                                
                                if let data12 = dict["data"] as? [[String:AnyObject]]{
                                    
                                    //print("data12 from response:\(data12)")
                                    
                                    let mychat = MInvitdchatList.resultFromServerData(data1: data12)
                                    let a = vc.viewControllers[0] as? ChatVC
                                    
                                    //print("this is the value\(mychat)")
                                    
                                    if mychat.count == 2{
//                                            print("000000000000")
//                                            print(mychat[0])
//                                            print("000000000000")
//                                            print(mychat[1])
                                        
                                            a?.chatlist = mychat[1]
                                            self.present(vc, animated: true, completion: nil)
                                       
                                        
                                    }else{
                                        a?.chatlist = mychat[0]
                                        //print("mychat is :\(mychat)")
                                        self.present(vc, animated: true, completion: nil)
                                    }
                                }
                            }
                            
                        }catch {
                            
                        }
                    }
            }
        }
    }
    
    func downloadImageFrom(urlString:String, completion:@escaping(Data?)->()) {
        guard let url = URL(string:urlString) else { return }
        let request = URLRequest(url: url)
        URLSession.shared.dataTask(with: request) { (data, _, err) in
            if err != nil {
                // handle error if any
            }
            if let data = data{
                completion(data)
            }
            
            }.resume()
    }
    
}

class chatMember {
    var name = ""
    var fileID = 0
    var fileUrl = ""
    var userID = 0
}

extension GuestsViewController:UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
        // tableView.reloadData()
        self.guestsCollectionView.setContentOffset(CGPoint(x: 0, y: 44), animated: true)
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
        isSearching = false
        guestsCollectionView.reloadData()
        self.guestsCollectionView.setContentOffset(CGPoint(x: 0, y: 44), animated: true)
        
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        //reloading here
        if searchBar.text == nil || searchBar.text == ""{
            isSearching = false
            searchBar.setShowsCancelButton(false, animated: true)
            view.endEditing(true)
            guestsCollectionView.reloadData()
            self.guestsCollectionView.setContentOffset(CGPoint(x: 0, y: 44), animated: true)
            
            return
        }else {
            isSearching = true
            filterData = guests.filter({
                return ($0.name?.lowercased().contains(searchText.lowercased()))!})
            guestsCollectionView.reloadData()
            
        }
        
    }
}
