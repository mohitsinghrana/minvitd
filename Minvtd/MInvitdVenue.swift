//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import Foundation

struct MInvitdVenue{
    
    // MARK: Properties
    
    let venueID : Int?
    let eventID : Int?
    let name : String?
    let email : String?
    let phone : String?
    let address : String?
    let coordinates : String?
    let status : String?
    let created : String?
    let updated : String?
    let eventDate : String?
    let eventName  : String?
    let contactPerson : String?
    var gallery: [MInvitdVenueGallery]?

    // MARK: Initializers
    
    // construct a dictionary
    init(dictionary: [String:AnyObject]) {
        venueID = dictionary[MInvitdClient.JSONResponseKeys.venueID] as? Int
        eventID = dictionary[MInvitdClient.JSONResponseKeys.eventID] as? Int
        name = dictionary[MInvitdClient.JSONResponseKeys.name] as? String
        address = dictionary[MInvitdClient.JSONResponseKeys.address] as? String
        email = dictionary[MInvitdClient.JSONResponseKeys.email] as? String
        phone = dictionary[MInvitdClient.JSONResponseKeys.phone] as? String
        coordinates = dictionary[MInvitdClient.JSONResponseKeys.coordinates] as? String
        status = dictionary[MInvitdClient.JSONResponseKeys.status] as? String
        created = dictionary[MInvitdClient.JSONResponseKeys.created] as? String
        updated = dictionary[MInvitdClient.JSONResponseKeys.updated] as? String
        eventName = dictionary[MInvitdClient.JSONResponseKeys.eventName] as? String
        eventDate = dictionary[MInvitdClient.JSONResponseKeys.eventDate] as? String
        contactPerson = dictionary[MInvitdClient.JSONResponseKeys.contactPerson] as? String
        if let gallery = dictionary["gallery"] as? [AnyObject], gallery.isEmpty == false {
            var temp1 = [MInvitdVenueGallery]()
            for i in 0...gallery.count-1 {
                let temp = MInvitdVenueGallery(dictionary: gallery[i] as! [String : AnyObject])
                temp1.append(temp)
            }
            self.gallery = temp1
        } else {
            self.gallery = nil
        }
    }
    
    static func venuesFromResults(_ results: [[String:AnyObject]]) -> [MInvitdVenue] {
        var venues = [MInvitdVenue]()
        
        // iterate through array of dictionaries, each Article is a dictionary
        for venue in results {
            venues.append(MInvitdVenue(dictionary: venue))
        }
        
        return venues
    }
}
