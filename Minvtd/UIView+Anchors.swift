//
//  UIView+Anchors.swift


import UIKit

extension Double {
    
    func socialTime()->String {
        
        let now = Date().timeIntervalSince1970
        _ = self
        let interval = Int(now - self)
        let minute = 60
        let hour = minute*60
        let day = hour*24
        let week = day*7
        
        if interval < minute {
            
            return "\(interval) second ago"
        }
            
        else if interval < hour {
            
            return "\(interval/minute) minute ago"
        }
            
        else if interval < day {
            
            return "\(interval/hour) hour ago"
        }
            
        else if interval < week {
            
            return "\(interval/day) day ago"
        }
            
        else {
            return "\(interval/week) week ago"
            
        }
        
    }
    
}

extension UIView {
    public func addConstraintsWithFormat(_ format: String, views: UIView...) {
        
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            viewsDictionary[key] = view
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))
    }
    
    @objc public func fillSuperview() {
        translatesAutoresizingMaskIntoConstraints = false
        if let superview = superview {
            leftAnchor.constraint(equalTo: superview.leftAnchor).isActive = true
            rightAnchor.constraint(equalTo: superview.rightAnchor).isActive = true
            topAnchor.constraint(equalTo: superview.topAnchor).isActive = true
            bottomAnchor.constraint(equalTo: superview.bottomAnchor).isActive = true
        }
    }
    
    @objc public func anchor(_ top: NSLayoutYAxisAnchor? = nil, left: NSLayoutXAxisAnchor? = nil, bottom: NSLayoutYAxisAnchor? = nil, right: NSLayoutXAxisAnchor? = nil, topConstant: CGFloat = 0, leftConstant: CGFloat = 0, bottomConstant: CGFloat = 0, rightConstant: CGFloat = 0, widthConstant: CGFloat = 0, heightConstant: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        
        _ = anchorWithReturnAnchors(top, left: left, bottom: bottom, right: right, topConstant: topConstant, leftConstant: leftConstant, bottomConstant: bottomConstant, rightConstant: rightConstant, widthConstant: widthConstant, heightConstant: heightConstant)
    }
    
    @objc public func anchorWithReturnAnchors(_ top: NSLayoutYAxisAnchor? = nil, left: NSLayoutXAxisAnchor? = nil, bottom: NSLayoutYAxisAnchor? = nil, right: NSLayoutXAxisAnchor? = nil, topConstant: CGFloat = 0, leftConstant: CGFloat = 0, bottomConstant: CGFloat = 0, rightConstant: CGFloat = 0, widthConstant: CGFloat = 0, heightConstant: CGFloat = 0) -> [NSLayoutConstraint] {
        translatesAutoresizingMaskIntoConstraints = false
        
        var anchors = [NSLayoutConstraint]()
        
        if let top = top {
            anchors.append(topAnchor.constraint(equalTo: top, constant: topConstant))
        }
        
        if let left = left {
            anchors.append(leftAnchor.constraint(equalTo: left, constant: leftConstant))
        }
        
        if let bottom = bottom {
            anchors.append(bottomAnchor.constraint(equalTo: bottom, constant: -bottomConstant))
        }
        
        if let right = right {
            anchors.append(rightAnchor.constraint(equalTo: right, constant: -rightConstant))
        }
        
        if widthConstant > 0 {
            anchors.append(widthAnchor.constraint(equalToConstant: widthConstant))
        }
        
        if heightConstant > 0 {
            anchors.append(heightAnchor.constraint(equalToConstant: heightConstant))
        }
        
        anchors.forEach({$0.isActive = true})
        
        return anchors
    }
    
    @objc public func anchorCenterXToSuperview(constant: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        if let anchor = superview?.centerXAnchor {
            centerXAnchor.constraint(equalTo: anchor, constant: constant).isActive = true
        }
    }
    
    @objc public func anchorCenterYToSuperview(constant: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        if let anchor = superview?.centerYAnchor {
            centerYAnchor.constraint(equalTo: anchor, constant: constant).isActive = true
        }
    }
    
    @objc public func anchorCenterSuperview() {
        anchorCenterXToSuperview()
        anchorCenterYToSuperview()
    }
    
    @objc public func anchorCornerSuperview() {
        
        if UIScreen.main.bounds.width == 375{
            anchorCenterXToSuperview(constant: 140)
            anchorCenterYToSuperview(constant: -60 )
        }else{
            anchorCenterXToSuperview(constant: 100)
            anchorCenterYToSuperview(constant: -60 )
        }
    }
    
    @objc public func commentNumberHeight() {
        
            anchorCenterXToSuperview(constant: -16)
            anchorCenterYToSuperview(constant: -10 )
    }
    
    @objc public func feedCommentNumberHeight() {
        
        anchorCenterXToSuperview(constant: -30)
        anchorCenterYToSuperview(constant: -10 )
    }
    
    @objc public func feedCommentCVsNumberHeight() {
        
        anchorCenterXToSuperview(constant: -63)
        anchorCenterYToSuperview(constant: -10 )
    }
    
    @objc public func feedphotosNumberHeight() {
        
        anchorCenterXToSuperview(constant: -28)
        anchorCenterYToSuperview(constant: -10 )
    }
    
    @objc public func feedvideosNumberHeight() {
        
        anchorCenterXToSuperview(constant: -20)
        anchorCenterYToSuperview(constant: -10 )
    }
}
