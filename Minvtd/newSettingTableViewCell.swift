//
//  newSettingTableViewCell.swift
//  Minvtd
//
//  Created by admin on 08/09/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit

class newSettingTableViewCell: UITableViewCell {
    
//    @objc let contextImageView = { () -> UIImageView in
//        let imd = UIImageView()
//        return imd
//    }()
    
    
    @objc var contextText = { () -> UILabel in
        let imd = UILabel()
        imd.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.semibold)
        return imd
    }()


    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
        selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.contentView.backgroundColor  = UIColor.init(hexString: "E3E3E3")


        // Configure the view for the selected state
    }

    
    
    @objc func setup() {
        self.contentView.backgroundColor  = UIColor.init(hexString: "E3E3E3")
        self.contentView.addSubview(contextText)
       // self.contentView.addSubview(contextImageView)

        
//        contextImageView.anchor(nil, left: leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 20.0, bottomConstant: 0, rightConstant: 0, widthConstant: 40.0, heightConstant: 40.0)
//        contextImageView.anchorCenterYToSuperview()
//
        contextText.anchor(nil, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 40.0, bottomConstant: 0, rightConstant: 10.0, widthConstant: 0, heightConstant: 0)
        contextText.anchorCenterYToSuperview()
    }
}
