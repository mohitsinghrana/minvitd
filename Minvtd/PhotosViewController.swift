
//  Copyright © 2017 Euroinfotek. All rights reserved.



import UIKit
import Alamofire
import AlamofireImage
import IDMPhotoBrowser

class PhotosViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var photos: [MInvtdAllFeed] = [MInvtdAllFeed]()
    @objc let userName = UserDefaults.standard.string(forKey: "userID")
    @objc let placeholderImage = UIImage(named: "eventCoverImage")!
    
    var currentIndexPath: IndexPath?

    //@IBOutlet weak var navigationBar: UINavigationBar!
    fileprivate let sectionInsets = UIEdgeInsets(top: 5.0, left: 10.0, bottom: 5.0, right: 1.0)
    fileprivate let itemsPerRow: CGFloat = 2
    
    //photoGallery
    var galleryPhotos : [IDMPhoto] = [IDMPhoto]()
    var browser:IDMPhotoBrowser? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        navigationBarSetup(nil)
        title = "Photos"
        let button2 = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissVC))
        self.navigationItem.rightBarButtonItem = button2
    }
    
    @objc func dismissVC(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        imagesLoad()
    }
    
    //due to the point passed in possibly not containing a cell IndexPath is an optional
    
    func indexPathForCellAtPoint(_ point : CGPoint) -> IndexPath? {
        return collectionView?.indexPathForItem(at: self.view.convert(point, to: collectionView!))
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let topLeftCell = CGPoint(x: 1, y: 44)
        if let indexPath = indexPathForCellAtPoint(topLeftCell) {
            print(indexPath.section,indexPath.row)
        }
    }
    
    
    @objc func imagesLoad(){
        
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")!
        let userID = UserDefaults.standard.string(forKey: "userID")!
        
        apiCallingMethod(apiEndPints: "invitation/files", method: .get, parameters: ["invitationID":invitationID,"fileType":"image","userID":userID]) { (data1:Any?, totalobj:Int?) in
            
            if data1 != nil {
                
                if let data = data1 as? [[String : Any]]
                {
                    let photos1 = MInvtdAllFeed.allFeedsFromResults(data as [[String : AnyObject]])
                    self.photos = photos1
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }
            }
        }
    }
}

extension PhotosViewController : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace/2
        let widthPerItem = availableWidth / itemsPerRow
        let height = widthPerItem + CGFloat(50)
        //30 before
        
        return CGSize(width: widthPerItem, height: height)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let photo = photos[(indexPath as NSIndexPath).row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photoCell", for: indexPath) as! PhotosCollectionViewCell
        
        self.currentIndexPath = indexPath
        
        print("\(cell.frame.size.height)")
        
        if let imgUrl = photo.fileUrl {
            cell.image.af_setImage(withURL: URL(string:imgUrl )! ,placeholderImage: placeholderImage)
        }
        
        cell.LikeImage.image = nil
        
        if let liked = photo.liked{
            cell.LikeImage.image = liked ?  UIImage(named: "likeFilled") : UIImage(named: "like")
        }
        
        
        cell.profileImage.af_setImage(withURL: URL(string: photo.userImageUrl!)!)
        
        cell.title.text =  photo.comment
        
        //Shadow Effect
        cell.contentView.layer.cornerRadius = 4.0
        cell.contentView.layer.borderWidth = 1.0
        cell.contentView.layer.borderColor = UIColor.clear.cgColor
        cell.contentView.layer.masksToBounds = false
        cell.layer.shadowColor = UIColor(red: 211/255, green: 211/255, blue: 211/255, alpha: 1.0).cgColor
        cell.layer.shadowOffset = CGSize(width: 4, height: 8)
        cell.layer.shadowRadius = 4.0
        cell.layer.shadowOpacity = 1.0
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: (cell.contentView.layer.cornerRadius)).cgPath
        
        //commentTapped
        let commentTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.commentTap(_:)))
        commentTapGesture.numberOfTapsRequired = 1
        cell.commentBtnView.tag = indexPath.row
        cell.commentBtnView.isUserInteractionEnabled = true
        cell.commentBtnView.addGestureRecognizer(commentTapGesture)
        cell.commentButton.isUserInteractionEnabled = false
        
        //imageTapped
        let imageTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(_:)))
        cell.image.tag = indexPath.row
        cell.image.isUserInteractionEnabled = true
        cell.image.addGestureRecognizer(imageTapGesture)
        cell.image.contentMode = .scaleAspectFill
        cell.image.clipsToBounds = true
        
        //likeTapped
        let likeTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.likeTapped(_:)))
        cell.LikeImage.tag = indexPath.row
        cell.LikeImage.isUserInteractionEnabled = true
        cell.LikeImage.addGestureRecognizer(likeTapGesture)
        cell.LikeImage.contentMode = .scaleAspectFill
        cell.LikeImage.clipsToBounds = true
        
        
        //like Label
        if let likeCount = photo.likes{
            cell.likeLabel.text = "\(likeCount)"
        }
        
        if let commentCount = photo.subCommentsCount{
            cell.commentLabel.text = "\(commentCount)"
        }
        
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    @objc func likeTapped(_ sender: UITapGestureRecognizer){
        
        let currentIndex = sender.view?.tag
        var feed  = self.photos[currentIndex!]
        
        let cell = self.collectionView.cellForItem(at: IndexPath(row: currentIndex!, section: 0)) as! PhotosCollectionViewCell
        let userID = UserDefaults.standard.string(forKey: "userID")
        let userName = UserDefaults.standard.string(forKey: "name")!
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")!
        let iName = UserDefaults.standard.string(forKey: "invitationTitle")
        
        let commentID = feed.commentID
        let postID = feed.postID
        
        let parameters: Parameters = ["commentID" : commentID as AnyObject, "userID" : userID as AnyObject,"userName":userName,"invitationID":invitationID , "postID" : postID!,"iName": iName!]
        
        print("\(parameters)")
        
        if  self.photos[currentIndex!].liked == true {
            self.photos[currentIndex!].likes! -= 1
            self.photos[currentIndex!].liked = false
            cell.LikeImage.image = UIImage(named: "like")
        }
        else {
            self.photos[currentIndex!].likes! += 1
            self.photos[currentIndex!].liked = true
            cell.LikeImage.image = UIImage(named: "likeFilled")
        }
        
        cell.likeLabel.text = "\(self.photos[currentIndex!].likes!)"
        
      let url = MInvitdClient.Constants.BaseUrl + "like"
    
      let authHeader = ["Authorization" : UserDefaults.standard.string(forKey: "userToken")!, "Content-Type" : "application/json" ]
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: authHeader).responseJSON { (response) in
            
       
            if response.data != nil{
                // do nothing here
                print("response->",response)
            }
            else{
                
                let alert = UIAlertController(title: "Couldn't connect to server", message: "Check Internet", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: { (action : UIAlertAction) in
                    alert.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    @objc func commentTap(_ sender: UITapGestureRecognizer) {
        
        let currentIndex = sender.view?.tag
        let feed  = self.photos[currentIndex!]
        if feed.fileType == "text"{
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let mainVC = storyBoard.instantiateViewController(withIdentifier: "feedCommentVC") as! FeedCommentVC
            mainVC.paramaters = feed
            mainVC.origin = "feed"
            self.present( UINavigationController(rootViewController:mainVC), animated: true, completion: nil)

        }else {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let mainVC = storyBoard.instantiateViewController(withIdentifier: "feedImageCommentVC") as! FeedImageCommentVC
            mainVC.paramaters = feed
            mainVC.origin = "feed"
            self.present( UINavigationController(rootViewController:mainVC), animated: true, completion: nil)
        }
    }
    
    @objc func imageTapped(_ sender: UITapGestureRecognizer){
        
        let currentIndex = sender.view?.tag
        let feed = self.photos
        
        print("This is feed object's data before opening photo\(feed)")
        
        print("This is feed's file type: \(feed)")
        
        if feed != nil{
            
            galleryPhotos = [IDMPhotoCustom]()
            
            var counter  = 1
            let total = feed.count
            print("\(total)")
            
            for photo in feed {
                let currentPhoto = IDMPhotoCustom(counter : "\(counter) of \(String(describing: total))" , imageUrl: URL(string : photo.fileUrl!)!)
                galleryPhotos.append(currentPhoto)
                counter  = counter + 1
            }
            
            browser = IDMPhotoBrowser.init(photos: galleryPhotos)
            browser?.displayCounterLabel = true
            browser?.displayActionButton = false
            browser?.doneButtonRightInset = CGFloat(5.0)
            browser?.doneButtonTopInset = CGFloat(30.0)
            browser?.doneButtonImage = UIImage(named: "cross")
            browser?.setInitialPageIndex(UInt(currentIndex!))
            present(browser!, animated: true, completion: nil)
            
        }
    }
}





