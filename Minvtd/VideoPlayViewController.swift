//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//


import UIKit

class VideoPlayViewController :  UIViewController {
    
    @objc var url : String = ""
    @IBOutlet weak var webView: UIWebView!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        navigationBarSetup(nil)
        let myURL = URL(string: self.url)
        let myRequest = URLRequest(url: myURL!)
        webView.loadRequest(myRequest)
        title = "Video"
        let button2 = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissVC))
        self.navigationItem.rightBarButtonItem = button2
        
        
    }
    
    
    @objc func dismissVC(_ sender: UIBarButtonItem) {
        
        self.dismiss(animated: true, completion: nil)
    }


}
