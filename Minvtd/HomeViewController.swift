//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import Alamofire
import AlamofireImage
import UIKit

class HomeViewController : UIViewController{
    
    var events : [MInvitdEvent] = [MInvitdEvent]()
    var venus:[Venue] = []

    @objc let placeholderImage = UIImage(named: "eventCoverImage")!
    
 //   @objc let objectID : String = ""
    @objc var currentIndex : Int = 0
    @objc var eventID : Int = 0
    @objc var commentID : Int = 0
    @objc var location :  String = ""
    @objc let layerGradient = CAGradientLayer()
    
    var allFeeds: [MInvtdAllFeed] = [MInvtdAllFeed]()
    var allSubComments: [MInvtdAllFeedSubComment]? = [MInvtdAllFeedSubComment]()
    var paramaters : MInvtdAllFeed?
    @objc var origin : String = "feed"
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var bgImage: UIImageView!
    
    @IBAction func menuButton(_ sender: UIBarButtonItem) {
        slideMenuController()?.openLeft()
    }
    
    @IBAction func viewEventGuests(_ sender: UIBarButtonItem) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "eventGuestsVC") as! EventsGuestViewController
        mainVC.ownerID = self.eventID
        self.present(UINavigationController(rootViewController:mainVC), animated: true, completion: nil)
    }
    
    @IBOutlet weak var myCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myCollectionView.delegate = self
        myCollectionView.dataSource = self
        
        
        self.bgImage.image = UIImage(named: "blurbg")

        self.tabBarItem.selectedImage = UIImage(named: "eventsActive")?.withRenderingMode(.alwaysOriginal)
        self.tabBarItem.image = UIImage(named: "eventsInactive")?.withRenderingMode(.alwaysOriginal)
        
        navigationBarSetup(nil)

        let colour1 = UserDefaults.standard.string(forKey: "colour1")
        let colour2 = UserDefaults.standard.string(forKey: "colour2")
        
        layerGradient.colors = [UIColor.init(hexString: colour1!)?.cgColor, UIColor.init(hexString: colour2!)?.cgColor]
        layerGradient.startPoint = CGPoint(x: 0, y: 0.5)
        layerGradient.endPoint = CGPoint(x: 1, y: 0.5)
        layerGradient.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height)
        self.tabBarController?.tabBar.layer.insertSublayer(layerGradient, at: 0)
        self.tabBarController?.tabBar.barTintColor  = UIColor.white
   
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getEvents()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func getEvents(){
        
            let userID = UserDefaults.standard.string(forKey: "userID")!
        //print(userID)
        let userToken = UserDefaults.standard.string(forKey: "userToken")!
        //print("the token is -> \(userToken)")
            let invitationID = UserDefaults.standard.string(forKey: "invitationID")!
        
        
            apiCallingMethod(apiEndPints: "invitation", method: .get, parameters: ["userID":userID,"invitationID":invitationID]) { (data1:Any?, totalObjects:Int?) in
                
                //print(data1)
                
                if totalObjects == 0
                {
                    //print("this is called")
                    self.CreateAlertPopUp("Alert", "You have no events", OnTap: nil)
                    return
                }

                if let data  = data1 as? [[String:Any]]  {
                    //print("this is called")
                    let mysettings = data[0]
                    self.events = MInvitdEvent.eventsFromResults(mysettings["events"] as! [[String : AnyObject]])
                    
                        if let venues = mysettings["venues"] as? [[String : AnyObject]] {
                            self.venus = Venue.venueFromResults(venues)
                        }
                    
                    DispatchQueue.main.async {
                        
                        self.eventID = self.events[self.currentIndex].eventID!
                        UserDefaults.standard.set(self.eventID, forKey: "eventID")
                        UserDefaults.standard.set(self.events[self.currentIndex].name, forKey: "invitationName")
                        self.commentID = self.events[self.currentIndex].commentID!
                        self.location = self.events[self.currentIndex].coordinates!
                        self.navigationItem.title = self.events[self.currentIndex].name
                        self.pageControl.numberOfPages = self.events.count

                        self.myCollectionView.reloadData()
                    }

                    }
                }
        

    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let center = CGPoint(x: scrollView.contentOffset.x + (scrollView.frame.width / 2), y: (scrollView.frame.height / 2))
        if let ip = self.myCollectionView.indexPathForItem(at: center) {
            self.currentIndex = ip.row
            self.eventID = events[self.currentIndex].eventID!
            UserDefaults.standard.set(self.eventID, forKey: "eventID")
            self.commentID = events[self.currentIndex].commentID!
            self.location = events[self.currentIndex].coordinates!
            self.pageControl.currentPage =  self.currentIndex
            let stringEventName = (events[self.currentIndex].name)?.replacingOccurrences(of: "amp;", with: "")
            self.navigationItem.title = stringEventName
        }
    }

}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let bounds = collectionView.bounds
        let width = bounds.width
        let height = bounds.height
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let event = events[(indexPath as NSIndexPath).row]
        let imageCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageEventCell", for: indexPath) as! ImageEventCollectionViewCell
        let imageOnlyCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageOnlyEventCell", for: indexPath) as! ImageOnlyCollectionViewCell
        let videoCell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoEventCell", for: indexPath) as! VideoEventCollectionViewCell
        let webCell = collectionView.dequeueReusableCell(withReuseIdentifier: "WebEventCell", for: indexPath) as! WebCollectionViewCell
        /* Get cell type */
  
        
        if event.category == "image"
        {
            if event.fileUrl != "" || event.fileUrl != nil
            {
                imageCell.image.af_setImage(
                    withURL: URL(string: event.fileUrl!)! ,
                    placeholderImage: placeholderImage
                )
            }
            else{
                imageCell.image.image = placeholderImage
            }
            
            //circle setup;
            
            imageCell.circleImage.layer.cornerRadius = imageCell.circleImage.frame.width / 2;
            imageCell.circleImage.layer.borderWidth = 2
            imageCell.circleImage.layer.borderColor = UIColor.gray.cgColor
            imageCell.circleImage.clipsToBounds = true
            
            if event.personThumbnail != "" || event.personThumbnail != nil
            {
                imageCell.circleImage.af_setImage(
                    withURL: URL(string: event.personThumbnail!)! ,
                    placeholderImage: placeholderImage
                )
            }
                
            else{
                imageCell.circleImage.image = placeholderImage
            }
            
            imageCell.circleVenue.text = event.venue
            imageCell.circleEventName.text = event.name
            
            //end
            
            let stringEventDescription = (event.description)?.replacingOccurrences(of: "amp;", with: "")
            imageCell.descript.text = stringEventDescription
            let stringEventName = (event.name)?.replacingOccurrences(of: "amp;", with: "")
            imageCell.eventName.text = stringEventName
            imageCell.venue.text = event.venue
            imageCell.date.text = event.date
            imageCell.time.text = event.time
            imageCell.commentLabel.text = "\(event.commentsCount!)"
            imageCell.likedLabel.text = "\(event.likesCount!)"
            
            
            imageCell.responseLabel.text  =  event.guestStatusName

            if event.liked! == true{
                
                var image = UIImage(named: "likeFilled")
                image = image?.withRenderingMode(.alwaysOriginal)
                let colour1 = UserDefaults.standard.string(forKey: "colour1")
                DispatchQueue.main.async {
                    
                    imageCell.heartBtn.setImage(image, for: .normal)
                    //imageCell.heartBtn.tintColor = UIColor(hexString: colour1!)
                }
               
            }else{
                var image = UIImage(named: "like")
                image = image?.withRenderingMode(.alwaysOriginal)
                let colour1 = UserDefaults.standard.string(forKey: "colour1")
                
                DispatchQueue.main.async {
                    
                    imageCell.heartBtn.setImage(image, for: .normal)
                    //imageCell.heartBtn.tintColor = UIColor(hexString: colour1!)
                }
            }
            
            
            // Remove all existing targets (in case cell is being recycled)
            imageCell.heartBtn.removeTarget(nil, action: nil, for: .allEvents)
            //imageCell.statusChangeBtn.removeTarget(nil, action: nil, for: .allEvents)
            imageCell.changeButton.removeTarget(nil, action: nil, for: .allEvents)

            // Add target
            imageCell.heartBtn.addTarget(self, action: #selector(likeBtnTap(sender:)), for: .touchUpInside)
            //imageCell.statusChangeBtn.addTarget(self, action: #selector(statusChange(sender:)), for: .touchUpInside)
            imageCell.changeButton.addTarget(self, action: #selector(statusChange(sender:)), for: .touchUpInside)

            let commentTap = UITapGestureRecognizer(target: self, action: #selector(self.commentBtnTap(_:)))
            commentTap.numberOfTapsRequired = 1
            imageCell.commentBtn.isUserInteractionEnabled = true
            imageCell.commentBtn.addGestureRecognizer(commentTap)
            
            let scheduleTap = UITapGestureRecognizer(target: self, action: #selector(self.scheduleBtnTap(_:)))
            scheduleTap.numberOfTapsRequired = 1
            imageCell.scheduleBtn.isUserInteractionEnabled = true
            imageCell.scheduleBtn.addGestureRecognizer(scheduleTap)
            
            let locationTap = UITapGestureRecognizer(target: self, action: #selector(self.venueTapDetected(_:)))
            locationTap.numberOfTapsRequired = 1
            imageCell.locationBtn.isUserInteractionEnabled = true
            imageCell.locationBtn.addGestureRecognizer(locationTap)


            
            if event.guestStatus == 0 {
                imageCell.statusButton.backgroundColor = UIColor.init(hexString: "#F18037")
            }else if event.guestStatus == 1 {
                imageCell.statusButton.backgroundColor = UIColor.init(hexString: "#61BCA7")
                
            }else if event.guestStatus == 2{
                imageCell.statusButton.backgroundColor = UIColor.init(hexString: "#DA4C45")
            }else{
                imageCell.statusButton.backgroundColor = UIColor.init(hexString: "#2C7EC3")
            }
            
            imageCell.statusButton.layer.cornerRadius = imageCell.statusButton.bounds.size.width/2
            
            

            
            return imageCell
        }
        else if event.category == "image_text"
        {
            if event.fileUrl != "" || event.fileUrl != nil
            {
                imageOnlyCell.image.af_setImage(
                    withURL: URL(string: event.fileUrl!)! ,
                    placeholderImage: placeholderImage
                )
                
            }
            else{
                imageOnlyCell.image.image = placeholderImage
            }
            
            //circle setup;
            
            imageOnlyCell.circleImage.layer.cornerRadius = imageCell.circleImage.frame.width / 2;
            imageOnlyCell.circleImage.layer.borderWidth = 2
            imageOnlyCell.circleImage.layer.borderColor = UIColor.gray.cgColor
            imageOnlyCell.circleImage.clipsToBounds = true
            
            if event.personThumbnail != "" || event.personThumbnail != nil
            {
                imageOnlyCell.circleImage.af_setImage(
                    withURL: URL(string: event.personThumbnail!)! ,
                    placeholderImage: placeholderImage
                )
            }
                
            else{
                imageOnlyCell.circleImage.image = placeholderImage
            }
            
            imageOnlyCell.circleVenue.text = event.venue
            imageOnlyCell.circleEventName.text = event.name
            
            //end
            
            
            imageOnlyCell.image.contentMode = .scaleAspectFill
            imageOnlyCell.image.clipsToBounds = true
            let stringEventDescription = (event.description)?.replacingOccurrences(of: "amp;", with: "")
            imageOnlyCell.descript.text = stringEventDescription
            imageOnlyCell.commentLabel.text = "\(event.commentsCount!)"
            imageOnlyCell.likedLabel.text = "\(event.likesCount!)"
            
            
            if event.liked! == true{
                
                var image = UIImage(named: "likeFilled")
                image = image?.withRenderingMode(.alwaysOriginal)
                let colour1 = UserDefaults.standard.string(forKey: "colour1")
                DispatchQueue.main.async {
                    
                    imageOnlyCell.heartBtn.setImage(image, for: .normal)
                    imageOnlyCell.heartBtn.tintColor = UIColor(hexString: colour1!)
                }
                
            }else{
                var image = UIImage(named: "like")
                image = image?.withRenderingMode(.alwaysOriginal)
                let colour1 = UserDefaults.standard.string(forKey: "colour1")
                
                DispatchQueue.main.async {
                    
                    imageOnlyCell.heartBtn.setImage(image, for: .normal)
                    imageOnlyCell.heartBtn.tintColor = UIColor(hexString: colour1!)
                }
            }
            
            
            let imageTap = UITapGestureRecognizer(target: self, action: #selector(self.imageOnlyCellImageTap(_:)))
            imageTap.numberOfTapsRequired = 1
            imageOnlyCell.image.isUserInteractionEnabled = true
            imageOnlyCell.image.addGestureRecognizer(imageTap)
            
            // Remove all existing targets (in case cell is being recycled)
            imageOnlyCell.heartBtn.removeTarget(nil, action: nil, for: .allEvents)
            imageOnlyCell.statusChangeBtn.removeTarget(nil, action: nil, for: .allEvents)

            // Add target
            imageOnlyCell.heartBtn.addTarget(self, action: #selector(likeBtnTap(sender:)), for: .touchUpInside)
            imageOnlyCell.statusChangeBtn.addTarget(self, action: #selector(statusChange(sender:)), for: .touchUpInside)

            let commentTap = UITapGestureRecognizer(target: self, action: #selector(self.commentBtnTap(_:)))
            commentTap.numberOfTapsRequired = 1
            imageOnlyCell.commentBtn.isUserInteractionEnabled = true
            imageOnlyCell.commentBtn.addGestureRecognizer(commentTap)
            
            let scheduleTap = UITapGestureRecognizer(target: self, action: #selector(self.scheduleBtnTap(_:)))
            scheduleTap.numberOfTapsRequired = 1
            imageOnlyCell.scheduleBtn.isUserInteractionEnabled = true
            imageOnlyCell.scheduleBtn.addGestureRecognizer(scheduleTap)

            let locationTap = UITapGestureRecognizer(target: self, action: #selector(self.venueTapDetected(_:)))
            locationTap.numberOfTapsRequired = 1
            imageOnlyCell.locationBtn.isUserInteractionEnabled = true
            imageOnlyCell.locationBtn.addGestureRecognizer(locationTap)

            if event.guestStatus == 0 {
                imageOnlyCell.statusButton.backgroundColor = UIColor.init(hexString: "#F18037")
            }else if event.guestStatus == 1 {
                imageOnlyCell.statusButton.backgroundColor = UIColor.init(hexString: "#61BCA7")
                
            }else if event.guestStatus == 2{
                imageOnlyCell.statusButton.backgroundColor = UIColor.init(hexString: "#DA4C45")
            }else{
                imageOnlyCell.statusButton.backgroundColor = UIColor.init(hexString: "#2C7EC3")
            }
            imageOnlyCell.statusButton.layer.cornerRadius = imageOnlyCell.statusButton.bounds.size.width/2
            

            
            return imageOnlyCell
        }
        else if  event.category == "video"
        {
            
            //circle setup;
            
            videoCell.circleImage.layer.cornerRadius = imageCell.circleImage.frame.width / 2;
            videoCell.circleImage.layer.borderWidth = 2
            videoCell.circleImage.layer.borderColor = UIColor.gray.cgColor
            videoCell.circleImage.clipsToBounds = true
            
            
            
            if event.personThumbnail != "" || event.personThumbnail != nil
            {
                videoCell.circleImage.af_setImage(
                    withURL: URL(string: event.personThumbnail!)! ,
                    placeholderImage: placeholderImage
                )
            }
                
            else{
                videoCell.circleImage.image = placeholderImage
            }
            
            videoCell.circleVenue.text = event.venue
            videoCell.circleEventName.text = event.name
            
            //end
            
            
           
            let stringEventDescription = (event.description)?.replacingOccurrences(of: "amp;", with: "")
            videoCell.descript.text = stringEventDescription
            let stringEventName = (event.name)?.replacingOccurrences(of: "amp;", with: "")
            videoCell.eventName.text = stringEventName
            videoCell.venue.text = event.venue
            videoCell.date.text = event.date
            videoCell.time.text = event.time

            videoCell.commentLabel.text = "\(event.commentsCount!)"
            videoCell.likedLabel.text = "\(event.likesCount!)"
            
            videoCell.responseLabel.text  =  event.guestStatusName
            
            if event.liked! == true{
                
                var image = UIImage(named: "likeFilled")
                image = image?.withRenderingMode(.alwaysOriginal)
                let colour1 = UserDefaults.standard.string(forKey: "colour1")
                DispatchQueue.main.async {
                    
                    videoCell.heartBtn.setImage(image, for: .normal)
                    videoCell.heartBtn.tintColor = UIColor(hexString: colour1!)
                }
                
            }else{
                var image = UIImage(named: "like")
                image = image?.withRenderingMode(.alwaysOriginal)
                let colour1 = UserDefaults.standard.string(forKey: "colour1")
                
                DispatchQueue.main.async {
                    
                    videoCell.heartBtn.setImage(image, for: .normal)
                    videoCell.heartBtn.tintColor = UIColor(hexString: colour1!)
                }
            }
            
            let myURL = URL(string: event.fileUrl!)
            let myRequest = URLRequest(url: myURL!)
            videoCell.videoView.loadRequest(myRequest)
            
            // Remove all existing targets (in case cell is being recycled)
            videoCell.heartBtn.removeTarget(nil, action: nil, for: .allEvents)
            //videoCell.statusChangeBtn.removeTarget(nil, action: nil, for: .allEvents)
            videoCell.changeButton.removeTarget(nil, action: nil, for: .allEvents)


            // Add target
            videoCell.heartBtn.addTarget(self, action: #selector(likeBtnTap(sender:)), for: .touchUpInside)
            //videoCell.statusChangeBtn.addTarget(self, action: #selector(statusChange(sender:)), for: .touchUpInside)
            videoCell.changeButton.addTarget(self, action: #selector(statusChange(sender:)), for: .touchUpInside)


            let commentTap = UITapGestureRecognizer(target: self, action: #selector(self.commentBtnTap(_:)))
            commentTap.numberOfTapsRequired = 1
            videoCell.commentBtn.isUserInteractionEnabled = true
            videoCell.commentBtn.addGestureRecognizer(commentTap)

            let scheduleTap = UITapGestureRecognizer(target: self, action: #selector(self.scheduleBtnTap(_:)))
            scheduleTap.numberOfTapsRequired = 1
            videoCell.scheduleBtn.isUserInteractionEnabled = true
            videoCell.scheduleBtn.addGestureRecognizer(scheduleTap)
            
            let locationTap = UITapGestureRecognizer(target: self, action: #selector(self.venueTapDetected(_:)))
            locationTap.numberOfTapsRequired = 1
            videoCell.locationBtn.isUserInteractionEnabled = true
            videoCell.locationBtn.addGestureRecognizer(locationTap)
        
            
            if event.guestStatus == 0 {
                videoCell.statusButton.backgroundColor = UIColor.init(hexString: "#F18037")
            }else if event.guestStatus == 1 {
                videoCell.statusButton.backgroundColor = UIColor.init(hexString: "#61BCA7")
                
            }else if event.guestStatus == 2{
                videoCell.statusButton.backgroundColor = UIColor.init(hexString: "#DA4C45")
            }else{
                videoCell.statusButton.backgroundColor = UIColor.init(hexString: "#2C7EC3")
            }
            videoCell.statusButton.layer.cornerRadius = videoCell.statusButton.bounds.size.width/2
            

            return videoCell
        }
        else if event.category == "web"
        {
            
            //circle setup;
            
            webCell.circleImage.layer.cornerRadius = imageCell.circleImage.frame.width / 2;
            webCell.circleImage.layer.borderWidth = 2
            webCell.circleImage.layer.borderColor = UIColor.gray.cgColor
            webCell.circleImage.clipsToBounds = true
            
            if event.personThumbnail != "" || event.personThumbnail != nil
            {
                webCell.circleImage.af_setImage(
                    withURL: URL(string: event.personThumbnail!)! ,
                    placeholderImage: placeholderImage
                )
            }
            else{
                webCell.circleImage.image = placeholderImage
            }
            
            webCell.circleVenue.text = event.venue
            webCell.circleEventName.text = event.name
            
            //end
            
            let stringEventDescription = (event.description)?.replacingOccurrences(of: "amp;", with: "")
            webCell.descript.text = stringEventDescription
            webCell.commentLabel.text = "\(event.commentsCount!)"
            webCell.likedLabel.text = "\(event.likesCount!)"
            
            if event.liked! == true{
                
                var image = UIImage(named: "likeFilled")
                image = image?.withRenderingMode(.alwaysOriginal)
                let colour1 = UserDefaults.standard.string(forKey: "colour1")
                DispatchQueue.main.async {
                    
                    webCell.heartBtn.setImage(image, for: .normal)
                    webCell.heartBtn.tintColor = UIColor(hexString: colour1!)
                }
                
            }else{
                var image = UIImage(named: "like")
                image = image?.withRenderingMode(.alwaysOriginal)
                let colour1 = UserDefaults.standard.string(forKey: "colour1")
                
                DispatchQueue.main.async {
                    
                    webCell.heartBtn.setImage(image, for: .normal)
                    webCell.heartBtn.tintColor = UIColor(hexString: colour1!)
                }
            }
            
            let myURL = URL(string: event.fileUrl!)
            let myRequest = URLRequest(url: myURL!)
            webCell.webView.loadRequest(myRequest)
            
            // Remove all existing targets (in case cell is being recycled)
            webCell.heartBtn.removeTarget(nil, action: nil, for: .allEvents)
            webCell.statusChangeBtn.removeTarget(nil, action: nil, for: .allEvents)

            // Add target
            webCell.heartBtn.addTarget(self, action: #selector(likeBtnTap(sender:)), for: .touchUpInside)
            webCell.statusChangeBtn.addTarget(self, action: #selector(statusChange(sender:)), for: .touchUpInside)

            let commentTap = UITapGestureRecognizer(target: self, action: #selector(self.commentBtnTap(_:)))
            commentTap.numberOfTapsRequired = 1
            webCell.commentBtn.isUserInteractionEnabled = true
            webCell.commentBtn.addGestureRecognizer(commentTap)

            let scheduleTap = UITapGestureRecognizer(target: self, action: #selector(self.scheduleBtnTap(_:)))
            scheduleTap.numberOfTapsRequired = 1
            webCell.scheduleBtn.isUserInteractionEnabled = true
            webCell.scheduleBtn.addGestureRecognizer(scheduleTap)
            
            let locationTap = UITapGestureRecognizer(target: self, action: #selector(self.venueTapDetected(_:)))
            locationTap.numberOfTapsRequired = 1
            webCell.locationBtn.isUserInteractionEnabled = true
            webCell.locationBtn.addGestureRecognizer(locationTap)
            
//            webCell.mainView.layer.cornerRadius = 10
//            webCell.mainView.clipsToBounds = true
            
            if event.guestStatus == 0 {
                webCell.statusButton.backgroundColor = UIColor.init(hexString: "#F18037")
            }else if event.guestStatus == 1 {
                webCell.statusButton.backgroundColor = UIColor.init(hexString: "#61BCA7")
                
            }else if event.guestStatus == 2{
                webCell.statusButton.backgroundColor = UIColor.init(hexString: "#DA4C45")
            }else{
                webCell.statusButton.backgroundColor = UIColor.init(hexString: "#2C7EC3")
            }
            webCell.statusButton.layer.cornerRadius = webCell.statusButton.bounds.size.width/2
            
            return webCell
        }

        else{
            
            return imageCell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return events.count
    }

    
    @objc func likeBtnTap(sender : UIButton){
        
       // let userID = UserDefaults.standard.string(forKey: "userID")
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")
        let userName = UserDefaults.standard.string(forKey: "name")!
        let iName = UserDefaults.standard.string(forKey: "invitationTitle")
        //let userToken = UserDefaults.standard.string(forKey: "userToken")

        let parameters: Parameters = [
            "postID" : self.events[self.currentIndex].commentID as AnyObject,
            "userName":userName,
            "invitationID": invitationID as AnyObject,
            "parentID" : "0",
            "type": self.events[self.currentIndex].type!,
            "likeID": self.events[self.currentIndex].likeID!,
            "iName": iName!,
            "eventID": self.events[self.currentIndex].eventID!
        ]
        
        let authHeader = ["Authorization" : UserDefaults.standard.string(forKey: "userToken")!, "Content-Type" : "application/json" ]
        
        let url = MInvitdClient.Constants.BaseUrl + "like"
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: authHeader).responseJSON { (response) in
            
            if response.data != nil{
                self.getEvents()
            }else{                
                let alert = UIAlertController(title: "Couldn't connect to server", message: "Check Internet", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: { (action : UIAlertAction) in
                    alert.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    @objc func statusChange(sender : UIButton){
        
        let alertController = UIAlertController(title: "Minvitd", message: "Change your response", preferredStyle: .alert)
        
        if self.events[self.currentIndex].guestStatus == 1 {
            
            // Create the actions
            let accept  = UIAlertAction(title: "Accept", style: UIAlertActionStyle.destructive) {
                UIAlertAction in
                self.changeStatus(status: "1")

            }
            let reject = UIAlertAction(title: "Reject", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.changeStatus(status: "2")

            }
            let maybe = UIAlertAction(title: "Maybe", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.changeStatus(status: "3")

            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                // when cancel is tapped
            }
            
            // Add the actions
            alertController.addAction(accept)
            alertController.addAction(reject)
            alertController.addAction(maybe)
            alertController.addAction(cancelAction)

            // Present the controller
            self.present(alertController, animated: true, completion: nil)
            

            
        }else if self.events[self.currentIndex].guestStatus == 2 {
            // Create the actions
            let accept  = UIAlertAction(title: "Accept", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.changeStatus(status: "1")

            }
            let reject = UIAlertAction(title: "Reject", style: UIAlertActionStyle.destructive) {
                UIAlertAction in
                self.changeStatus(status: "2")

            }
            let maybe = UIAlertAction(title: "Maybe", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.changeStatus(status: "3")

            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                // when cancel is tapped
            }
            
            // Add the actions
            alertController.addAction(accept)
            alertController.addAction(reject)
            alertController.addAction(maybe)
            alertController.addAction(cancelAction)

            // Present the controller
            self.present(alertController, animated: true, completion: nil)
            

            
        }else if self.events[self.currentIndex].guestStatus == 3 {
            
            // Create the actions
            let accept  = UIAlertAction(title: "Accept", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.changeStatus(status: "1")

            }
            let reject = UIAlertAction(title: "Reject", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.changeStatus(status: "2")

            }
            let maybe = UIAlertAction(title: "Maybe", style: UIAlertActionStyle.destructive) {
                UIAlertAction in
                self.changeStatus(status: "3")

            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                // when cancel is tapped
            }
            
            // Add the actions
            alertController.addAction(accept)
            alertController.addAction(reject)
            alertController.addAction(maybe)
            alertController.addAction(cancelAction)

            // Present the controller
            self.present(alertController, animated: true, completion: nil)
            

            
        }else{
            
            // Create the actions
            let accept  = UIAlertAction(title: "Accept", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.changeStatus(status: "1")
                
            }
            let reject = UIAlertAction(title: "Reject", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.changeStatus(status: "2")

            }
            let maybe = UIAlertAction(title: "Maybe", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.changeStatus(status: "3")
            }
           
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                // when cancel is tapped
            }
            
            // Add the actions
            alertController.addAction(accept)
            alertController.addAction(reject)
            alertController.addAction(maybe)
            alertController.addAction(cancelAction)

            // Present the controller
            self.present(alertController, animated: true, completion: nil)
            
        }
        
    }

    
    @objc func changeStatus(status : String){
        
        let guestID = self.events[self.currentIndex].guestID
        
        let parameters: Parameters = ["guestID" : guestID as AnyObject, "status" : status as AnyObject]
        
        let url = MInvitdClient.Constants.BaseUrl + "guest/status"
        
        let authHeader = ["Authorization" : UserDefaults.standard.string(forKey: "userToken")!, "Content-Type" : "application/json" ]
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: authHeader).responseJSON { (response) in
           
            
            if response.data != nil{
                self.getEvents()
            }else{
                
                let alert = UIAlertController(title: "Couldn't connect to server", message: "Check Internet", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: { (action : UIAlertAction) in
                    alert.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }

    @objc func commentBtnTap(_ sender: UITapGestureRecognizer) {
        
        UserDefaults.standard.set(self.eventID, forKey: "eventID")
        UserDefaults.standard.set(self.commentID, forKey: "postID")
        
        self.getFeedsRequest(commentID: self.commentID)

    }
    
    @objc func getFeedsRequest(commentID : Int){
        
        Spinner.sharedinstance.startSpinner(viewController: self)
        // Activity Incator
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")!
        
        var params = Parameters()
        
        if self.origin == "feed"{
            params = ["postID":commentID,"invitationID":invitationID,"limitSubComment":1000]
        }
        
        
        apiCallingMethod(apiEndPints: "comments", method: .get, parameters: params) { (data:Any?, totalobj:Int?) in
            
            Spinner.sharedinstance.stopSpinner(viewController: self)
            
            if let data1 = data as? [[String : AnyObject]]{
                if let allFeeds1 = MInvtdAllFeed.allFeedsFromResults(data1) as? [MInvtdAllFeed] {
                  
                    //print("this is invitation comment ^^^^^^^^\(allFeeds1)")
                    self.allFeeds = allFeeds1
                    self.paramaters = allFeeds1[0]
                    if allFeeds1[0].subCommentArray != nil
                    {
                        self.allSubComments = allFeeds1[0].subCommentArray!
                    }
                    
                    
                    if self.paramaters?.fileType == "text"{
                        
                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let mainVC = storyBoard.instantiateViewController(withIdentifier: "feedCommentVC") as! FeedCommentVC
                        mainVC.paramaters = self.paramaters
                        mainVC.origin = "feed"
                        self.present( UINavigationController(rootViewController:mainVC), animated: true, completion: nil)
                    }else {
                        
                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let mainVC = storyBoard.instantiateViewController(withIdentifier: "feedImageCommentVC") as! FeedImageCommentVC
                        mainVC.paramaters = self.paramaters
                        mainVC.origin = "feed"
                        self.present( UINavigationController(rootViewController:mainVC), animated: true, completion: nil)
                    }
                }
            }
        }
    }

    
    
    @objc func scheduleBtnTap(_ sender: UITapGestureRecognizer) {
        
        UserDefaults.standard.set(self.eventID, forKey: "eventID")
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let category = UserDefaults.standard.string(forKey: "category")

        if category == "professional" {
            
            let mainVC = storyBoard.instantiateViewController(withIdentifier: "eventScheduleVC") as! EventScheduleViewController
            self.present(mainVC, animated: true, completion: nil)

        }else {
          
            let vc = storyBoard.instantiateViewController(withIdentifier: "PersonalSchedularTableVC")
            
            self.present(vc, animated: true, completion: nil)

            // expandable
        }
    }


    @objc func imageOnlyCellImageTap(_ sender: UITapGestureRecognizer) {
        
        let event = self.events[self.currentIndex]
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "popUpImageCellVC") as! PopUpImageOnlyCellVC
        mainVC.event = event
        mainVC.modalPresentationStyle = .overCurrentContext
        mainVC.modalTransitionStyle = .crossDissolve
        self.present(mainVC, animated: true, completion: nil)
    }
    
    @objc func venueTapDetected(_ sender: UITapGestureRecognizer) {
        
        let mainVC = VenueVC()
        mainVC.arr = venus
        mainVC.currentIndex = currentIndex
        pushPresentAnimation()
        present(mainVC, animated: false, completion: nil)


    }

}
