//
//  WritePostTableCell.swift
//  Minvtd
//
//  Created by Tushar Aggarwal on 21/07/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit

class WritePostTableCell: UITableViewCell {

    @IBOutlet weak var playIcon: UIButton!
    @IBOutlet weak var crossButton: UIButton!
    @IBOutlet weak var imageMain: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
