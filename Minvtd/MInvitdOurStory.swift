//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import UIKit


struct MInvitdOurStory{
    
    // MARK: Properties
    
 //   let objectID : Int?
    let invitationID : Int?
    let title : String?
    let description : String?
    let imageUrl : String?
    let date : String?
    let created : Int?
    let updated : Int?
    let likes : Int?
    let liked : Bool?
    
    
    
    // MARK: Initializers
    
    // construct dictionary
    init(dictionary: [String:AnyObject]) {
    //    objectID = dictionary[MInvitdClient.JSONResponseKeys.objectID] as? Int
        invitationID = dictionary[MInvitdClient.JSONResponseKeys.invitationID] as? Int
        title = dictionary[MInvitdClient.JSONResponseKeys.title] as? String
        description = dictionary[MInvitdClient.JSONResponseKeys.description] as? String
        imageUrl = dictionary[MInvitdClient.JSONResponseKeys.imageUrl] as? String
        date = dictionary[MInvitdClient.JSONResponseKeys.date] as? String
        created = dictionary[MInvitdClient.JSONResponseKeys.created] as? Int
        updated = dictionary[MInvitdClient.JSONResponseKeys.updated] as? Int
        likes = dictionary[MInvitdClient.JSONResponseKeys.likes] as? Int
        liked = dictionary[MInvitdClient.JSONResponseKeys.liked] as? Bool


        
        
    }
    
    static func ourStoryFromResults(_ results: [[String:AnyObject]]) -> [MInvitdOurStory] {
        var stories = [MInvitdOurStory]()
        
        // iterate through array of dictionaries, each Article is a dictionary
        for story in results {
            stories.append(MInvitdOurStory(dictionary: story))
        }
        
        return stories
    }
}

