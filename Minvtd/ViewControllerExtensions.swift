
//  ViewControllerExtensions.swift
//  Minvtd
//  Created by admin on 31/08/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.

import UIKit
import Alamofire
import NVActivityIndicatorView
import Firebase
import FBSDKLoginKit

extension UIViewController {
    
    @objc func navigationBarSetup(_ nav:UINavigationBar?){
        
        let navigationBar:UINavigationBar = nav ?? (self.navigationController?.navigationBar)!
        
        navigationBar.isTranslucent = false
        navigationBar.tintColor = UIColor.white
        let fontDictionary = [ NSAttributedStringKey.foregroundColor:UIColor.white ]
        self.navigationController?.navigationBar.titleTextAttributes = fontDictionary
        navigationBar.setBackgroundImage(imageLayerForGradientBackground(navBar: navigationBar), for: .default)
    }
   
    @objc func dissmissVc() {
        self.navigationController?.popViewController(animated: true)
    }

    @objc func disssmissKeyboard() {
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dissmiss)))
    }
    
    @objc private func dissmiss(){
        view.endEditing(true)
    }
    
    @objc func pushPresentAnimation(){
        let transition = CATransition()
        //transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.layer.add(transition, forKey: kCATransition)

    }
    
    @objc func pushpopAnimation(){
        
        let transition = CATransition()
        transition.type = kCATransitionMoveIn
        transition.subtype = kCATransitionFromLeft
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.layer.add(transition, forKey: kCATransition)

      //  view.window!.layer.add(transition, forKey: kCATransition)

    }
    
    @objc func imageLayerForGradientBackground(navBar:UINavigationBar?) -> UIImage {
        
        let updatedFrame = navBar?.bounds ?? CGRect(x: 0, y: 0, width: view.bounds.width, height: 44.0)
     
        // take into account the status bar
        let layer = CAGradientLayer.gradientLayerForBounds(bounds: CGRect(x:0, y:-20, width: updatedFrame.width, height:64))
        UIGraphicsBeginImageContext(layer.bounds.size)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    // Custom Alert
    @objc func CreateAlertPopUp( _ tittle:String?, _ message:String?, OnTap:  (()->())?) {
            let alertVc = UIAlertController(title: tittle, message: message, preferredStyle: .alert)
            
            alertVc.addAction(UIAlertAction(title: "Okay", style: .default, handler: { (action:UIAlertAction) in
                
                if OnTap != nil {
                    OnTap!()
                }
                
                if message! == "Forbidden Access" {
                    //print(message!)
                    //print("inside perform here logout")
                    self.logout()
                }
                
            }))
            self.present(alertVc, animated: true, completion: nil)
        }
    
    // Dissmiss Alert
    @objc func dismissAlert( _ message:String?) {
        
        
        let alertVc = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        
        alertVc.addAction(UIAlertAction(title: "Dismiss", style: .default, handler:
        nil))
        
        self.present(alertVc, animated: true, completion: nil)
    }

   // Api Function
    
    func apiCallingMethod(apiEndPints:String,method: HTTPMethod, parameters: Parameters?,complition:@escaping (_ data:Any?,_ totalObjects:Int?)->() ){
        
        let url = MInvitdClient.Constants.BaseUrl + apiEndPints
        var prams = parameters
        let colour1 = UserDefaults.standard.string(forKey: "colour1")
        let colour2 = UserDefaults.standard.string(forKey: "colour2")
        
        if let colour = colour1 {
            prams!["colour"] = colour
        }
        if let colour = colour2 {
            prams!["colour_2"] = colour
        }
        
        Alamofire.request(url, method: method, parameters: prams, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
          
            Spinner.sharedinstance.stopSpinner(viewController: self)
           
            //print("this is response ->\(response)")
            //debugPrint(response)
        
            if response.error == nil {
                self.apiHelperMethod(JSONData : response.data!, complition: {
                    data, totalObjects
                    in
                    complition(data, totalObjects)
                })
            }else {
                self.CreateAlertPopUp("Couldn't connect to server", "Check Internet", OnTap: nil)
            }
        }
    }
    
    @objc func logout() {
        
        let manager = FBSDKLoginManager()
        manager.logOut()
        slideMenuController()?.closeLeft()
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        var parameters = Parameters()
        
        if let userToken = UserDefaults.standard.string(forKey: "userToken"){
            parameters = ["userID": userID! , "userToken" : userToken, "device" : "iOS" ]
            
        }else {
            parameters  = ["userID": userID! , "userToken" : "11", "device" : "iOS"]
        }
        
        apiCallingMethod(apiEndPints: "user/logout", method: .post, parameters: parameters) { (data1:Any?, totalObjects:Int?) in
            
            try! Auth.auth().signOut()
            
            let defaults = UserDefaults.standard
            let dictionary = defaults.dictionaryRepresentation()
            
            dictionary.keys.forEach {
                key in defaults.removeObject(forKey: key)
            }
            defaults.synchronize()
            
//            UserDefaults.standard.removeObject(forKey: "userID")
//            UserDefaults.standard.removeObject(forKey: "userToken")
//            UserDefaults.standard.removeObject(forKey: "imageUrl")
//            UserDefaults.standard.removeObject(forKey: "name")
//            UserDefaults.standard.removeObject(forKey: "coverUrl")
//            UserDefaults.standard.removeObject(forKey: "eventID")
//            UserDefaults.standard.removeObject(forKey: "invitationID")
//            UserDefaults.standard.removeObject(forKey: "commentID")
//            UserDefaults.standard.removeObject(forKey: "localSocialAccount")
//            UserDefaults.standard.removeObject(forKey: "colour")
//            UserDefaults.standard.removeObject(forKey: "colour2")
//            UserDefaults.standard.removeObject(forKey: "imageID")
//            UserDefaults.standard.removeObject(forKey: "name")
//            UserDefaults.standard.removeObject(forKey: "email")
//            UserDefaults.standard.removeObject(forKey: "userAvatar")
//            UserDefaults.standard.removeObject(forKey: "imageUrl")
//            UserDefaults.standard.removeObject(forKey: "dob")
//            UserDefaults.standard.set(false, forKey: "isRegister")

            
            DispatchQueue.main.async {
                let storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
                
                let vc:NewOtpScreen =  (storyboard.instantiateViewController(withIdentifier: "NewOtpScreen") as? NewOtpScreen)!
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = vc
                appDelegate.window?.rootViewController? .dismiss(animated: false, completion: nil)
            }
        }
    }
    
    func apiCallingMethod(apiEndPints:String,method: HTTPMethod, parameters: Parameters?,complition:@escaping (_ data:Any?,_ totalObjects:Int?,_ Message:String?)->() ){
        
        let url = MInvitdClient.Constants.BaseUrl + apiEndPints
        var prams = parameters
        let colour1 = UserDefaults.standard.string(forKey: "colour1")
        let colour2 = UserDefaults.standard.string(forKey: "colour2")
        
        if let colour = colour1 {
            prams!["colour"] = colour
        }
        if let colour = colour2 {
            prams!["colour_2"] = colour
        }
        
      let authHeader = ["Authorization" : UserDefaults.standard.string(forKey: "userToken")!, "Content-Type" : "application/json" ]

        Alamofire.request(url, method: method, parameters: prams, encoding: JSONEncoding.default, headers: authHeader).responseJSON { (response) in
            Spinner.sharedinstance.stopSpinner(viewController: self)
            
           //print("this is response ->\(response)")
           //debugPrint(response)
            
            if response.error == nil {
                self.apiHelperMethod(JSONData : response.data!, complition: {
                    data, totalObjects,Message
                    in
                    complition(data, totalObjects, Message)
                })
            }else {
                self.CreateAlertPopUp("Couldn't connect to server", "Check Internet", OnTap: nil)
            }
        }
    }
    
    fileprivate func  apiHelperMethod(JSONData : Data,complition:(_ data:Any?,_ totalObjects:Int?,_ Message:String?)->()) {
        
        var totalObjects:Int?
        var serverData:Any?
        var message:String?
        do{
            let json = try JSONSerialization.jsonObject(with: JSONData, options: .allowFragments)
            
            if let dictionary = json as? [String : Any] {
                let totalObjects1 = dictionary["totalObjects"] as? Int
                
                totalObjects = totalObjects1
                if totalObjects == 0 {
                    
                    complition(serverData, totalObjects,nil)
                    return
                    
                }
                
                if let status = dictionary["status"] as? String
                {
                    if status == "success"
                    {
                        let data = dictionary["data"]
                        serverData = data
                        
                        if let mess = (dictionary["messages"] as? [[String : String]])                             {
                            if let messdict = mess.first {
                                 message = messdict["message"] ?? ""
                            }}
                        
                        complition(serverData, totalObjects,message)
                        return
                        
                    }else
                    {
                        if let mess = (dictionary["messages"] as? [[String : String]])                             {
                            if let messdict = mess.first {
                                let message = messdict["message"] ?? "Error status returned from server"
                                
                                DispatchQueue.main.async {
                                    self.CreateAlertPopUp(status, message, OnTap: nil)
                                }
                            }
                        }
                    }
                }
            }
            
        }
        catch{
            Spinner.sharedinstance.stopSpinner(viewController: self)
            self.CreateAlertPopUp("Error", "Error in JSON Serialization", OnTap: nil)
        }
    }
    
    fileprivate func  apiHelperMethod(JSONData : Data,complition:(_ data:Any?,_ totalObjects:Int?)->()) {
    
        var totalObjects:Int?
        var serverData:Any?
        
            do{
                let json = try JSONSerialization.jsonObject(with: JSONData, options: .allowFragments)

                if let dictionary = json as? [String : Any] {
                    let totalObjects1 = dictionary["totalObjects"] as? Int
                    
                    totalObjects = totalObjects1
                    if totalObjects == 0 {
                        
                        complition(serverData, totalObjects)
                        return
                    }
                    
                    if let status = dictionary["status"] as? String
                    {
                        if status == "success"
                        {
                             let data = dictionary["data"]
                            serverData = data
                            
                            complition(serverData, totalObjects)
                            return
                            
                        }else
                        {
                            if let mess = (dictionary["messages"] as? [[String : String]])                             {
                                if let messdict = mess.first {
                                    
                                    let message = messdict["message"] ?? "Error status returned from server"

                                    DispatchQueue.main.async {
                                        
                                        self.CreateAlertPopUp(status, message, OnTap: nil)
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch{
                Spinner.sharedinstance.stopSpinner(viewController: self)

                self.CreateAlertPopUp("Error", "Error in JSON Serialization", OnTap: nil)
            }
    }
    
    @objc func uploadImage(image:UIImage?,Complition:@escaping (_ fid:String,_ fileUrl:String)->()){
        
        
        if let name1 = image {
            
            let imgData =  UIImageJPEGRepresentation(name1, 0.4)
            let mimeType = "image/jpg"
            let fileName = "file.jpg"
            let parameters = ["name": "file" , "file_input" : "fileset" ]
            let authHeader = ["Authorization" : UserDefaults.standard.string(forKey: "userToken")!, "Content-Type" : "application/json" ]
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                
                multipartFormData.append(imgData!, withName: "fileset",fileName: fileName, mimeType: mimeType)
                
                for (key, value) in parameters {
                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
            },
                             to:MInvitdClient.Constants.BaseUrl + "files/upload",
                             method : .post,
                             headers : authHeader)
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    
                  //print("\(result)")
                    
                    upload.responseJSON { response in
                        
                        //print("This is Response\(response)")
                        
                        if let result = response.result.value {
                            let JSON = result as! NSDictionary
                            if let status = JSON["status"] as? String
                            {
                                if status == "success"
                                {
                                    if let data = JSON["data"] as? [[String : Any]]
                                    {
                                        let fileUrl  = data[0]["fileUrl"] as? String
                                        let fileID  = data[0]["fid"] as? Int
                                        
                                        Complition(fileID!.description,fileUrl!)
                                    }
                                }else if status=="error" {
                                    let message:String?
                                    if let messages = JSON["messages"] as? [String] {
                                        message = messages.first ?? "Error occured uringUploading"
                                        self.dismissAlert(message)
                                    }
                                    return
                                }
                            }
                        }
                    }
                case .failure(let encodingError):
                    print(encodingError)
                }}
        }
    }
}
