//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import UIKit


struct MInvitdComment{
    
    // MARK: Properties
    
    let postID : Int?
    let userID : Int?
    let invitationID : Int?
    let eventID : Int?
    let commentID : Int?
    let comment : String?
    let fileID : Int?
    let created : String?
    let updated : String?
    let userName : String?
    let userImageUrl : String?
    let likes : Int?
    let liked : Bool?
    let subCommentsCount : Int?
    
    
    // MARK: Initializers
    
    // construct a ZXQKArticle from a dictionary
    init(dictionary: [String:AnyObject]) {
        
        postID = dictionary[MInvitdClient.JSONResponseKeys.postID] as? Int
        userID = dictionary[MInvitdClient.JSONResponseKeys.userID] as? Int
        invitationID = dictionary[MInvitdClient.JSONResponseKeys.invitationID] as? Int
        eventID = dictionary[MInvitdClient.JSONResponseKeys.eventID] as? Int
        commentID = dictionary[MInvitdClient.JSONResponseKeys.commentID] as? Int
        fileID = dictionary[MInvitdClient.JSONResponseKeys.fileID] as? Int
        comment = dictionary[MInvitdClient.JSONResponseKeys.comment] as? String
        created = dictionary[MInvitdClient.JSONResponseKeys.created] as? String
        updated = dictionary[MInvitdClient.JSONResponseKeys.updated] as? String
        userImageUrl = dictionary[MInvitdClient.JSONResponseKeys.userImageUrl] as? String
        userName = dictionary[MInvitdClient.JSONResponseKeys.userName] as? String
        likes = dictionary[MInvitdClient.JSONResponseKeys.likes] as? Int
        liked = dictionary[MInvitdClient.JSONResponseKeys.liked] as? Bool
        subCommentsCount = dictionary[MInvitdClient.JSONResponseKeys.subCommentsCount] as? Int
    }
    
    static func eventCommentsFromResults(_ results: [[String:AnyObject]]) -> [MInvitdComment] {
        var comments = [MInvitdComment]()
        
        // iterate through array of dictionaries, each Article is a dictionary
        for comment in results {
            comments.append(MInvitdComment(dictionary: comment))
        }
        
        return comments
    }
}
