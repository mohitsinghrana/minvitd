//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

struct MInvitdGuest{
    
    // MARK: Properties
    var userID : Int!
    var guestID : Int!
    var name : String!
    var phone : String?
    var email : String!
    var status : Int!
    var statusName : String!
    var statusColour: String?
    var firebaseID : String!
    var imageUrl : String!
    var isSelected:Bool = false
    var fileID: Int!
    var imageThumbnail: String!
    var chatID: String!
    
    // MARK: Initializers
    init(dictionary: [String:AnyObject]) {
        userID = dictionary[MInvitdClient.JSONResponseKeys.userID] as? Int
        guestID = dictionary[MInvitdClient.JSONResponseKeys.guestID] as? Int
        let num = dictionary[MInvitdClient.JSONResponseKeys.phone] as? Int64
        phone = num?.description
        email = dictionary[MInvitdClient.JSONResponseKeys.email] as? String
        name = dictionary[MInvitdClient.JSONResponseKeys.name] as? String
        status = dictionary[MInvitdClient.JSONResponseKeys.status] as? Int
        statusName = dictionary[MInvitdClient.JSONResponseKeys.statusName] as? String
        statusColour = dictionary[MInvitdClient.JSONResponseKeys.statusColour] as? String
//        firebaseID = dictionary[MInvitdClient.JSONResponseKeys.firebaseID] as? String
        imageUrl = dictionary[MInvitdClient.JSONResponseKeys.imageUrl] as? String
        fileID = dictionary["fileID"] as? Int
        imageThumbnail = dictionary["imageUrlThumbnail"] as? String
        chatID = dictionary["chatID"] as? String
    }
    
    init(name : String?,phone : String?,email : String?) {
        self.name = name
        self.phone = phone
        self.email = email
    }
    
    func selectionFalse()->MInvitdGuest{
        var guest = self
        guest.isSelected = false
        return guest
    }
    
    static func dictionaryFromGuests(_ results: [MInvitdGuest]) -> [[String:String]] {

        var dictArray = [[String:String]]()
        // iterate through array of dictionaries, each Article is a dictionary
        for guest in results {
            
            
            if let phone = guest.phone{
                
                var guest1 = [String:String]()
                
                guest1["phone"] = phone

                if let name = guest.name{
                    guest1["name"] = name

                }
                if let email = guest.email{
                    guest1["email"] = email
                }
                
                dictArray.append(guest1)
                
            }
        }
        
        return dictArray
    }
    
    static func guestsFromResults(_ results: [[String:AnyObject]]) -> [MInvitdGuest] {
        var guests = [MInvitdGuest]()
        for guest in results {
            guests.append(MInvitdGuest(dictionary: guest))
        }
        return guests
    }
}
