//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

struct MInvitdVideos{
    
    // MARK: Properties
    
    let fileUrl : String?
    let fileThumbnail : String?
    
    // MARK: Initializers
    
    // construct a dictionary
    init(dictionary: [String:AnyObject]) {
        fileUrl = dictionary[MInvitdClient.JSONResponseKeys.fileUrl] as? String
        fileThumbnail =  dictionary[MInvitdClient.JSONResponseKeys.fileThumbnail] as? String
    }
    
    static func videosFromResults(_ results: [[String:AnyObject]]) -> [MInvitdVideos] {
        var videos = [MInvitdVideos]()
        
        // iterate through array of dictionaries, each Article is a dictionary
        for video in results {
            videos.append(MInvitdVideos(dictionary: video))
        }
        
        return videos
    }
}
