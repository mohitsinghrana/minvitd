//
//  ManageGuestUndecided.swift
//  Minvtd
//
//  Created by admin on 28/09/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.
//


import UIKit
import Alamofire

class ManageGuestUndecided: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var guests: [MInvitdGuest]?{
        didSet{
            let filter = self.guests?.filter({return $0.isSelected})

    }
}
    
    var orignalguest:[MInvitdGuest]?
    @IBOutlet weak var guestsCollectionView: UITableView!

    var eventID:Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        guestsCollectionView.delegate = self
        guestsCollectionView.dataSource = self
        guestsCollectionView.allowsMultipleSelection = true
        guestsCollectionView.tableFooterView = UIView()
        

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    
    
    
    
     func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return guests!.count
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let guest = guests![(indexPath as NSIndexPath).row]
        let cell = guestsCollectionView.dequeueReusableCell(withIdentifier: "guestEventListCell", for: indexPath) as! GuestsCollectionViewCell
        
        cell.nameLabel.text = guest.name
        var firstLetter : String?
        if let firstChar = guest.name?.characters.first {
            
            firstLetter = String(describing: firstChar)
            
        }else {
            firstLetter = "?"
        }
        cell.imageLabel.text = firstLetter
        cell.imageLabel.layer.cornerRadius  = cell.imageLabel.frame.size.width/2
        cell.imageLabel.layer.masksToBounds = true
        cell.imageLabel.clipsToBounds = true
        cell.emailLabel.text = guest.statusName
        
        let bgImage : UIImage = self.imageLayerForGradientBackgroundSmall()
        cell.imageLabel.backgroundColor = UIColor(patternImage: bgImage)
        cell.inviteView.backgroundColor = UIColor.init(hexString: guest.statusColour!)
        
//        if guest.status == 0 {
//            cell.inviteView.backgroundColor = UIColor.init(hexString: "#F18037")
//        }else if guest.status == 1 {
//            cell.inviteView.backgroundColor = UIColor.init(hexString: "#61BCA7")
//
//        }else if guest.status == 2{
//            cell.inviteView.backgroundColor = UIColor.init(hexString: "#DA4C45")
//        }else{
//            cell.inviteView.backgroundColor = UIColor.init(hexString: "#2C7EC3")
//        }
        cell.inviteView.layer.cornerRadius = cell.inviteView.bounds.size.width/2
        
        if guests![indexPath.row].isSelected {
            cell.accessoryType = .checkmark
            
        }else{
            cell.accessoryType = .none
            
        }
        return cell
    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return CGFloat(60)
    }
    
    
     func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as? GuestsCollectionViewCell
        self.guests![indexPath.row].isSelected = false
        cell?.accessoryType = .none
        
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as? GuestsCollectionViewCell
        self.guests![indexPath.row].isSelected = true
        cell?.accessoryType = .checkmark
        
    }

}

extension ManageGuestUndecided {
    
    @objc public func imageLayerForGradientBackgroundSmall() -> UIImage {
        
        let updatedFrame = CGRect(x: 0, y: 0, width: 40, height: 40)
        // take into account the status bar
        let layer = CAGradientLayer.gradientLayerForBounds(bounds: updatedFrame)
        UIGraphicsBeginImageContext(layer.bounds.size)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }

    
    
    @objc func selectAllAction(nature:Bool){
    var new:[MInvitdGuest] = []
    for guest in guests! {
        var new1 = guest
        new1.isSelected =  nature
        new.append(new1)
    }
    self.guests = new
    self.guestsCollectionView.reloadData()
    }
    
    @objc func sendAgainAction() {

        let selected = self.guests?.filter({return $0.isSelected})
        let invitaionID = UserDefaults.standard.string(forKey: "invitationID")!
        let userID = UserDefaults.standard.string(forKey: "userID")!
        var guests:[Int] = []
        selected?.forEach({guests.append($0.userID!)})
//        print("-----otherGuests-----\(guests)")
        
        let prams:Parameters = ["invitationID":invitaionID,"userID":userID,"guests":guests,"eventID":eventID]
        
        apiCallingMethod(apiEndPints: "guests/remind", method: .post, parameters: prams) { (data1, totalObj,message) in
            if data1 != nil {
                self.CreateAlertPopUp("Success", message ?? "Guest have been sent a notification", OnTap: {
                    // self.sendButton.isHidden = true
                    self.guests = self.orignalguest
                    self.guestsCollectionView.reloadData()
                })
            }
        }
    }
}
