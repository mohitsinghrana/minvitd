//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//


import UIKit
import Alamofire
import AlamofireImage
import UITextView_Placeholder
import IDMPhotoBrowser

class EventCommentViewController: UIViewController , IDMPhotoBrowserDelegate{
    
    
    @objc var photos : [IDMPhoto] = [IDMPhoto]()
    @objc var browser:IDMPhotoBrowser? = nil

    
    var allFeeds: [MInvtdAllFeed] = [MInvtdAllFeed]()
    var paramaters : MInvtdAllFeed?
    @objc var subCommentString  : String  = ""
    @objc let placeholderImage = UIImage(named: "eventCoverImage")!
    @objc var currentIndex : Int = 0
    fileprivate let sectionInsets = UIEdgeInsets(top: 5.0, left: 10.0, bottom: 5.0, right: 10.0)
    fileprivate let itemsPerRow: CGFloat = 1
    
    @IBOutlet weak var feedCollectionView: UICollectionView!
    
    @objc func dismissVC(_ sender: UIBarButtonItem) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        feedCollectionView.delegate = self
        feedCollectionView.dataSource = self
        self.feedCollectionView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dissmisskeyboard)))
        
        browser?.delegate = self

        navigationBarSetup(nil)
        title = "Comments"
        let button2 = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissVC))
        self.navigationItem.rightBarButtonItem = button2

        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @objc func dissmisskeyboard(){
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getFeedsRequest()
    }
    
    @objc func getFeedsRequest(){
        
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")!
        let userID = UserDefaults.standard.string(forKey: "userID")!
        let eventID = UserDefaults.standard.string(forKey: "eventID")!
        let commentID = UserDefaults.standard.string(forKey: "commentID")!
        
        var params : Parameters = [:]
        
        params = ["commentID":commentID,"invitationId":invitationID,"userId":userID,"limitSubComment":"1000","eventId":eventID] as [String : Any]

        
        print("\(params)")
        
        apiCallingMethod(apiEndPints: "comments", method: .get, parameters: params) { (data1:Any?, totalobj:Int?) in
            
            if let data = data1 as? [[String : Any]]
            {
                let allFeeds = MInvtdAllFeed.allFeedsFromResults(data as [[String : AnyObject]])
                self.allFeeds = allFeeds
                DispatchQueue.main.async {
                    self.feedCollectionView.reloadData()
                }
            }
        }
    }
}

// MARK: - AllFeedViewController: UICollectionViewDelegate, UICollectionViewDataSource



extension EventCommentViewController : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout , UITextFieldDelegate {
    

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let feed = allFeeds[(indexPath as NSIndexPath).row]
        
        var additionalSize : Int  = Int()
        if (feed.comment ?? "").isEmpty{
            additionalSize = 0
        }else{
            
            let width = UIScreen.main.bounds.width
            var maxChars = 50
            if width == CGFloat(320) {
                maxChars = 45
            }else if width == CGFloat(375){
                maxChars = 50
            }else{
                maxChars = 55
            }
            let countLetter =  feed.comment?.characters.count
            additionalSize  = (countLetter! / maxChars) * 18
            let labelTokens =  feed.comment?.components(separatedBy: "\n")
            additionalSize  += ((labelTokens?.count)! - 1 ) * 18
            
//            let labelTokens =  feed.comment?.components(separatedBy: "\n")
//            additionalSize  = ((labelTokens?.count)! - 1 ) * 18
        }
        
        
        if feed.fileType == "text" {
            
            if feed.subCommentsCount == 0 {
                let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
                let availableWidth = view.frame.width - paddingSpace
                let widthPerItem = availableWidth / itemsPerRow
                let height = CGFloat(229)
                
                return CGSize(width: widthPerItem, height: height +  CGFloat(additionalSize))
            }else{
                
                let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
                let availableWidth = view.frame.width - paddingSpace
                let widthPerItem = availableWidth / itemsPerRow
                let height = CGFloat(327)
                
                return CGSize(width: widthPerItem, height: height +  CGFloat(additionalSize))
            }
            
        }else
        {
            if feed.subCommentsCount == 0 {
                
                let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
                let availableWidth = view.frame.width - paddingSpace
                let widthPerItem = availableWidth / itemsPerRow
                let height = CGFloat(429)
                
                return CGSize(width: widthPerItem, height: height +  CGFloat(additionalSize))
            }else{
                let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
                let availableWidth = view.frame.width - paddingSpace
                let widthPerItem = availableWidth / itemsPerRow
                let height = CGFloat(528)
                
                return CGSize(width: widthPerItem, height: height +  CGFloat(additionalSize))
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let userID = UserDefaults.standard.integer(forKey: "userID")
        let feed = allFeeds[(indexPath as NSIndexPath).row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "allEventCell", for: indexPath) as! EventCollectionViewCell
        let cellNoImage = collectionView.dequeueReusableCell(withReuseIdentifier: "allEventCellNoImage", for: indexPath) as! EventsCollectionViewNoImage
        let cellNoComment = collectionView.dequeueReusableCell(withReuseIdentifier: "allEventCellNoComment", for: indexPath) as! EventsCollectionViewNoComment
        let cellNoCommentImage = collectionView.dequeueReusableCell(withReuseIdentifier: "allEventCellNoCommentImage", for: indexPath) as! EventsCollectionViewNoCommentImage
        
        print("Feed File Type :\(feed.fileType)")
        
        if feed.fileType == "text"{
            
            if feed.subCommentsCount == 0 {
                
                if UserDefaults.standard.integer(forKey: "userID") == feed.userID!{
                    cellNoCommentImage.moreButton.isHidden = false
                }else{
                    cellNoCommentImage.moreButton.isHidden = true
                }

                
                cellNoCommentImage.title.text = feed.comment
                cellNoCommentImage.name.text = feed.userName
                let filter = AspectScaledToFillSizeWithRoundedCornersFilter(
                    size: cellNoCommentImage.userImage.frame.size,
                    radius: cellNoCommentImage.userImage.bounds.height/2
                )
                cellNoCommentImage.userImage.af_setImage(
                    withURL: URL(string: feed.userImageUrl!)! ,
                    placeholderImage: placeholderImage,
                    filter : filter
                )
                if feed.liked! == true{
                    
                    var image = UIImage(named: "likeFilled")
                    image = image?.withRenderingMode(.alwaysOriginal)
                    let colour1 = UserDefaults.standard.string(forKey: "colour1")
                    cellNoCommentImage.heartBtn.setImage(image, for: .normal)
                    //cellNoCommentImage.tintColor = UIColor(hexString: colour1!)
                    //cellNoCommentImage.heartBtn.setImage(UIImage(named: "heartLiked")!, for: .normal)
                
                }else{
                    
                    var image = UIImage(named: "like")
                    image = image?.withRenderingMode(.alwaysOriginal)
                    let colour1 = UserDefaults.standard.string(forKey: "colour1")
                    cellNoCommentImage.heartBtn.setImage(image, for: .normal)
                    //cellNoCommentImage.tintColor = UIColor(hexString: colour1!)
                    //cellNoCommentImage.heartBtn.setImage(UIImage(named: "hearts")!, for: .normal)
                }
                
                let imageAttachment =  NSTextAttachment()
                imageAttachment.image = UIImage(named:"clock-1")
                let imageOffsetY:CGFloat = -5.0;
                imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: imageAttachment.image!.size.width, height: imageAttachment.image!.size.height)
                let attachmentString = NSAttributedString(attachment: imageAttachment)
                var completeText = NSMutableAttributedString(string: "")
                completeText.append(attachmentString)
                
                let dateFormater = DateFormatter()
                dateFormater.dateFormat = "MM/dd/yy"
                let date = dateFormater.string(from: NSDate(timeIntervalSince1970: feed.created!) as Date)
                
                let  textAfterIconTop = NSMutableAttributedString(string: " \(date)")
                completeText.append(textAfterIconTop)
                cellNoCommentImage.date.attributedText =  completeText
                
                
                cellNoCommentImage.postSubCommentTF.text = ""
                cellNoCommentImage.heartLabel.text = "\(feed.likes!)"
                cellNoCommentImage.commentLabel.text = "\(feed.subCommentsCount!)"
                cellNoCommentImage.postSubCommentTF.layer.borderWidth = 0.5
                cellNoCommentImage.postSubCommentTF.layer.borderColor = UIColor.gray.cgColor
                cellNoCommentImage.postSubCommentTF.placeholder = "Write a comment..."
                
                cellNoCommentImage.heartBtn.tag = indexPath.row
                cellNoCommentImage.commentBtn.tag = indexPath.row
                cellNoCommentImage.postSubCommentTF.tag = indexPath.row
                cellNoCommentImage.postSubCommentTF.delegate = self
                cellNoCommentImage.postSubCommentTF.returnKeyType = .done
                cellNoCommentImage.moreButton.tag = indexPath.row

                
                cellNoCommentImage.layer.shadowColor = UIColor.gray.cgColor
                cellNoCommentImage.layer.shadowOffset = CGSize(width: 0, height: 1)
                cellNoCommentImage.layer.shadowOpacity = 1
                cellNoCommentImage.layer.shadowRadius = 1.0
                cellNoCommentImage.clipsToBounds = false
                cellNoCommentImage.layer.masksToBounds = false
                cellNoCommentImage.layer.cornerRadius = 2.5
                
                
                // Remove all existing targets (in case cell is being recycled)
                cellNoCommentImage.heartBtn.removeTarget(nil, action: nil, for: .allEvents)
                cellNoCommentImage.commentBtn.removeTarget(nil, action: nil, for: .allEvents)
                cellNoCommentImage.moreButton.removeTarget(nil, action: nil, for: .allEvents)

                // Add target
                cellNoCommentImage.heartBtn.addTarget(self, action: #selector(likeBtnTap(sender:)), for: .touchUpInside)
                cellNoCommentImage.commentBtn.addTarget(self, action: #selector(commentBtnTap(sender:)), for: .touchUpInside)
                cellNoCommentImage.moreButton.addTarget(self, action: #selector(moreButtonTap(sender:)), for: .touchUpInside)

                
                print("Returning cellNoCommentImage")
                return cellNoCommentImage
                
            }else {
                
                
                if UserDefaults.standard.integer(forKey: "userID") == feed.userID!{
                    cellNoImage.moreButton.isHidden = false
                }else{
                    cellNoImage.moreButton.isHidden = true
                }
                

                cellNoImage.title.text = feed.comment
                cellNoImage.name.text = feed.userName
                let filter = AspectScaledToFillSizeWithRoundedCornersFilter(
                    size: cellNoImage.userImage.frame.size,
                    radius: cellNoImage.userImage.bounds.height/2
                )
                cellNoImage.userImage.af_setImage(
                    withURL: URL(string: feed.userImageUrl!)! ,
                    placeholderImage: placeholderImage,
                    filter : filter
                )
                if feed.liked! == true{
                    
                    var image = UIImage(named: "likeFilled")
                    image = image?.withRenderingMode(.alwaysOriginal)
                    let colour1 = UserDefaults.standard.string(forKey: "colour1")
                    cellNoImage.heartBtn.setImage(image, for: .normal)
                    //cellNoImage.tintColor = UIColor(hexString: colour1!)
                    
                    //cellNoImage.heartBtn.setImage(UIImage(named: "heartLiked")!, for: .normal)
                    
                    
                }else{
                    
                    var image = UIImage(named: "like")
                    image = image?.withRenderingMode(.alwaysOriginal)
                    let colour1 = UserDefaults.standard.string(forKey: "colour1")
                    cellNoImage.heartBtn.setImage(image, for: .normal)
                    //cellNoImage.tintColor = UIColor(hexString: colour1!)
                    //cellNoImage.heartBtn.setImage(UIImage(named: "hearts")!, for: .normal)
                    
                }
                
                
                let imageAttachment =  NSTextAttachment()
                imageAttachment.image = UIImage(named:"clock-1")
                let imageOffsetY:CGFloat = -5.0;
                imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: imageAttachment.image!.size.width, height: imageAttachment.image!.size.height)
                let attachmentString = NSAttributedString(attachment: imageAttachment)
                var completeText = NSMutableAttributedString(string: "")
                completeText.append(attachmentString)
                
                let dateFormater = DateFormatter()
                dateFormater.dateFormat = "MM/dd/yy"
                let date = dateFormater.string(from: NSDate(timeIntervalSince1970: feed.created!) as Date)
                let  textAfterIconTop = NSMutableAttributedString(string:" \(date)")
                completeText.append(textAfterIconTop)
                cellNoImage.date.attributedText =  completeText
                
                cellNoImage.postSubCommentTF.text = ""
                cellNoImage.heartLabel.text = "\(feed.likes!)"
                cellNoImage.commentLabel.text = "\(feed.subCommentsCount!)"
                cellNoImage.postSubCommentTF.layer.borderWidth = 0.5
                cellNoImage.postSubCommentTF.layer.borderColor = UIColor.gray.cgColor
                cellNoImage.postSubCommentTF.placeholder = "Write a comment..."
                
                cellNoImage.heartBtn.tag = indexPath.row
                cellNoImage.commentBtn.tag = indexPath.row
                cellNoImage.postSubCommentTF.tag = indexPath.row
                cellNoImage.postSubCommentTF.delegate = self
                cellNoImage.postSubCommentTF.returnKeyType = .done
                cellNoImage.moreButton.tag = indexPath.row

                cellNoImage.layer.shadowColor = UIColor.gray.cgColor
                cellNoImage.layer.shadowOffset = CGSize(width: 0, height: 1)
                cellNoImage.layer.shadowOpacity = 1
                cellNoImage.layer.shadowRadius = 1.0
                cellNoImage.clipsToBounds = false
                cellNoImage.layer.masksToBounds = false
                cellNoImage.layer.cornerRadius = 2.5
                
                cellNoImage.subCommentUserImage.af_setImage(
                    withURL: URL(string: (feed.subCommentArray?[0].userImageUrl!)!)! ,
                    placeholderImage: placeholderImage,
                    filter : filter
                )
                
                let subCommentString = NSMutableAttributedString()

                if(feed.subCommentArray?[0].comment != nil) {

                subCommentString
                    .bold((feed.subCommentArray?[0].userName)!)
                    .normal("  ")
                    .normal((feed.subCommentArray?[0].comment)!)
 
                }else{
                }
                
                cellNoImage.subComment.attributedText = subCommentString
                
                let dateSubComment = dateFormater.string(from: NSDate(timeIntervalSince1970: (feed.subCommentArray?[0].created)!) as Date)
                
                completeText = NSMutableAttributedString(string: "")
                completeText.append(attachmentString)
                let  textAfterIcon = NSMutableAttributedString(string:" \(dateSubComment)")
                completeText.append(textAfterIcon)
                cellNoImage.subCommentTime.attributedText =  completeText
                
                let upperBorder = CALayer()
                upperBorder.backgroundColor = UIColor.gray.cgColor
                upperBorder.frame = CGRect(x: 0, y: 0, width: cellNoImage.frame.width, height: 1.0)
                cellNoImage.subCommentView.layer.addSublayer(upperBorder)
                
                
                // Remove all existing targets (in case cell is being recycled)
                cellNoImage.heartBtn.removeTarget(nil, action: nil, for: .allEvents)
                cellNoImage.commentBtn.removeTarget(nil, action: nil, for: .allEvents)
                cellNoImage.moreButton.removeTarget(nil, action: nil, for: .allEvents)

                // Add target
                cellNoImage.heartBtn.addTarget(self, action: #selector(likeBtnTap(sender:)), for: .touchUpInside)
                cellNoImage.commentBtn.addTarget(self, action: #selector(commentBtnTap(sender:)), for: .touchUpInside)
                cellNoImage.moreButton.addTarget(self, action: #selector(moreButtonTap(sender:)), for: .touchUpInside)

                
                print("Returning cellNoImage")
                return cellNoImage
            }
            
        }else {
            
            if feed.subCommentsCount == 0 {
                
                
                if UserDefaults.standard.integer(forKey: "userID") == feed.userID!{
                    cellNoComment.moreButton.isHidden = false
                }else{
                    cellNoComment.moreButton.isHidden = true
                }
                
                cellNoComment.title.text = feed.comment
                cellNoComment.name.text = feed.userName
                let filter = AspectScaledToFillSizeWithRoundedCornersFilter(
                    size: cellNoComment.userImage.frame.size,
                    radius: cellNoComment.userImage.bounds.height/2
                )
                cellNoComment.userImage.af_setImage(
                    withURL: URL(string: feed.userImageUrl!)! ,
                    placeholderImage: placeholderImage,
                    filter : filter
                )
                
                if feed.liked! == true{
                    
                    var image = UIImage(named: "likeFilled")
                    image = image?.withRenderingMode(.alwaysOriginal)
                    let colour1 = UserDefaults.standard.string(forKey: "colour1")
                    cellNoComment.heartBtn.setImage(image, for: .normal)
                    //cellNoComment.tintColor = UIColor(hexString: colour1!)
                    //cellNoComment.heartBtn.setImage(UIImage(named: "heartLiked")!, for: .normal)
                    
                    
                }else{
                    var image = UIImage(named: "like")
                    image = image?.withRenderingMode(.alwaysOriginal)
                    let colour1 = UserDefaults.standard.string(forKey: "colour1")
                    cellNoComment.heartBtn.setImage(image, for: .normal)
                    //cellNoComment.tintColor = UIColor(hexString: colour1!)
                    //cellNoComment.heartBtn.setImage(UIImage(named: "hearts")!, for: .normal)
                    
                }
                let imageAttachment =  NSTextAttachment()
                imageAttachment.image = UIImage(named:"clock-1")
                let imageOffsetY:CGFloat = -5.0;
                imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: imageAttachment.image!.size.width, height: imageAttachment.image!.size.height)
                let attachmentString = NSAttributedString(attachment: imageAttachment)
                var completeText = NSMutableAttributedString(string: "")
                completeText.append(attachmentString)
                
                let dateFormater = DateFormatter()
                dateFormater.dateFormat = "MM/dd/yy"
                let date = dateFormater.string(from: NSDate(timeIntervalSince1970: feed.created!) as Date)
                
                let  textAfterIconTop = NSMutableAttributedString(string: " \(date)")
                completeText.append(textAfterIconTop)
                cellNoComment.date.attributedText =  completeText
                
                cellNoComment.postSubCommentTF.text = ""
                cellNoComment.heartLabel.text = "\(feed.likes!)"
                cellNoComment.commentLabel.text = "\(feed.subCommentsCount!)"
                cellNoComment.postSubCommentTF.layer.borderWidth = 0.5
                cellNoComment.postSubCommentTF.layer.borderColor = UIColor.gray.cgColor
                cellNoComment.postSubCommentTF.placeholder = "Write a comment..."
                
                cellNoComment.heartBtn.tag = indexPath.row
                cellNoComment.commentBtn.tag =  indexPath.row
                cellNoComment.postSubCommentTF.tag = indexPath.row
                cellNoComment.postSubCommentTF.delegate = self
                cellNoComment.postSubCommentTF.returnKeyType = .done
                cellNoComment.image.tag = indexPath.row
                cellNoComment.moreButton.tag = indexPath.row
                

                cellNoComment.layer.shadowColor = UIColor.gray.cgColor
                cellNoComment.layer.shadowOffset = CGSize(width: 0, height: 1)
                cellNoComment.layer.shadowOpacity = 1
                cellNoComment.layer.shadowRadius = 1.0
                cellNoComment.clipsToBounds = false
                cellNoComment.layer.masksToBounds = false
                cellNoComment.layer.cornerRadius = 2.5
                
                if feed.fileType == "image" {
                    
                    cellNoComment.playIcon.isHidden = true
                    // Remove all existing targets (in case cell is being recycled)
                    cellNoComment.playIcon.removeTarget(nil, action: nil, for: .allEvents)
                    cellNoComment.image.af_setImage(
                        withURL: URL(string: feed.fileUrl!)! ,
                        placeholderImage: placeholderImage
                    )
                    let imageTap = UITapGestureRecognizer(target: self, action: #selector(self.imageTapFunc(_:)))
                    imageTap.numberOfTapsRequired = 1
                    cellNoComment.image.isUserInteractionEnabled = true
                    cellNoComment.image.addGestureRecognizer(imageTap)
                }else{
                    
                    cellNoComment.playIcon.isHidden = false
                    cellNoComment.image.af_setImage(
                        withURL: URL(string: feed.fileUrl!)! ,
                        placeholderImage: placeholderImage
                    )
                    let videoTap = UITapGestureRecognizer(target: self, action: #selector(self.videoTapFunc(_:)))
                    videoTap.numberOfTapsRequired = 1
                    cellNoComment.image.isUserInteractionEnabled = true
                    cellNoComment.image.addGestureRecognizer(videoTap)
                    

                    
                }
                
                // Remove all existing targets (in case cell is being recycled)
                cellNoComment.heartBtn.removeTarget(nil, action: nil, for: .allEvents)
                cellNoComment.commentBtn.removeTarget(nil, action: nil, for: .allEvents)
                cellNoComment.moreButton.removeTarget(nil, action: nil, for: .allEvents)

                // Add target
                cellNoComment.heartBtn.addTarget(self, action: #selector(likeBtnTap(sender:)), for: .touchUpInside)
                cellNoComment.commentBtn.addTarget(self, action: #selector(commentBtnTap(sender:)), for: .touchUpInside)
                cellNoComment.moreButton.addTarget(self, action: #selector(moreButtonTap(sender:)), for: .touchUpInside)

                
                print("Returning cellNoComment")
                return cellNoComment
                
                
            }else {
                
                if UserDefaults.standard.integer(forKey: "userID") == feed.userID!{
                    cell.moreButton.isHidden = false
                }else{
                    cell.moreButton.isHidden = true
                }
                
                cell.title.text = feed.comment
                cell.name.text = feed.userName
                let filter = AspectScaledToFillSizeWithRoundedCornersFilter(
                    size: cell.userImage.frame.size,
                    radius: cell.userImage.bounds.height/2
                )
                cell.userImage.af_setImage(
                    withURL: URL(string: feed.userImageUrl!)! ,
                    placeholderImage: placeholderImage,
                    filter : filter
                )
                if feed.liked! == true{
                    
                    
                    var image = UIImage(named: "likeFilled")
                    image = image?.withRenderingMode(.alwaysOriginal)
                    let colour1 = UserDefaults.standard.string(forKey: "colour1")
                    cell.heartBtn.setImage(image, for: .normal)
                    //cell.tintColor = UIColor(hexString: colour1!)
                    //cell.heartBtn.setImage(UIImage(named: "heartLiked")!, for: .normal)
                    
                    
                }else{
                    
                    var image = UIImage(named: "like")
                    image = image?.withRenderingMode(.alwaysOriginal)
                    let colour1 = UserDefaults.standard.string(forKey: "colour1")
                    cell.heartBtn.setImage(image, for: .normal)
                    //cell.tintColor = UIColor(hexString: colour1!)
                    
                    //cell.heartBtn.setImage(UIImage(named: "hearts")!, for: .normal)
                    
                }
                
                //                cell.typeImage.image = cell.typeImage.image?.withRenderingMode(.alwaysTemplate)
                //                let colour1 = UserDefaults.standard.string(forKey: "colour1")
                //                cell.typeImage.tintColor = UIColor(hexString: colour1!)
                //
                
                let imageAttachment =  NSTextAttachment()
                imageAttachment.image = UIImage(named:"clock-1")
                let imageOffsetY:CGFloat = -5.0;
                imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: imageAttachment.image!.size.width, height: imageAttachment.image!.size.height)
                let attachmentString = NSAttributedString(attachment: imageAttachment)
                var completeText = NSMutableAttributedString(string: "")
                completeText.append(attachmentString)
                
                let dateFormater = DateFormatter()
                dateFormater.dateFormat = "MM/dd/yy"
                let date = dateFormater.string(from: NSDate(timeIntervalSince1970: feed.created!) as Date)
                
                let  textAfterIconTop = NSMutableAttributedString(string: " \(date)")
                completeText.append(textAfterIconTop)
                cell.date.attributedText =  completeText
                
                
                cell.postSubCommentTF.text = ""
                cell.heartLabel.text = "\(feed.likes!)"
                cell.commentLabel.text = "\(feed.subCommentsCount!)"
                cell.postSubCommentTF.layer.borderWidth = 0.5
                cell.postSubCommentTF.layer.borderColor = UIColor.gray.cgColor
                cell.postSubCommentTF.placeholder = "Write a comment..."
                
                cell.heartBtn.tag = indexPath.row
                cell.commentBtn.tag  = indexPath.row
                cell.postSubCommentTF.tag = indexPath.row
                cell.postSubCommentTF.delegate = self
                cell.postSubCommentTF.returnKeyType = .done
                cell.image.tag = indexPath.row
                cell.moreButton.tag = indexPath.row
                

                cell.layer.shadowColor = UIColor.gray.cgColor
                cell.layer.shadowOffset = CGSize(width: 0, height: 1)
                cell.layer.shadowOpacity = 1
                cell.layer.shadowRadius = 1.0
                cell.clipsToBounds = false
                cell.layer.masksToBounds = false
                cell.layer.cornerRadius = 2.5
                
                if feed.fileType == "image" {
                    
                    
                    
                    cell.playIcon.isHidden = true
                    // Remove all existing targets (in case cell is being recycled)
                    cell.playIcon.removeTarget(nil, action: nil, for: .allEvents)
                    cell.image.af_setImage(
                        withURL: URL(string: feed.fileUrl!)! ,
                        placeholderImage: placeholderImage
                    )
                    
                    let imageTap = UITapGestureRecognizer(target: self, action: #selector(self.imageTapFunc(_:)))
                    imageTap.numberOfTapsRequired = 1
                    cell.image.isUserInteractionEnabled = true
                    cell.image.addGestureRecognizer(imageTap)
                    

                    
                }else{
                    
                    // Add target
                    //                    cell.playIcon.addTarget(self, action: #selector(playButtonTap(sender:)), for: .touchUpInside)
                    
                    cell.playIcon.isHidden = false
                    cell.image.af_setImage(
                        withURL: URL(string: feed.fileUrl!)! ,
                        placeholderImage: placeholderImage
                    )
                    let videoTap = UITapGestureRecognizer(target: self, action: #selector(self.videoTapFunc(_:)))
                    videoTap.numberOfTapsRequired = 1
                    cell.image.isUserInteractionEnabled = true
                    cell.image.addGestureRecognizer(videoTap)
                }
                
                cell.subCommentUserImage.af_setImage(
                    withURL: URL(string: (feed.subCommentArray?[0].userImageUrl!)!)! ,
                    placeholderImage: placeholderImage,
                    filter : filter
                )
                
                let subCommentString = NSMutableAttributedString()
                subCommentString
                    .bold((feed.subCommentArray?[0].userName)!)
                    .normal("  ")
                    .normal((feed.subCommentArray?[0].comment)!)
                
                cell.subComment.attributedText = subCommentString
                
                let dateSubComment = dateFormater.string(from: NSDate(timeIntervalSince1970: (feed.subCommentArray?[0].created)!) as Date)
                
                completeText = NSMutableAttributedString(string: "")
                completeText.append(attachmentString)
                let  textAfterIcon = NSMutableAttributedString(string: " \(dateSubComment)")
                completeText.append(textAfterIcon)
                cell.subCommentTime.attributedText =  completeText
                
                
                let upperBorder = CALayer()
                upperBorder.backgroundColor = UIColor.gray.cgColor
                upperBorder.frame = CGRect(x: 0, y: 0, width: cell.frame.width, height: 1.0)
                cell.subCommentView.layer.addSublayer(upperBorder)
                
                // Remove all existing targets (in case cell is being recycled)
                cell.heartBtn.removeTarget(nil, action: nil, for: .allEvents)
                cell.commentBtn.removeTarget(nil, action: nil, for: .allEvents)
                cell.moreButton.removeTarget(nil, action: nil, for: .allEvents)

                // Add target
                cell.heartBtn.addTarget(self, action: #selector(likeBtnTap(sender:)), for: .touchUpInside)
                cell.commentBtn.addTarget(self, action: #selector(commentBtnTap(sender:)), for: .touchUpInside)
                cell.moreButton.addTarget(self, action: #selector(moreButtonTap(sender:)), for: .touchUpInside)

                print("Returning cell")
                return cell
            }
            
        }
        print("Returning outer cell #wierd")
        return cell
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allFeeds.count
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = self.feedCollectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "eventsHeader", for: indexPath) as! EventsCollectionReusableView
        
        
        header.postFeedTV.layer.borderWidth = 0.8
        header.postFeedTV.layer.cornerRadius = 5
        header.postFeedTV.layer.borderColor = UIColor.gray.cgColor
        header.postFeedTV.placeholder = "Write Something ..."
        
        let writePostTap = UITapGestureRecognizer(target: self, action: #selector(self.writePostTap(_:)))
        writePostTap.numberOfTapsRequired = 1
        header.postFeedTV.isUserInteractionEnabled = true
        header.postFeedTV.addGestureRecognizer(writePostTap)
        
        return header
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        return CGSize(width: self.feedCollectionView.bounds.width, height: 52)
    }
    
    
    @objc func writePostTap(_ sender: UITapGestureRecognizer) {
        
        let mainVC = storyboard?.instantiateViewController(withIdentifier: "writePost") as! PostViewController
        mainVC.origin = "event"
        self.present( UINavigationController(rootViewController:mainVC), animated: true, completion: nil)
    }
    
    
    @objc func likeBtnTap(sender : UIButton){
        
        let currentIndex = sender.tag
        let userID = UserDefaults.standard.string(forKey: "userID")!
        let eventID = UserDefaults.standard.string(forKey: "eventID")!
        let userName = UserDefaults.standard.string(forKey: "name")!
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")!
        let commentID = UserDefaults.standard.string(forKey: "commentID")!

        //added event ID
        let parameters: Parameters = ["commentID" : commentID , "userID" : userID,"userName":userName,"invitationID":invitationID,"eventID":eventID , "postID" : self.allFeeds[currentIndex].commentID! , "type": self.allFeeds[currentIndex].type!, "likeID": self.allFeeds[currentIndex].likeID!,"eventID": self.allFeeds[currentIndex].eventID!]
        
        apiCallingMethod(apiEndPints: "like", method: .post, parameters: parameters) { (data1:Any?, totalobj:Int?) in
            if data1 != nil {
                self.getFeedsRequest()
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        let rowInt = textField.tag
        let commentID = self.allFeeds[rowInt].commentID!
        let string =  textField.text!
        if validateString(string: string){
            self.sendSubComment(commentID: commentID, string: string )
            
        }else{
            
            let alert = UIAlertController(title: "Minvitd", message: "Field cannot be empty", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: { (action : UIAlertAction) in
                alert.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
        
        return true
        
    }
    
    @objc func validateString(string : String) -> Bool{
        
        let charactersNew = String.trimmingCharacters(string)
        let stringNew = String(describing: charactersNew)
        if string == ""{
            return false
        }
        if stringNew == ""{
            return false
        }
        return true
    }
    
    @objc func sendSubComment (commentID : Int , string : String ){
        
        let invitaionID = UserDefaults.standard.string(forKey: "invitationID")
        let userName = UserDefaults.standard.string(forKey: "name")!
        let userID = UserDefaults.standard.string(forKey: "userID")
        let eventID = UserDefaults.standard.string(forKey: "eventID")

        let parameters: Parameters = ["comment" : string as AnyObject, "userID" : userID as AnyObject, "invitationID" : invitaionID as AnyObject, "commentID" : commentID as AnyObject ,"eventID" : eventID as AnyObject,"userName":userName]
        
        apiCallingMethod(apiEndPints: "comment", method: .post, parameters: parameters) { (data1:Any?, totalobj:Int?) in
            

                self.getFeedsRequest()
        }
    }
    
    @objc func commentBtnTap(sender : UIButton) {
        
        let currentIndex = sender.tag
      
        let feed  = allFeeds[currentIndex]
        if feed.fileType == "text"{
            
            print("*** Opening FeedCommentVC ***")
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let mainVC = storyBoard.instantiateViewController(withIdentifier: "feedCommentVC") as! FeedCommentVC
            mainVC.paramaters = feed
            mainVC.origin = "events"
            self.present( UINavigationController(rootViewController:mainVC), animated: true, completion: nil)
            
        }else {
            print("*** Opening FeedImageCommentVC ***")
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let mainVC = storyBoard.instantiateViewController(withIdentifier: "feedImageCommentVC") as! FeedImageCommentVC
            mainVC.paramaters = feed
            mainVC.origin = "events"
            self.present( UINavigationController(rootViewController:mainVC), animated: true, completion: nil)
        }
    }
    
    @objc func moreButtonTap(sender : UIButton) {
        
        let currentIndex = sender.tag
        let feed  = allFeeds[currentIndex]
        
        let parameters: Parameters = [ "eventID" : feed.eventID! as AnyObject]
        
        let url = MInvitdClient.Constants.BaseUrl + "comment"
        
        let alert = UIAlertController(title: "Alert", message: "Are you sure you want to delete this post", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: { (action : UIAlertAction) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        let authHeader = ["Authorization" : UserDefaults.standard.string(forKey: "userToken")!, "Content-Type" : "application/json" ]
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action : UIAlertAction) in
            Alamofire.request(url, method: .delete, parameters: parameters, encoding: JSONEncoding.default, headers: authHeader).responseJSON { (response) in
               
                if response.data != nil{

                    self.getFeedsRequest()
                }else{
                    self.dismissAlert("Error Returned by server")
                }
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func imageTapFunc(_ sender: UITapGestureRecognizer) {
        
        let rowInt  = sender.view?.tag
        let feed  = allFeeds[rowInt!]
        
        photos = [IDMPhoto]()
        let currentPhoto = IDMPhoto(url: URL(string : feed.fileUrl!))
        photos.append(currentPhoto!)
        browser = IDMPhotoBrowser.init(photos: photos)
        browser?.delegate = self
        browser?.displayCounterLabel = true
        browser?.displayActionButton = false
        browser?.doneButtonRightInset = CGFloat(5.0)
        browser?.doneButtonTopInset = CGFloat(30.0)
        browser?.doneButtonImage = UIImage(named: "cross")
        present(browser!, animated: true, completion: nil)

        
    }
    
    
    @objc func videoTapFunc(_ sender: UITapGestureRecognizer) {
        
        let rowInt  = sender.view?.tag
        let feed  = allFeeds[rowInt!]
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "videoPlayVC") as! VideoPlayViewController
        mainVC.url = feed.fileUrl!
        
        self.present(UINavigationController(rootViewController:mainVC), animated: true, completion: nil)
        
    }
    
    
}
