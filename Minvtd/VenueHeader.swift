//
//  VenueHeader.swift
//  Experiment
//
//  Created by admin on 13/11/17.
//  Copyright © 2017 admin. All rights reserved.
//

import UIKit
import AlamofireImage

class VenuHeaderView: UIView {
    
    var cellID = "hhhhhhh"
    var images:[MInvitdPhotos] = []
    
    var vc:VenueVC?
    
    let backButton = { () -> UIButton in
        let tableView = UIButton()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.tintColor = UIColor.white
        tableView.setImage(#imageLiteral(resourceName: "back").withRenderingMode(.alwaysTemplate), for: .normal)
        return tableView
    }()
    
    let backButtonView = { () -> UIView in
        let view = UIView()
        view.backgroundColor = UIColor.lightGray
        view.layer.cornerRadius = 15
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
        
    }()

    @objc func backAction(){
        //pushpopAnimation()
        vc?.pushpopAnimation()
        vc?.dismiss(animated: false, completion: nil)
    }
    
    @objc let appsCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.clear
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        appsCollectionView.delegate = self
        appsCollectionView.dataSource = self
        appsCollectionView.isPagingEnabled = true
        appsCollectionView.register(VenueimageCell.self, forCellWithReuseIdentifier: cellID)

        addSubview(appsCollectionView)
        
        appsCollectionView.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
        appsCollectionView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        appsCollectionView.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        appsCollectionView.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        
        // for back
        //back
        addSubview(backButtonView)

        backButtonView.topAnchor.constraint(equalTo: topAnchor, constant: 20).isActive = true
        backButtonView.leftAnchor.constraint(equalTo: leftAnchor, constant: 20).isActive = true
        backButtonView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        backButtonView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        backButtonView.addSubview(backButton)
        backButton.anchorCenterSuperview()
        backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension VenuHeaderView: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! VenueimageCell
        
        //print("\(images)")
        let img = URL(string: images[indexPath.row].fileUrl!)!

        cell.imageView.af_setImage(withURL:img)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width, height: frame.height)
    }
}

class VenueimageCell:UICollectionViewCell{
    
    let imageView = { () -> UIImageView in
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.tintColor = UIColor.white
        return img
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(imageView)
        imageView.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
        imageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        imageView.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        imageView.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
    }
    
    override func prepareForReuse() {
        imageView.image = nil
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
