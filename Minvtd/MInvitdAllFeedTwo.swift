//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//


import UIKit


struct MInvitdAllFeedTwo{
    
    
    // MARK: Properties
    
//    let objectID : Int?
//    let ownerID : Int?
    let invitationID : Int?
    let eventID : Int?
    let commentID : Int?
    let comment : String?
    let created : Double?
    let userName : String?
    let userImageUrl : String?
    var likes : Int?
    var liked : Bool?
    var subCommentsCount : Int?
    let fileUrl : String?
    let fileThumbnail : String?
    let fileType : String?
    var subCommentArray : [MInvtdAllFeedSubComment]?
    let imageUrl : String?
    let title : String?
    let description : String?
    let date : String?

    
    // MARK: Initializers
    
    // construct a ZXQKArticle from a dictionary
    init(dictionary: [String:AnyObject]) {
        
//        objectID = dictionary[MInvitdClient.JSONResponseKeys.objectID] as? Int
//        ownerID = dictionary[MInvitdClient.JSONResponseKeys.ownerID] as? Int
        invitationID = dictionary[MInvitdClient.JSONResponseKeys.invitationID] as? Int
        eventID = dictionary[MInvitdClient.JSONResponseKeys.eventID] as? Int
        commentID = dictionary[MInvitdClient.JSONResponseKeys.commentID] as? Int
        comment = dictionary[MInvitdClient.JSONResponseKeys.comment] as? String
        created = dictionary[MInvitdClient.JSONResponseKeys.created] as? Double
        userImageUrl = dictionary[MInvitdClient.JSONResponseKeys.userImageUrl] as? String
        userName = dictionary[MInvitdClient.JSONResponseKeys.userName] as? String
        likes = dictionary[MInvitdClient.JSONResponseKeys.likes] as? Int
        liked = dictionary[MInvitdClient.JSONResponseKeys.liked] as? Bool
        subCommentsCount = dictionary[MInvitdClient.JSONResponseKeys.subCommentsCount] as? Int
        fileUrl = dictionary[MInvitdClient.JSONResponseKeys.fileUrl] as? String
        fileThumbnail = dictionary[MInvitdClient.JSONResponseKeys.fileThumbnail] as? String
        fileType = dictionary[MInvitdClient.JSONResponseKeys.fileType] as? String
        description = dictionary[MInvitdClient.JSONResponseKeys.description] as? String
        title = dictionary[MInvitdClient.JSONResponseKeys.title] as? String
        date = dictionary[MInvitdClient.JSONResponseKeys.date] as? String
        imageUrl = dictionary[MInvitdClient.JSONResponseKeys.imageUrl] as? String
        
        
        if let subCommentArray = dictionary["subComments"] as? [AnyObject], subCommentArray.isEmpty == false {
            var temp1 = [MInvtdAllFeedSubComment]()
            for i in 0...subCommentArray.count-1 {
                let temp = MInvtdAllFeedSubComment(dictionary: subCommentArray[i] as! [String : AnyObject])
                temp1.append(temp)
            }
            self.subCommentArray = temp1
        } else {
            self.subCommentArray = nil
        }


        
    }
    
    static func allFeedsFromResults(_ results: [[String:AnyObject]]) -> [MInvitdAllFeedTwo] {
        var feeds = [MInvitdAllFeedTwo]()
        
        // iterate through array of dictionaries, each Article is a dictionary
        for feed in results {
            feeds.append(MInvitdAllFeedTwo(dictionary: feed))
        }
        
        return feeds
    }
}

