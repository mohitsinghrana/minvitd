//
//  ChatUsers.swift
//  Minvtd
//
//  Created by Vineet Singh on 12/08/18.
//  Copyright © 2018 Euro Infotek Pvt Ltd. All rights reserved.
//

import Foundation

struct ChatUsers {
    
    var userID: String?
    var name: String?
    var fileID: String?
    var fileUrl: String?
    
    init(dictionary: [String:Any]) {
        userID = dictionary["userID"] as? String
        name = dictionary["name"] as? String
        fileID = dictionary["fileID"] as? String
        fileUrl = dictionary["fileUrl"] as? String
    }
    
    static func dictionaryFromUsers(_ results: [ChatUsers]) -> [[String:String]] {
        
        var dictArray = [[String:String]]()
        // iterate through array of dictionaries, each Article is a dictionary
        for user in results {
            
            if let userID = user.userID{
                
                var guest1 = [String:String]()
                
                guest1["userID"] = userID
                
                if let name = user.name{
                    guest1["name"] = name
                }
                if let fileID = user.fileID{
                    guest1["fileID"] = fileID
                }
                if let fileUrl = user.fileUrl{
                    guest1["fileUrl"] = fileUrl
                }
                
                dictArray.append(guest1)
                
            }
        }
        
        return dictArray
    }
    
    static func guestsFromResults(_ results: [[String:Any]]) -> [ChatUsers] {
        var guests = [ChatUsers]()
        for guest in results {
            
            guests.append(ChatUsers(dictionary: guest))
        }
        
        
        return guests
    }
    
    
    
    
}
