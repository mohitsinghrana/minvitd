//
//  FeedSectionHeader.swift
//  Minvtd
//
//  Created by admin on 01/12/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit

class FeedSectionHeader:UITableViewHeaderFooterView {
    
    var VC:AllFeedViewController?

    lazy var textLabel1 = { () -> UIButton in
        let li = UIButton()
        li.setTitle("ALL", for: .normal)
        li.titleLabel?.textAlignment = .center
        li.titleLabel?.font = UIFont(name: "GothamRounded-Medium", size: 13)
        li.setTitleColor(UIColor.black, for: .normal)
        li.translatesAutoresizingMaskIntoConstraints = false
        return li
    }()
    
   lazy var textLabel2 = { () -> UIButton in
        let li = UIButton()
        li.setTitle("UPDATED", for: .normal)
        li.titleLabel?.textAlignment = .center
        li.setTitleColor(UIColor.black, for: .normal)
        li.titleLabel?.font = UIFont(name: "GothamRounded-Medium", size: 13)
        li.translatesAutoresizingMaskIntoConstraints = false
        return li
    }()
    
    let stack = { () -> UIStackView in
        let li = UIStackView()
        li.axis = .horizontal
        li.distribution = .fillEqually
        li.translatesAutoresizingMaskIntoConstraints = false
        return li
    }()

    let stack2 = { () -> UIStackView in
        let li = UIStackView()
        li.axis = .horizontal
        li.distribution = .fillEqually
        li.translatesAutoresizingMaskIntoConstraints = false
        return li
    }()
    
    let viewsep = UIView()
    let viewsep2 = UIView()

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        stack.addArrangedSubview(textLabel1)
        stack.addArrangedSubview(textLabel2)
        contentView.addSubview(stack)
        stack2.addArrangedSubview(viewsep)
        stack2.addArrangedSubview(viewsep2)
        contentView.addSubview(stack2)
        contentView.backgroundColor = UIColor.white
        
        stack.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 3, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        stack2.anchor(stack.bottomAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
//      viewsep.backgroundColor = UIColor.red

        viewsep2.tag = 1
        
        textLabel1.addTarget(self, action: #selector(actionswitch(_:)), for: .touchUpInside)
        textLabel2.addTarget(self, action: #selector(actionswitch(_:)), for: .touchUpInside)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
   @objc  func actionswitch(_ sender: Any) {
        let btn = sender as? UIButton
        
        let colour1 = UserDefaults.standard.string(forKey: "colour1")!
        
        if btn == textLabel1 {
            viewsep.backgroundColor = UIColor.init(hexString: colour1)
            viewsep2.backgroundColor = .clear
        }else {
            viewsep2.backgroundColor = UIColor.init(hexString: colour1)
            viewsep.backgroundColor = .clear
        }
    }

    
    @objc func configure(num:Int){
        
        let colour1 = UserDefaults.standard.string(forKey: "colour1")!
        
        if num == 0 {
            viewsep.backgroundColor = UIColor.init(hexString: colour1)
            viewsep2.backgroundColor = .clear
        }else {
            viewsep2.backgroundColor = UIColor.init(hexString: colour1)
            viewsep.backgroundColor = .clear
        }
    }
}
