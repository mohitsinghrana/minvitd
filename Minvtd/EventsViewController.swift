//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//


import UIKit
import Alamofire
import AlamofireImage
import SlideMenuControllerSwift

class EventsViewController: UIViewController {

    var events : [MInvitdEvent] = [MInvitdEvent]()
    
    //@objc let placeholderImage = UIImage(named: "eventCoverImage")!
    @objc let placeholderImage = UIImage(named: "myImage")!
    
 //   @objc let objectID : String = ""
    @objc var currentIndex : Int = 0
    @objc var eventID : Int = 0
    @objc var commentID : Int = 0
    @objc var guestID : Int = 0
    @objc var guestStatus : Int  = 0
    @objc var guestStatusName : String  = ""
    @objc var location :  String = ""
    var venus:[Venue] = []

    @IBOutlet weak var myCollectionView: UICollectionView!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var buttonStackView: UIStackView!
    @IBOutlet weak var inviteStatus: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBAction func dismissVC(_ sender: UIBarButtonItem) {
        
         slideMenuController()?.closeLeft()
        
         let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
         let leftDrawerVC = storyBoard.instantiateViewController(withIdentifier: "SlideMenuViewController") as! SlideMenuViewController
         leftDrawerVC.menuTag = 2
         let mainVC = storyBoard.instantiateViewController(withIdentifier: "tabVC")
         let vc = SlideMenuController(mainViewController: mainVC, leftMenuViewController: leftDrawerVC)
         pushPresentAnimation()
         self.present(vc, animated: false, completion: nil)
    }
    
    @IBAction func backVC(_ sender: UIBarButtonItem) {
        slideMenuController()?.closeLeft()
        self.dismiss(animated: false, completion: nil)
    }
    @IBAction func acceptBtn(_ sender: UIButton) {
        inviteResponse(status: 1)
        //self.inviteStatus.text = "Accepted"
    }
    @IBAction func rejectBtn(_ sender: UIButton) {
        inviteResponse(status: 2)
        //self.inviteStatus.text = "Rejected"
    }
    @IBAction func maybeBtn(_ sender: UIButton) {
        inviteResponse(status: 3)
        //self.inviteStatus.text = "Maybe"
    }
    
    
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var navigationBar: UINavigationBar!
    
    func setupNavBarContent(){
        
        
        let button1 = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(backVC(_:)))
        self.navigationItem.leftBarButtonItem = button1
        self.title = "Events"
        let button2 = UIBarButtonItem(title: "SKIP", style: .plain, target: self, action: #selector(dismissVC(_:)))
        self.navigationItem.rightBarButtonItem = button2
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        myCollectionView.delegate = self
        myCollectionView.dataSource = self
        
        self.bgImage.image = UIImage(named: "blurbg")

        navigationBarSetup(nil)
        setupNavBarContent()
        
        let bgImage : UIImage = self.imageLayerForGradientBackground(navBar:nil)
        self.buttonView.backgroundColor = UIColor(patternImage: bgImage)
    }
    
    
    @objc func getEvents(){
        
        let userID = UserDefaults.standard.string(forKey: "userID")!
        let userToken = UserDefaults.standard.string(forKey: "userToken")!
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")!
        
        
        apiCallingMethod(apiEndPints: "invitation", method: .get, parameters: ["userID":userID,"invitationID":invitationID]) { (data1:Any?, totalObjects:Int?) in
    
            
            if totalObjects == 0
            {
                //print("this is called")
                self.CreateAlertPopUp("Alert", "You have no events", OnTap: nil)
                return
            }
            
            if let data  = data1 as? [[String:Any]]  {
                //print("this is called")
                let mysettings = data[0]
                self.events = MInvitdEvent.eventsFromResults(mysettings["events"] as! [[String : AnyObject]])
                
                if let venues = mysettings["venues"] as? [[String : AnyObject]] {
                    self.venus = Venue.venueFromResults(venues)
                }
                
                DispatchQueue.main.async {
                    
                    self.eventID = self.events[self.currentIndex].eventID!
                    UserDefaults.standard.set(self.eventID, forKey: "eventID")
                    self.guestID = self.events[self.currentIndex].guestID!
                    UserDefaults.standard.set(self.events[self.currentIndex].name, forKey: "invitationName")
                    self.commentID = self.events[self.currentIndex].commentID!
                    self.guestStatus = self.events[self.currentIndex].guestStatus!
                    self.guestStatusName = self.events[self.currentIndex].guestStatusName!
                    self.location = self.events[self.currentIndex].coordinates!
                    self.navigationItem.title = self.events[self.currentIndex].name
                    self.pageControl.numberOfPages = self.events.count
                    
                        if self.guestStatus == 0 {
                        self.buttonStackView.isHidden  = false
                        self.inviteStatus.isHidden =  true
                    
                        }else if self.guestStatus == 1 {
                        self.buttonStackView.isHidden  = true
                        self.inviteStatus.isHidden =  false
                        self.inviteStatus.text = self.guestStatusName
                    
                        }else if self.guestStatus == 2{
                        self.buttonStackView.isHidden  = true
                        self.inviteStatus.isHidden =  false
                        self.inviteStatus.text = self.guestStatusName
                    
                        }else{
                        self.buttonStackView.isHidden  = true
                        self.inviteStatus.isHidden =  false
                        self.inviteStatus.text = self.guestStatusName
                    }
                    self.myCollectionView.reloadData()
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getEvents()
    }
    
    @objc func inviteResponse (status : Int ){
        
        let parameters: Parameters = ["guestID" : guestID as AnyObject, "status" : status as AnyObject]
    
        apiCallingMethod(apiEndPints: "guest/status", method: .post, parameters: parameters) { (data1, totalObject) in
            
            print(data1)
            self.getEvents()

            
//                DispatchQueue.main.async {
//
//                    self.events[self.currentIndex].guestStatus = status
//                    if status == 0 {
//                        self.buttonStackView.isHidden  = false
//                        self.inviteStatus.isHidden =  true
//
//                    }else if status == 1 {
//                        self.buttonStackView.isHidden  = true
//                        self.inviteStatus.isHidden =  false
//                        self.inviteStatus.text = "Accepted"
//                        self.events[self.currentIndex].guestStatusName = "Accepted"
//
//
//                    }else if status == 2{
//                        self.buttonStackView.isHidden  = true
//                        self.inviteStatus.isHidden =  false
//                        self.inviteStatus.text = "Declined"
//                        self.events[self.currentIndex].guestStatusName = "Declined"
//
//
//                    }else{
//                        self.buttonStackView.isHidden  = true
//                        self.inviteStatus.isHidden =  false
//                        self.inviteStatus.text = "Maybe"
//                        self.events[self.currentIndex].guestStatusName = "Maybe"
//
//                    }
//                    self.getEvents()
//                    self.myCollectionView.reloadData()
//                }
            }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let center = CGPoint(x: scrollView.contentOffset.x + (scrollView.frame.width / 2), y: (scrollView.frame.height / 2))
        if let ip = self.myCollectionView.indexPathForItem(at: center) {
            self.currentIndex = ip.row
            self.eventID = events[self.currentIndex].eventID!
            self.guestID = events[self.currentIndex].guestID!
            self.guestStatus = events[self.currentIndex].guestStatus!
            self.guestStatusName = events[self.currentIndex].guestStatusName!
            self.location = events[self.currentIndex].coordinates!
            self.pageControl.currentPage =  self.currentIndex
            self.inviteStatus.isHidden = true
            self.buttonStackView.isHidden = false
            self.setInviteBar()
            
            self.pageControl.currentPage =  self.currentIndex
            let stringEventName = (events[self.currentIndex].name)?.replacingOccurrences(of: "amp;", with: "")
            self.navigationItem.title = stringEventName
        }
    }

    @objc func setInviteBar()
    {
        if self.guestStatus == 0 {
           self.buttonStackView.isHidden  = false
           self.inviteStatus.isHidden =  true
            
        }else if self.guestStatus == 1 {
            self.buttonStackView.isHidden  = true
            self.inviteStatus.isHidden =  false
            self.inviteStatus.text = self.guestStatusName
            
        }else if self.guestStatus == 2{
            self.buttonStackView.isHidden  = true
            self.inviteStatus.isHidden =  false
            self.inviteStatus.text = self.guestStatusName

        }else{
            self.buttonStackView.isHidden  = true
            self.inviteStatus.isHidden =  false
            self.inviteStatus.text = self.guestStatusName
        }
    }
}

extension EventsViewController : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let bounds = collectionView.bounds
        let width = bounds.width
        let height = bounds.height
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let event = events[(indexPath as NSIndexPath).row]
        let imageCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageEventCell", for: indexPath) as! ImageEventCell
        let imageOnlyCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageOnlyEventCell", for: indexPath) as! ImageOnlyEventCell
        let videoCell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoEventCell", for: indexPath) as! VideoEventCell
        let webCell = collectionView.dequeueReusableCell(withReuseIdentifier: "WebEventCell", for: indexPath) as! WebEventCell
        
        if event.category == "image"
        {
            
            imageCell.circleImage.layer.cornerRadius = (imageCell.circleImage.frame.width) / 2;
            imageCell.circleImage.layer.borderWidth = 2
            imageCell.circleImage.layer.borderColor = UIColor.gray.cgColor
            imageCell.circleImage.clipsToBounds = true
            
            if event.fileUrl != "" || event.fileUrl != nil
            {
                imageCell.image.af_setImage(
                    withURL: URL(string: event.fileUrl!)! ,
                    placeholderImage: placeholderImage
                )
            }
                
            else{
                imageCell.image.image = placeholderImage
            }
            if event.personThumbnail != "" || event.personThumbnail != nil
            {
                imageCell.circleImage.af_setImage(
                    withURL: URL(string: event.personThumbnail!)! ,
                    placeholderImage: placeholderImage
                )
            }
                
            else{
                imageCell.circleImage.image = placeholderImage
            }
            
            let stringEventDescription = (event.description)?.replacingOccurrences(of: "amp;", with: "")
            imageCell.descript.text = stringEventDescription
            let stringEventName = (event.name)?.replacingOccurrences(of: "amp;", with: "")
            imageCell.eventName.text = stringEventName
            imageCell.imageEventName.text = stringEventName
            imageCell.venue.text = event.venue
            imageCell.venueCircle.text = event.venue
            imageCell.date.text = event.date
            imageCell.time.text = event.time
            imageCell.commentLabel.text = "\(event.commentsCount!)"
            imageCell.likedLabel.text = "\(event.likesCount!)"
            
            
            if event.liked! == true{
                
                var image = UIImage(named: "likeFilled")
                image = image?.withRenderingMode(.alwaysOriginal)
                //let colour1 = UserDefaults.standard.string(forKey: "colour1")
                DispatchQueue.main.async {
                    
                    imageCell.heartBtn.setImage(image, for: .normal)
                    //imageCell.heartBtn.tintColor = UIColor(hexString: colour1!)
                }
                
            }else{
                var image = UIImage(named: "like")
                image = image?.withRenderingMode(.alwaysOriginal)
                //let colour1 = UserDefaults.standard.string(forKey: "colour1")
                
                DispatchQueue.main.async {
                    
                    imageCell.heartBtn.setImage(image, for: .normal)
                    //imageCell.heartBtn.tintColor = UIColor(hexString: colour1!)
                }
            }

            // Remove all existing targets (in case cell is being recycled)
            imageCell.heartBtn.removeTarget(nil, action: nil, for: .allEvents)
            imageCell.statusChangeBtn.removeTarget(nil, action: nil, for: .allEvents)

            // Add target
            imageCell.heartBtn.addTarget(self, action: #selector(likeBtnTap(sender:)), for: .touchUpInside)
            imageCell.statusChangeBtn.addTarget(self, action: #selector(statusChange(sender:)), for: .touchUpInside)

            let commentTap = UITapGestureRecognizer(target: self, action: #selector(self.commentBtnTap(_:)))
            commentTap.numberOfTapsRequired = 1
            imageCell.commentBtn.isUserInteractionEnabled = true
            imageCell.commentBtn.addGestureRecognizer(commentTap)
            
            let scheduleTap = UITapGestureRecognizer(target: self, action: #selector(self.scheduleBtnTap(_:)))
            scheduleTap.numberOfTapsRequired = 1
            imageCell.scheduleBtn.isUserInteractionEnabled = true
            imageCell.scheduleBtn.addGestureRecognizer(scheduleTap)
            
            let locationTap = UITapGestureRecognizer(target: self, action: #selector(self.venueTapDetected(_:)))
            locationTap.numberOfTapsRequired = 1
            imageCell.locationBtn.isUserInteractionEnabled = true
            imageCell.locationBtn.addGestureRecognizer(locationTap)
            
            if event.guestStatus == 0 {
                imageCell.statusButton.backgroundColor = UIColor.init(hexString: "#F18037")
            }else if event.guestStatus == 1 {
                imageCell.statusButton.backgroundColor = UIColor.init(hexString: "#61BCA7")
                
            }else if event.guestStatus == 2{
                imageCell.statusButton.backgroundColor = UIColor.init(hexString: "#DA4C45")
            }else{
                imageCell.statusButton.backgroundColor = UIColor.init(hexString: "#2C7EC3")
            }
            imageCell.statusButton.layer.cornerRadius = imageCell.statusButton.bounds.size.width/2
            
            return imageCell
        }
        else if event.category == "image_text"
        {
            if event.fileUrl != "" || event.fileUrl != nil
            {
                imageOnlyCell.image.af_setImage(
                    withURL: URL(string: event.fileUrl!)! ,
                    placeholderImage: placeholderImage
                )
                
            }
            else{
                imageOnlyCell.image.image = placeholderImage
            }
            
            //circular Image Setup;
            
            imageOnlyCell.circleImage.layer.cornerRadius = (imageCell.circleImage.frame.width) / 2;
            imageOnlyCell.circleImage.layer.borderWidth = 2
            imageOnlyCell.circleImage.layer.borderColor = UIColor.gray.cgColor
            imageOnlyCell.circleImage.clipsToBounds = true
            
            
            if event.personThumbnail != "" || event.personThumbnail != nil
            {
                imageOnlyCell.circleImage.af_setImage(
                    withURL: URL(string: event.personThumbnail!)! ,
                    placeholderImage: placeholderImage
                )
                
            }
            else{
                imageOnlyCell.circleImage.image = placeholderImage
            }
            imageOnlyCell.image.contentMode = .scaleAspectFill
            imageOnlyCell.image.clipsToBounds = true
            let stringEventDescription = (event.description)?.replacingOccurrences(of: "amp;", with: "")
            imageOnlyCell.descript.text = stringEventDescription
            imageOnlyCell.imageOnlyEventName.text = event.name
            imageOnlyCell.venueCircle.text = event.venue
            imageOnlyCell.commentLabel.text = "\(event.commentsCount!)"
            imageOnlyCell.likedLabel.text = "\(event.likesCount!)"
            
            
            if event.liked! == true{
                
                var image = UIImage(named: "likeFilled")
                image = image?.withRenderingMode(.alwaysOriginal)
                let colour1 = UserDefaults.standard.string(forKey: "colour1")
                DispatchQueue.main.async {
                    
                    imageOnlyCell.heartBtn.setImage(image, for: .normal)
                    //imageOnlyCell.heartBtn.tintColor = UIColor(hexString: colour1!)
                }
                
            }else{
                var image = UIImage(named: "like")
                image = image?.withRenderingMode(.alwaysOriginal)
                let colour1 = UserDefaults.standard.string(forKey: "colour1")
                
                DispatchQueue.main.async {
                    
                    imageOnlyCell.heartBtn.setImage(image, for: .normal)
                    //imageOnlyCell.heartBtn.tintColor = UIColor(hexString: colour1!)
                }
            }

            let imageTap = UITapGestureRecognizer(target: self, action: #selector(self.imageOnlyCellImageTap(_:)))
            imageTap.numberOfTapsRequired = 1
            imageOnlyCell.image.isUserInteractionEnabled = true
            imageOnlyCell.image.addGestureRecognizer(imageTap)
            
            // Remove all existing targets (in case cell is being recycled)
            imageOnlyCell.heartBtn.removeTarget(nil, action: nil, for: .allEvents)
            imageOnlyCell.statusChangeBtn.removeTarget(nil, action: nil, for: .allEvents)

            // Add target
            imageOnlyCell.heartBtn.addTarget(self, action: #selector(likeBtnTap(sender:)), for: .touchUpInside)
            imageOnlyCell.statusChangeBtn.addTarget(self, action: #selector(statusChange(sender:)), for: .touchUpInside)

            
            let commentTap = UITapGestureRecognizer(target: self, action: #selector(self.commentBtnTap(_:)))
            commentTap.numberOfTapsRequired = 1
            imageOnlyCell.commentBtn.isUserInteractionEnabled = true
            imageOnlyCell.commentBtn.addGestureRecognizer(commentTap)
            
            let scheduleTap = UITapGestureRecognizer(target: self, action: #selector(self.scheduleBtnTap(_:)))
            scheduleTap.numberOfTapsRequired = 1
            imageOnlyCell.scheduleBtn.isUserInteractionEnabled = true
            imageOnlyCell.scheduleBtn.addGestureRecognizer(scheduleTap)
            
            let locationTap = UITapGestureRecognizer(target: self, action: #selector(self.venueTapDetected(_:)))
            locationTap.numberOfTapsRequired = 1
            imageOnlyCell.locationBtn.isUserInteractionEnabled = true
            imageOnlyCell.locationBtn.addGestureRecognizer(locationTap)
            
            if event.guestStatus == 0 {
                imageOnlyCell.statusButton.backgroundColor = UIColor.init(hexString: "#F18037")
            }else if event.guestStatus == 1 {
                imageOnlyCell.statusButton.backgroundColor = UIColor.init(hexString: "#61BCA7")
                
            }else if event.guestStatus == 2{
                imageOnlyCell.statusButton.backgroundColor = UIColor.init(hexString: "#DA4C45")
            }else{
                imageOnlyCell.statusButton.backgroundColor = UIColor.init(hexString: "#2C7EC3")
            }
            imageOnlyCell.statusButton.layer.cornerRadius = imageOnlyCell.statusButton.bounds.size.width/2
            
            return imageOnlyCell
        }
        else if  event.category == "video"
        {
            
            //circular Image Setup;
            
            videoCell.circleImage.layer.cornerRadius = (imageCell.circleImage.frame.width) / 2;
            videoCell.circleImage.layer.borderWidth = 2
            videoCell.circleImage.layer.borderColor = UIColor.gray.cgColor
            videoCell.circleImage.clipsToBounds = true
            
            
            if event.personThumbnail != "" || event.personThumbnail != nil
            {
                videoCell.circleImage.af_setImage(
                    withURL: URL(string: event.personThumbnail!)! ,
                    placeholderImage: placeholderImage
                )
                
            }
            else{
                videoCell.circleImage.image = placeholderImage
            }
            
            
            let stringEventDescription = (event.description)?.replacingOccurrences(of: "amp;", with: "")
            videoCell.descript.text = stringEventDescription
            let stringEventName = (event.name)?.replacingOccurrences(of: "amp;", with: "")
            videoCell.eventName.text = stringEventName
            videoCell.videoEventName.text = stringEventName
            videoCell.venue.text = event.venue
            videoCell.venueCircle.text = event.venue
            videoCell.date.text = event.date
            videoCell.time.text = event.time
            
            videoCell.commentLabel.text = "\(event.commentsCount!)"
            videoCell.likedLabel.text = "\(event.likesCount!)"
            
            if event.liked! == true{
                
                var image = UIImage(named: "likeFilled")
                image = image?.withRenderingMode(.alwaysOriginal)
                let colour1 = UserDefaults.standard.string(forKey: "colour1")
                DispatchQueue.main.async {
                    
                    videoCell.heartBtn.setImage(image, for: .normal)
                }
                
            }else{
                var image = UIImage(named: "like")
                image = image?.withRenderingMode(.alwaysOriginal)
                let colour1 = UserDefaults.standard.string(forKey: "colour1")
                
                DispatchQueue.main.async {
                    
                    videoCell.heartBtn.setImage(image, for: .normal)
                }
            }

            let myURL = URL(string: event.fileUrl!)
            let myRequest = URLRequest(url: myURL!)
            videoCell.videoView.loadRequest(myRequest)
            
            // Remove all existing targets (in case cell is being recycled)
            videoCell.heartBtn.removeTarget(nil, action: nil, for: .allEvents)
            videoCell.statusChangeBtn.removeTarget(nil, action: nil, for: .allEvents)

            // Add target
            videoCell.heartBtn.addTarget(self, action: #selector(likeBtnTap(sender:)), for: .touchUpInside)
            videoCell.statusChangeBtn.addTarget(self, action: #selector(statusChange(sender:)), for: .touchUpInside)

            
            let commentTap = UITapGestureRecognizer(target: self, action: #selector(self.commentBtnTap(_:)))
            commentTap.numberOfTapsRequired = 1
            videoCell.commentBtn.isUserInteractionEnabled = true
            videoCell.commentBtn.addGestureRecognizer(commentTap)
            
            let scheduleTap = UITapGestureRecognizer(target: self, action: #selector(self.scheduleBtnTap(_:)))
            scheduleTap.numberOfTapsRequired = 1
            videoCell.scheduleBtn.isUserInteractionEnabled = true
            videoCell.scheduleBtn.addGestureRecognizer(scheduleTap)
            
            let locationTap = UITapGestureRecognizer(target: self, action: #selector(self.venueTapDetected(_:)))
            locationTap.numberOfTapsRequired = 1
            videoCell.locationBtn.isUserInteractionEnabled = true
            videoCell.locationBtn.addGestureRecognizer(locationTap)
            

            if event.guestStatus == 0 {
                videoCell.statusButton.backgroundColor = UIColor.init(hexString: "#F18037")
            }else if event.guestStatus == 1 {
                videoCell.statusButton.backgroundColor = UIColor.init(hexString: "#61BCA7")
                
            }else if event.guestStatus == 2{
                videoCell.statusButton.backgroundColor = UIColor.init(hexString: "#DA4C45")
            }else{
                videoCell.statusButton.backgroundColor = UIColor.init(hexString: "#2C7EC3")
            }
            videoCell.statusButton.layer.cornerRadius = videoCell.statusButton.bounds.size.width/2
            
            return videoCell
        }
        else if event.category == "web"
        {
            
            //circular Image Setup;
            
            webCell.circleImage.layer.cornerRadius = (imageCell.circleImage.frame.width) / 2;
            webCell.circleImage.layer.borderWidth = 2
            webCell.circleImage.layer.borderColor = UIColor.gray.cgColor
            webCell.circleImage.clipsToBounds = true
            
            
            if event.personThumbnail != "" || event.personThumbnail != nil
            {
                webCell.circleImage.af_setImage(
                    withURL: URL(string: event.personThumbnail!)! ,
                    placeholderImage: placeholderImage
                )
            }
            else{
                webCell.circleImage.image = placeholderImage
            }
            
            let stringEventDescription = (event.description)?.replacingOccurrences(of: "amp;", with: "")
            webCell.descript.text = stringEventDescription
            webCell.commentLabel.text = "\(event.commentsCount!)"
            webCell.likedLabel.text = "\(event.likesCount!)"
            
            webCell.webEventName.text = event.name
            webCell.venueCircle.text = event.venue

            if event.liked! == true{
                
                var image = UIImage(named: "likeFilled")
                image = image?.withRenderingMode(.alwaysOriginal)
                let colour1 = UserDefaults.standard.string(forKey: "colour1")
                DispatchQueue.main.async {
                    
                    webCell.heartBtn.setImage(image, for: .normal)
                    //webCell.heartBtn.tintColor = UIColor(hexString: colour1!)
                }
                
            }else{
                var image = UIImage(named: "like")
                image = image?.withRenderingMode(.alwaysOriginal)
                let colour1 = UserDefaults.standard.string(forKey: "colour1")
                
                DispatchQueue.main.async {
                    
                    webCell.heartBtn.setImage(image, for: .normal)
                    //webCell.heartBtn.tintColor = UIColor(hexString: colour1!)
                }
            }
            let myURL = URL(string: event.fileUrl!)
            let myRequest = URLRequest(url: myURL!)
            webCell.webView.loadRequest(myRequest)
            
            // Remove all existing targets (in case cell is being recycled)
            webCell.heartBtn.removeTarget(nil, action: nil, for: .allEvents)
            webCell.statusChangeBtn.removeTarget(nil, action: nil, for: .allEvents)

            // Add target
            webCell.heartBtn.addTarget(self, action: #selector(likeBtnTap(sender:)), for: .touchUpInside)
            webCell.statusChangeBtn.addTarget(self, action: #selector(statusChange(sender:)), for: .touchUpInside)

            let commentTap = UITapGestureRecognizer(target: self, action: #selector(self.commentBtnTap(_:)))
            commentTap.numberOfTapsRequired = 1
            webCell.commentBtn.isUserInteractionEnabled = true
            webCell.commentBtn.addGestureRecognizer(commentTap)
            
            let scheduleTap = UITapGestureRecognizer(target: self, action: #selector(self.scheduleBtnTap(_:)))
            scheduleTap.numberOfTapsRequired = 1
            webCell.scheduleBtn.isUserInteractionEnabled = true
            webCell.scheduleBtn.addGestureRecognizer(scheduleTap)
            
            let locationTap = UITapGestureRecognizer(target: self, action: #selector(self.venueTapDetected(_:)))
            locationTap.numberOfTapsRequired = 1
            webCell.locationBtn.isUserInteractionEnabled = true
            webCell.locationBtn.addGestureRecognizer(locationTap)
            
//            webCell.mainView.layer.cornerRadius = 10
//            webCell.mainView.clipsToBounds = true
            
        
            if event.guestStatus == 0 {
                webCell.statusButton.backgroundColor = UIColor.init(hexString: "#F18037")
            }else if event.guestStatus == 1 {
                webCell.statusButton.backgroundColor = UIColor.init(hexString: "#61BCA7")
                
            }else if event.guestStatus == 2{
                webCell.statusButton.backgroundColor = UIColor.init(hexString: "#DA4C45")
            }else{
                webCell.statusButton.backgroundColor = UIColor.init(hexString: "#2C7EC3")
            }
            webCell.statusButton.layer.cornerRadius = webCell.statusButton.bounds.size.width/2
            
            return webCell
        }
            
        else{
            
            return imageCell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return events.count
    }
    
    @objc func likeBtnTap(sender : UIButton){
        
        
        let userID = UserDefaults.standard.string(forKey: "userID")!
        let userName = UserDefaults.standard.string(forKey: "name")!
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")!
        let invitationName = UserDefaults.standard.string(forKey: "invitationTitle")
      

        let parameters: Parameters = ["commentID" : self.events[self.currentIndex].commentID as AnyObject, "userID" : userID as AnyObject,"userName":userName,"invitationID":invitationID , "postID" : "0", "type": self.events[self.currentIndex].type!, "likeID": self.events[self.currentIndex].likeID!, "iName": invitationName!,"eventID": eventID]
        
        
        apiCallingMethod(apiEndPints: "like", method: .post, parameters: parameters) { (data1:Any?, totalobjects:Int?) in
            
            self.getEvents()

        }

    }
    
    @objc func statusChange(sender : UIButton){
        
        let alertController = UIAlertController(title: "Minvitd", message: "Change your response", preferredStyle: .actionSheet)
        
        if self.events[self.currentIndex].guestStatus == 1 {
            
            // Create the actions
            let accept  = UIAlertAction(title: "Accept", style: UIAlertActionStyle.destructive) {
                UIAlertAction in
                self.changeStatus(status: "1")
                
            }
            let reject = UIAlertAction(title: "Reject", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.changeStatus(status: "2")
                
            }
            let maybe = UIAlertAction(title: "Maybe", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.changeStatus(status: "3")
                
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                // when cancel is tapped
            }
            
            // Add the actions
            alertController.addAction(accept)
            alertController.addAction(reject)
            alertController.addAction(maybe)
            alertController.addAction(cancelAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
            
            
            
        }else if self.events[self.currentIndex].guestStatus == 2 {
            // Create the actions
            let accept  = UIAlertAction(title: "Accept", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.changeStatus(status: "1")
                
            }
            let reject = UIAlertAction(title: "Reject", style: UIAlertActionStyle.destructive) {
                UIAlertAction in
                self.changeStatus(status: "2")
                
            }
            let maybe = UIAlertAction(title: "Maybe", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.changeStatus(status: "3")
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                // when cancel is tapped
            }
            
            // Add the actions
            alertController.addAction(accept)
            alertController.addAction(reject)
            alertController.addAction(maybe)
            alertController.addAction(cancelAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
            
            
            
        }else if self.events[self.currentIndex].guestStatus == 3 {
            
            // Create the actions
            let accept  = UIAlertAction(title: "Accept", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.changeStatus(status: "1")
                
            }
            let reject = UIAlertAction(title: "Reject", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.changeStatus(status: "2")
                
            }
            let maybe = UIAlertAction(title: "Maybe", style: UIAlertActionStyle.destructive) {
                UIAlertAction in
                self.changeStatus(status: "3")
                
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                // when cancel is tapped
            }
            
            // Add the actions
            alertController.addAction(accept)
            alertController.addAction(reject)
            alertController.addAction(maybe)
            alertController.addAction(cancelAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
            
            
            
        }else{
            
            // Create the actions
            let accept  = UIAlertAction(title: "Accept", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.changeStatus(status: "1")
                
            }
            let reject = UIAlertAction(title: "Reject", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.changeStatus(status: "2")
                
            }
            let maybe = UIAlertAction(title: "Maybe", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.changeStatus(status: "3")
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                // when cancel is tapped
            }
            
            // Add the actions
            alertController.addAction(accept)
            alertController.addAction(reject)
            alertController.addAction(maybe)
            alertController.addAction(cancelAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
            
        }
        
    }
    
    
    @objc func changeStatus(status : String){
        
        let guestID = self.events[self.currentIndex].guestID
        
        let parameters: Parameters = ["guestID" : guestID as AnyObject, "status" : status as AnyObject]
        
        
        apiCallingMethod(apiEndPints: "guest/status", method: .post, parameters: parameters) { (data1:Any?, totalObjects:Int?) in
            
            if data1 != nil {
                self.getEvents()
            }}
        
    }
    
    @objc func commentBtnTap(_ sender: UITapGestureRecognizer) {
        
        UserDefaults.standard.set(self.eventID, forKey: "eventID")
        UserDefaults.standard.set(self.commentID, forKey: "commentID")

        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "eventCommentVC") as! EventCommentViewController
        self.present(UINavigationController(rootViewController: mainVC), animated: true, completion: nil)
    }
    
    
    @objc func scheduleBtnTap(_ sender: UITapGestureRecognizer) {
        
        
        UserDefaults.standard.set(self.eventID, forKey: "eventID")
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let category = UserDefaults.standard.string(forKey: "category")
        
        
        if category == "professional" {
            
            let mainVC = storyBoard.instantiateViewController(withIdentifier: "eventScheduleVC") as! EventScheduleViewController
            self.present(mainVC, animated: true, completion: nil)
            
        }else {
            
            let vc = storyBoard.instantiateViewController(withIdentifier: "PersonalSchedularTableVC")
            
            self.present(vc, animated: true, completion: nil)
            // expandable
        }}
    
    
//    @objc func locationBtnTap(_ sender: UITapGestureRecognizer) {
//
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let mainVC = storyBoard.instantiateViewController(withIdentifier: "eventLocationVC") as! EventLocationViewController
//        mainVC.location = self.location
//
//        self.present(UINavigationController(rootViewController:mainVC), animated: true, completion: nil)
//    }
    
    @objc func imageOnlyCellImageTap(_ sender: UITapGestureRecognizer) {
        
        
        let event = self.events[self.currentIndex]
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "popUpImageCellVC") as! PopUpImageOnlyCellVC
        mainVC.event = event
        mainVC.modalPresentationStyle = .overCurrentContext
        mainVC.modalTransitionStyle = .crossDissolve
        self.present(mainVC, animated: true, completion: nil)
    }
    
    
    @objc func venueTapDetected(_ sender: UITapGestureRecognizer) {
        let mainVC = VenueVC()
        mainVC.currentIndex = currentIndex
        mainVC.arr = venus
        pushPresentAnimation()
        present(mainVC, animated: false, completion: nil)

    }


    
}


