//
//  AFICommentCVC.swift
//  Minvtd
//
//  Created by Tushar Aggarwal on 06/06/17.
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import Foundation
import UIKit

class AFICommentCVC : UICollectionViewCell {
    
    @IBOutlet weak var subUserImage: UIImageView!
    
    @IBOutlet weak var subUserName: UILabel!
    
    @IBOutlet weak var subComment: UILabel!
    @IBOutlet weak var date: UILabel!
    
    @IBOutlet weak var deleteBtn: UIButton!
    
    @IBOutlet weak var clockIcon: UIImageView!
    //Subcomment Like setup:
    
    let likeBtnView = { () -> UIView in
        let lab = UIView()
        
        return lab
    }()
    
    let likeButton = { () -> UIButton in
        let btn = UIButton(type: .system)
        let colour1 = UserDefaults.standard.string(forKey: "colour1")
        //btn.tintColor = UIColor.init(hexString: colour1!)
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        return btn
    }()
    
    let likeLblview: UIView = {
        let temp = UIView()
        return temp
    }()
    
    let likeLabel = { () -> UILabel in
        let lab = UILabel()
        lab.text = "234"
        lab.textColor = UIColor.init(hexString: "#9d9d9d")
        lab.font = UIFont(name: "GothamRounded-Book", size: 12)
        return lab
    }()
    
    private lazy var likeStack = { () -> UIStackView in
        let base = UIStackView(arrangedSubviews: [likeBtnView,likeLblview])
        base.axis = .horizontal
        base.distribution = .fillEqually
        base.spacing = 0
        return base
    }()
    
    //Subcomment Comment setup:
    
    let commentButton = { () -> UIButton in
        let btn = UIButton(type: .system)
        btn.setImage(#imageLiteral(resourceName: "msg").withRenderingMode(.alwaysOriginal), for: .normal)
        //btn.tintColor = UIColor.init(hexString: "#9d9d9d")
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        return btn
    }()
    
    let commentLabel = { () -> UILabel in
        let lab = UILabel()
        lab.text = "234"
        lab.textColor = UIColor.init(hexString: "#9d9d9d")
        lab.font = UIFont(name: "GothamRounded-Book", size: 12)
        return lab
    }()
    
    let commentLblview: UIView = {
        let temp = UIView()
        return temp
    }()
    
    let commentBtnView = { () -> UIView in
        let btnView = UIView()
        return btnView
    }()
    
    private lazy var commentStack = { () -> UIStackView in
        let base = UIStackView(arrangedSubviews: [commentBtnView,commentLblview])
        base.axis = .horizontal
        base.distribution = .fillEqually
        base.spacing = 0
        return base
    }()
    
    private lazy var leftStack = { () -> UIStackView in
        let base = UIStackView(arrangedSubviews: [likeStack,commentStack])
        base.axis = .horizontal
        base.distribution = .fillEqually
        return base
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        addSubview(leftStack)
        likeBtnView.addSubview(likeButton)
        likeLblview.addSubview(likeLabel)
        commentBtnView.addSubview(commentButton)
        commentLblview.addSubview(commentLabel)
        
        // like frame here
        likeButton.widthAnchor.constraint(equalToConstant: 18).isActive = true
        likeButton.heightAnchor.constraint(equalToConstant: 18).isActive = true
        likeButton.anchorCenterSuperview()
        likeLabel.feedCommentCVsNumberHeight()
        
        // comment frame here
        commentButton.widthAnchor.constraint(equalToConstant: 18).isActive = true
        commentButton.heightAnchor.constraint(equalToConstant: 18).isActive = true
        commentButton.anchorCenterSuperview()
        commentLabel.feedCommentCVsNumberHeight()
        
        leftStack.anchor(subComment.bottomAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 8, leftConstant: 29, bottomConstant: 8, rightConstant: 20, widthConstant: 0, heightConstant: 35)
        
        clockIcon.image = #imageLiteral(resourceName: "clock").withRenderingMode(.alwaysTemplate)
        clockIcon.tintColor = UIColor.lightGray
        
    }

}
