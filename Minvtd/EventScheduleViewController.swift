//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import UIKit
import Alamofire

class EventScheduleViewController : UIViewController {
    
    var schedules: [MInvitdSchedule] = [MInvitdSchedule]()
    @objc var expandedRows = Set<Int>()

    @IBOutlet weak var navigationBar: UINavigationBar!
    
    @IBOutlet weak var myCollectionView: UITableView!

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    @IBAction func dismissVC(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        
        myCollectionView.delegate = self
        myCollectionView.dataSource = self
        self.myCollectionView.rowHeight = UITableViewAutomaticDimension
        
        navigationBarSetup(navigationBar)
        
        let colour1 = UserDefaults.standard.string(forKey: "colour1")
        let colour2 = UserDefaults.standard.string(forKey: "colour2")
        
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.setStatusBarBackgroundColor(color: UIColor(hexString: colour1!)!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        scheduleApi()
    }
    
    @objc func scheduleApi(){
        let eventID = UserDefaults.standard.string(forKey: "eventID")!
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")!

        apiCallingMethod(apiEndPints: "schedule", method: .get, parameters: ["invitationID" : invitationID, "eventID" : eventID]) { (data1:Any?, totalObjects:Int?) in

            if totalObjects == 0 {
                //self.CreateAlertPopUp("MInvtd", "Schedule not yet specified", OnTap: nil)
                return
            }
            
            if data1 != nil {
                if let data = data1 as? [[String : Any]]
                {
                    let schedules = MInvitdSchedule.schedulesFromResults(data as [[String : AnyObject]])
                    self.schedules = schedules
                    DispatchQueue.main.async {
                        self.myCollectionView.reloadData()
                    }
                }
            }
        }
    }
}

// MARK: - EventScheduleViewController: UICollectionViewDelegate, UICollectionViewDataSource

extension EventScheduleViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let schedule = schedules[(indexPath as NSIndexPath).row]
        let cell = self.myCollectionView.dequeueReusableCell(withIdentifier: "eventScheduleCell", for: indexPath) as! EventScheduleCollectionViewCell
        
        cell.name.text = schedule.name
        cell.time.text = schedule.time
        let color = UserDefaults.standard.string(forKey: "colour1")
        cell.colorView.backgroundColor = UIColor.init(hexString: color!)
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.schedules.count
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let schedule  = schedules[(indexPath as NSIndexPath).row]

        let mainVC = storyboard?.instantiateViewController(withIdentifier: "scheduleDetailVC") as! ScheduleDetailViewController
            
        mainVC.schedule = schedule
        pushPresentAnimation()
        self.present(mainVC, animated: false, completion: nil)
    }
}
