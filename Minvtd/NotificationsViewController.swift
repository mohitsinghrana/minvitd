//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//


import UIKit
import SlideMenuControllerSwift
import Alamofire
import AlamofireImage

class NotificationsViewController : UIViewController {
    
    
    lazy var refresher:UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .gray
        refreshControl.addTarget(self, action: #selector(requestData), for: .valueChanged )
        
        return refreshControl
    }()
    
    //3DotMenuView;
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var firstRowView: UIView!
    @IBOutlet weak var onOffSwitch: UISwitch!
    @IBOutlet weak var secondRowView: UIView!
    @IBOutlet weak var allAdminSwitch: UISwitch!
    @IBOutlet weak var threeDotMenuBtn: UIBarButtonItem!
    
    @IBAction func threeDotMenuBtnTapped(_ sender: Any) {
        
        if mainView.isHidden == true{
            UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.mainView.isHidden = false
                self.firstRowView.isHidden = false
                self.secondRowView.isHidden = false
                self.mainView.frame.size.height = 100
                
            }) { (true) in
            }
        }else{
            UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.mainView.frame.size.height = 100
                self.mainView.isHidden = true
               self.firstRowView.isHidden = true
               self.secondRowView.isHidden = true
                
            }) { (true) in
            }
        }
    }
    
    @IBAction func onOffSwitchTapped(_ sender: Any) {
        
        if self.onOffSwitch.isOn == true{
            self.submitFunc(status: "1")
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.secondRowView.frame.size.height = 50
                self.secondRowView.isHidden = false
                self.mainView.frame.size.height = 100
                
            }) { (true) in
            }
        }else{
            self.submitFunc(status: "0")
            UIView.animate(withDuration: 0, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.secondRowView.frame.size.height = 0
                self.mainView.frame.size.height = 50
                self.secondRowView.isHidden = true
                
            }) { (true) in
            }
        }
    }
    
    @IBAction func allAdminSwitchTapped(_ sender: Any) {
        
        if self.allAdminSwitch.isOn == true{
            self.submitFunc(status: "2")
        }else{
            self.submitFunc(status: "1")
        }
    }
    
    func threeDotMenuSetup(){
        mainView.isHidden = true
        firstRowView.isHidden = true
//        if self.onOffSwitch.isOn == true{secondRowView.isHidden = true}
//        else{secondRowView.isHidden = false}
        secondRowView.isHidden = true
        self.mainView.frame.size.height = 0
    }
    
    var notis : [MInvitdNoti] = [MInvitdNoti]()
    
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var threeDotMenu: UIBarButtonItem!
    
    @objc let layerGradient = CAGradientLayer()
    var allFeeds: [MInvtdAllFeed] = [MInvtdAllFeed]()
    var allSubComments: [MInvtdAllFeedSubComment]? = [MInvtdAllFeedSubComment]()
    var paramaters : MInvtdAllFeed?
    @objc var origin : String = "feed"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        threeDotMenuSetup()
    
        //refresh setup
        myTableView.refreshControl = refresher
        
        let colour1 = UserDefaults.standard.string(forKey: "colour1")
        let colour2 = UserDefaults.standard.string(forKey: "colour2")
        
        myTableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
        self.onOffSwitch.onTintColor =  UIColor.init(hexString: colour1!, alpha: 1.0)
        self.allAdminSwitch.onTintColor =  UIColor.init(hexString: colour1!, alpha: 1.0)
        self.allAdminSwitch.backgroundColor = UIColor.init(hexString: colour1!, alpha: 1.0)
        self.allAdminSwitch.layer.cornerRadius = 16.0;
        self.onOffSwitch.backgroundColor = UIColor.init(hexString: colour1!, alpha: 1.0)
        self.onOffSwitch.layer.cornerRadius = 16.0;

        //Now
        if let receiveNotifications = UserDefaults.standard.string(forKey: "receiveNotifications"){
            print("This is receiveNotifications User Default:\(receiveNotifications)")
            if receiveNotifications == "0"{
                self.onOffSwitch.setOn(false, animated: false)
                self.secondRowView.frame.size.height = 0
                self.secondRowView.isHidden = true
            }
                
            else if receiveNotifications == "1"{
                self.allAdminSwitch.setOn(false, animated: false)
                self.onOffSwitch.setOn(true, animated: false)
                self.secondRowView.frame.size.height = 50
                self.secondRowView.isHidden = false
                
            }else if receiveNotifications == "2"{
                self.allAdminSwitch.setOn(true, animated: false)
            }
        }
        
        myTableView.delegate = self
        myTableView.dataSource = self
        navigationItem.title = "Notifications"
        self.navigationController?.navigationBar.isTranslucent = false
        navigationBarSetup(nil)
        
        layerGradient.colors = [UIColor.init(hexString: colour1!)?.cgColor, UIColor.init(hexString: colour2!)?.cgColor]
        layerGradient.startPoint = CGPoint(x: 0, y: 0.5)
        layerGradient.endPoint = CGPoint(x: 1, y: 0.5)
        layerGradient.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height)
        self.tabBarController?.tabBar.layer.insertSublayer(layerGradient, at: 0)
        self.tabBarController?.tabBar.barTintColor  = UIColor.white
    }
    @objc func requestData() {
        //print("request refresh")
        getAllNotification()
        self.myTableView.reloadData()
        
        let deadline = DispatchTime.now() + .milliseconds(500)
        DispatchQueue.main.asyncAfter(deadline: deadline) {
            self.refresher.endRefreshing()
        }
        // Code to refresh table view
    }
    
    @IBAction func menuButton(_ sender: Any) {
        slideMenuController()?.openLeft()
        
    }
 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getAllNotification()
    }
    
    @objc func getAllNotification() {
        
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")!
        let userID = UserDefaults.standard.string(forKey: "userID")!
        
        apiCallingMethod(apiEndPints: "notifications", method: .get, parameters: ["invitationID":invitationID,"userID":userID]) { (data1:Any?, objCount:Int?) in
            //print("this is data1 - >> ", data1)
            
            if let data = data1 as? [[String : Any]]
            {
                let notis = MInvitdNoti.notiFromResults(data as [[String : AnyObject]])
                self.notis = notis
                //print("This is Notification list :\n \(self.notis)")
                
                DispatchQueue.main.async {
                    self.myTableView.reloadData()
                }
            }
            else{
                //print("Can't Update")
            }
        }
    }
}


extension NotificationsViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notis.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let noti = notis[(indexPath as NSIndexPath).row]
        let cell = myTableView.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath) as! NotificationTableViewCell
        
        cell.title.text = noti.message
        cell.userImage.af_setImage(withURL: URL(string : noti.fileUrl!)!)
        
        cell.userImage.contentMode = .scaleAspectFill
        cell.userImage.layer.cornerRadius  =  cell.userImage.frame.size.width/2
        cell.userImage.layer.masksToBounds = true
        cell.userImage.clipsToBounds = true
        
        //let dateFormater = DateFormatter()
        //dateFormater.dateFormat = "MM/dd/yy"
        //let date = dateFormater.string(from: NSDate(timeIntervalSince1970: noti.created!) as Date)
        //let formattedDate = noti.createdTimeAgo!
        //print("noti.createdTimeAgo: \( noti.createdTimeAgo )")
        //let date = noti.createdTimeAgo!
        //let string = "🕒  " + date
        //let string1 = " " + date
        
        //cell.date.text = string1
        cell.date.text = noti.createdTimeAgo
        
        let moduleType : String = noti.module!
        if moduleType == "chat"{
            cell.typeImage.image = UIImage(named : "chatActive")
        }else if moduleType == "comment"{
            cell.typeImage.image = UIImage(named : "comment")
        }else if moduleType == "post"{
            cell.typeImage.image = UIImage(named : "feedsActive")
        }else if moduleType == "image"{
            cell.typeImage.image = UIImage(named : "camera")
        }else if moduleType == "video"{
            cell.typeImage.image = UIImage(named : "videoPlayIcon")
        }else if moduleType == "gallery"{
            cell.typeImage.image = UIImage(named : "photoGalleryIcon")
        }else if moduleType == "story"{
            cell.typeImage.image = UIImage(named : "photoGalleryIcon")
        }else if moduleType == "album"{
            cell.typeImage.image = UIImage(named : "photoGalleryIcon")
        }else if moduleType == "likes"{
            cell.typeImage.image = UIImage(named : "heartLiked")
        }else if moduleType == "invitation"{
            cell.typeImage.image = UIImage(named : "globesActive")
        }else if moduleType == "event"{
            cell.typeImage.image = UIImage(named : "eventsInactive")
        }else if moduleType == "vendor"{
            cell.typeImage.image = UIImage(named : "globesActive")
        }else{
            cell.typeImage.image = UIImage(named : "homeActive")
        }
        cell.typeImage.image = cell.typeImage.image?.withRenderingMode(.alwaysTemplate)
        let colour1 = UserDefaults.standard.string(forKey: "colour1")
        cell.typeImage.tintColor = UIColor(hexString: colour1!)
        
        return cell
    }
    
    
    
    @objc func submitFunc(status : String )  {
        
        let userID = UserDefaults.standard.string(forKey: "userID")
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")!
        
        let parameters: Parameters = ["status" : status  , "userID" : userID  as AnyObject,"invitationID": invitationID]
        
        apiCallingMethod(apiEndPints: "invitation/notify", method: .post, parameters: parameters) { (data1:Any?, ObjCount:Int?) in
            
            if data1 != nil {
                UserDefaults.standard.set(status, forKey: "receiveNotifications")
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let noti = notis[indexPath.row]
        
        //var postID = noti.pid
        //print("This is noti after selection \n\(noti)")
        
        if noti.module == "likes" || noti.module == "comment" || noti.module == "gallery" || noti.module == "image" || noti.module == "video" || noti.module == "story" || noti.module == "album" || noti.module == "post" || noti.module == "guests"{
            
            self.getFeedsRequest( invitationID: noti.invitationID!, postID: noti.postID! )
        }
        else if noti.module == "chat"  {
            
            //print(noti)
            
            let invitationID = UserDefaults.standard.integer(forKey: "invitationID")
            
            guard let str = noti.rid as? String else {return}
            
            let chatroom =  MInvitdchatList(chatID: str, name: noti.creatorName!, invitationID: invitationID)
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Chat") as! UINavigationController
            let a = vc.viewControllers[0] as? ChatVC
            a?.chatlist = chatroom
            
            
            self.present(vc, animated: true, completion: nil)
        }
        else if noti.module == "vendor"{
            print("vendor need to be added")
        }
    }
    
    
    @objc func getFeedsRequest(invitationID : Int, postID : Int){
        
        Spinner.sharedinstance.startSpinner(viewController: self)
        // Activity Incator
        //let invitationID = UserDefaults.standard.string(forKey: "invitationID")!
        //let userID = UserDefaults.standard.string(forKey: "userID")!
        
        var params = Parameters()
        
//        if self.origin == "feed"{
//        }
        params = [
            "postID": postID,
            "invitationID": invitationID,
            "limitSubComment": 1000
        ]
        
        apiCallingMethod(apiEndPints: "comments", method: .get, parameters: params) { (data:Any?, totalobj:Int?) in
            
            Spinner.sharedinstance.stopSpinner(viewController: self)
            
            if let data1 = data as? [[String : AnyObject]]{
                if let allFeeds1 = MInvtdAllFeed.allFeedsFromResults(data1) as? [MInvtdAllFeed] {
                    self.allFeeds = allFeeds1
                    self.paramaters = allFeeds1[0]
                    if allFeeds1[0].subCommentArray != nil
                    {
                        self.allSubComments = allFeeds1[0].subCommentArray!
                    }
                    
                    // Close Activity Indicator
                    
                    if self.paramaters?.fileType == "text"{
                        
                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let mainVC = storyBoard.instantiateViewController(withIdentifier: "feedCommentVC") as! FeedCommentVC
                        mainVC.paramaters = self.paramaters
                        mainVC.origin = "feed"
                        self.present( UINavigationController(rootViewController:mainVC), animated: true, completion: nil)
                        
                    }else {
                        
                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let mainVC = storyBoard.instantiateViewController(withIdentifier: "feedImageCommentVC") as! FeedImageCommentVC
                        mainVC.paramaters = self.paramaters
                        mainVC.origin = "feed"
                        self.present( UINavigationController(rootViewController:mainVC), animated: true, completion: nil)
                        
                    }
                }
            }
        }
    }
}
