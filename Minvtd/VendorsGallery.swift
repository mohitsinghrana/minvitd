//
//  VendorsGallery.swift
//  Minvtd
//
//  Created by Vineet Singh on 03/07/18.
//  Copyright © 2018 Euro Infotek Pvt Ltd. All rights reserved.
//

import Foundation

struct VendorsGallery {
    
    let galleryID: Int?
    let title: String?
    let description: String?
    let imageUrl: String?
    
    init(ID : Int, title : String, description : String, imageUrl : String) {
        self.galleryID = ID
        self.title = title
        self.description = description
        self.imageUrl = imageUrl
    }
    
    // construct a ZXQKGallery from a dictionary
    init(dictionary: [String:AnyObject]) {
        if let galleryID = dictionary["objectID"] as? String, galleryID.isEmpty == false {
            self.galleryID = Int(galleryID)
        } else {
            self.galleryID = nil
        }
        title = dictionary["title"] as? String
        description = dictionary["description"] as? String
        imageUrl = dictionary["imageUrl"] as? String
    }
    
    static func vendorsFromResults(_ results: [[String:AnyObject]]) -> [VendorsGallery] {
        var vendorsGallery = [VendorsGallery]()
        
        // iterate through array of dictionaries, each Article is a dictionary
        for vendors in results {
            vendorsGallery.append(VendorsGallery(dictionary: vendors))
        }
        
        return vendorsGallery
    }
}
