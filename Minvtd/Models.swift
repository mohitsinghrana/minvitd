//
//  Models.swift
//  Minvtd
//
//  Created by admin on 22/09/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.
//

import Foundation


/*
 "objectID": 8,
 "categoryID": 8,
 "title": "Cake 2",
 "description": "Cake2",
 "link": "http://euroinfotek.com",
 "rating": 5,
 "featured": 5,
 "imageUrl": "",
 "weight": 1000,
 "created": 1509963628,
 "updated": 0,
 "vendorID": 4
 */

struct AppCategory {
    
    let name: String?
    let products: [MInvitdVendorsProducts]?
    let type: String?
    let productsCount:Int?
    let categoryID:Int?
    
    init(dictionary: [String:AnyObject]) {
    
        self.type = dictionary["name"] as? String
        self.name = dictionary[MInvitdClient.JSONResponseKeys.name] as? String
        self.categoryID = dictionary[MInvitdClient.JSONResponseKeys.categoryID] as? Int
        self.productsCount = dictionary["productsCount"] as? Int

        if let dict = dictionary["products"] as? [[String:AnyObject]] {
               self.products = MInvitdVendorsProducts.productsFromResults(dict)   }
        else{ products = [] }
    }
    
    init(products: [MInvitdVendorsProducts]?){
        self.products = products
        self.name = nil
        self.categoryID = nil
        self.type = nil
        self.productsCount = products?.count
    }

       static func eventsFromResults(_ results: [[String:AnyObject]]) -> [AppCategory] {
        var events = [AppCategory]()
        
        for event in results {
            let appcategory = AppCategory(dictionary: event)
            if appcategory.productsCount != 0 {
                events.append(AppCategory(dictionary: event))
            }
        }
        
        return events
    }
}
