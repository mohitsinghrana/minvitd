//
//  SessionTracker.swift
//  Minvtd
//
//  Created by Vineet Singh on 04/08/18.
//  Copyright © 2018 Euro Infotek Pvt Ltd. All rights reserved.
//

import Foundation
import Alamofire

class SessionTracker: NSObject {
    
    // Api Function
    
    func apiCallingMethod(apiEndPints:String,method: HTTPMethod, parameters: Parameters?,complition:@escaping (_ data:Any?,_ totalObjects:Int?)->() ){
        
        let url = MInvitdClient.Constants.BaseUrl + apiEndPints
        var prams = parameters
        let colour1 = UserDefaults.standard.string(forKey: "colour1")
        let colour2 = UserDefaults.standard.string(forKey: "colour2")
        let modelName = UIDevice.modelName
 //     let version = "18.10.25"
        let systemVersion = UIDevice.current.systemVersion
        let nsObject: AnyObject? = Bundle.main.infoDictionary!["CFBundleVersion"] as AnyObject
        let version = nsObject as! String
        
        if let colour = colour1 {
            prams!["colour"] = colour
        }
        if let colour = colour2 {
            prams!["colour_2"] = colour
        }
        
        prams!["model"] = modelName
        prams!["version"] = version
        prams!["os"] = systemVersion
        
       let authHeader = ["Authorization" : UserDefaults.standard.string(forKey: "userToken")!, "Content-Type" : "application/json" ]
        
        Alamofire.request(url, method: method, parameters: prams, encoding: JSONEncoding.default, headers: authHeader).responseJSON { (response) in
            //debugPrint(response)
            if response.error == nil {
                self.apiHelperMethod(JSONData : response.data!, complition: {
                    data, totalObjects
                    in
                    complition(data, totalObjects)
                })
            }else {
                print("Session Not Updated")
            }
        }
    }
    
    fileprivate func  apiHelperMethod(JSONData : Data,complition:(_ data:Any?,_ totalObjects:Int?)->()) {
        
        var totalObjects:Int?
        var serverData:Any?
        
        do{
            let json = try JSONSerialization.jsonObject(with: JSONData, options: .allowFragments)
            
            if let dictionary = json as? [String : Any] {
                let totalObjects1 = dictionary["totalObjects"] as? Int
                
                totalObjects = totalObjects1
                if totalObjects == 0 {
                    
                    complition(serverData, totalObjects)
                    return
                }
                
                if let status = dictionary["status"] as? String
                {
                    if status == "success"
                    {
                        let data = dictionary["data"]
                        serverData = data
                        
                        complition(serverData, totalObjects)
                        return
                        
                    }else
                    {
                        if let mess = (dictionary["messages"] as? [[String : String]])                             {
                            if let messdict = mess.first {
                                let message = messdict["message"] ?? "Error status returned from server"
                            }
                        }
                    }
                }
            }
        }
        catch{
            print("Cannot Parse Session Data")
        }
    }
    
    class func sharedInstance() -> SessionTracker{
        struct Singleton{
            static var sharedInstance = SessionTracker()
        }
        return Singleton.sharedInstance
    }
}
