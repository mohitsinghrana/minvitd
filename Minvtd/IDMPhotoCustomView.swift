//
//  IDMPhotoCustomView.swift
//  Minvtd
//
//  Created by Vineet Singh on 03/07/18.
//  Copyright © 2018 Euro Infotek Pvt Ltd. All rights reserved.
//

import Foundation
import IDMPhotoBrowser


class IDMPhotoCustomView:IDMCaptionView {
    
    let padding:CGFloat = 10.0
    let padding1:CGFloat = 70.0
    
    let counterLabel = { () -> UILabel in
        let lab = UILabel()
        lab.textAlignment = .center
        lab.numberOfLines = 0
        lab.textColor = UIColor.white
        lab.lineBreakMode = .byWordWrapping
        lab.font = UIFont(name: "GothamRounded-Book", size: 15.0)
        
        return lab
    }()
    
    
    let titlelabel = { () -> UILabel in
        let lab = UILabel()
        lab.textAlignment = .center
        lab.numberOfLines = 0
        lab.textColor = UIColor.white
        lab.lineBreakMode = .byWordWrapping
        lab.font = UIFont(name: "GothamRounded-Bold", size: 13.0)
        
        return lab
    }()
    
    let detaillabel = { () -> UILabel in
        let lab = UILabel()
        lab.textAlignment = .center
        lab.numberOfLines = 0
        lab.textColor = UIColor.white
        lab.lineBreakMode = .byWordWrapping
        lab.alpha = 0.5
        lab.font = UIFont(name: "GothamRounded-Book", size: 12.0)
        
        return lab
    }()
    
    let sepratorView = { () -> UIView in
        let vi = UIView()
        vi.backgroundColor = .white
        vi.alpha = 0.6
        return vi
    }()
    
    
    
    init(data:IDMPhotoCustom2) {
        
        super.init(photo: data)
        titlelabel.text = data.title
        detaillabel.text = data.detail
        counterLabel.text = data.counter
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func setupCaption() {
        
        self.addSubview(counterLabel)
        self.addSubview(titlelabel)
        self.addSubview(detaillabel)
        self.addSubview(sepratorView)
        
        let height = self.bounds.size.height/4
        let width = self.bounds.size.width-padding*2
        let width1 = self.bounds.size.width-padding1*2
        
        counterLabel.frame = CGRect(x: padding, y: 0, width: width, height: height + 5)
        
        titlelabel.frame = CGRect(x: padding, y: 15, width: width, height: height + 5)
        
        sepratorView.frame = CGRect(x: padding1, y: height + 23.0, width: width1, height: 1.0)
        
        detaillabel.frame = CGRect(x: padding, y: 2*height + 18.0, width: width, height: height + 2)
        
        
        
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize{
        return CGSize(width: size.width, height: 100.0)
    }
    
}
