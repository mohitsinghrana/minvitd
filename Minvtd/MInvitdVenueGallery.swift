//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//


import Foundation

struct MInvitdVenueGallery{
    
    // MARK: Properties
    
    let fileUrl : String?
    let fileThumbnail : String?
    
    // MARK: Initializers
    
    // construct a dictionary
    init(dictionary: [String:AnyObject]) {
        
        fileUrl = dictionary[MInvitdClient.JSONResponseKeys.fileUrl] as? String
        fileThumbnail = dictionary[MInvitdClient.JSONResponseKeys.fileThumbnail] as? String
    }
    
    static func venuesFromResults(_ results: [[String:AnyObject]]) -> [MInvitdVenueGallery] {
        var photos = [MInvitdVenueGallery]()
        
        // iterate through array of dictionaries, each Article is a dictionary
        for photo in results {
            photos.append(MInvitdVenueGallery(dictionary: photo))
        }
        
        return photos
    }
}
