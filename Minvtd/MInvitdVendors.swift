//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import UIKit


struct MInvitVendor{
    
    // MARK: Properties
    
    let imageUrl : String?
    let name : String?
    let vendorID : Int?
    let thumbnailImage :  String?
    let categoryID : Int?
    let category : String?
    
    
    // MARK: Initializers
    init(dictionary: [String:AnyObject]) {
        imageUrl = dictionary[MInvitdClient.JSONResponseKeys.imageUrl] as? String
        name = dictionary[MInvitdClient.JSONResponseKeys.name] as? String
        vendorID = dictionary[MInvitdClient.JSONResponseKeys.vendorID] as? Int
        categoryID = dictionary[MInvitdClient.JSONResponseKeys.categoryID] as? Int
        category = dictionary[MInvitdClient.JSONResponseKeys.category] as? String
        thumbnailImage = dictionary["fileThumbnail"] as? String
    }
    
    static func vendorsFromResults(_ results: [[String:AnyObject]]) -> [MInvitVendor] {
        var vendors = [MInvitVendor]()
        
        // iterate through array of dictionaries, each Article is a dictionary
        for vendor in results {
            vendors.append(MInvitVendor(dictionary: vendor))
        }
        
        return vendors
    }
}

