//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import UIKit
import GoogleMaps

class EventLocationViewController: UIViewController  {

    @objc var location : String = ""
    @objc var mapsView : GMSMapView?
    
    @objc func gotoMaps(_ sender: UIBarButtonItem) {
        
        var locationArray = self.location.components(separatedBy: ",")
        let latitude : String = locationArray[0]
        let doublelLatitude = Double(latitude)
        let longitude :  String = locationArray[1].trimmingCharacters(in: .whitespaces)
        let doublelLongitude = Double(longitude)
        
        let url = URL(string:"https://www.google.com/maps/@\(String(describing: doublelLatitude)),\(String(describing: doublelLongitude)),6z")
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        } else {
            // Fallback on earlier versions
        }
    }
    
    @IBOutlet weak var mapView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationBarSetup(nil)
        title = "Maps"
        setupNavBarContent()
        
        var locationArray = self.location.components(separatedBy: ",")
        let latitude : String = locationArray[0]
        let doublelLatitude = Double(latitude)
        let longitude :  String = locationArray[1].trimmingCharacters(in: .whitespaces)
        let doublelLongitude = Double(longitude)

        GMSServices.provideAPIKey(" AIzaSyDwd4buLpBC6FRyudcK9yICfRL31VjV03Y ")
        let camera  =  GMSCameraPosition.camera(withLatitude: doublelLatitude!, longitude: doublelLongitude!, zoom: 12)
        mapsView = GMSMapView.map(withFrame: mapView.bounds, camera: camera)
        
        mapView.addSubview(mapsView!)
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: doublelLatitude!, longitude: doublelLongitude!)
        marker.map = mapsView
    }
   
    @objc func dismissVC(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setupNavBarContent(){
        let button1 = UIBarButtonItem(barButtonSystemItem: .stop,target: self, action: #selector(dismissVC(_:)))
        self.navigationItem.rightBarButtonItem = button1
        
        let button2 = UIBarButtonItem(title:"Go to Maps" , style: .plain, target: self, action: #selector(gotoMaps))
        self.navigationItem.leftBarButtonItem = button2
    }
}
