//
//  FeedsCollectionView.swift
//  Minvtd
//
//  Created by Tushar Aggarwal on 20/07/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit

class FeedsCollectionView: UICollectionViewCell {
    
    @IBOutlet weak var galleryButton: UIButton!
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var heartBtn: UIButton!
    @IBOutlet weak var heartLabel: UILabel!
    @IBOutlet weak var commentBtn: UIButton!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var postSubCommentTF: UITextField!
    @IBOutlet weak var subCommentView: UIView!
    @IBOutlet weak var subCommentUserImage: UIImageView!
    @IBOutlet weak var subComment: UILabel!
    
    @IBOutlet weak var subCommentTime: UILabel!

    @IBOutlet weak var playIcon: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        image.contentMode = .scaleAspectFill
        image.clipsToBounds = true
        postSubCommentTF.returnKeyType = .send
    }

}
