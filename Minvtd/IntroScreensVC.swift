//
//  IntroScreensVC.swift
//  Minvtd
//
//  Created by Vivek Aggarwal on 20/05/18.
//  Copyright © 2018 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit

class IntroScreensVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    let arr = ["0","1","2","3"]
    var currentIndex : Int = 0
    var i : Int = 0
    
    @IBOutlet weak var myCollectionView: UICollectionView!
    @IBOutlet weak var segmentedControl: UIPageControl!
    
    static var myView: UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        IntroScreensVC.myView = self.view
        
        myCollectionView.delegate = self
        myCollectionView.dataSource = self
        
        myCollectionView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        
        self.navigationController?.isNavigationBarHidden = true
        
        if self.i == 3 {
            print(i)
            let screenWidth = UIScreen.main.bounds.width
            let pageWidth = CGFloat(screenWidth)
            let scrollTo = CGPoint(x: pageWidth * CGFloat(i) , y: 0)
            DispatchQueue.main.async {
                
                self.myCollectionView.setContentOffset(scrollTo, animated: false)
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let center = CGPoint(x: scrollView.contentOffset.x + (scrollView.frame.width / 2), y: (scrollView.frame.height / 2))
        if let ip = self.myCollectionView.indexPathForItem(at: center) {
            self.currentIndex = ip.row
            self.segmentedControl.currentPage =  self.currentIndex
        }
    }
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let bounds = UIScreen.main.bounds
        let width = bounds.width
        let height = bounds.height
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if arr[indexPath.row] == "0" {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell1", for: indexPath)  as! IntroScreen1CollectionViewCell
            cell.loginBtn.addTarget(self, action: #selector(self.loginBtnTapped), for: .touchUpInside)
            return cell
            
        }else if arr[indexPath.row] == "1" {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell2", for: indexPath)  as! IntroScreen2CollectionViewCell
            cell.loginBtn.addTarget(self, action: #selector(self.loginBtnTapped), for: .touchUpInside)
            return cell
            
        }
            
        else if arr[indexPath.row] == "2" {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell3", for: indexPath)  as! IntroScreen3CollectionViewCell
            cell.loginBtn.addTarget(self, action: #selector(self.loginBtnTapped), for: .touchUpInside)
            return cell
            
        }else {
            
            let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell4", for: indexPath) as! IntroScreen4CollectionViewCell
            
            cell.loginBtn.addTarget(self, action: #selector(self.loginBtnTapped), for: .touchUpInside)
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    @objc func loginBtnTapped(){
        
        let storyboard = UIStoryboard(name: "Storyboard", bundle: nil)
        let vc =  storyboard.instantiateViewController(withIdentifier: "NewOtpScreen") as? NewOtpScreen
        self.present(vc!, animated: true, completion: nil)
    }
}

