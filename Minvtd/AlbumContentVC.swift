//
//  AlbumContentVC.swift
//  UIPageViewControllerDemo
//


import UIKit
import Alamofire
import AlamofireImage

protocol AlbumContainerDelegate:class {
    func didpressmovebtn(btn:UIButton,tag:Int)
}






class AlbumContentVC: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var previousBtn: UIButton!
    //    @IBOutlet weak var emailViewTap: UIView!
    @IBOutlet weak var likeViewTap: UIView!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var commentBtn: UIButton!
    @IBOutlet weak var likeLabel: UILabel!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var commentViewTap: UIView!
//    @IBOutlet weak var emailButton: UIButton!
//    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var likeCount: UILabel!
    @IBOutlet weak var commentCount: UILabel!
    @IBOutlet var bgImageView:UIImageView!
    @IBOutlet weak var ExitButton: UIButton!
    @objc var pageIndex: Int = 0
    @objc var strTitle: String!
    @objc var strPhotoName: String!

    var gallery: [MInvtdAllFeed] = [MInvtdAllFeed]()
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    weak var delegate:AlbumContainerDelegate?
    
    @IBAction func moveBtnAction(_ sender: UIButton) {
        delegate?.didpressmovebtn(btn: sender, tag: sender.tag)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapAction)))
        lblTitle.center.x = CGFloat (20.0)
        lblTitle.center.y = UIScreen.main.bounds.height/2
        self.imageView.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 2)
        self.imageView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        
        mainView.isHidden = true
        
        let url:URL = URL(string: strPhotoName)!
        self.imageView.af_setImage(withURL: url)
        lblTitle.text = ""
        self.lblTitle.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 2)
        
        let currentIndex = self.pageIndex
        
        if gallery[currentIndex].liked == true {
            var image2 = UIImage(named: "likeFilled")
            image2 = image2?.withRenderingMode(.alwaysOriginal)
            self.likeBtn.setImage(image2, for: .normal)
            self.likeBtn.tintColor = UIColor.white
        }else{
            var image2 = UIImage(named: "like")
            image2 = image2?.withRenderingMode(.alwaysOriginal)
            self.likeBtn.setImage(image2, for: .normal)
            self.likeBtn.tintColor = UIColor.white
        }
        var image1 = UIImage(named: "msg")
        image1 = image1?.withRenderingMode(.alwaysOriginal)
        self.commentBtn.setImage(image1, for: .normal)
        self.commentBtn.tintColor = UIColor.white
        var image3 = UIImage(named: "contactUs")
        image3 = image3?.withRenderingMode(.alwaysTemplate)
        
        if let likecount = gallery[currentIndex].likes{
            self.likeCount.text = "\(likecount)"
        }
        if let msgcount = gallery[currentIndex].subCommentsCount{
            self.commentCount.text = "\(msgcount)"
        }
        

        
        self.commentLabel.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 2)
        self.commentBtn.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 2)
        self.likeLabel.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 2)
        self.likeBtn.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 2)
        self.likeCount.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 2)
        self.commentCount.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 2)

        self.commentBtn.isUserInteractionEnabled = false
        self.likeBtn.isUserInteractionEnabled = false
        let commentTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.commentTap(_:)))
        commentTapGesture.numberOfTapsRequired = 1
        commentViewTap.isUserInteractionEnabled = true
        commentViewTap.addGestureRecognizer(commentTapGesture)
        let likeTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.likeTap(_:)))
        likeTapGesture.numberOfTapsRequired = 1
        likeViewTap.isUserInteractionEnabled = true
        likeViewTap.addGestureRecognizer(likeTapGesture)

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    
    @IBAction func exitButtonAction(_ sender: UIButton) {
        
        let previousView = self.presentingViewController
        
        // Dismiss the current view controller then pop the others
        // upon completion
        self.dismiss(animated: false, completion:  {
            
            previousView?.dismiss(animated: true, completion: nil)
            
        });
    }
    
    @objc func commentTap(_ sender: UITapGestureRecognizer) {
        
        let currentIndex = self.pageIndex
        let feed  = self.gallery[currentIndex]
        if feed.fileType == "text"{
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let mainVC = storyBoard.instantiateViewController(withIdentifier: "feedCommentVC") as! FeedCommentVC
            mainVC.paramaters = feed
            mainVC.origin = "feed"
            self.present( UINavigationController(rootViewController:mainVC), animated: true, completion: nil)

        }else {
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let mainVC = storyBoard.instantiateViewController(withIdentifier: "feedImageCommentVC") as! FeedImageCommentVC
            mainVC.paramaters = feed
            mainVC.origin = "feed"
            self.present( UINavigationController(rootViewController:mainVC), animated: true, completion: nil)

        }
        
        
    }
    

    
    @objc func likeTap(_ sender: UITapGestureRecognizer) {
        
        let currentIndex = self.pageIndex
        let feed  = self.gallery[currentIndex]
        let userID = UserDefaults.standard.string(forKey: "userID")!
        let userName = UserDefaults.standard.string(forKey: "name")!
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")!
        let iName = UserDefaults.standard.string(forKey: "invitationName")
        
        let parameters: Parameters = [
            "postID" : feed.postID as AnyObject,
            "userID" : userID as AnyObject,
            "userName": userName,
            "invitationID": invitationID ,
            "type": feed.type,
            "likeID": feed.likeID!,
            "iName": iName!,
            "eventID": feed.eventID!
        ]
        
        apiCallingMethod(apiEndPints: "like", method: .post, parameters: parameters) { (data1:Any?, totalobj:Int?) in
            if data1 != nil {
                
                let currentIndex = self.pageIndex
                if self.gallery[currentIndex].liked == true {
                    var image2 = UIImage(named: "like")
                    image2 = image2?.withRenderingMode(.alwaysOriginal)
                    self.likeBtn.setImage(image2, for: .normal)
                    self.likeBtn.tintColor = UIColor.white
                    self.gallery[currentIndex].liked = false
                    self.gallery[currentIndex].likes = self.gallery[currentIndex].likes! - 1
                    self.likeCount.text = "\(self.gallery[currentIndex].likes!)"
                    
                }else{
                    var image2 = UIImage(named: "likeFilled")
                    image2 = image2?.withRenderingMode(.alwaysOriginal)
                    self.likeBtn.setImage(image2, for: .normal)
                    self.likeBtn.tintColor = UIColor.white
                    self.gallery[currentIndex].liked = true
                    self.gallery[currentIndex].likes = self.gallery[currentIndex].likes! + 1
                    self.likeCount.text = "\(self.gallery[currentIndex].likes!)"
                }
            }
            else{
                print("can't")
            }
        }
    }
    
    @objc func tapAction() {
        if ExitButton.tag == 0 {
            ExitButton.tag = 1
            ExitButton.isHidden = false
            mainView.isHidden = false
            lblTitle.isHidden = false
        }
        else{
            ExitButton.tag = 0
            ExitButton.isHidden = true
            mainView.isHidden = true
            lblTitle.isHidden = true
        }
    }
}
