//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import Alamofire
import AlamofireImage
import IDMPhotoBrowser
class VendorsProductsViewController: UIViewController , IDMPhotoBrowserDelegate{
    
    var products: [MInvitdVendorsProducts] = [MInvitdVendorsProducts]()
    var vendorID : Int?

    var photos : [IDMPhotoCustom2] = [IDMPhotoCustom2]()
    var gallery: [VendorsGallery]?

    @objc let placeholderImage = UIImage(named: "eventCoverImage")!
    var listing : MInvitdVendorsListing?
    var titleName   : String  = ""
    @IBOutlet weak var listingsCollectionView: UICollectionView!
    @objc var browser:IDMPhotoBrowser? = nil
    
    var currentIndex: IndexPath?
    var descOpen: Bool?
    
    var contactNo: String?
    
    static var view2InitialFrame: CGRect?
    static var descInitialFrame: CGRect?
    static var listCollectionInitialFrame: CGRect?
    static var headerInitialFrame: CGRect?
    static var estimateDescHeight: CGFloat?
    static var currentViewFrame: CGRect?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        browser?.delegate = self
        listingsCollectionView.delegate = self
        listingsCollectionView.dataSource = self
        self.navigationItem.title = titleName
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         VendorProducts()
    }
    
    @objc func VendorProducts() {
        
        //let invitationID = UserDefaults.standard.string(forKey: "invitationID")!
        let categoryID = self.listing?.categoryID!
        
        let params = [
            "vendorID": vendorID!,
            "categoryID": categoryID,
            "invitationID": UserDefaults.standard.string(forKey: "invitationID")!,
            "type": UserDefaults.standard.string(forKey: "category"),
            "device": "iOS",
            "iRole" : UserDefaults.standard.string(forKey: "userRole"),
        ] as [String : AnyObject]
        
        //  ["vendorID":vendorID!,"categoryID":categoryID,"invitationID":invitationID]
        //print("-----ParamsVendor-----\(params)")
        apiCallingMethod(apiEndPints: "vendor/listings", method: .get, parameters: params) { (data1:Any?, totalobj:Int?) in
            //debugPrint(data1)
            if let data = data1 as? [[String : Any]]
            {
                let products = MInvitdVendorsProducts.productsFromResults(data as [[String : AnyObject]])
                self.products = products
                let gdata = VendorsGallery.vendorsFromResults(data as [[String : AnyObject]])
                self.gallery = gdata
                 DispatchQueue.main.async {
                    self.listingsCollectionView.reloadData()
                }
            }
        }
    }
    
    @objc func imageTapFunc(_ sender: UITapGestureRecognizer) {
        
        photos = [IDMPhotoCustom2]()
        let tag = sender.view?.tag
        
        var counter  = 1
        let total = self.gallery?.count
        for photo in self.gallery! {
            
            let currentPhoto = IDMPhotoCustom2(counter : "\(counter) of \(String(describing: total!))"   ,imageUrl: URL(string : photo.imageUrl!)!, detail: photo.description, title: photo.title)
            photos.append(currentPhoto)
            counter  = counter + 1
        }
        
        browser = IDMPhotoBrowser.init(photos: photos)
        browser?.delegate = self
        browser?.displayCounterLabel = true
        browser?.displayActionButton = false
        browser?.doneButtonRightInset = CGFloat(5.0)
        browser?.doneButtonTopInset = CGFloat(30.0)
        browser?.doneButtonImage = UIImage(named: "cross")
        browser?.autoHideInterface = false
        browser?.setInitialPageIndex(UInt(tag!))
        present(browser!, animated: true, completion: nil)
    }
    
    func photoBrowser(_ photoBrowser: IDMPhotoBrowser!, captionViewForPhotoAt index: UInt) -> IDMCaptionView! {
        //let photo = photos[index.hashValue] as? IDMPhotoCustom2
        let photo = photos[Int(index)] as? IDMPhotoCustom2
        let captionView = IDMPhotoCustomView(data: photo!)
        return captionView
    }
}

// MARK: - InvitationViewController: UICollectionViewDelegate, UICollectionViewDataSource

extension VendorsProductsViewController : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let bounds = UIScreen.main.bounds
        let width = bounds.width
        let height = bounds.height/8
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let product = products[(indexPath as NSIndexPath).row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productsCell", for: indexPath) as! ProductsCollectionViewCell
        
        cell.title.text = product.title
        cell.descript.text = product.description
        cell.weight.text = " "
        
        let filter = AspectScaledToFillSizeWithRoundedCornersFilter(
            size: cell.image.frame.size,
            radius: cell.image.bounds.height/2
        )
        cell.image.af_setImage(
            withURL: URL(string: product.imageUrl!)! ,
            placeholderImage: placeholderImage , filter : filter
        )
        
        let imageTap = UITapGestureRecognizer(target: self, action: #selector(self.imageTapFunc(_:)))
        imageTap.numberOfTapsRequired = 1
        cell.image.isUserInteractionEnabled = true
        cell.image.addGestureRecognizer(imageTap)
        cell.image.tag = indexPath.row
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    @objc func callNo(){
        if let contact = self.contactNo{
            //print("\(contact)")
            if let phoneCallURL:NSURL = NSURL(string:"tel://\(contact)") {
                let application:UIApplication = UIApplication.shared
                if (application.canOpenURL(phoneCallURL as URL)) {
                    application.open(phoneCallURL as URL, options: [:], completionHandler: nil)
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = self.listingsCollectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "ListingHeaderView", for: indexPath) as! ListingCollectionReusableView
        
        self.currentIndex = indexPath
        
        header.image.af_setImage(
            withURL: URL(string: (self.listing?.imageUrl)!)! ,
            placeholderImage: placeholderImage
        )
        
//        header.title.text = self.listing?.title
        header.name.text = self.listing?.name
        header.contact.text = String (describing : (self.listing?.contact)!)
        header.desc.text = self.listing?.description
        header.desc.sizeToFit()
        header.address.text = self.listing?.address
        self.contactNo = header.contact.text
        header.callButton.addTarget(self, action: #selector(callNo), for: .touchUpInside)
        
        //Card View Look for view1
        
        header.view1.layer.cornerRadius = 2.0
        header.view1.layer.shadowColor = UIColor(red: 211/255, green: 211/255, blue: 211/255, alpha: 1.0).cgColor
        header.view1.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        header.view1.layer.shadowRadius = 4.0
        header.view1.layer.shadowOpacity = 1.0
        header.view1.layer.masksToBounds = false
        header.view1.layer.shadowPath = UIBezierPath(roundedRect: header.view1.bounds, cornerRadius: header.view1.layer.cornerRadius).cgPath
        
        //Card View Look for view2
        
        header.view2.layer.cornerRadius = 2.0
        header.view2.layer.shadowColor = UIColor(red: 211/255, green: 211/255, blue: 211/255, alpha: 1.0).cgColor
        header.view2.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        header.view2.layer.shadowRadius = 4.0
        header.view2.layer.shadowOpacity = 1.0
        header.view2.layer.masksToBounds = false
        header.view2.layer.shadowPath = UIBezierPath(roundedRect: header.view2.bounds, cornerRadius: header.view2.layer.cornerRadius).cgPath
        
        header.descriptionViewExpandBtn.addTarget(self, action: #selector(self.detailedDescTapped), for: .touchUpInside)
        
        let colour1 = UserDefaults.standard.string(forKey: "colour1") ?? "#E12459"

        header.vni.image = header.vni.image?.withRenderingMode(.alwaysTemplate)
        header.vni.tintColor = UIColor(hexString: colour1 )
        header.vci.image = header.vci.image?.withRenderingMode(.alwaysTemplate)
        header.vci.tintColor = UIColor(hexString: colour1)
        header.vai.image = header.vai.image?.withRenderingMode(.alwaysTemplate)
        header.vai.tintColor = UIColor(hexString: colour1)
        header.vdi.image = header.vdi.image?.withRenderingMode(.alwaysTemplate)
        header.vdi.tintColor = UIColor(hexString: colour1)
        
        return header
    }
    
    @objc func detailedDescTapped(){
        
        let header = self.listingsCollectionView.supplementaryView(forElementKind: UICollectionElementKindSectionHeader, at: currentIndex!) as! ListingCollectionReusableView
        
        header.layoutIfNeeded()
        
        if self.descOpen == nil{
            VendorsProductsViewController.view2InitialFrame = header.view2.frame
            VendorsProductsViewController.descInitialFrame = header.desc.frame
            VendorsProductsViewController.listCollectionInitialFrame = self.listingsCollectionView.frame
            VendorsProductsViewController.headerInitialFrame = self.listingsCollectionView.supplementaryView(forElementKind: UICollectionElementKindSectionHeader, at: self.currentIndex!)?.frame
            VendorsProductsViewController.currentViewFrame = self.view.frame
        }
        
        if self.descOpen == nil{
            self.descOpen = false
        }
        
        
        let size = CGSize(width: UIScreen.main.bounds.width - 64, height: 1000)
        let options:NSStringDrawingOptions =  NSStringDrawingOptions.usesFontLeading.union(NSStringDrawingOptions.usesLineFragmentOrigin)
        
        let estimatedFrame = NSString(string: (header.desc.text)!).boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font:UIFont(name: "GothamRounded-Book", size: 12)!], context: nil)
        
        VendorsProductsViewController.estimateDescHeight = estimatedFrame.height
        
       if self.descOpen == true{
            self.descOpen = false
            header.desc.isHidden = true
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
                
                header.descriptionViewExpandBtn.transform = CGAffineTransform.identity
                
                self.listingsCollectionView.supplementaryView(forElementKind: UICollectionElementKindSectionHeader, at: self.currentIndex!)?.frame = VendorsProductsViewController.headerInitialFrame!
                
                header.view2.frame = VendorsProductsViewController.view2InitialFrame!
                header.desc.frame = VendorsProductsViewController.descInitialFrame!
                //print("this static value for self.view.frame\n\(VendorsProductsViewController.descInitialFrame!)")
                self.view.frame = VendorsProductsViewController.currentViewFrame!
                self.listingsCollectionView.reloadData()
                
            }, completion: nil)
        
            
        }else{
            self.descOpen = true
        
            header.desc.isHidden = false
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
                
                header.descriptionViewExpandBtn.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
                
                self.listingsCollectionView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: (VendorsProductsViewController.listCollectionInitialFrame?.height)! + estimatedFrame.height)
                self.listingsCollectionView.supplementaryView(forElementKind: UICollectionElementKindSectionHeader, at: self.currentIndex!)?.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 420 + estimatedFrame.height + 30)
                
                header.view2.frame = CGRect(x: header.view2.frame.minX, y: header.view2.frame.minY, width: header.view2.frame.width, height: 50 + estimatedFrame.height + 20)
                header.desc.frame = CGRect(x: 16, y: 50, width: header.desc.frame.width, height: estimatedFrame.height)
                self.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: (VendorsProductsViewController.currentViewFrame?.height)! + estimatedFrame.height)
                self.listingsCollectionView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: (VendorsProductsViewController.currentViewFrame?.height)! + 100 + estimatedFrame.height)
                //+ estimatedFrame.height
                self.listingsCollectionView.reloadData()
                
                //trying code
                let animationDuration: CGFloat = 0;
                let shadowAnimation: CABasicAnimation =  CABasicAnimation(keyPath: "shadowPath")
                shadowAnimation.duration = CFTimeInterval(animationDuration)
                shadowAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut) // Match the easing of the UIView block animation
                shadowAnimation.fromValue = header.view2.layer.shadowPath
                UIView.animate(withDuration: 0.1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
                    header.view2.frame = CGRect(x: header.view2.frame.origin.x, y: header.view2.frame.origin.y, width: header.view2.frame.size.width, height: header.view2.frame.size.height)
                }, completion: nil)
                shadowAnimation.toValue = UIBezierPath(roundedRect: header.view2.bounds, cornerRadius: header.view2.layer.cornerRadius).cgPath
                header.view2.layer.add(shadowAnimation, forKey: "shadowPath")
                header.view2.layer.shadowPath = UIBezierPath(roundedRect: header.view2.bounds, cornerRadius: header.view2.layer.cornerRadius).cgPath
                
                //trying end
            
            }, completion: {(true) in
                
                header.view2.layer.cornerRadius = 2.0
                header.view2.layer.shadowColor = UIColor(red: 211/255, green: 211/255, blue: 211/255, alpha: 1.0).cgColor
                header.view2.layer.shadowOffset = CGSize(width: 0, height: 2.0)
                header.view2.layer.shadowRadius = 4.0
                header.view2.layer.shadowOpacity = 1.0
                header.view2.layer.masksToBounds = false
                header.view2.layer.shadowPath = UIBezierPath(roundedRect: header.view2.bounds, cornerRadius: header.view2.layer.cornerRadius).cgPath
            })

        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {

        if self.descOpen == false || self.descOpen == nil{
            return CGSize(width: self.listingsCollectionView.bounds.width, height: 420)
        }else{
            return CGSize(width: self.listingsCollectionView.bounds.width, height: 420 + VendorsProductsViewController.estimateDescHeight! + 20)
        }
    }
}
