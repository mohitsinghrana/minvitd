//
//  AdminEventVC.swift
//  Minvtd
//
//  Created by admin on 21/09/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit


class AdminEventVC:UITableViewController{
    
    var myEvents = [MInvitdEvent]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        setup()
    }
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myEvents.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuse", for: indexPath)
        cell.textLabel?.text = myEvents[indexPath.row].name
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        let storyBoard : UIStoryboard = UIStoryboard(name: "Storyboard", bundle: nil)
        
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "admin") as! AdminVC
        
        mainVC.Event = myEvents[indexPath.row]
        
        self.navigationController?.pushViewController(mainVC, animated: true)
        
    }
    
    
    @objc func setup(){
        navigationBarSetup(nil)
        title = "Admin"
        let button1 = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(dissmissVc))
        self.navigationItem.leftBarButtonItem  = button1
        
        let button2 = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addAdmin))
        
        self.navigationItem.rightBarButtonItem  = button2

        tableView.tableFooterView = UIView()
    }
    

    @objc func addAdmin() {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Storyboard", bundle: nil)
        
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "NewAdminVc") as! NewAdminVc
        self.navigationController?.pushViewController(mainVC, animated: true)
        //self.present( UINavigationController(rootViewController:mainVC), animated: true, completion: nil)
    }
}
