//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import Foundation

// MARK: - MInvitdClient (Constants)

extension MInvitdClient {
    
    // MARK: Constants
    struct Constants {
        
        // MARK: API Key
        
        // MARK: URLs
        
        static let BaseUrl = "https://minvitd.com/api/"
    }
    
    // MARK: Methods
    struct Methods {
        
        // MARK: Invitation
        static let Invitation = "invitations"
        
    }
    
    // MARK: JSON Body Keys
    struct JSONBodyKeys {
        static let invitationID = "invitationID"
        static let userID = "userID"
        static let categoryID = "categoryID"
        static let orderBy = "orderBy"
        static let otp = "otp"
        static let type = "type"
        static let name = "name"
        static let date = "date"
        static let fileID = "fileID"
        static let fileUrl = "fileUrl"
        static let eventID = "eventID"
        static let scheduleID = "scheduleID"
        static let venueID = "venueID"
        static let guestID = "guestID"
        static let category = "category"
        static let userToken = "userToken"
        static let password = "password"
        static let username = "username"
        static let account = "account"
        static let device = "device"
        static let token = "token"
        static let location = "location"
        static let email = "email"
        static let phone = "phone"
        static let gender = "gender"
    }
    
    // MARK: JSON Response Keys
    struct JSONResponseKeys {
        
        // MARK: General
        static let messages = "messages"
        static let message = "message"
        static let status = "status"
        static let statusName  = "statusName"
        static let statusColour  = "statusColour"
        static let data = "data"
        
        // MARK: Authorization
        //static let ownerID = "ownerID"
        //static let objectID = "objectID"
        static let categoryID = "categoryID"
        
        // MARK: Nodes
        static let adminID = "adminID"
        static let authorID = "authorID"
        static let creatorID = "creatorID"
        static let category = "category"
        static let name = "name"
        static let description = "description"
        static let imageUrl = "imageUrl"
        static let coordinates = "coordinates"
        static let fileUrl = "fileUrl"
        static let filename = "filename"
        static let time = "time"
        static let venue = "venue"
        static let color = "color"
        static let weight = "weight"
        static let created = "created"
        static let updated = "updated"
        static let date = "date"
        static let email = "email"
        static let phone = "phone"
        static let address = "address"
        static let contact = "contact"
        static let profileImageUrl = "profileImageUrl"
        static let title = "title"
        static let userID = "userID"
        static let fileType = "fileType"
        static let invitationID = "invitationID"
        static let showRsvp = "showRsvp"
        static let commentID = "commentID"
        static let venueID = "venueID"
        static let scheduleID = "scheduleID"        
        static let fileThumbnail = "fileThumbnail"
        static let eventID = "eventID"
        static let userImageUrl = "userImageUrl"
        static let userName = "userName"
        static let guestID = "guestID"
        static let guestStatus = "guestStatus"
        static let guestStatusName = "guestStatusName"
        static let liked = "liked"
        static let comment = "comment"
        static let fileID = "fileID"
        static let likes = "likes"
        static let commentsCount = "commentsCount"
        static let likesCount = "likesCount"
        static let subCommentsCount = "subCommentsCount"
        static let module = "module"
        static let colour = "colour"
        static let colour_2 = "colour_2"
        static let firebaseID = "firebaseID"
        static let eventDate = "eventDate"
        static let eventName = "eventName"
        static let contactPerson = "contactPerson"
        static let header = "header"
        static let albumBgUrl = "albumBgUrl"
        static let coverUrl = "coverUrl"
        
        static let parentTypeName = "parentTypeName"
        static let type = "type"
        static let likeID = "likeID"
        
        static let personThumbnail = "personThumbnail"
        static let vendorID = "vendorID"
        static let chatID = "chatID"
        static let postID = "postID"
        static let productID = "productID"
    }
}
