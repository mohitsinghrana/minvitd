//
//  CollapsibleTableViewController.swift
//  ios-swift-collapsible-table-section
//
//  Created by Yong Su on 5/30/16.
//  Copyright © 2016 Yong Su. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

//
class PersonalSchedularTableVC: UITableViewController {
    

    var schedules:[MInvitdSchedule] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 25.0
        tableView.rowHeight = UITableViewAutomaticDimension
        self.view.backgroundColor = .clear
        let blurEffect = UIBlurEffect(style: .light)
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        
//        let imageBackgroundUrl =  UserDefaults.standard.string(forKey: "invitationImageUrl")
//        let imageView = UIImageView()
//        imageView.backgroundColor = UIColor.init(hexString: "e3e3e3")
//        imageView.frame = tableView.frame
//        imageView.af_setImage(
//            withURL: URL(string: imageBackgroundUrl!)!)

//        blurredEffectView.alpha = 0.97
//        blurredEffectView.frame = tableView.frame
//        blurredEffectView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
//        tableView.backgroundColor = .clear
//        tableView.backgroundView = imageView
//        tableView.backgroundView?.insertSubview(blurredEffectView, at: 0)
//        tableView.contentInset = UIEdgeInsets(top: 8,left: 0,bottom: 0,right: 0)
        
          tableView.backgroundColor = UIColor(red: 245 / 255, green: 245 / 255, blue: 245 / 255, alpha: 1.0)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        scheduleApi()
         Setup()
        
    }
    
    
    
    @IBAction func dismissVC(_ sender: UIBarButtonItem) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @objc func Setup() {
        
            navigationBarSetup(nil)
        
    }
}




//
// MARK: - View Controller DataSource and Delegate
//
extension PersonalSchedularTableVC {

    override func numberOfSections(in tableView: UITableView) -> Int {
        if schedules.count == 0 {
            return 0
        }
       return schedules.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schedules[section].collapsed ? 0 : 1
    }
    
    // Cell
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CollapsibleTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as? CollapsibleTableViewCell ??
            CollapsibleTableViewCell(style: .default, reuseIdentifier: "cell")
        
        cell.detailLabel.text = schedules[indexPath.section].description
        cell.containerView1.backgroundColor = UIColor.white
        cell.selectionStyle = .none
        cell.containerView1.round(corners: [.bottomLeft,.bottomRight], radius: 8.0)
        return cell
    }
    
    // Header
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header") as? CollapsibleTableViewHeader ?? CollapsibleTableViewHeader(reuseIdentifier: "header")
        
        header.titleLabel.text = schedules[section].name
        header.timeLabel.text = schedules[section].time
        header.locationLabel.text = schedules[section].venue ?? ""
        header.detailLabel.text = schedules[section].description ?? ""
        
//      header.arrowLabel.text = ">"
        header.setCollapsed(schedules[section].collapsed)
        header.section = section
        header.delegate = self
        return header
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 84
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 8.0
    }
    override func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        let view1:UITableViewHeaderFooterView  = view as! UITableViewHeaderFooterView
        view1.backgroundView?.alpha = 0.0
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let view1:CollapsibleTableViewHeader  = view as! CollapsibleTableViewHeader
        view1.backgroundView?.alpha = 0.0
        view1.contentView.backgroundColor = .clear
        view1.containerView1.alpha = 0.8
        
        if self.schedules[section].collapsed {
            view1.containerView1.round(corners: [.topLeft,.topRight,.bottomRight,.bottomLeft], radius: 8)
        }else {
            view1.containerView1.round(corners: [.topLeft,.topRight], radius: 12)
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let cell1 = cell as! CollapsibleTableViewCell
        cell1.backgroundColor = .clear
        cell1.contentView.backgroundColor = .clear
        cell1.containerView1.alpha = 0.7
        cell1.containerView1.round(corners: [.bottomLeft,.bottomRight], radius: 12.0)
    }
}

//
// MARK: - Section Header Delegate
//
extension PersonalSchedularTableVC: CollapsibleTableViewHeaderDelegate {
    
    @objc func toggleSection(_ header: CollapsibleTableViewHeader, section: Int) {
        let collapsed = !schedules[section].collapsed
        
        // Toggle collapse
        schedules[section].collapsed = collapsed
        header.setCollapsed(collapsed)
        
        let   index1 =  NSIndexSet(index: section) as IndexSet;        tableView.reloadSections(index1, with: .automatic)
        tableView.reloadData()

        
        //reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
    }
    
    
    // copied from mohit Professional
    
    
    @objc func  scheduleApi() {
        let eventID = UserDefaults.standard.string(forKey: "eventID")!
        let invitationID = UserDefaults.standard.string(forKey: "invitationID")!

        
        
        apiCallingMethod(apiEndPints: "schedule", method: .get, parameters: ["eventID":eventID,"invitationID": invitationID]) { (data1:Any?, totalObjects:Int?) in
            
            
            if totalObjects == 0 {

                //self.dismissAlert("Schedule not yet specified")
                return
            }
            
            if let data = data1 as? [[String : Any]]
            {
                let schedules = MInvitdSchedule.schedulesFromResults(data as [[String : AnyObject]])
                self.schedules = schedules
                DispatchQueue.main.async {
                    
                    self.tableView.reloadData()
                }
            }
        }
    }
}
