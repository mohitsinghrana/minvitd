//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import Foundation

extension Date {
    
    
    func today () -> String{
        let date = Date()
        let calendar = Calendar.current
        let components = (calendar as NSCalendar).components([.day , .month , .year, .hour,.minute,.nanosecond], from: date)
        return "\(components.year)" + "-" + "\(components.month)" + "-" + "\(components.day)" + "-" + "\(components.hour)" + "-" + "\(components.minute)" + "-" +  "\(components.nanosecond)"
    }
    
    
    func convert() ->String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d h:mm a"
        let date = dateFormatter.string(from: self)
        return date
        
    }
    
    func  venueDate() ->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "E\nd"
        let date = dateFormatter.string(from: self)
        return date

        
    }
        
}
extension String {
    
    func dateconvert() -> Date {
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = .current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: self)
        return date!
    }
}
    extension Double {
        
        func  dateconvert() ->Date {
            
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = .current
            dateFormatter.dateFormat = "MMM d h:mm a"
            let date = Date(timeIntervalSince1970: self)
            return date
        }
        
        
    }


