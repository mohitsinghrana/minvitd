//
//  RecommandationVc.swift
//  Minvtd
//
//  Created by admin on 22/09/17.
//  Copyright © 2017 Euro Infotek Pvt Ltd. All rights reserved.
//

import UIKit
import Alamofire

class FeaturedAppsController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    fileprivate let cellID = "cellID"
    fileprivate let largeCellID = "largeCellID"
    fileprivate let headerID = "headerID"
    fileprivate let TextCell = "TextCell"
    
    var featuredApps: AppCategory?
    var featuredApps2: AppCategory?

    var appCategories: [AppCategory]? = []
    var selectediteam:AppCategory?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Recommendations for You"
        collectionView?.backgroundColor = UIColor.white
        collectionView?.register(CategoryCell.self, forCellWithReuseIdentifier: cellID)
        collectionView?.register(LargeCategoryCell.self, forCellWithReuseIdentifier: largeCellID)
        collectionView?.register(TextCollectionViewCell.self, forCellWithReuseIdentifier: TextCell)
        collectionView?.register(Header.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerID)
        
        navigationBarSetup(nil)
        let button1 = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(back))
        
        self.navigationItem.leftBarButtonItem = button1        
    }
    
    @objc  func back(_ sender: Any) {
        pushpopAnimation()
        dismiss(animated: false, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
         loadDataFromServer()
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CategoryCell

        if self.featuredApps2?.productsCount != 0  && self.featuredApps2?.productsCount != nil{
            
            if indexPath.row == 0 {
                
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: largeCellID, for: indexPath) as! LargeCategoryCell
                
                cell.appCategory = featuredApps2
                cell.featuredAppsController = self
                return cell
            }else if indexPath.row == 1 {
               let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: TextCell, for: indexPath) as! TextCollectionViewCell
                
                return cell
            }else {
                cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! CategoryCell
                
                cell.appCategory = appCategories?[indexPath.item - 2]
                cell.featuredAppsController = self
                
                return cell
            }
        }else {
            
            if indexPath.row != 0 {
            
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! CategoryCell
            cell.appCategory = appCategories?[indexPath.item - 1]
            cell.featuredAppsController = self
            
            return cell
            }else{
              let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: TextCell, for: indexPath) as! TextCollectionViewCell
                return cell
            }
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if self.featuredApps2?.productsCount != 0  && self.featuredApps2?.productsCount != nil{
           return  (appCategories?.count)! + 2

        }else{
            return (appCategories?.count)! + 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if self.featuredApps2?.productsCount != 0 && self.featuredApps2?.productsCount != nil{

            if indexPath.row == 0 {
                return CGSize(width: view.frame.width, height: 145)
            }else if indexPath.row == 1 {
                return CGSize(width: view.frame.width, height: 30)
            }else {
                return CGSize(width: view.frame.width, height: 150)
            }
        }else {
           if  indexPath.row == 0 {
                return CGSize(width: view.frame.width, height: 30)
            }else {
                return CGSize(width: view.frame.width, height: 150)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if featuredApps?.productsCount != 0{
            return CGSize(width: view.frame.width, height: 200)
        }else {
            return CGSize.zero
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerID, for: indexPath) as! Header
        
        header.appCategory = featuredApps
        header.featuredAppsController = self
        
        return header
    }    
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if self.featuredApps2?.productsCount != 0 && self.featuredApps2?.productsCount != nil {
            
            if indexPath.row == 0 {
                let iteam = featuredApps2
                selectediteam = iteam

            }else if indexPath.row == 1  {
                
            }
            else {
                let iteam = appCategories![indexPath.row-2]
                selectediteam = iteam
            }
        }else {
            if indexPath.row != 0 {
                let iteam = appCategories![indexPath.row - 1]
                selectediteam = iteam
            }
        }
    }
    
    func showVendoreProfileVC(app:MInvitdVendorsProducts){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Storyboard", bundle: nil)
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "vendorRecommandProfile") as! vendorRecommandProfile
        mainVC.vendorID = app.vendorID!
        mainVC.titleName = app.title!
        mainVC.categoryID = app.categoryID
        mainVC.vendorID = app.vendorID
        pushPresentAnimation()
        self.present(UINavigationController(rootViewController:mainVC), animated: false, completion: nil)
    }
   
    func showListing(category:AppCategory){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "listingVC") as! VendorsListingViewController
   //     mainVC.ownerID = category.objectID!
        mainVC.categoryID = category.categoryID!
        //print(category.categoryID!)
        mainVC.titleName = category.name!
        self.navigationController?.pushViewController(mainVC, animated: true)
    }
}

extension FeaturedAppsController {
    
    func loadDataFromServer(){
       
        //let url = MInvitdClient.Constants.BaseUrl + "vendor/recommendations"
        //let userID = UserDefaults.standard.string(forKey: "userID")
        //let invitationID = UserDefaults.standard.string(forKey: "invitationID")

        let params = [
            "invitationID" : UserDefaults.standard.string(forKey: "invitationID"),
            "iRole" : UserDefaults.standard.string(forKey: "userRole"),
            //"userID" : UserDefaults.standard.string(forKey: "userID"),
            "type" : UserDefaults.standard.string(forKey: "category"),
            "category" : UserDefaults.standard.string(forKey: "type")
        ]
        //let params = ["userID":userID!,"invitationID":invitationID!]
        let authHeader = ["Authorization" : UserDefaults.standard.string(forKey: "userToken")!, "Content-Type" : "application/json" ]
        
        //print("======params------\(params)")
        
        Alamofire.request( MInvitdClient.Constants.BaseUrl + "vendor/recommendations", method: .get,parameters: params, encoding: JSONEncoding.default, headers: authHeader).responseJSON { (response) in
            //debugPrint(response)
            if let json = response.result.value as? [String:Any] {
                if   let status = json["status"] as? String {
                    
                    if status == "success" {
                        
                        if let data1 = json["data"] as? [[String:AnyObject]]{
                            
                            self.appCategories = AppCategory.eventsFromResults(data1)
 
                            // look here
                            if let featured_1 = json["featured_1"] as? [[String:AnyObject]]{
                                
                                let products = MInvitdVendorsProducts.productsFromResults(featured_1)
                                self.featuredApps = AppCategory.init(products: products)

                            }
                            // featured_2
                            if let featured_2 = json["featured_2"] as? [[String:AnyObject]]{
                                
                                let products2 = MInvitdVendorsProducts.productsFromResults(featured_2)
                                self.featuredApps2 = AppCategory.init(products: products2)
                                
                            }
                            self.collectionView?.reloadData()
                        }
                    }else {
                        return
                    }
                }
            }
        }
    }
}
