//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//


import UIKit

class GuestsCollectionViewCell : UITableViewCell{

    
    @IBOutlet weak var imageLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var inviteView: UIView!
    @IBOutlet weak var userImageView: UIView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var statusName: UILabel!
}
