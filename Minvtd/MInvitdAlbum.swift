//
//  MInvitdAlbum.swift
//  Minvtd
//
//  Created by Tushar Aggarwal on 05/06/17.
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import Foundation
struct MInvitdAlbum{
    
    // MARK: Properties
    let comment : String?
    let fileUrl : String?
    let fileID : Int?
    
    // MARK: Initializers
    
    // construct a ZXQKArticle from a dictionary
    init(dictionary: [String:AnyObject]) {
        
        comment = dictionary[MInvitdClient.JSONResponseKeys.comment] as? String
        fileUrl = dictionary[MInvitdClient.JSONResponseKeys.fileUrl] as? String
        fileID = dictionary[MInvitdClient.JSONResponseKeys.fileID] as? Int
        
    }
    
    static func galleryFromResults(_ results: [[String:AnyObject]]) -> [MInvitdAlbum] {
        var photos = [MInvitdAlbum]()
        
        // iterate through array of dictionaries, each Article is a dictionary
        for photo in results {
            photos.append(MInvitdAlbum(dictionary: photo))
        }
        
        return photos
    }
}
