//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//


import Foundation

// MARK: - MInvitdClient: NSObject

class MInvitdClient : NSObject {
    
    // MARK: Properties
    
    // shared session
    @objc var session = URLSession.shared
    
    // configuration object
    
    // authentication state
    
    // MARK: Initializers
    
    override init() {
        super.init()
    }

    // MARK: Shared Instance
    
    @objc class func sharedInstance() -> MInvitdClient {
        struct Singleton {
            static var sharedInstance = MInvitdClient()
        }
        return Singleton.sharedInstance
    }
}
