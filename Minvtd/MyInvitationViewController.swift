//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import Alamofire
import AlamofireImage

class MyInvitationViewController: UIViewController {
    
    
    var invitations: [MInvitdInvitation] = [MInvitdInvitation]()
    var temp: [MInvitdInvitation] = [MInvitdInvitation]()
    var superInv: [MInvitdInvitation] = [MInvitdInvitation]()
    var filterData: [MInvitdInvitation] = [MInvitdInvitation]()

    @objc let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
    @objc var blurEffectView:UIVisualEffectView!
   // @IBOutlet weak var searchBar: UISearchBar!
    @objc let placeholderImage = UIImage(named: "eventCoverImage")!
    @objc var searchString : String = ""
    @IBOutlet weak var cancelButton: UIButton!
    @objc var invitationTag : String = ""
    @IBOutlet weak var searchView: UIView!
    var  isSearching = false
    var headerID = "djgndgnkds"
    
    @IBAction func menu(_ sender: UIBarButtonItem) {
        slideMenuController()?.openLeft()
    }

    
    @IBOutlet weak var invitationsCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       //searchBar.delegate = self
        self.getUserData()
        invitationsCollectionView.delegate = self
        invitationsCollectionView.dataSource = self
        setupNavBarContent()
        navigationBarSetup(nil)
        invitationsCollectionView.register(SearchBarHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerID)
    }
    func setupNavBarContent(){
        
        let button1 = UIBarButtonItem(image: #imageLiteral(resourceName: "menu"), style: .plain, target: self, action: #selector(menu(_:)))
        self.navigationItem.leftBarButtonItem = button1
        self.title = "My Invitations"
  
    }

    
    @objc func dissmisskeyboard()
    {
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        MyinvitationCall()
        
        let swipeDownGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeDown(_:)))
        swipeDownGesture.direction = .down
        self.view.addGestureRecognizer(swipeDownGesture)
        self.invitationsCollectionView.addGestureRecognizer(swipeDownGesture)
        
    }
    @objc func swipeDown(_ sender: UISwipeGestureRecognizer){
        
        switch sender.direction {
        case .down:
            print("swipedown")
            self.invitationsCollectionView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        default:
            print("nothing")
        }
        
        
    }
    
    
    @objc func MyinvitationCall() {
        
        
        apiCallingMethod(apiEndPints: "invitations", method: .get, parameters: [:]) { (data1:Any?, totalObject:Int?) in
            
            if totalObject == 0 {
                
                self.CreateAlertPopUp("MInvtd", "You have no events", OnTap: nil)
                
                return
            }
            
            if let data = data1 as? [[String : Any]]
            {
                let invitations = MInvitdInvitation.invitationsFromResults(data as [[String : AnyObject]])
                self.invitations = invitations
                self.superInv = self.invitations
                DispatchQueue.main.async {
                    self.invitationsCollectionView.reloadData()
                }
            }

            
        }

    }

    
}

// MARK: - InvitationViewController: UICollectionViewDelegate, UICollectionViewDataSource

extension MyInvitationViewController : UICollectionViewDelegateFlowLayout,UICollectionViewDelegate, UICollectionViewDataSource,UISearchDisplayDelegate {
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let invitation = invitations[(indexPath as NSIndexPath).row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "myInvitationCell", for: indexPath) as! MyInvitationCollectionViewCell
        
        cell.title.text =  invitation.name
        cell.date.text = invitation.date
        cell.image.af_setImage(
            withURL: URL(string: invitation.imageUrl!)! ,
            placeholderImage: placeholderImage
        )
        
        cell.bellView.layer.cornerRadius = 12.5
        cell.bellView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        
        cell.notiView.layer.cornerRadius = cell.notiView.frame.size.width / 2
        cell.notiView.clipsToBounds = true
        
        if invitation.notificationsCount! > 0 {
            
            //print("If *** Block ran for row : \(indexPath.row) and count is : \(invitation.notificationsCount!) for title : \(invitation.name!)")
            
            cell.notiView.isHidden = false
            
            let noti = String(describing: invitation.notificationsCount!)
            //print("This is If ***  count value: \(noti)")
            if invitation.notificationsCount! < 10{
                cell.notificationLabel.text = noti
                cell.notificationLabel.anchorCenterSuperview()
            }else{
                cell.notificationLabel.text = noti
                cell.notificationLabel.anchorCenterSuperview()
            }
            
        }else {
            
            cell.notiView.isHidden = true
            cell.notificationLabel.text = ""
            
            //print("Else *** Block ran for row : \(indexPath.row) and count is : \(invitation.notificationsCount!) for title : \(invitation.name!)")
        }
        
        
        cell.button.tag = indexPath.row
        
        //Remove Target
        cell.button.removeTarget(nil, action: nil, for: .allEvents)
        cell.notificationButton.removeTarget(nil, action: nil, for: .allEvents)

        
        // Add target
        cell.button.addTarget(self, action: #selector(didSelectInvitation(sender:)), for: .touchUpInside)
        cell.notificationButton.addTarget(self, action: #selector(didClickNotification(sender:)), for: .touchUpInside)


        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return invitations.count
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let search = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerID, for: indexPath) as! SearchBarHeader
        search.mysearchBar.delegate = self as! UISearchBarDelegate
        return search
    }
    
    
    
func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
    let bounds = UIScreen.main.bounds
    let height = (bounds.height - 100)/3
    let width = bounds.width
    
    return CGSize(width: width, height: height)
    
    }

    
    @objc func getUserData(){
        
//        let userID = UserDefaults.standard.string(forKey: "userID")
//        let url  = "\(MInvitdClient.Constants.BaseUrl)users?userID=" + userID!
//        let authHeader = ["Authorization" : UserDefaults.standard.string(forKey: "userToken")!, "Content-Type" : "application/json" ]
    
         let userID = UserDefaults.standard.string(forKey: "userID")
         let url = MInvitdClient.Constants.BaseUrl + "users"
         let parameters: Parameters = ["userID" : userID!]
         let authHeader = ["Authorization" : UserDefaults.standard.string(forKey: "userToken")!, "Content-Type" : "application/json" ]
        
         Alamofire.request(url, method: .get, parameters: parameters, encoding: JSONEncoding.default, headers: authHeader).responseJSON { (response) in
         
            if response.error == nil {
                
                self.actionUserData(JSONData : response.data!)
            }else{
                
            }
        }
        
    }
    
    
    @objc func actionUserData(JSONData : Data)
    {
        do{
            let json = try JSONSerialization.jsonObject(with: JSONData, options: [])
            
            if let dictionary = json as? [String : Any] {
                
                if let status = dictionary["status"] as? String
                {
                    if status == "success"
                    {
                        if let data = dictionary["data"] as? [[String : Any]]
                        {
                            let name  = data[0]["name"] as? String
                            UserDefaults.standard.set(name, forKey: "name")
                            let email = data[0]["email"] as? String
                            UserDefaults.standard.set(email, forKey: "email")
                            let imageUrl = data[0]["imageUrl"] as? String
                            //UserDefaults.standard.set(imageUrl, forKey: "imageUrl")
                            let coverUrl = data[0]["coverUrl"] as? String
                            UserDefaults.standard.set(coverUrl, forKey: "coverUrl")
                        }
                    }
                }
            }
            
        }
        catch{
            
        }
    }
    

    @objc func didSelectInvitation(sender : UIButton){
        
        let indexPath =  sender.tag
        let invitation = invitations[indexPath]
//        let userID =  UserDefaults.standard.integer(forKey: "userID")
//        let ownerID : Int =  invitation.ownerID!
//        
//        if ownerID == userID{
//            UserDefaults.standard.set("admin", forKey: "userRole")
//        }
//
//        UserDefaults.standard.set(invitation.objectID, forKey: "objectID")
//        UserDefaults.standard.set(invitation.objectID, forKey: "invitationID")
        UserDefaults.standard.set(invitation.imageUrl, forKey: "invitationImageUrl")
        //serDefaults.standard.set(invitation.ownerID, forKey: "ownerID")
        UserDefaults.standard.set(invitation.name, forKey: "invitationName")
        UserDefaults.standard.set(invitation.name, forKey: "invitationTitle")
        UserDefaults.standard.set(invitation.colour, forKey: "colour1")
        UserDefaults.standard.set(invitation.colour_2, forKey: "colour2")
        UserDefaults.standard.set(invitation.albumBgUrl, forKey: "albumBgUrl")
        UserDefaults.standard.set(invitation.receiveNotifications, forKey: "receiveNotifications")
        UserDefaults.standard.set(invitation.coverUrl, forKey: "invitationCoverUrl")
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let leftDrawerVC = storyBoard.instantiateViewController(withIdentifier: "SlideMenuViewController") as! SlideMenuViewController
        leftDrawerVC.menuTag = 2
        let mainVC = storyBoard.instantiateViewController(withIdentifier: "tabVC")
        let vc = SlideMenuController(mainViewController: mainVC, leftMenuViewController: leftDrawerVC)
        pushPresentAnimation()
        self.present(vc, animated: false, completion: nil)
    }
    
    
    @objc func didClickNotification(sender : UIButton){
        
        let indexPath =  sender.tag
        let invitation = invitations[indexPath]
//        let userID =  UserDefaults.standard.integer(forKey: "userID")
//        let ownerID : Int  =  invitation.ownerID!
//        if ownerID == userID{
//            UserDefaults.standard.set("admin", forKey: "userRole")
//        }
//        UserDefaults.standard.set(invitation.objectID, forKey: "objectID")
//        UserDefaults.standard.set(invitation.objectID, forKey: "invitationID")
        UserDefaults.standard.set(invitation.imageUrl, forKey: "invitationImageUrl")
        UserDefaults.standard.set(invitation.albumBgUrl, forKey: "albumBgUrl")
     //   UserDefaults.standard.set(invitation.ownerID, forKey: "ownerID")
        UserDefaults.standard.set(invitation.name, forKey: "invitationName")
        UserDefaults.standard.set(invitation.colour, forKey: "colour1")
        UserDefaults.standard.set(invitation.colour_2, forKey: "colour2")
        UserDefaults.standard.set(invitation.receiveNotifications, forKey: "receiveNotifications")
        UserDefaults.standard.set(invitation.coverUrl, forKey: "invitationCoverUrl")

        let mainVC = storyboard?.instantiateViewController(withIdentifier: "notificationsVCOuter") as! NotificationOuterViewController
        
        self.present(mainVC, animated : true ,  completion : nil )

    }
}


extension MyInvitationViewController:UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
        // tableView.reloadData()
        isSearching = true
        filterData = invitations.filter({
            return ($0.name?.contains(searchBar.text!))!})
        print("\(filterData)")
        invitationsCollectionView.reloadData()
        self.invitationsCollectionView.setContentOffset(CGPoint(x: 0, y: 44), animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
        isSearching = false
        
        // remember reloading
        //tableView.layoutIfNeeded()
        invitationsCollectionView.reloadData()
        self.invitationsCollectionView.setContentOffset(CGPoint(x: 0, y: 44), animated: true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        //reloading here
        if searchBar.text == nil || searchBar.text == ""{
            isSearching = false
            searchBar.setShowsCancelButton(false, animated: true)
            view.endEditing(true)
            invitationsCollectionView.reloadData()
            self.invitationsCollectionView.setContentOffset(CGPoint(x: 0, y: 44), animated: true)
            
            return
        }else {
            
        }
    }
    
    @objc func CreateBlur () {
        blurEffectView = UIVisualEffectView(effect: blurEffect)
        
        blurEffectView.alpha = 0.9
        
        //always fill the view
        blurEffectView.frame = self.view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(blurEffectView)
    }
    
    
    // childViewController
    fileprivate func add(asChildViewController viewController: UIViewController) {
        
        CreateBlur()
        self.addChildViewController(viewController)
        
        viewController.view.frame = CGRect(x: 25, y: 100, width: view.bounds.width - 50, height: view.bounds.height - 150)
        
        view.addSubview(viewController.view)
        viewController.didMove(toParentViewController: self)
        navigationController?.view.setNeedsLayout()
        
        // Configure Child View
        //        viewController.view.autoresizingMask = false
    }
}





