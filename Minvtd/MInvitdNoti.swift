//
//  MInvitdNoti.swift
//  Minvtd
//
//  Created by Tushar Aggarwal on 12/06/17.
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import Foundation
struct MInvitdNoti{
    
    // MARK: Properties

    let created : Double?
    let fileUrl : String?
    let message : String?
    let module : String?
    let nid : Int?
    let rid : AnyObject?
    let pid : Int?
    let postID : Int?
    let invitationID : Int?
    let creatorName : String?
    let createdTimeAgo : String?
    // MARK: Initializers
    
    // construct a ZXQKArticle from a dictionary
    init(dictionary: [String:AnyObject]) {
        created = dictionary["created"] as? Double
        createdTimeAgo = dictionary["createdTimeAgo"] as? String
        fileUrl = dictionary["fileUrl"] as? String
        message = dictionary["message"] as? String
        module = dictionary["module"] as? String
        postID = dictionary["postID"] as? Int
        invitationID = dictionary["invitationID"] as? Int
        nid = dictionary["nid"] as? Int
        rid = dictionary["rid"]
        pid = dictionary["pid"] as? Int
        creatorName = dictionary["creatorName"] as? String
    }
    
    static func notiFromResults(_ results: [[String:AnyObject]]) -> [MInvitdNoti] {
        var notis = [MInvitdNoti]()
        
        // iterate through array of dictionaries, each Article is a dictionary
        for noti in results {
            notis.append(MInvitdNoti(dictionary: noti))
        }
        
        return notis
    }
}
