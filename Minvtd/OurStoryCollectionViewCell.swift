//
//  Copyright © 2017 Euroinfotek. All rights reserved.
//

import UIKit
import AlamofireImage

class OurStoryCollectionViewCell : UITableViewCell {
    
   
    @IBOutlet weak var roundImageWidth: NSLayoutConstraint!
    @IBOutlet weak var roundImageHeight: NSLayoutConstraint!
    @IBOutlet weak var borderImage: UIImageView!
    @IBOutlet weak var storyImage: UIImageView!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var desc: UITextView!
    @IBOutlet weak var roundImage: UIImageView!
    @IBOutlet weak var likedLabel: UILabel!
    @IBOutlet weak var likeImage: UIImageView!
    let colour1 = UserDefaults.standard.string(forKey: "colour1")

    var controler:OurStoryViewController?
    var data:MInvtdAllFeed?{
        
        didSet{
            
            title.text = data?.title
            date.text = data?.date
            likedLabel.text = "Likes : " + (data?.likes?.description)!
            desc.text = data?.description
            storyImage.af_setImage(withURL: URL(string: (data?.fileThumbnail!)!)!)
            if data?.liked == true{
                
                DispatchQueue.main.async {
                    //self.roundImage.backgroundColor = UIColor.init(hexString: self.colour1!)

                    self.likeImage.image = #imageLiteral(resourceName: "likeFilled")
                    
                }
            }else{
                DispatchQueue.main.async {
                    //self.roundImage.backgroundColor = UIColor.init(hexString: "#FFFFFF")
                    self.roundImage.image = nil
                    self.likeImage.image = #imageLiteral(resourceName: "like")
                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //roundImage.layer.cornerRadius = 20
        roundImage.clipsToBounds = true
      //  roundImage.layer.borderWidth = 3
       // roundImage.layer.borderColor = UIColor(hexString: "#B4B4B4")?.cgColor
        likedLabel.textColor = UIColor.init(hexString: colour1!)
        roundImage.tintColor = UIColor.init(hexString: colour1!)
        title.textColor = UIColor.init(hexString: colour1!)
        //roundImage.layer.borderWidth = 5.5
        
//        let circlePath = UIBezierPath(arcCenter: CGPoint(x: roundImage.center.x - 8.0,y: roundImage.center.y - 4.0), radius: CGFloat(12), startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
//
//        let shapeLayer = CAShapeLayer()
//        shapeLayer.path = circlePath.cgPath
//
//        //change the fill color
//        shapeLayer.fillColor = UIColor.clear.cgColor
//        //you can change the stroke color
//        shapeLayer.strokeColor = UIColor.init(hexString: "#AAAAAA")!.cgColor
//        //you can change the line width
//        shapeLayer.lineWidth = 4.0
//
//        contentView.layer.addSublayer(shapeLayer)
    }
    
}
