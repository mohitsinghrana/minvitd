//
//  HeaderView.swift
//  MinvitdFeeds
//
//  Created by admin on 03/11/17.
//  Copyright © 2017 Sachin. All rights reserved.
//

import UIKit

class FeedHeaderView: UIView {
 
    var VC:AllFeedViewController?
    
    let coverImg = { () -> UIImageView in
        let imgView = UIImageView()
        imgView.contentMode = .scaleAspectFill
        imgView.isUserInteractionEnabled = true
        imgView.clipsToBounds = true
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.backgroundColor = .purple
        return imgView
    }()
    
    let profileImg = { () -> UIImageView in
        let imgView = UIImageView()
        imgView.layer.cornerRadius = 40.0
        imgView.clipsToBounds = true
        imgView.layer.borderWidth = 4.0
        imgView.contentMode = .scaleAspectFill
        imgView.isUserInteractionEnabled = true
        imgView.layer.borderColor = UIColor.white.cgColor
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.backgroundColor = .black
        return imgView
    }()
    
    let textLabel = { () -> UILabel in
        let li = UILabel()
        let invitationName:String = UserDefaults.standard.string(forKey:"feedStatus") ?? ""
        
        li.text = invitationName
        li.textAlignment = .center
        li.font = UIFont(name: "GothamRounded-Medium", size: 14)
        //li.font = UIFont.boldSystemFont(ofSize: 14)
        let colour1 = UserDefaults.standard.string(forKey: "colour1")
        li.textColor = UIColor.init(hexString: colour1!)
        li.translatesAutoresizingMaskIntoConstraints = false
        return li
    }()
    
    let photoButton = { () -> UIButton in
        let li = UIButton(type: .system)
        let colour1 = UserDefaults.standard.string(forKey: "colour1")
        li.tintColor = UIColor.init(hexString: colour1!)
        li.setTitle("Photos", for: .normal)
        li.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        li.translatesAutoresizingMaskIntoConstraints = false
        return li
    }()
    
    let photoBorderView = {() -> UIView in
        let vi = UIView()
        vi.translatesAutoresizingMaskIntoConstraints = false
        vi.backgroundColor = UIColor.clear
        vi.layer.cornerRadius = 18
        vi.layer.masksToBounds = true
        vi.layer.borderWidth = 2
        return vi
    }()
    
    let videoBorderView = {() -> UIView in
        let vi = UIView()
        vi.translatesAutoresizingMaskIntoConstraints = false
        vi.backgroundColor = UIColor.clear
        vi.layer.cornerRadius = 18
        vi.layer.masksToBounds = true
        vi.layer.borderWidth = 2
        return vi
    }()
    
  private  let photoButtonImg = { () -> UIImageView in
        let imgView = UIImageView()
        let colour1 = UserDefaults.standard.string(forKey: "colour1")
        imgView.tintColor = UIColor.init(hexString: colour1!)
        imgView.image = #imageLiteral(resourceName: "cameraSmall").withRenderingMode(.alwaysTemplate)
        imgView.translatesAutoresizingMaskIntoConstraints = false
        return imgView
    }()
    
   private  let videoButtonImg = { () -> UIImageView in
        let imgView = UIImageView()
        let colour1 = UserDefaults.standard.string(forKey: "colour1")
        imgView.tintColor = UIColor.init(hexString: colour1!)
        imgView.image = #imageLiteral(resourceName: "videoSmall").withRenderingMode(.alwaysTemplate)
        imgView.translatesAutoresizingMaskIntoConstraints = false
        return imgView
    }()

    let videoButton = { () -> UIButton in
        let li = UIButton(type: .system)
        let colour1 = UserDefaults.standard.string(forKey: "colour1")
        li.tintColor = UIColor.init(hexString: colour1!)
        li.setTitle("Videos", for: .normal)
        li.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        li.translatesAutoresizingMaskIntoConstraints = false
        return li
    }()
    
    
    

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
        photoButton.addTarget(self, action: #selector(photosTap), for: .touchUpInside)
        videoButton.addTarget(self, action: #selector(videosTap), for: .touchUpInside)

    }
    
        @objc func photosTap() {
    
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainVC = storyboard.instantiateViewController(withIdentifier: "photosFeedVC") as! PhotosViewController
            VC?.present(UINavigationController(rootViewController:mainVC), animated: true, completion: nil)
        }
    
        @objc func videosTap() {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainVC = storyboard.instantiateViewController(withIdentifier: "videosFeedVC") as! VideosViewController
            VC?.present(UINavigationController(rootViewController:mainVC), animated: true, completion: nil)
        }
    
    func setupUI()  {
        
        addSubview(photoBorderView)
        addSubview(videoBorderView)
        addSubview(coverImg)
        addSubview(profileImg)
        addSubview(textLabel)
        addSubview(photoButton)
        addSubview(videoButton)
        addSubview(photoButtonImg)
        addSubview(videoButtonImg)

        // coverimg
        coverImg.anchor(topAnchor, left: leftAnchor, bottom: nil, right:rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: bounds.width*9/16 - 44.0)
        
        // profile
        profileImg.anchor(nil, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 80, heightConstant: 80)
        profileImg.anchorCenterXToSuperview()
        profileImg.centerYAnchor.constraint(equalTo: coverImg.bottomAnchor, constant: 0).isActive = true
        
        // text
        textLabel.anchorCenterXToSuperview()
        textLabel.bottomAnchor.constraint(equalTo: profileImg.bottomAnchor, constant: 45).isActive = true
        textLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
        textLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: 0).isActive = true
        
        let imageBackgroundCoverUrl =  UserDefaults.standard.string(forKey: "invitationImageUrl")
            profileImg.af_setImage(withURL: URL(string: imageBackgroundCoverUrl!)!)
        let coverImgUrl =  UserDefaults.standard.string(forKey: "invitationCoverUrl")
        coverImg.af_setImage(withURL: URL(string: coverImgUrl!)!)
        
        let colour1 = UserDefaults.standard.string(forKey: "colour1")
        photoBorderView.layer.borderColor = UIColor.init(hexString: colour1!)?.cgColor
        videoBorderView.layer.borderColor = UIColor.init(hexString: colour1!)?.cgColor


        // photo  button
        
        photoButton.centerYAnchor.constraint(equalTo: profileImg.centerYAnchor, constant: 35).isActive = true
        photoButton.widthAnchor.constraint(equalToConstant: 80).isActive = true
        photoButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        photoButton.leftAnchor.constraint(equalTo: leftAnchor, constant: 30).isActive = true

        // videoButton
        videoButton.centerYAnchor.constraint(equalTo: photoButton.centerYAnchor, constant: 0).isActive = true
        videoButton.widthAnchor.constraint(equalToConstant: 80).isActive = true
        videoButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        videoButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -12).isActive = true

        // videophoto & imagePhoto
        
        photoButtonImg.heightAnchor.constraint(equalToConstant: 18).isActive = true
        photoButtonImg.widthAnchor.constraint(equalToConstant: 18).isActive = true
        photoButtonImg.centerYAnchor.constraint(equalTo: videoButton.centerYAnchor, constant: 0).isActive = true
        photoButtonImg.rightAnchor.constraint(equalTo: photoButton.leftAnchor, constant: 10).isActive = true

        videoButtonImg.heightAnchor.constraint(equalToConstant: 18).isActive = true
        videoButtonImg.widthAnchor.constraint(equalToConstant: 18).isActive = true
        videoButtonImg.centerYAnchor.constraint(equalTo: videoButton.centerYAnchor, constant: 0).isActive = true
        videoButtonImg.rightAnchor.constraint(equalTo: videoButton.leftAnchor, constant: 10).isActive = true
        
        // border view
        photoBorderView.heightAnchor.constraint(equalToConstant: 36).isActive = true
        photoBorderView.centerYAnchor.constraint(equalTo: videoButton.centerYAnchor, constant: 0).isActive = true
        photoBorderView.rightAnchor.constraint(equalTo: photoButton.rightAnchor, constant: 0).isActive = true
        photoBorderView.leftAnchor.constraint(equalTo: photoButtonImg.leftAnchor, constant: -12).isActive = true

        videoBorderView.heightAnchor.constraint(equalToConstant: 36).isActive = true
        videoBorderView.centerYAnchor.constraint(equalTo: videoButton.centerYAnchor, constant: 0).isActive = true
        videoBorderView.rightAnchor.constraint(equalTo: videoButton.rightAnchor, constant: 0).isActive = true
        videoBorderView.leftAnchor.constraint(equalTo: videoButtonImg.leftAnchor, constant: -12).isActive = true

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}



